<?php 
$args = array(
	'post_type' => 'current_magazine',
	'order_by' => 'menu_order',
	'posts_per_page' => -1 //this grabs all the posts with no limit
);
$currentMags = new WP_Query($args);
?>

<div class="currentMagsArea">


<?php
if( $currentMags->have_posts() ) : while ( $currentMags->have_posts() ) : $currentMags->the_post();
	$description = get_field('description');
	$date = get_field('date');
	$coverImageArray = get_field('cover_image');
	$coverImage = $coverImageArray['url'];
	$thumbnail1Array = get_field('thumbnail_1');
	$thumbnail1 = $thumbnail1Array['url'];
	$thumbnail2Array = get_field('thumbnail_2');
	$thumbnail2 = $thumbnail2Array['url'];
	$thumbnail3Array = get_field('thumbnail_3');
	$thumbnail3 = $thumbnail3Array['url'];
	$logoImageArray = get_field('logo_image');
	$logoImage = $logoImageArray['url'];
	$link = get_field('link');

?>

		<div class="currentMag">
			<a href="<?= $link ?>" class="imagesArea">
				<img src="<?= $coverImage ?>"  class="currentMagImageCover">
				<img src="<?= $thumbnail1 ?>" class="currentMagImageThumbs">
				<img src="<?= $thumbnail2 ?>" class="currentMagImageThumbs">
				<img src="<?= $thumbnail3 ?>" class="currentMagImageThumbs">
			</a>
			<div class="currentMagLogoArea">
				<a href=<?= $link ?> ><img src="<?= $logoImage ?>"></a>
				<p><?= $date ?> </p>
			</div>
			<p class="currentMagDescription"><?= $description ?> </p>
		</div>


<?php
endwhile; endif;
?>





</div><!-- end currentMags Area -->

<style type="text/css">
.currentMagsArea{
	width: 100%;
	max-width: 1200px;
	height: auto;
	margin: 0 auto;
	/*overflow: hidden;*/
	display: flex;
	flex-flow: row wrap;
	justify-content: space-between;
}

.currentMag{
	background: #fff;
	border-radius: 6px;
	padding: 5px;
	width: 30%;
	height: auto;
	margin-top: 20px;
	margin-bottom: 20px;
	-webkit-box-shadow: 0px 4px 6px -1px rgba(0,0,0,0.4); 
	box-shadow: 0px 4px 6px -1px rgba(0,0,0,0.4);
	overflow: hidden;
}

.imagesArea{
	width: 94%;
	margin: 2% auto 1% auto;
	display: block;
}

.currentMagImageCover{
	width: 57.5%;
	float: left;
}
.currentMagImageThumbs{
	width: 40.2%;
	float: right;
	margin-left:1%;
	margin-bottom: 1%;
}


.currentMagLogoArea{
	width: 94%;
	height: auto;
	margin: 2% auto;
	position: relative;
	overflow: hidden;
	display: block;
}

.currentMagLogoArea a{
	width: 40%;
	height: auto;
	float: left;
}
.currentMagLogoArea a img{
	vertical-align: bottom;
	width: 100%;
}		

.currentMagLogoArea p{
	width: 40%;
	height: auto;
	color: #666;
	font-size: 14px;
	text-align: right;
	padding:0;
	margin:0;
	position:absolute;
    bottom:0;
    right: 0;
}	

.currentMagDescription{
	width: 94%;
	height: auto;
	margin:2% auto;
	padding-top:2%;
	border-top: 1px #ccc solid;
	color: #676767;
	display: block;
	line-height: 22px;
}





</style>