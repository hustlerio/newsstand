<?php
/**
* Template Name: CCBill Success
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
// do not get the full header.php file, only need core wp files
get_header();

?>
<style>
	body {
		display: none;
	}
</style>
<?php if ($_REQUEST['wc_orderid']) {
//$test_order = wc_get_product($_REQUEST['wc_orderid']);
//$test_order_key = $test_order->get_order_key();
	$order = wc_get_order($_REQUEST['wc_orderid']);

// Added a check to make sure it's a real order

if ($order && !is_wp_error($order)) {
    $order_key = $order->get_order_key();
}
	
$successurl = wc_get_endpoint_url( 'order-received', '', wc_get_checkout_url() );
	$successurl .= '/'.$_REQUEST['wc_orderid'].'/?key='.$order_key;

?>

<?php 
/**
<pre><?php print_r($_REQUEST); ?></pre>
		**/

} ?>
		<script>
	
		window.parent.location.href = "<?php echo $successurl; ?>"; 
</script>
		
		
		

<?php 
// no need for footer, navigating away
//wp_footer();  ?>
