<?php
/**
 * Astra functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Define Constants
 */
define( 'ASTRA_THEME_VERSION', '3.3.2' );
define( 'ASTRA_THEME_SETTINGS', 'astra-settings' );
define( 'ASTRA_THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'ASTRA_THEME_URI', trailingslashit( esc_url( get_template_directory_uri() ) ) );


/**
 * Minimum Version requirement of the Astra Pro addon.
 * This constant will be used to display the notice asking user to update the Astra addon to the version defined below.
 */
define( 'ASTRA_EXT_MIN_VER', '3.3.1' );

/**
 * Setup helper functions of Astra.
 */
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-theme-options.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-theme-strings.php';
require_once ASTRA_THEME_DIR . 'inc/core/common-functions.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-icons.php';

/**
 * Update theme
 */
require_once ASTRA_THEME_DIR . 'inc/theme-update/class-astra-theme-update.php';
require_once ASTRA_THEME_DIR . 'inc/theme-update/astra-update-functions.php';
require_once ASTRA_THEME_DIR . 'inc/theme-update/class-astra-theme-background-updater.php';
require_once ASTRA_THEME_DIR . 'inc/theme-update/class-astra-pb-compatibility.php';


/**
 * Fonts Files
 */
require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-font-families.php';
if ( is_admin() ) {
	require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-fonts-data.php';
}

require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-fonts.php';

require_once ASTRA_THEME_DIR . 'inc/dynamic-css/container-layouts.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-walker-page.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-enqueue-scripts.php';
require_once ASTRA_THEME_DIR . 'inc/core/class-gutenberg-editor-css.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-dynamic-css.php';

/**
 * Custom template tags for this theme.
 */
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-attr.php';
require_once ASTRA_THEME_DIR . 'inc/template-tags.php';

require_once ASTRA_THEME_DIR . 'inc/widgets.php';
require_once ASTRA_THEME_DIR . 'inc/core/theme-hooks.php';
require_once ASTRA_THEME_DIR . 'inc/admin-functions.php';
require_once ASTRA_THEME_DIR . 'inc/core/sidebar-manager.php';

/**
 * Markup Functions
 */
require_once ASTRA_THEME_DIR . 'inc/markup-extras.php';
require_once ASTRA_THEME_DIR . 'inc/extras.php';
require_once ASTRA_THEME_DIR . 'inc/blog/blog-config.php';
require_once ASTRA_THEME_DIR . 'inc/blog/blog.php';
require_once ASTRA_THEME_DIR . 'inc/blog/single-blog.php';
/**
 * Markup Files
 */
require_once ASTRA_THEME_DIR . 'inc/template-parts.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-loop.php';
require_once ASTRA_THEME_DIR . 'inc/class-astra-mobile-header.php';

/**
 * Functions and definitions.
 */
require_once ASTRA_THEME_DIR . 'inc/class-astra-after-setup-theme.php';

// Required files.
require_once ASTRA_THEME_DIR . 'inc/core/class-astra-admin-helper.php';

require_once ASTRA_THEME_DIR . 'inc/schema/class-astra-schema.php';

if ( is_admin() ) {

	/**
	 * Admin Menu Settings
	 */
	require_once ASTRA_THEME_DIR . 'inc/core/class-astra-admin-settings.php';
	require_once ASTRA_THEME_DIR . 'inc/lib/notices/class-astra-notices.php';

	/**
	 * Metabox additions.
	 */
	require_once ASTRA_THEME_DIR . 'inc/metabox/class-astra-meta-boxes.php';
}

require_once ASTRA_THEME_DIR . 'inc/metabox/class-astra-meta-box-operations.php';


/**
 * Customizer additions.
 */
require_once ASTRA_THEME_DIR . 'inc/customizer/class-astra-customizer.php';


/**
 * Compatibility
 */
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-jetpack.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/woocommerce/class-astra-woocommerce.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/edd/class-astra-edd.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/lifterlms/class-astra-lifterlms.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/learndash/class-astra-learndash.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-beaver-builder.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-bb-ultimate-addon.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-contact-form-7.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-visual-composer.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-site-origin.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-gravity-forms.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-bne-flyout.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-ubermeu.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-divi-builder.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-amp.php';
require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-yoast-seo.php';
require_once ASTRA_THEME_DIR . 'inc/addons/transparent-header/class-astra-ext-transparent-header.php';
require_once ASTRA_THEME_DIR . 'inc/addons/breadcrumbs/class-astra-breadcrumbs.php';
require_once ASTRA_THEME_DIR . 'inc/addons/heading-colors/class-astra-heading-colors.php';
require_once ASTRA_THEME_DIR . 'inc/builder/class-astra-builder-loader.php';

// Elementor Compatibility requires PHP 5.4 for namespaces.
if ( version_compare( PHP_VERSION, '5.4', '>=' ) ) {
	require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-elementor.php';
	require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-elementor-pro.php';
	require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-web-stories.php';
}

// Beaver Themer compatibility requires PHP 5.3 for anonymus functions.
if ( version_compare( PHP_VERSION, '5.3', '>=' ) ) {
	require_once ASTRA_THEME_DIR . 'inc/compatibility/class-astra-beaver-themer.php';
}

require_once ASTRA_THEME_DIR . 'inc/core/markup/class-astra-markup.php';

/**
 * Load deprecated functions
 */
require_once ASTRA_THEME_DIR . 'inc/core/deprecated/deprecated-filters.php';
require_once ASTRA_THEME_DIR . 'inc/core/deprecated/deprecated-hooks.php';
require_once ASTRA_THEME_DIR . 'inc/core/deprecated/deprecated-functions.php';

//////allows shortcode for woocommerce product search
function storefront_product_search() {
    if ( function_exists('storefront_is_woocommerce_activated' ) ) {
        if ( storefront_is_woocommerce_activated() ) { ?>
            <div class="site-search">
            <?php
            if ( function_exists( 'woocommerce_product_search' ) ) {
                echo woocommerce_product_search();
            } else {
                the_widget( 'WC_Widget_Product_Search', 'title=' );
            }
            ?>
            </div>
            <?php
        }
    }
}



// Shortcode to output custom PHP in Elementor
function displayCurrentMagazineArea( $atts ) {
    ob_start();
	include get_template_directory() . '/test.php';
	
	//this and the ob_start() above fix some bug in the Elementor editor
	//where the code from this function shows at the top of the page
	return ob_get_clean();
}
add_shortcode( 'currentMagazines-elementor_php_output', 'displayCurrentMagazineArea');


function displayHeaderBackIssueArea( $atts ) {
    include "wp-content/themes/astra/backissue-header-area.php";
}
add_shortcode( 'HeaderBackIssueArea', 'displayHeaderBackIssueArea');

function displayDynamicStoreHeader( $atts ) {
    include "wp-content/themes/astra/assets/custom-php-sections/storeHeader.php";
}
add_shortcode( 'storeDynamicHeader', 'displayDynamicStoreHeader');

function displayRelatedTitles( $atts ) {
    include "wp-content/themes/astra/assets/custom-php-sections/related-titles-hack.php";
}
add_shortcode( 'displayRelatedTitlesHack', 'displayRelatedTitles');

function displayLandingPageHeaders( $atts ) {
    include "wp-content/themes/astra/assets/custom-php-sections/landing-page-headers.php";
}
add_shortcode( 'displayHeaders', 'displayLandingPageHeaders');

function displayProductPageBreadcrumbs( $atts ) {
    include "wp-content/themes/astra/assets/custom-php-sections/product-page-breadcrumbs.php";
}
add_shortcode( 'displayBreadcrumbs', 'displayProductPageBreadcrumbs');

function displayHomepageSlider( $atts ) {
    include "wp-content/themes/astra/assets/custom-php-sections/homepageSlider.php";
}
add_shortcode( 'displayHomeSlider', 'displayHomepageSlider');








add_filter('jpeg_quality', function($arg){return 100;});
add_filter( 'wp_editor_set_quality', function($arg){return 100;} );

add_theme_support('woocommerce');

////////////this lets you call the menus with shortcode
function menu_function($atts, $content = null) {
	extract(
		shortcode_atts(
			array( 'name' => null, ),
			$atts
		)
	);
	return wp_nav_menu(
		array(
		'menu' => $name,
		'echo' => false
		)
	);
}
add_shortcode('menu', 'menu_function');


/* ///////////////////////////////////WooCommerce Add To Cart Text/////////////////////////////////////////////////// */

add_filter( 'add_to_cart_text', 'woo_custom_single_add_to_cart_text' );                // < 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' );  // 2.1 + 
function woo_custom_single_add_to_cart_text() {
    return __( 'Add to Cart', 'woocommerce' );
}

add_filter( 'add_to_cart_text', 'woo_custom_product_add_to_cart_text' );            // < 2.1
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_custom_product_add_to_cart_text' );  // 2.1 +
function woo_custom_product_add_to_cart_text() {
    return __( 'Add to Cart', 'woocommerce' );
}





//////increase the # of search results. The default was 100. so stupid
add_filter('aws_page_results', 'aws_page_results');
function aws_page_results( $num ) {
    return 999;
}


//////////////////add the product images on the checkout page//////////////////////////////////////////////////////////////////////
///TURNED OFF THIS FUNCTION BC IT WAS BREAKING THE SHIPPING OPTIONS ON CHECKOUT
////add_filter( 'woocommerce_cart_item_name', 'ts_product_image_on_checkout', 10, 3 );
 
function ts_product_image_on_checkout( $name, $cart_item, $cart_item_key ) {
     
    /* Return if not checkout page */
    if ( ! is_checkout() ) {
        return $name;
    }
     
    /* Get product object */
    $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
 
    /* Get product thumbnail */
    $thumbnail = $_product->get_image();
 
    /* Add wrapper to image and add some css */
    $image = '<div class="ts-product-image" style="width: 52px; height: 45px; float:left; padding-right: 7px; vertical-align: middle;">'
                . $thumbnail .
            '</div>'; 
 
    /* Prepend image to name and return it */
    return $image . $name;
}



///////////////////////////////////////////////////////////////////////////hide shipping rates on cart page when its free
function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();

	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}

	return ! empty( $free ) ? $free : $rates;
}

add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );
/////////////////////////////////////////////////////////////////////////////////////////////////end shipping rates




add_filter( 'woocommerce_checkout_fields', 'misha_no_email_validation' );
 
function misha_no_email_validation( $fields ){
 
	unset( $fields['billing']['billing_email']['validate'] );
	return $fields;
 
}


/////////////////////////////////////this wil prevent ALL those error messages from displaying on checkout and only show one
add_action( 'woocommerce_after_checkout_validation', 'customFormVaildation', 10, 2);
function customFormVaildation( $fields, $errors ){
 
	if( !empty($errors->get_error_codes() )){
		foreach($errors->get_error_codes() as $code){
			$errors->remove($code);	
		}
		$errors->add('validation', 'Please Fill Out All Required Fields' );
	}

	
// 	if(  empty($fields[ 'billing_first_name' ])  ){
// 		//$errors->add( 'validation', 'Fill out your fucking name if you even know it you asshole' );
// 	}
	
	
	
}

////////////////////////////////////////////////////////////////////////////////////////show mag covers on thank you page
add_filter( 'woocommerce_order_item_name', 'ts_product_image_on_thankyou', 10, 3 );
  
function ts_product_image_on_thankyou( $name, $item, $visible ) {
 
    /* Return if not thankyou/order-received page */
    if ( ! is_order_received_page() ) {
        return $name;
    }
     
    /* Get product id */
    $product_id = $item->get_product_id();
      
    /* Get product object */
    $_product = wc_get_product( $product_id );
  
    /* Get product thumbnail */
    $thumbnail = $_product->get_image();
  
    /* Add wrapper to image and add some css */
    $image = '<div class="ts-product-image" style="height: auto; display: inline-block; padding-right: 7px; float:left;">'
                . $thumbnail .
            '</div>'; 
  
    /* Prepend image to name and return it */
    return $image . $name;
}


///////////////////////////////////////////////////////hide the admin bar for all users except admins
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}



/**
 * Prevent update notification for plugin
 */
function disable_plugin_updates( $value ) {
  if ( isset($value) && is_object($value) ) {
    if ( isset( $value->response['woocommerce-payment-gateway-ccbill/wc-gateway-ccbill.php'] ) ) {
      unset( $value->response['woocommerce-payment-gateway-ccbill/wc-gateway-ccbill.php'] );
    }
  }
  return $value;
}
add_filter( 'site_transient_update_plugins', 'disable_plugin_updates' );

///////////////////////////////////////////////////////////allow the progress bar for free shipping to show for non members
add_filter( 'xoo_wsc_shipping_bar_args', function( $args ){

    $freeValue = 75;
    $subtotal = WC()->cart->get_subtotal() + WC()->cart->get_subtotal_tax();
    $isFree = $subtotal >= $freeValue;
    $amountLeft = $isFree ? 0 : $freeValue - $subtotal;
    $text = $amountLeft ? str_replace( '%s', wc_price( $amountLeft ), xoo_wsc_helper()->get_general_option('sct-sb-remaining') ) : xoo_wsc_helper()->get_general_option( 'sct-sb-free' );

    $data = array(
		'free'                                       => $isFree,
		'amount_left'              => $isFree ? 0 : $amountLeft,
		'fill_percentage'         => $isFree ? 100 : 100 - (($amountLeft/$freeValue) * 100),
    );
    $args['data'] = $data;
    $args['text'] = $text;
    return $args;
} );


/** Customize Order Received Page ***/

add_filter('woocommerce_thankyou_order_received_text', 'woo_change_order_received_text', 10, 2 );
function woo_change_order_received_text( $str, $order ) {
    $new_str = '<div class="order-thankyoutext"><h1>Thank You</h1> <div class="mb-4">We have received your order and will process it shortly.</div></div>';
    return $new_str;
}


/////attempt to display attributes on the order page but not working
// add_filter( 'woocommerce_is_attribute_in_product_name', '__return_true' );
// add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_true' );


//Add Custom Content After 'Add To Cart' Button On Product Page
add_action( 'woocommerce_after_add_to_cart_button', 'content_after_addtocart_button' );
function content_after_addtocart_button() {
	if(get_the_id() == 110197) {
echo 'Coupons and discounts are not available for this issue.';
	}
}

/*
 // WooCommerce - Add order notes column to orders list
  // Code goes in functions.php for your child theme
  // not tested in a PHP snippet plugin
  
  // Add column "Order Notes" on the orders page
  add_filter( 'manage_edit-shop_order_columns', 'add_order_notes_column' );
  function add_order_notes_column( $columns ) {
    $new_columns = ( is_array( $columns ) ) ? $columns : array();
    $new_columns['order_notes'] = 'Order Notes';
    return $new_columns;
  }
  
  add_action( 'admin_print_styles', 'add_order_notes_column_style' );
  function add_order_notes_column_style() {
    $css = '.post-type-shop_order table.widefat.fixed { table-layout: auto; width: 100%; }';
    $css .= 'table.wp-list-table .column-order_notes { min-width: 280px; text-align: left; }';
    $css .= '.column-order_notes ul { margin: 0 0 0 18px; list-style-type: disc; }';
    $css .= '.order_customer_note { color: #ee0000; }'; // red
    $css .= '.order_private_note { color: #0000ee; }'; // blue
    wp_add_inline_style( 'woocommerce_admin_styles', $css );
  }
  
  // Add order notes to the "Order Notes" column
  add_action( 'manage_shop_order_posts_custom_column', 'add_order_notes_content' );
  function add_order_notes_content( $column ) {
    if( $column != 'order_notes' ) return;      
    global $post, $the_order;
    if( empty( $the_order ) || $the_order->get_id() != $post->ID ) {
      $the_order = wc_get_order( $post->ID );
    }    
    $args = array();
    $args['order_id'] = $the_order->get_id();
    $args['order_by'] = 'date_created';
    $args['order'] = 'ASC';
    $notes = wc_get_order_notes( $args );
    if( $notes ) {
      print '<ul>';
      foreach( $notes as $note ) {
        if( $note->customer_note ) {
          print '<li class="order_customer_note">';
        } else {
          print '<li class="order_private_note">';
        }
        $date = date( 'd/m/y H:i', strtotime( $note->date_created ) );
        print $date.' by '.$note->added_by.'<br>'.$note->content.'</li>';
      }
      print '</ul><br/><br/><br/>';
    }
  } // end function
  */

/** add role to body tag for CSS purposes **/
add_filter('body_class','add_role_to_body');
function add_role_to_body($classes) {
$current_user = new WP_User(get_current_user_id());
$user_role = array_shift($current_user->roles);
$classes[] = 'role-'. $user_role;
return $classes;
}