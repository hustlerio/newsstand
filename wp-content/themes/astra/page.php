<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php if ($_GET['woo-paypal-return'] == true) { ?>
		<style>
			.wc-gateway-ppec-cancel {
				display: none;
			}
			.elementor-heading-title.elementor-size-default:after {
				content: " Summary";
			}
			body .woocommerce-form-login-toggle, .elementor-element-2da4b0e {
				display: none !important;
			}
			.woocommerce-shipping-fields{
				display: none;
			}
			.woocommerce-page.woocommerce-checkout form #order_review {
				display: flex;
flex-direction: column-reverse;
			}
		</style>
		<?php } ?>
		<?php astra_content_page_loop(); ?>

		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
