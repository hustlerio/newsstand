

<?php 
	$domain= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	//$backIssueHeaderImagePath = $domain . "wp-content/themes/astra/assets/images/";
	$backIssueHeaderImagePath = "https://woocomm.hustlernewsstand.com/wp-content/themes/astra/assets/images/";
?>

	<div class="backIssueHeader-container">
		
		<a href="/product-category/hustler/"  class="backIssueHeader-magazine" id="backIssueHeader-hustler">
			<div class="textArea" id="hustler">
				<p>ALL</p>
				<img src="<?= $backIssueHeaderImagePath ?>logo-hustler-white.png">
				<p>BACK ISSUES</p>
			</div>
			<div class="button"  id="hustler">SHOP NOW</div>
		</a>

		<a href="/product-category/barely-legal/" class="backIssueHeader-magazine" id="backIssueHeader-bl">
			<div class="textArea" id="bl">
				<p>ALL</p>
				<img src="<?= $backIssueHeaderImagePath ?>logo-bl-white.png">
				<p>BACK ISSUES</p>
			</div>
			<div class="button"  id="bl" >SHOP NOW</div>
		</a>

		<a href="/product-category/taboo/" class="backIssueHeader-magazine" id="backIssueHeader-taboo">
			<div class="textArea" id="taboo">
				<p>ALL</p>
				<img src="<?= $backIssueHeaderImagePath ?>logo-taboo-white.png">
				<p>BACK ISSUES</p>
			</div>
			<div class="button" id="taboo">SHOP NOW</div>
		</a>



	</div>




<style>


.backIssueHeader-container{
	display: flex;
	width: 98%;
	max-width: 1200px;
	height:auto;
	margin:0 auto;
	text-align: justify;
	justify-content: space-between;
/*	font-family: 'Roboto', sans-serif;*/
	font-weight: 100;
}	

.backIssueHeader-magazine{
	width: 32%;
	height: auto;
	padding:0 0 0 0; 
	background-size: cover;
	background-position:center top;
	text-decoration: none;
}

.backIssueHeader-magazine:hover > .textArea{
	padding:20% 0 10% 0;
}

.backIssueHeader-magazine:hover > .button{
	background-color: rgba(0,0,0,0.4);
	width: 180px;
}


#backIssueHeader-hustler{
	background-image: url("<?= $backIssueHeaderImagePath ?>backissue-hustler-bg.jpg");  
}
#backIssueHeader-bl{
	background-image: url("<?= $backIssueHeaderImagePath ?>backissue-bl-bg-mobile.jpg");  
}
#backIssueHeader-taboo{
	background-image: url("<?= $backIssueHeaderImagePath ?>backissue-taboo-bg.jpg");  
}

.backIssueHeader-magazine>.textArea{
	/*background-color: rgba(0,0,0,0.6);*/
	width: 100%;
	height: auto; 
	padding:24% 0 6% 0;
/*	border-top: 1px solid #fff;
	border-bottom: 1px solid #fff;*/
	transition: all .3s ease-in-out; 

}

.backIssueHeader-magazine>.textArea#hustler{
	/*background-color: rgba(0,0,0,0.6);*/
}
.backIssueHeader-magazine>.textArea#bl{
	/*background-color: rgba(251,79,192,0.8);*/
}
.backIssueHeader-magazine>.textArea#taboo{
	/*background-color: rgba(198,12,33,0.6);*/
}


.backIssueHeader-magazine>.textArea p{
	width: 100%;
	height: auto; 
	text-align: center;
	color: #fff;
	padding:0;
	margin:0;
	font-size: 24px;
	line-height: 26px;
}
.backIssueHeader-magazine>.textArea img{
	width: 50%;
	max-width: 160px;
	margin:0 auto;
	display: block;
	height: auto; 
	text-align: center;
	color: #fff;
}

.backIssueHeader-magazine> .button{
	width: 140px;
	margin: 12% auto 2% auto;
	padding: 2% 0;
	display: block;
	height: auto; 
	text-align: center;
	color: #fff;
	text-decoration: none;
	border:1px solid #fff;
	font-size: 18px;
	transition: all .3s ease-in-out; 
	background-color: rgba(0,0,0,0);
}

.backIssueHeader-magazine> .button#hustler{
	/*background-color: #000;*/
}
.backIssueHeader-magazine> .button#hustler:hover{
	/*background-color: #666;*/
}
.backIssueHeader-magazine> .button#bl{
	/*background-color: #fb4fc0;*/
}
.backIssueHeader-magazine> .button#bl:hover{
	/*background-color: #d62299;*/
}
.backIssueHeader-magazine> .button#taboo{
	/*background-color: #c60c21;*/
}
.backIssueHeader-magazine> .button#taboo:hover{
	/*background-color: #970414;*/
}



@media only screen and (max-width: 767px ) {
	.backIssueHeader-container{
		display: block; 
	}
	.backIssueHeader-magazine{
		width: 100%;
/*		flex:0 0 auto;
 		flex-direction:row;*/
 		padding:2% 0 0.2% 0; 
 		margin:0 auto 2% auto;
 		background-size: cover;
		background-position:center top;
		float: left;
	}
	.backIssueHeader-magazine>.textArea{
		background-color: rgba(0,0,0,0);
		padding:1% 0;
		border-top: 0;
		border-bottom: 0;
	}

	.backIssueHeader-magazine:hover > .textArea{
		padding:1% 0 1% 0;
	}

	.backIssueHeader-magazine> .button{
		margin:1% auto 1% auto;
		padding:1.6%;
		display: block;		
	}
	.backIssueHeader-magazine>.textArea#hustler{
		background-color: rgba(0,0,0,0);
	}
	.backIssueHeader-magazine>.textArea#bl{
		background-color: rgba(251,79,192,0);
	}
	.backIssueHeader-magazine>.textArea#taboo{
		background-color: rgba(198,12,33,0);
	}
	#backIssueHeader-bl{
		background-image: url("<?= $backIssueHeaderImagePath ?>backissue-bl-bg-mobile.jpg");  
	}
}


@media only screen and (max-width: 800px ) {
	.backIssueHeader-magazine>.textArea p{
		font-size: 18px;
		line-height: 26px;
	}
}




</style>