<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<?php astra_content_bottom(); ?>
	</div> <!-- ast-container -->
	</div><!-- #content -->
<?php 
	astra_content_after();
		
	astra_footer_before();
		
	astra_footer();
		
	astra_footer_after(); 
?>
	</div><!-- #page -->
<?php 
	astra_body_bottom();    
	wp_footer(); 
?>

<?php include "wp-content/themes/astra/assets/custom-php-sections/myMobileMenu.php"; ?>


<div class="mobileSearchOverlay">

		<div class="mobileSearchCloseWrapper">
			<span>X</span>
			<p>Close</p>
		</div>
		<div class="mobileSearchContainer">

			<?php
			aws_get_search_form( true );
			?>
		</div>
</div>


<?php if( is_product() ){ ?>
<div class="myLightBoxCloseButton"> Close </div>
<div class="productPageOverlay"> 

	<div class="productImageHolder ">
		<img class="myLightBoxImage"  src=""  />
	</div>

</div>
<?php } ?>
	

<?php if (is_checkout()) { ?>
<style>
	#hiddeniframe {
		height: 100%;
		border: 0px;
		    margin: 0px -15px;
    width: calc(100% + 30px);
    max-width: none;
		border-top: 1px solid #bbb;
		display: flex;
		flex-grow: 1;
	}
	#TB_ajaxContent.TB_modal {
		padding: 30px 15px 0px;
	}
	#TB_ajaxContent.TB_modal {
		padding-bottom: 0px;
	}
	.ccbill-modal-close {
	position: absolute;
	top: 0;
	right: 0;
	font-size: 30px;
	z-index: 3;
	padding: 9px;
	margin: 4px;
	opacity: 0.6;
	cursor: pointer;
}
.ccbill-modal-close:hover {
	opacity: 1;
}
	#TB_ajaxContent.TB_modal {
		width: 100% !important;
		height: 100% !important;
		    display: flex;
    justify-content: center;
    align-items: center;
		flex-direction: column;
}
	#TB_window {
		width: 100% !important;
		max-width: 700px;
    height: 90vh;
    margin: auto !important;
		left: 0 !important;
		right: 0 !important;
		top: 0 !important;
		bottom: 0 !important;
		
	}
	.iframe_logoimg {
		display:block;
		margin-bottom: 1rem;
		margin-left: auto;
		margin-right: auto;
	}
	.text-center {
		text-align: center;
	}
	.wait-icon {
		    padding: 2rem 0px;
    font-size: 16px;
    opacity: 0.6;
	}
</style>
<a href="#TB_inline?&inlineId=hiddencontent&modal=true" title="Checkout" class="thickbox" id="checkoutiframebtn" ></a>  

<div id="hiddencontent" style="display: none;">
		<div class="ccbill-modal-close">
			<i class="fas fa-times"></i>
	
		</div>
	<img src="/wp-content/themes/astra/assets/images/newstandLogo-bg.png" class="iframe_logoimg">
	<!-- <h4 class="text-center">Complete Your Payment</h4> -->
	<div class="text-center wait-icon">
	<i class="fas fa-circle-notch fa-spin"></i> Please wait...</div>
<iframe src="" id="hiddeniframe" ></iframe>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-hashchange/1.3/jquery.ba-hashchange.min.js"></script>
<script>
jQuery(function(){

  // Bind the event.
  jQuery(window).hashchange( function(){
    // Alerts every time the hash changes!
	  var fullhash = location.hash;
    console.log( location.hash );
	  if(fullhash.indexOf('#ccpay?=') == 0) {
		  var ccbillurl = fullhash.replace('#ccpay?=', '');
		  console.log(ccbillurl);
		  window.location.hash = '#ccpaying';
		  jQuery('#hiddeniframe').attr('src',ccbillurl);
		  jQuery('#checkoutiframebtn').trigger('click');
		  
		  jQuery('#hiddeniframe').on("load", function() {
    			jQuery('.wait-icon').hide();
		  });
		  
	  }
  })

  // Trigger the event (useful on page load).
  jQuery(window).hashchange();

});
	jQuery('.ccbill-modal-close').on('click', function() {
		// self.parent.tb_remove();
		//must reload, otherwise woocommerce infinite spins
		location.reload(true);
	});
/*	function triggerComplete(redirect) {
  	formcomplete = true;
	window.location.replace(redirect);
}*/
	
</script>

<?php } ?>




<style>
/* overwrite the left position of the "add to cart" buttons here since elementor isnt working right now	*/
.featuredMagWidget .add_to_cart_button{
	padding-left:20px!important;
	padding-right:20px!important;
}
	
.betterCarousel .add_to_cart_button {
	left: 0 !important;
	right: 0 !important;
	margin-right: 14%!important;
	margin-left: 14%!important;	
}	

@media only screen and (min-width: 767px){	
	.betterCarousel .add_to_cart_button {
		margin-right: 12%!important;
		margin-left: 12%!important;	
	}		
}	
@media only screen and (min-width: 1200px){	
	.betterCarousel .add_to_cart_button {
		margin-right: 16%!important;
		margin-left: 16%!important;	
	}		
}	
	
	
	
/* @media only screen and (min-width: 500px){
	.betterCarousel .add_to_cart_button {
    	left: 14%!important;
	}
}

@media only screen and (min-width: 600px){
	.betterCarousel .add_to_cart_button {
    	left: 16%!important;
	}
}
@media only screen and (min-width: 650px){
	.betterCarousel .add_to_cart_button {
    	left: 20%!important;
	}
}
	
	
	
@media only screen and (min-width: 660px){
	.betterCarousel .add_to_cart_button {
    	left: 22%!important;
	}
}

@media only screen and (min-width: 750px){
	.betterCarousel .add_to_cart_button {
    	left: 24%!important;
	}
}

@media only screen and (min-width: 767px){
	.betterCarousel .add_to_cart_button {
    	left: 18%!important;
	}
}

@media only screen and (min-width: 810px){
	.betterCarousel .add_to_cart_button {
    	left: 20%!important;
	}
}

@media only screen and (min-width: 820px){
	.betterCarousel .add_to_cart_button {
    	left: 22%!important;
	}
}

@media only screen and (min-width: 980px){
	.betterCarousel .add_to_cart_button {
    	left: 24%!important;
	}
}

@media only screen and (min-width: 1020px){
	.betterCarousel .add_to_cart_button {
    	left: 18%!important;
	}
}

@media only screen and (min-width: 1080px){
	.betterCarousel .add_to_cart_button {
    	left: 18%!important;
	}
}

@media only screen and (min-width: 1140px){
	.betterCarousel .add_to_cart_button {
    	left: 21%!important;
	}
}
		
@media only screen and (min-width: 1160px){
	.betterCarousel .add_to_cart_button {
    	left: 22%!important;
	}
}
	
@media only screen and (min-width: 1200px){
	.betterCarousel .add_to_cart_button {
    	left: 23%!important;
	}
} */

/*eventually put this in one of the external css files. this controls the mobile carousels with the 'featured' option */	
.eael-woo-product-carousel-container.preset-3 .eael-product-carousel{
	margin:5px;
} 
@media only screen and (min-width: 600px){
	.elementor-widget-eael-woo-product-carousel{
		display:none;
	} 
}
	.eael-woo-product-carousel-container .eael-woo-product-carousel a.button.add_to_cart_button.added{
		display:block!important;
	}	
	
	
	
</style>






	</body>
</html>
