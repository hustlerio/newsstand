<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>


<?php 
$currentURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";	

/////////////////redirect to the correct subscription page from old incorrect incomming links
if (strpos($currentURL, '/magazine-subscription/') !== false) {
	header("Location: https://hustlernewsstand.com/subscriptions/");
}



///////////////if youre not logged in, you cant view the 50% discount page. redirect to login page
if(!is_user_logged_in()){
	if (strpos($currentURL, '/discount/') !== false) {
		header("Location: /my-account-2/");
	}	
}

?>


<!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
<?php astra_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">

<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/astra/assets/images/favicons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/astra/assets/images/favicons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/astra/assets/images/favicons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/astra/assets/images/favicons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/astra/assets/images/favicons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/astra/assets/images/favicons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/astra/assets/images/favicons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/astra/assets/images/favicons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/astra/assets/images/favicons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/wp-content/themes/astra/assets/images/favicons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/astra/assets/images/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/wp-content/themes/astra/assets/images/favicons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/astra/assets/images/favicons/favicon-16x16.png">
<link rel="manifest" href="/wp-content/themes/astra/assets/images/favicons/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/wp-content/themes/astra/assets/images/favicons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">	
	
	<?php  //wp_enqueue_style( 'fontCSS', get_template_directory_uri() . '/assets/css/hustlerfonts.css',false,'1.1','all');  ?>	
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Heebo:wght@100;200;300;400;500;600;700;800;900" rel="stylesheet">
	<?php wp_head(); ?>
<?php astra_head_bottom(); ?>	
<?php  wp_enqueue_style( 'topLoadedCSS', get_template_directory_uri() . '/assets/css/topLoaded.css',false,'1.1','all');  ?>		


<?php 
///////// all the files for MY "superior" mobile menu bar
wp_enqueue_style( 'myMobileMenuCSS', get_template_directory_uri() . '/assets/css/myMobileMenu.css',false,'1.1','all'); 
wp_enqueue_script( 'myMobileMenuJS', get_template_directory_uri() . '/assets/js/myMobileMenu.js', array ( 'jquery' ), 1.1, true); 	
?>	

<?php wp_enqueue_script( 'newsletterScript', get_template_directory_uri() . '/assets/js/newsletterAjax.js', array ( 'jquery' ), 1.1, true); ?>
<?php wp_enqueue_script( 'discountScript', get_template_directory_uri() . '/assets/js/discountAjax.js', array ( 'jquery' ), 1.1, true); ?>
<?php 
/////////////if youre on the store page, include these files. We'll clean this up later	

if (strpos($currentURL, '?s=') !== false || strpos($currentURL, '/shop/') !== false || strpos($currentURL, '/product-category/') !== false) {
    wp_enqueue_style( 'storeCSS', get_template_directory_uri() . '/assets/css/store-tweaks.css',false,'1.1','all');  
	wp_enqueue_script( 'storeJS', get_template_directory_uri() . '/assets/js/store-tweaks.js', array ( 'jquery' ), 1.1, true); 
}
?>
	
	
<?php 	
/////////////if youre on the product page
if (is_product()) {
	//wp_enqueue_style( 'productCSS', get_template_directory_uri() . '/assets/css/product-page.css',false,'1.1','all'); 
	wp_enqueue_script( 'productPagescript', get_template_directory_uri() . '/assets/js/productPage.js', array ( 'jquery' ), 1.1, true); 
}
?>	
	
<?php 
	/////////////if youre logged in, display the members menu with Wishlist, else the tour menu without it
	if(is_user_logged_in()){
		wp_enqueue_style( 'membersMenuCSS', get_template_directory_uri() . '/assets/css/topMenuButtonsMembers.css',false,'1.1','all');  
	}
	else{
		wp_enqueue_style( 'tourMenuCSS', get_template_directory_uri() . '/assets/css/topMenuButtonsTour.css',false,'1.1','all');  
	}
?>
	
<?php  wp_enqueue_style( 'overwrite', get_template_directory_uri() . '/assets/css/overwrite-tweaks.css',false,'1.1','all');  ?>		
<?php wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/overwrite-tweaks.js', array ( 'jquery' ), 1.1, true); ?>
	

	

<?php 
/////////////if youre on the NEW cart and checkout pages

if (strpos($currentURL, '/cart') !== false  ) {
    wp_enqueue_style( 'cartStyle', get_template_directory_uri() . '/assets/css/cartNew.css',false,'1.1','all');
	wp_enqueue_script( 'cartPageScript', get_template_directory_uri() . '/assets/js/cartPage.js', array ( 'jquery' ), 1.1, true); 
}

$lastOfURL = substr($currentURL, -16);	
//echo "testing: " . $lastOfURL;	

if (strpos($currentURL, '/checkout') !== false )  {
    wp_enqueue_style( 'checkoutStyle', get_template_directory_uri() . '/assets/css/checkout.css',false,'1.1','all'); 
	wp_enqueue_script( 'checkOutJS', get_template_directory_uri() . '/assets/js/checkout.js', array ( 'jquery' ), 1.1, true); 
}		
?>	
	
<?php 
$orderReceived = false;
$normalCheckoutPage = false;
//////////if youre on the checkout page but not any of the others
if ( strpos($currentURL, '/checkout') !== false && strpos($lastOfURL, 'checkout') !== false)  { 
	//echo "DEBUG:option1 - normal checkout page";
	$shopTableWidth = '50%!important';
	$normalCheckoutPage = true;
}	
else if(strpos($currentURL, 'woo-paypal-return') !== false ){
	//echo "DEBUG:option2 - hopefully paypay return page";
	$shopTableWidth = '50%!important';
	$confirmationPage = true;
}	
else if(strpos($currentURL, 'order-received') !== false ){
	//echo "DEBUG:option3 - hopefully order received page";
	$shopTableWidth = '100%!important';
	$orderReceived = true;
}		
	
?>

<?php
wp_enqueue_style( 'flexStyle', get_template_directory_uri() . '/assets/css/flexslider.css',false,'1.1','all'); 
?>
	
	
	
<style>
	@media only screen and (min-width:1200px ) {
		.shop_table{
			width: <?= $shopTableWidth ?>;
		}
	}
	
	<?php if($confirmationPage){ ?>
		.checkout_coupon, #coupon_code, .couponHackHolder{
			display:none!important;
		}	
		.woocommerce-form-coupon .button{
			display:none!important;
		}		
	<?php } ?>
	
	
</style>
	

<?php  if($normalCheckoutPage == true){     ?>	
<style>
@media only screen and (max-width:1199px ) {
/*	.woocommerce-checkout{
		float:left!important;
		padding-top: 4px;
	}*/
}
</style>	
<?php  } ?>		
	

	

	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-208162268-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-208162268-1');
</script>	

	
<script type="text/javascript" charset="utf-8">
jQuery(window).load(function() {
jQuery('.flexslider').flexslider();
});
</script>
	
	
</head>

<body  <?php astra_schema_body(); ?> <?php body_class(); ?>>
<style>
	body{
		margin-top:42px;
	}
	@media only screen and (min-width: 1025px ) {
		body{
			margin-top:96px;
		}		
	}
</style>
<!-- a little hackeroo to have a black empty top bar before the actual content loads -->
<div class="hackeroo" style="width:100%;height:44px;float:left;background-color:#000;position:fixed;
	top:0;left:0;z-index:200;background-position:0  0;background-repeat:no-repeat;">
</div>

	<style>
/* 		.hackeroo{background-image: url('/wp-content/themes/astra/assets/images/hackerooLogo.png');}
		@media only screen and (min-width:1200px ) {
		.hackeroo{background-image: url('/wp-content/themes/astra/assets/images/empty.gif');}
		} */
	
	</style>
	

	
	
	
<?php astra_body_top(); ?>
<?php wp_body_open(); ?>
	
	
<div 
<?php
	echo astra_attr(
		'site',
		array(
			'id'    => 'page',
			'class' => 'hfeed site',
		)
	);
	?>
>
	<a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>
	<?php 
	
	
	
	astra_header_before(); 

	astra_header(); 

	astra_header_after();

	astra_content_before(); 
	?>
	<div id="content" class="site-content">
		<div class="ast-container">
		<?php astra_content_top(); ?>
