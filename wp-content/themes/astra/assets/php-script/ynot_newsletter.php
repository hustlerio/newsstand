<?php

/**
 * Description of ynot_api 1.0
 *
 * @author BKrakov
 * 
 * 
 * 
 * 
 */

$name = htmlspecialchars($_REQUEST['name']);
$email = htmlspecialchars($_REQUEST['email']);
//$email = 'bkrakov@hotmail.com';
//$name = 'boris';
echo 'name '. $name.'  email '.$email.PHP_EOL;
$ynot = new ynot_api();

$data = array('type' => 'mailing', 'method' => 'addSubscriberToList',
                'details' => array('listId' => '37345',
                'subscriber' => array('email' => $email, 'format' => 'html', 'confirmed' => 'u',
                'customfields' => array('field2' => $name)
                    )
                )
);
//echo 'req '. $ynot->setXMLRequest($data) .$email.PHP_EOL;
$ynot->request($ynot->setXMLRequest($data));
echo 'resp ';


class ynot_api {
    const API_USERNAME = 'news@hustler.com';
    const API_TOKEN = '75dc488909d7d9cd894fe531f64f2974b45579cf';
    const API_URL = 'https://www.ynotmail.com/clients/remote-api.php';
    const ERROR_LOG_PATH = 'ynot_newsletter_error.log';
    const API_LOG_PATH = 'ynot_newsletter.log';

    protected static $whitelist_ips = array('66.126.24.4','66.115.128.34');
    protected static $response_arr = array('SUCCESS','FAILED');
    protected static $curl_conn;
    protected static $list_id;
    
    /**
    * Constructor
    * 
    */
    public function __construct() {
        self::$curl_conn = curl_init();
        curl_setopt(self::$curl_conn, CURLOPT_URL, static::API_URL);
        curl_setopt(self::$curl_conn, CURLOPT_HEADER, false);
        curl_setopt(self::$curl_conn, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(self::$curl_conn, CURLOPT_POST, 1);
        curl_setopt(self::$curl_conn, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));

    }
    
    /**
    * Destructor
    */
    public function __destruct() {
	if (is_resource(self::$curl_conn)) {
            curl_close(self::$curl_conn);
	}
    }

    /**
	 * Perform a request
	 *
	 * @throws Requests_Exception On a cURL error (`curlerror`)
	 *
	 * @data string|array $data Data to send as the POST body
	 * @return string Raw HTTP result
    */
    public function request($data) {
        curl_setopt(self::$curl_conn, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec(self::$curl_conn);
        // get email from submit data
        $customer = new SimpleXMLElement($data);
        $email= $customer->details->subscriber->email;
        
        // Check response. On error log it
        if(curl_exec(self::$curl_conn) === false) {
            static::setMessage(array('error' => 'CURL error: '.curl_error(self::$curl_conn)));
            curl_close(self::$curl_conn);
        } else {
            // Parse response
            $res = self::parseAPIXMLResponse($response, 'status');
            // log results
            if($res == 'SUCCESS') {
                $response_arr['log'] = date('Y-m-d H:i:s').'|type:'.self::parseAPIXMLResponse($data, 'requesttype').'|method:'.self::parseAPIXMLResponse($data, 'requestmethod').'|email:'.$email.PHP_EOL;
                static::setMessage($response_arr);
            } else {
                static::setMessage(array('error' => 'type:'.self::parseAPIXMLResponse($data, 'requesttype').'|method:'.self::parseAPIXMLResponse($data, 'requestmethod').'|email:'.$email));
            }
            curl_close(self::$curl_conn);
        }    
		
	return $response;
    }

  
    /**
    * Create XML request document
    */
    public function setXMLRequest($data = array()) {
        $dom = new DOMDocument();
        $dom->encoding = 'utf-8';
        $dom->xmlVersion = '1.0';
        $dom->formatOutput = true;
	$dom_root = $dom->createElement('xml');
        
        // Create top header follow YNot API Reference https://www.ynotmail.com/clients/api-description.php#api-subscribers-set-confirmed-status
        $top_request = $dom->createElement('xmlrequest');
        $dom_root->appendChild($top_request);
        $username = $dom->createElement('username', static::API_USERNAME);
        $usertoken = $dom->createElement('usertoken', static::API_TOKEN);
        $dom_root->appendChild($username);
        $dom_root->appendChild($usertoken);
        // Create request type and method
        $type = $dom->createElement('requesttype', $data['type']);
        $method = $dom->createElement('requestmethod', $data['method']);
        $dom_root->appendChild($type);
        $dom_root->appendChild($method);
        // Create details and related info
        $details = $dom->createElement('details');
        $dom_data = self::setDetails($dom, $data['details'], '');        
        foreach ($dom_data as $value) {
            $details->appendChild($value);
        }
        // Add details
        $dom_root->appendChild($details);
        // Add root
        $dom->appendChild($dom_root);
        
        return $dom->saveXML(); 
    }
    
     
    /**
    * Create XML details nodes
    */
    static protected function setDetails($dom, $data) {
        $dom_data = array();
        $dom_data2 = array();
        $dom_data3 = array();
        $step = 0;

        foreach($data as $key => $value){
            //If $value is an array.
            if(is_array($value)){
                $dom_data[$step] = $dom->createElement($key);
                //We need to loop through it.
                $step2 = 0;
                foreach($value as $key2 => $val2){ 
                    if(is_array($val2)){
                        $dom_data2[$step2] = $dom->createElement($key2);
                        //We need to loop through it.
                        $step3 = 0;
                        foreach($val2 as $key3 => $val3){
                                $dom_data3[$step3] = $dom->createElement($key3, $val3);
                                $step3++;
                        }
                        foreach ($dom_data3 as $value3) {
                            $dom_data2[$step2]->appendChild($value3);
                        }
                    } else {
                        $dom_data2[$step2] = $dom->createElement($key2, $val2);                       
                    }
                    $step2++;
                }
                foreach ($dom_data2 as $value2) {
                        $dom_data[$step]->appendChild($value2);
                }
            } else{
                //It is not an array
                    $dom_data[$step] = $dom->createElement($key, $value);
            }
            $step++;
        }    
        
        return $dom_data;
    }    

    
    
    /**
    * Lo error and Ynot API messages
    */
    static protected function setMessage(array $arr) {
        if(isset($arr['error'])) {
            $str_date = date('Y-m-d H:i:s').'|'.$arr['error'].PHP_EOL;
            error_log($str_date, 3, static::ERROR_LOG_PATH);
        } else {
            error_log($arr['log'], 3, static::API_LOG_PATH);
        }    
    }

    /**
    * Convert Ynot API response from XML to array
    */
    static protected function parseAPIXMLResponse($data, $method) {
        $response = new SimpleXMLElement($data);
        
        //return $response->status;
        return $response->$method;

    }
    
}

