

<?php
$imagePath = "https://hustlernewsstand.com/wp-content/themes/astra/assets/images/homepage-slider/";
?>

<div>
hey now Im the homepage slider
</div>


<div class="flexslider">

  <ul class="slides">

    <li>

      <img id="firstSlide" src="<?= $imagePath ?>girlsMagBG.jpg" />

    </li>

    <li>

      <img src="<?= $imagePath ?>feb2022-black.jpg" />

    </li>

    <li>

      <img src="<?= $imagePath ?>candice.jpg" />

    </li>

  </ul>

</div>


<style>


.flexslider {
	overflow: hidden;
	border: 0!important;
	margin-bottom: 0;
	border-radius: 0px;
}
.flex-control-nav{
	display: none;
}
.topSlide{
	position: relative;
}
.topSlideText{
	position: absolute;
	bottom: 0px;
	left: 0px;
	color: #fff;
	background-color: rgba(0,0,0,0.7);
	padding: 10px;
	width: 100%;
	font-display:swap;
}

.flex-prev{
	background-color: rgba(255,255,255,0.6);
	left: 0px;
	padding: 0;
	color: #000;
}

.flex-next{
	background-color: rgba(255,255,255,0.6);
	right: 0px;
}


@media screen and (min-width: 768px) {
	.topSlideText{
		bottom: 20px;
		left: 20px;
		padding: 20px;
		width: auto;
		font-size: 18px;
	}
}

.flex-direction-nav .flex-next {
    background-color: rgba(0,0,0,0.6);
    z-index: 5;
}
.flexslider:hover .flex-direction-nav .flex-next:hover {
    background-color: rgba(0,0,0,0.6);
    z-index: 5;
}
.flexslider:hover .flex-direction-nav .flex-next:focus {
    background-color: rgba(0,0,0,0.6);
    z-index: 5;
}
.flex-direction-nav .flex-prev {
    background-color: rgba(0,0,0,0.6);
    z-index: 5;
}
.flexslider:hover .flex-direction-nav .flex-prev:hover {
    background-color: rgba(0,0,0,0.6);
    z-index: 5;
}
.flexslider:hover .flex-direction-nav .flex-prev:focus {
    background-color: rgba(0,0,0,0.6);
    z-index: 5;
}


.flex-direction-nav a::before{
	/*content:"\2039";*/
	/*font-size: 60px;*/
	color: #fff;
	padding: 0;
	margin: -32% 0 0 16% !important;
	z-index: 5;
  scale: .8;
}
.flex-direction-nav a::before:hover{
	/*content:"\2039";*/
	/*font-size: 60px;*/
	color: #fff;
	padding: 0;
	margin: -32% 0 0 16% !important;
	z-index: 5;
  scale: .8;
}
.flex-direction-nav a.flex-next::before{
	/*content:"\203A";*/
	/*font-size: 60px;*/
	color: #fff;
	padding: 0;
	margin: -32% 16% 0 0 !important;
	z-index: 5;
  scale: .8;
}
.flex-direction-nav a.flex-next::before:hover{
	/*content:"\203A";*/
	/*font-size: 60px;*/
	color: #fff;
	padding: 0;
	margin: -32% 16% 0 0 !important;
	z-index: 5;
  scale: .8;
}
</style>



<script type="text/javascript">


fixFlexsliderHeight();
	jQuery(window).load(function() {
	    fixFlexsliderHeight();
	});

	jQuery(window).resize(function() {
	    fixFlexsliderHeight();
	});

	function fixFlexsliderHeight() {
		console.log('fixFlexsliderHeight');
	    // Set fixed height based on the tallest slide
	    jQuery('.flexslider').each(function(){
	        var sliderHeight = 0;

	        
	        sliderHeight = jQuery("#firstSlide").height();
			jQuery('.slides, .flex-viewport').css({'height' : sliderHeight});
			
			//console.log("jasmine: " + sliderHeight);

			///////overwrite some elementor bullshit
			jQuery('.elementor-element-6b85f9e').css({'height' : sliderHeight + 40 });
			jQuery('.elementor-element-c7dbf2b').find('.elementor-widget-wrap').css({'padding' : 0 });
			jQuery('.elementor-element-c7dbf2b').find('.elementor-element-populated').css({'padding' : 0 });

	    });
	}






jQuery(document).ready(function(){
  	


	var desktop = "<?= $desktop ?>";
	

	  jQuery('.flexslider').flexslider({
	    animation: "slide",
	    slideshow: true,
		animationLoop:true,
		pauseOnHover: true, 
		drag: true,
		slideshowSpeed: 4000, 
	  });


	//force flexslider's resize() function to fix an initial layout bug
	setTimeout(function(){
		jQuery('.flexslider').data('flexslider').resize();
	},1000);
	

	/////swap out the slider images from big to mobile when less than 768px. kinda useless but whatever
	/////this will only happen on desktop screens
	var windowWidth = jQuery( window ).width();
	var resizeTimer;

	jQuery(window).on('resize', function(e) {
		var windowWidth = jQuery( window ).width();
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {


		if(desktop == true){
			if(windowWidth < 768){
				
				jQuery( ".desktopImage" ).each(function( index ) {
				  var mobileImage = jQuery(this).attr('data-mobile-img');
				  jQuery(this).attr('src',mobileImage);
				});
			}
			else{

				jQuery( ".desktopImage" ).each(function( index ) {
			  	  var desktopImage = jQuery(this).attr('data-desktop-img');
				  jQuery(this).attr('src',desktopImage);
				console.log("resize firing");
				});			
			}
		}


		}, 150);
	});
	/////////////////////////////end of desktop/mobile image swap

});//end ready()



</script>