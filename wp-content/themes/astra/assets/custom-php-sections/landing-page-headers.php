<?php 

$storeURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


$storeURLTrimmed = str_replace(".hustlernewsstand.com","",$storeURL );


$imagePath = "//" . $_SERVER['SERVER_NAME'] . "/wp-content/themes/astra/assets/images/store-headers/";

//$breadcrumbHome = "<a href='/' >Home</a> / "; 
$breadcrumbHome = ""; 

if (strpos($storeURLTrimmed , "/hustler-back-issues/" ) !== false) {
    $image = "shop-hustler";
    $textResults = "<h1>All HUSTLER<span class='trademark'>®</span> Back Issues</h1>";
    $breadcrumbs = $breadcrumbHome . "All Hustler Back Issues";
}
else if (strpos($storeURLTrimmed , "/barely-legal-back-issues/" ) !== false) {
    $image = "shop-bl";
    $textResults = "<h1>All Barely Legal Back Issues</h1>";
    $breadcrumbs = $breadcrumbHome . "All Barely Legal Back Issues";
}
else if (strpos($storeURLTrimmed , "/taboo-back-issues/" ) !== false) {
    $image = "shop-taboo";
    $textResults = "<h1>All Taboo Back Issues</h1>";
    $breadcrumbs = $breadcrumbHome . "All Taboo Back Issues";
}
else if (strpos($storeURLTrimmed , "/xxx-back-issues/" ) !== false) {
    $image = "shop-xxx";
    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> XXX Back Issues</h1>";
    $breadcrumbs = $breadcrumbHome . "All Hustler XXX Back Issues";
}
else if (strpos($storeURLTrimmed , "/specials-back-issues/" ) !== false) {
    $image = "specials";
    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Specials Back Issues</h1>";
    $breadcrumbs = $breadcrumbHome . "All Specials Back Issues";
}
else if (strpos($storeURLTrimmed , "/more/" ) !== false) {
    $image = "more";
    $textResults = "<h1>Even more from HUSTLER<span class='trademark'>®</span> </h1>";
    $breadcrumbs = $breadcrumbHome . "More";
}
else if (strpos($storeURLTrimmed , "/bundle-deal-sales/" ) !== false) {
    $image = "bundles";
    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Back Issues Bundle Deal Sales</h1>";
    $breadcrumbs = $breadcrumbHome . "Hustler Bundle Deal";
}
else if (strpos($storeURLTrimmed , "/subscriptions/" ) !== false) {
    $image = "subscriptions";
    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Subscriptions</h1>";
    $breadcrumbs = $breadcrumbHome . "Hustler Subscriptions";
}





else{
	$image = "shop-header";
	$textResults = "<h1> Enjoy all we have to offer!</h1>";

}



?>



<!--this will change dynamically in the assets/js/store-tweaks.js -->
<div class="storeHeaderImageContainer">
   <!--  <img src="<?= $imagePath . $image ?>" class="storeHeaderImage"> -->
	<div class="storeHeaderImageHolder" style="">

	<?php echo $textResults; ?>

	</div>
</div>

<div class="breadcrumbsWrapper">
	<?= $breadcrumbs ?> 
</div>




<style>

.breadcrumbsWrapper{
	width: 100%;
	max-width: 1200px;
	height: auto;
	padding: 4px 24px;
	margin: 0 auto;
	color: #999;
}

@media only screen and (min-width:1200px ) {
	.breadcrumbsWrapper{
		padding:  4px 0;
	}
}


.breadcrumbsWrapper a{
	color: #333;
}
.breadcrumbsWrapper a:hover{
	color: #c60c21;
}


.orderby{
	float:right;
}

.storeHeaderImageHolder h1{
	text-align: center;
    font-size: 20px!important;
    color: #fff!important;
    padding: 6px 0 6px 0;
    background-color: rgba(0,0,0,0.6);
    position: absolute;
	bottom: 0;
	width: 100%;
	margin: 0;
}





.storeHeaderImageContainer{
	text-align: center;
}
.storeHeaderImageContainer img{
	text-align: center;
	width: 100%;
	max-width: 1200px;
}

.storeHeaderImageHolder{
	position: relative;
	text-align: center;
	margin: 0 auto;
	width: 100%;
	max-width: 1200px;
	height: 156px;
	background-position: left center;
	background-image:url("<?= $imagePath . $image .'-mobile-small.jpg' ?>");
}


@media only screen and (min-width:480px ) {
	.storeHeaderImageHolder{
		background-image:url("<?= $imagePath . $image .'-mobile.jpg' ?>");
		background-position: center center;
	}
}


@media only screen and (min-width:768px ) {
	.storeHeaderImageHolder{
		height: 240px;
	}
	.storeHeaderImageHolder h1{
		font-size: 24px!important;
	}	
	.storeHeaderImageHolder{
		background-image:url("<?= $imagePath . $image .'-mid.jpg' ?>");
	}
}

@media only screen and (min-width:980px ) {
	.storeHeaderImageHolder{
		background-image:url("<?= $imagePath . $image .'.jpg' ?>");
	}
}






</style>










