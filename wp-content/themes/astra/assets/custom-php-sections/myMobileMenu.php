<!-- <!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Mobile Menu better</title>
	<link rel="stylesheet" type="text/css" href="myMobileMenu.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
	<script
	  src="https://code.jquery.com/jquery-3.6.0.min.js"
	  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
	  crossorigin="anonymous">
	 </script>
	 <script type="text/javascript" src="myMobileMenu.js"></script>
</head>
<body> -->
	


<div class="myTopMenuBarWide">
	<div class="myTopMenuBar">
		
		<div class="animatedMenuMobileButton">
			<div id="nav-icon3">
			  <span></span>
			  <span></span>
			  <span></span>
			  <span></span>
			</div>		
		</div>

<style>
	.newsstandSVGLogo {
		fill: #fff;
		/*filter: invert(22%) sepia(100%) saturate(4363%) hue-rotate(343deg) brightness(76%) contrast(108%);*/
	}	

	.desktopLoginIcon{
		width: 26px;
		height: 26px;
		background-color: #333;
		overflow: hidden;
		margin-top: 2px;
	}

	.searchSVGDesktop{
		width: 26px;
		height: 26px;		
	}

	.searchSVGHolder{
	    display: block;
	    width: 40px;
	    height: 36px;
	    float: left;
	    margin: 0px 0 0 0;
	    overflow: hidden;
	    padding: 6px;
	    background-color: #000;
	}



</style>
		<a href="/" class="mobileLogoHolder">
			<svg>
			    <image class="newsstandSVGLogo" xlink:href="/wp-content/themes/astra/assets/images/newstandLogo.svg" width="160" height="34" src="/wp-content/themes/astra/assets/images/mobileLogoWhite160.gif" />
			</svg>
		</a>




		<!-- right side -->
		<div class="myTopBarRightSide">
			<div class="newCart">
				<div class="itemCountCopy"></div>
			</div>
		
			<div class="menuNavButtonsHolder">
			    <span class="divide" style="margin-left:4px">|</span>
				<a href="/my-account-2/" class="loginIcon">
					<svg>
					    <image class="desktopLoginIcon" xlink:href="/wp-content/themes/astra/assets/images/loginIconWhite.svg" width="25" height="26" src="/wp-content/themes/astra/assets/images/mobileLogoWhite160.gif" />
					</svg>					
				</a> 
<!-- 				<div class="searchSVGHolder">
					<svg>
					    <image class="searchSVGDesktop" xlink:href="/wp-content/themes/astra/assets/images/searchIconSmallWhite.svg" width="22" height="22" src="/wp-content/themes/astra/assets/images/search-icon.png" />
					</svg>	
				</div> -->

				<span class="divide afterWishlistDivide">|</span>
				<a href="/wishlist/" class="wishlistMenuText">Wishlist</a> 
				<!-- <span class="divide beforeWishlistDivide">|</span> -->
				<div class="mobileSearchButton">
				
					<div class="searchSVGHolder">
						<svg>
						    <image class="searchSVGDesktop" xlink:href="/wp-content/themes/astra/assets/images/searchIconSmallWhite.svg" width="22" height="22" src="/wp-content/themes/astra/assets/images/search-icon.png" />
						</svg>	
					</div> 

				</div>
			</div>
		
<!-- 			<div class="deskTopNavSearch">
				<?php // get_product_search_form(); ?>
			</div> -->
			<div class="deskTopNavSearch">
				<?php aws_get_search_form( true ); ?>
			</div>



			<a href="/subscriptions/" class="subscribeMenuButton">
				Subscribe Now
			</a> 


		</div>
		<!-- END right side -->
		

	</div>
</div>

<?php 
	if( is_user_logged_in() ){
		$accountText = "Log Out";
	}
	else{
		$accountText = "Have an Account? Log In";
	}
?>

<div class="mobileMenuOverlay">
	<div class="mobileLoginContainer">
		<a href="/my-account-2/" id="mobileMenuMyAccount" class="mobileMenuTopButtons"><?= $accountText ?></a>
		<?php if( is_user_logged_in() ){ ?>
		<a href="/my-account-2/" id="mobileMenuWishlist" class="mobileMenuTopButtons">Wish List</a>
		<?php } ?>
	</div>
	<div class="scrollable">
		<div class="myMenu">
			
				<?php echo do_shortcode("[menu name=”main-menu”]"); ?>
				<?php //include "menuCode.html"; ?>
			
		</div><!-- end MY menu -->
	</div>
</div><!-- end MY menu overlay-->


<script>

jQuery(document).ready(function(){
	













	// jQuery(document).on('click', '.searchSVGHolder', function(event) { 
	//     event.preventDefault(); 
	//     // jQuery(".woocommerce-product-search button").click(); 
	// });
});

</script>
























<!-- 


</body>
</html> -->