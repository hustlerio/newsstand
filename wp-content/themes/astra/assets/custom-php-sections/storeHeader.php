<?php 

$storeURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


$storeURLTrimmed = str_replace(".hustlernewsstand.com","",$storeURL );

if(!empty($_GET["s"])){
	$searchedTermActual = htmlspecialchars($_GET["s"]);
	$searchedTerm = strtolower($searchedTermActual);
}

$imagePath = "//" . $_SERVER['SERVER_NAME'] . "/wp-content/themes/astra/assets/images/store-headers/";

//$breadcrumbHome = "<a href='/' >Home</a> / "; 
$breadcrumbHome = ""; 
$breadHustlerBase = "<a href='/hustler-back-issues/' >All HUSTLER Back Issues</a> / "; 
$breadBarelyLegalBase = "<a href='/barely-legal-back-issues/' >All Barely Legal Back Issues</a> / "; 
$breadTabooBase = "<a href='/taboo-back-issues/' >All Taboo Back Issues</a> / "; 
$breadXXXBase = "<a href='/xxx-back-issues/' >All XXX Back Issues</a> / "; 
$breadSpecialsBase = "<a href='/specials-back-issues/' >HUSTLER Specials Back Issues </a> / "; 
$breadBookStoreBase = "<a href='/specials-back-issues/' >HUSTLER Bookstore </a> / "; 


////this will check to somewhat match what people search AND check for specific categories
////for the search results and category pages

$onShopPage = false;
///if youre on the category page or main shop page
if (strpos($storeURLTrimmed , "/shop/" ) !== false) {
    $image = "shop-header";
    $textResults = "<h1>Shop for HUSTLER<span class='trademark'>®</span> Back Issues</h1>";
    $onShopPage = true;
}
else if(empty($searchedTerm)){
	
	if (strpos($storeURLTrimmed , "/hustler/" ) !== false) {
	    $image = "shop-hustler";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Magazine Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadHustlerBase . "HUSTLER Magazine Back Issues";
	}
	else if (strpos($storeURLTrimmed , "/hustler-humor" ) !== false) {
	    $image = "shop-humor";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Humor Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . "All Hustler Back Issues";
	    $breadcrumbs = $breadcrumbHome . $breadHustlerBase . "HUSTLER Humor Back Issues";
	}
	else if (strpos($storeURLTrimmed , "/hustler-anniversary/" ) !== false) {
	    $image = "shop-hustler";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Anniversary Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadHustlerBase . "HUSTLER Anniversary Back Issues";
	}
	else if (strpos($storeURLTrimmed , "/best-of-hustler/" ) !== false) {
	    $image = "shop-hustler";
	    $textResults = "<h1>Best of HUSTLER<span class='trademark'>®</span> Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadHustlerBase . "Best of HUSTLER Back Issues";
	}
	else if (strpos($storeURLTrimmed , "/hustler-xxx/" ) !== false) {
	    $image = "shop-xxx";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> XXX Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadHustlerBase . "HUSTLER XXX Back Issues";

	}
	else if (strpos($storeURLTrimmed , "/hustler-fantasies/" ) !== false) {
	    $image = "shop-hustler";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Fantasies Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadHustlerBase . "HUSTLER Fantasies Back Issues";
	}

	else if (strpos($storeURLTrimmed , "/clearance/" ) !== false) {
	    $image = "shop-header";
	    $textResults = "<h1>Clearance Sale - Get em before they're gone!</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadHustlerBase . "HUSTLER Fantasies Back Issues";
	}

	else if (strpos($storeURLTrimmed , "/barely-legal/" ) !== false) {
	    $image = "shop-bl";
	    $textResults = "<h1>Barely Legal Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadBarelyLegalBase . "Barely Legal Magazine Back Issues";
	}
	else if (strpos($storeURLTrimmed , "/barely-legal-anniversary/" ) !== false) {
	    $image = "shop-bl";
	    $textResults = "<h1>Barely Legal Anniversary Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadBarelyLegalBase . "Barely Legal Anniversary Back Issues";
	}
	else if (strpos($storeURLTrimmed , "/best-of-barely-legal/" ) !== false) {
	    $image = "shop-bl";
	    $textResults = "<h1>Best of Barely Legal Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadBarelyLegalBase . "Best of Barely Legal Back Issues";
	}
	else if (strpos($storeURLTrimmed , "/barely-legal-xxx/" ) !== false) {
	    $image = "shop-xxx";
	    $textResults = "<h1>Barely Legal XXX Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadBarelyLegalBase . "Barely Legal XXX Back Issues";
	}
	else if (strpos($storeURLTrimmed , "/girls-of-barely-legal/" ) !== false) {
	    $image = "shop-bl";
	    $textResults = "<h1>Girls of Barely Legal Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadBarelyLegalBase . "Girls of Barely Legal Back Issues";
	}
	else if (strpos($storeURLTrimmed , "/all-bush" ) !== false) {
	    $image = "shop-all-bush";
	    $textResults = "<h1>Barely Legal All Bush Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadBarelyLegalBase . "Barely Legal All Bush Back Issues";
	}



	else if (strpos($storeURLTrimmed , "best-of-taboo/" ) !== false || strpos($storeURLTrimmed , "taboo-bestof" ) !== false) {
	    $image = "shop-taboo";
	    $textResults = "<h1>Best of Taboo Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadTabooBase . "Best of Taboo Back Issues";
	}
	else if (strpos($storeURLTrimmed , "taboo/" ) !== false) {
	    $image = "shop-taboo";
	    $textResults = "<h1>Taboo Magazine Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadTabooBase . "Taboo Magazine Back Issues";
	}

	else if (strpos($storeURLTrimmed , "taboo-anniversary/" ) !== false) {
	    $image = "shop-taboo";
	    $textResults = "<h1>Taboo Anniversary Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadTabooBase . "Taboo Anniversary Back Issues";
	}
	else if (strpos($storeURLTrimmed , "illustr" ) !== false) {
	    $image = "shop-taboo-illustrated";
	    $textResults = "<h1>Taboo Illustrated Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadTabooBase . "Taboo Illustrated Back Issues";
	}


	else if (strpos($storeURLTrimmed , "extreme-xxx" ) !== false) {
	    $image = "shop-xxx";
	    $textResults = "<h1>Extreme XXX Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadXXXBase . "Extreme XXX Back Issues";
	}
	else if (strpos($storeURLTrimmed , "milf-mania-xxx/" ) !== false) {
	    $image = "shop-xxx";
	    $textResults = "<h1>Milf Mania XXX Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadXXXBase . "MILF Mania XXX Back Issues";
	}



	else if (strpos($storeURLTrimmed , "beaver-hunt/" ) !== false) {
	    $image = "shop-beaverhunt";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Beaver Hunt Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Beaver Hunter Back Issues";
	}
	else if (strpos($storeURLTrimmed , "all-sex" ) !== false) {
	    $image = "shop-all-sex";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> All Sex Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "All Sex Back Issues";
	}
	else if (strpos($storeURLTrimmed , "backdoor-babes/" ) !== false) {
	    $image = "shop-backdoor-babes";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Back Door Babes Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Back Door Babes Back Issues";
	}	
	else if (strpos($storeURLTrimmed , "bj-babes/" ) !== false) {
	    $image = "shop-bj-babes";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> BJ Babes Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "BJ Babes Back Issues";
	}	
	else if (strpos($storeURLTrimmed , "busty/" ) !== false) {
	    $image = "shop-busty";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Busty Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Busty Back Issues";
	}
	else if (strpos($storeURLTrimmed , "dirty" ) !== false) {
	    $image = "shop-dirty-blondes";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Dirty Blonds Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Dirty Blonds Back Issues";
	}
	else if (strpos($storeURLTrimmed , "extreme-orgies/" ) !== false) {
	    $image = "shop-extreme-orgies";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Extreme Orgies Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Extreme Orgies Back Issues";
	}
	else if (strpos($storeURLTrimmed , "extreme-sex/" ) !== false) {
	    $image = "shop-extreme-sex";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Extreme Sex Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Extreme Sex Back Issues";
	}
	else if (strpos($storeURLTrimmed , "hot-couples/" ) !== false) {
	    $image = "shop-hot-couples";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Hot Couples Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Hot Couples Back Issues";
	}
	else if (strpos($storeURLTrimmed , "hot-sluts/" ) !== false) {
	    $image = "shop-hot-couples";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Hot Sluts Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Hot Sluts Back Issues";
	}
	else if (strpos($storeURLTrimmed , "hustler-girls-on-dvd/" ) !== false) {
	    $image = "shop-header";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Girls on DVD Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "HUSTLER Girls on DVD Back Issues";
	}
	else if (strpos($storeURLTrimmed , "latin-girls" ) !== false) {
	    $image = "shop-latin-girls";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Latin Women/Latin Girls Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Latin Women Latin Girls Back Issues";
	}
	else if (strpos($storeURLTrimmed , "young-&-sexy" ) !== false) {
	    $image = "shop-young-and-sexy";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Young & Sexy Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Young & Sexy Back Issues";
	}
	else if (strpos($storeURLTrimmed , "girlgirl" ) !== false) {
	    $image = "more";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> Girl Girl Back Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . $breadSpecialsBase . "Girl+Girl Back Issues";
	}



	else if (strpos($storeURLTrimmed , "/bookstore/" ) !== false) {
	    $image = "shop-auto";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span>Bookstore</h1>";
	    $breadcrumbs = $breadcrumbHome . "HUSTLER Bookstore";
	}
	else if (strpos($storeURLTrimmed , "/90s-collectors" ) !== false) {
	    $image = "shop-auto";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span> 90's Collector's Issues</h1>";
	    $breadcrumbs = $breadcrumbHome . "90's Collectors Back Issues";
	}
	else if (strpos($storeURLTrimmed , "autographed/" ) !== false) {
	    $image = "shop-auto";
	    $textResults = "<h1>Autographed Back Issues!</h1>";
	    $breadcrumbs = $breadcrumbHome . "HUSTLER Autographed Back Issues";
	}
	else if (strpos($storeURLTrimmed , "larrys-overstock-specials" ) !== false) {
	    $image = "shop-header";
	    $textResults = "<h1>Larry's Overstock Specials</h1>";
	}
	else if (strpos($storeURLTrimmed , "bundl" ) !== false) {
	    $image = "shop-bundle";
	    $textResults = "<h1>Why stop at just one?</h1>";
	}
	else if (strpos($storeURLTrimmed , "hustlers-dirtiest-jokes" ) !== false) {
	    $image = "shop-header";
	    $textResults = "<h1>HUSTLER<span class='trademark'>®</span>'s Dirtiest Jokes</h1>";
	    $breadcrumbs = $breadcrumbHome . "HUSTLER Dirty Jokes";
	}
	else{
		$image = "shop-header";
		$textResults = "<h1> Enjoy all we have to offer!</h1>";
	}

}//end if(on category page)


//if youre on the search results
if(!empty($searchedTerm)){
	if (strpos("humor", $searchedTerm ) !== false  ) {
	    $image = "shop-humor";
	}
	else if (strpos("autographed", $searchedTerm ) !== false  ) {
	    $image = "shop-auto";
	}
	else if (strpos("latin", $searchedTerm ) !== false  ) {
	    $image = "shop-latin-girls";
	}
	else if (strpos("young", $searchedTerm ) !== false || strpos("sexy", $searchedTerm ) !== false ) {
	    $image = "shop-young-and-sexy";
	}
	else if (strpos("couples", $searchedTerm ) !== false ) {
	    $image = "shop-hot-couples";
	}
	else if (strpos("extreme", $searchedTerm ) !== false ||  strpos("sex", $searchedTerm ) !== false) {
	    $image = "shop-extreme-sex";
	}
	else if (strpos("extreme", $searchedTerm ) !== false ||  strpos("orgies", $searchedTerm ) !== false) {
	    $image = "shop-extreme-orgies";
	}
	else if (strpos("dirty", $searchedTerm ) !== false ||  strpos("blondes", $searchedTerm ) !== false) {
	    $image = "shop-dirty-blondes";
	}
	else if (strpos("busty", $searchedTerm ) !== false ) {
	    $image = "shop-busty";
	}
	else if (strpos("bj", $searchedTerm ) !== false ) {
	    $image = "shop-bj-babes";
	}
	else if (strpos("backdoor", $searchedTerm ) !== false ) {
	    $image = "shop-backdoor-babes";
	}
	else if (strpos("all sex", $searchedTerm ) !== false ) {
	    $image = "shop-all-sex";
	}
	else if (strpos("illust", $searchedTerm ) !== false ) {
	    $image = "shop-taboo-illustrated";
	}
	else if (strpos("bush", $searchedTerm ) !== false ) {
	    $image = "shop-all-bush";
	}
	else if (strpos("bundle", $searchedTerm ) !== false ) {
	    $image = "shop-bundle";
	}
	else if (strpos("xxx", $searchedTerm ) !== false ) {
	    $image = "shop-xxx";
	}
	else if (strpos("hustler", $searchedTerm ) !== false  || strpos("hustler anniversary", $searchedTerm ) !== false) {
	    $image = "shop-hustler";
	}
	else if (strpos("barely legal", $searchedTerm ) !== false || strpos("barely legal anniversary", $searchedTerm ) !== false) {
	    $image = "shop-bl";
	}
	else if (strpos("taboo", $searchedTerm ) !== false || strpos("taboo anniversary", $searchedTerm ) !== false) {
	    $image = "shop-taboo";
	}
	else if (strpos("beaver hunt", $searchedTerm ) !== false) {
	    $image = "shop-beaverhunt";
	}
	else{
		$image = "shop-header";
	}
	$textResults = "You searched for <span>" . $searchedTermActual . "</span>";
}//end if(!empty($searchedTerm))


?>



<!--this will change dynamically in the assets/js/store-tweaks.js -->
<div class="storeHeaderImageContainer">
   <!--  <img src="<?= $imagePath . $image ?>" class="storeHeaderImage"> -->
	<div class="storeHeaderImageHolder" style="">

	<?php 
		if(is_product_category() || $onShopPage == true ){
			//if youre not on search results pages, put the <h1> in the bg image
			if(empty($searchedTerm)){
				echo $textResults;
			}
	 	}
	?>

</div>

<?php

if(is_product_category() || $onShopPage == true ){
	//if youre not on search results pages, print the breadcrumbs
	if(empty($searchedTerm)){
		echo '<div class="breadcrumbsWrapper">';
		echo $breadcrumbs;
		echo '</div>';
	}
}

?>




</div>

	<?php 
		if(!empty($searchedTerm)){ 
			echo "<div class='searchedTerm'>";
			echo $textResults;
			echo "</div>";
		}
	?>
		
	

<style>
.woocommerce-breadcrumb, .elementor-widget-woocommerce-breadcrumb, .elementor-element-d89786b, [data-id="4469582"]{
	display: none;
}

.breadcrumbsWrapper{
	width: 100%;
	max-width: 1200px;
	height: auto;
	padding:  4px 24px;
	margin: 0 auto;
	color: #999;
	text-align: left;
	border-bottom: 1px solid #ccc;
}

@media only screen and (min-width:1200px ) {
	.breadcrumbsWrapper{
		padding:  4px 0;
	}
}

.breadcrumbsWrapper a{
	color: #333;
}
.breadcrumbsWrapper a:hover{
	color: #c60c21;
}



.orderby{
	float:right;
}

.storeHeaderImageHolder h1{
	text-align: center;
    font-size: 20px;
    color: #fff;
    padding: 6px 0 6px 0;
    background-color: rgba(0,0,0,0.6);
    position: absolute;
	bottom: 0;
	width: 100%;
}





.storeHeaderImageContainer{
	text-align: center;
}
.storeHeaderImageContainer img{
	text-align: center;
	width: 100%;
	max-width: 1200px;
}

.storeHeaderImageHolder{
	position: relative;
	text-align: center;
	margin: 0 auto;
	width: 100%;
	max-width: 1200px;
	height: 160px;
	background-position: left center;
	background-image:url("<?= $imagePath . $image .'-mobile-small.jpg' ?>");
}


@media only screen and (min-width:480px ) {
	.storeHeaderImageHolder{
		background-image:url("<?= $imagePath . $image .'-mobile.jpg' ?>");
		background-position: center center;
	}
}


@media only screen and (min-width:768px ) {
	.storeHeaderImageHolder{
		height: 245px;
	}
	.storeHeaderImageHolder h1{
		font-size: 24px;
	}	
	.storeHeaderImageHolder{
		background-image:url("<?= $imagePath . $image .'-mid.jpg' ?>");
	}
}

@media only screen and (min-width:980px ) {
	.storeHeaderImageHolder{
		background-image:url("<?= $imagePath . $image .'.jpg' ?>");
	}
}


.trademark{
	font-size: 14px;
	padding-bottom: 20px;
	vertical-align: super; 
}


.searchedTerm, .searchedTerm h1{
	text-align: center;
	font-size: 28px;
	color: #666;
	padding:10px 0 10px 0;
}

.searchedTerm span{
	font-weight: bold;
	color: #333;
}

.elementor-widget-woocommerce-breadcrumb{
	height: 26px;
}


@media only screen and (max-width:920px ) {
	.woocommerce-pagination{
		text-align: center!important;
	}
}

.page-numbers .current{
	color: #fff!important;
}

<?php if (strpos($storeURLTrimmed , "?s=" ) !== false)  {   ?>
	/*hide the breadcrumbs on the search results page*/
	.woocommerce-breadcrumb, [data-id="4469582"], [data-id="d89786b"], .woocommerce-ordering{
		display: none!important;
	}


<?php } ?>


@media only screen and (max-width:544px ) {
	ul.products{
		padding-top:20px!important;
	}
}


@media (max-width:767px) and (min-width:550px) {
	ul.products{
		display: block!important;
	}

	.products li{
		width: 30%!important;
		float: left!important;
		padding-bottom: 20px;
		margin-right: 3.2%!important;
	}
	.astra-shop-summary-wrap{
	    margin-bottom: 2.5em !important;
	}
}​

</style>



<script>
	
jQuery(document).ready(function(){
	var URL = window.location.href;

	var imageGrab = "<?= $image ?>";
	imageGrab = imageGrab.replace(".jpg", "");

	var domain = window.location.hostname;
	var imagePath = "https://" + domain + "/wp-content/themes/astra/assets/images/store-headers/";
	
	var windowWidth = jQuery( window ).width();
	
	var resizeTimer;
	jQuery(window).on('resize', function(e) {
	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {
		windowWidth = jQuery( window ).width();
		changeResultsText();
	  }, 200);
	});



	if(URL.indexOf("/hustler-fantasies/") != -1) {
		

		jQuery(document).scroll(function() {
		    keepAspectRatio();
			equalizeHeights();
		});


		jQuery(window).on('resize', function(e) {
			keepAspectRatio();
			equalizeHeights();
		});
		
		//try keepAspectRatio() a few times at start since we're waiting for the images to load
		setTimeout(function(){ keepAspectRatio();},500);
		setTimeout(function(){ keepAspectRatio();},1000);
		setTimeout(function(){ keepAspectRatio();},1500);
		setTimeout(function(){ keepAspectRatio();},200);

		keepAspectRatio();
		equalizeHeights();
	}







	function changeResultsText(){
		var resultCountText = jQuery(".woocommerce-result-count").text();
		if(windowWidth <= 600){
			resultCountText = resultCountText.replace("results", "");
			jQuery(".woocommerce-result-count").text(resultCountText);
		}
		else if(windowWidth > 600 && resultCountText.indexOf("results") === -1){
			jQuery(".woocommerce-result-count").text(resultCountText + " results");
		}	
	}
	changeResultsText();


	function swapHeaderImages(windowWidth){
		var newImage;
		if(windowWidth < 400){
			newImage = imagePath + imageGrab + "-mobile.jpg";
			jQuery(".storeHeaderImageHolder").css({'background-image':"url('" + newImage +  "')"});
		}
		else if(windowWidth >= 400 && windowWidth < 730){
			newImage = imagePath + imageGrab + "-mobileBig.jpg";
			jQuery(".storeHeaderImageHolder").css({'background-image':"url('" + newImage +  "')"});
		}
		else if(windowWidth >= 730 && windowWidth < 980){
			newImage = imagePath + imageGrab + "-mid.jpg";
			jQuery(".storeHeaderImageHolder").css({'background-image':"url('" + newImage +  "')"});
		}
		else{
			newImage = imagePath + imageGrab + ".jpg";
			jQuery(".storeHeaderImageHolder").css({'background-image':"url('" + newImage +  "')"});
		}
	}//end swapHeaderImages()


	//swapHeaderImages(windowWidth);




	///////////////////////////////make sure all images are the same height so the layout doesnt get fucked up
	function keepAspectRatio(){
		jQuery(".products li").each(function(e){
			var image = jQuery(this).find(".attachment-woocommerce_thumbnail"); 
			var magicHeightValue = 1.3333;
			var imageStartingHeight = image.height();
			var imageStartingWidth = image.width();
			var targetHeight = imageStartingWidth * magicHeightValue;

			//if(imageStartingHeight > targetHeight){ 
				image.css({"height":targetHeight - 5});
			//}
			//if it's way smaller, fix it too
			//if(imageStartingHeight + 20 < targetHeight){ 
				//image.css({"height":targetHeight - 5});
			//}
		});
		console.log("store function firing");
	}

	///////////////////////////////make sure all the info heights are good so the "add to cart" buttons line up
	function equalizeHeights(){
		jQuery( ".products").each(function() {
			var heightArray = new Array();
			var newHeight;
			var titleHeightArray = new Array();
			var newTitleHeight;
			jQuery(this).find("li").each(function() {
				//jQuery(this).find('.ast-loop-product__link').css({'background-color':'pink'});

				var titleHeight = jQuery(this).find('.ast-loop-product__link').height();
				//put all the title heights into an array and find the biggest height
				titleHeightArray.push(titleHeight); 
				newTitleHeight = Math.max.apply(Math,titleHeightArray);

			});//end li each()

			//after finding the biggest height, set it for each of this carousels mag titles
			jQuery(this).find(".ast-loop-product__link").css({"height":newTitleHeight});

		});//end products each()

	}//end equalizeHeights()

	equalizeHeights();




});//end all()



</script>






