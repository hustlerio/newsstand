jQuery(document).ready(function(){
	
	////////////////Cant change the mobile logo. WTF? Why doesn't the theme do this in the CMS?
	//////I think they want you to buy the pro version for mobile logo editing? 

	var hostname = window.location.hostname;

	//just get rid of all the bullshit Astra puts in at the start
	jQuery(".site-header-primary-section-left").empty();
	
	var desktopLogo ="https://" +  hostname + "/wp-content/themes/astra/assets/images/newsstandLogo.png";
	
	//create an <a> and put it into the div
	var logoLink = document.createElement('a');
	logoLink.href = 'https://woocomm.hustlernewsstand.com/';
	jQuery('.site-header-primary-section-left').append(logoLink);

	//create an <img> and put it into the just created <a>
	var img = jQuery('<img id="dynamic">'); //Equivalent: $(document.createElement('img'))
	img.attr('src', desktopLogo);
	img.appendTo('.site-header-primary-section-left a');
	jQuery('#dynamic').attr('title', "Hustler Newsstand");
	jQuery('#dynamic').attr('alt', "Hustler Newsstand");



	///////////////////////////////////////mobile search functionality/////////////////////////////////////////////////////
	jQuery(".mobileSearchButton").click(function(){
		jQuery(".mobileSearchOverlay").css({"display":"block"});
		jQuery(".mobileSearchOverlay").stop().animate({"opacity":1},"medium");
	});

	jQuery(".mobileSearchOverlay").click(function(e){
		if (e.target !== this){
			return;
		}
    	

		jQuery(".mobileSearchOverlay").stop().animate({"opacity":0},"medium",function(){
			jQuery(this).css({"display":"none"});
		});
	});



	///////////////////////////////styles the titles for the mags in carousels/////////////////////////////////////////////
	///loop through all the carousels h2s, split them in half and change their style
	jQuery(".eael-product-title h2").each(function(e){
		//get the h2 and split it into the title and the date
		var html = jQuery(this).html();
		var both = html.split("<br>");
		var title = both[0];
		var date = both[1];
		//console.log(date);
		//replace the html with our new version where we can style both parts separately
		var fixedStyle = "<span class='styledMagTitle'>" + title + "</span> <span class='styledMagDate'>" + date + "</span>";
		jQuery(this).html(fixedStyle);
	});




///////////////////////////////this controls ALL the dropdown menus in the magazine areas//////////////////////////////////////
//we only need the code once so i can store it here/////////
	jQuery( ".currentIssueMoreButton" ).click(function() {
	  jQuery(this).find( ".currentIssueDropdown" ).slideToggle( "fast", function() {
	    // Animation complete.
	  });
	  jQuery(this).find( "span" ).toggleClass('dropdownOpen');
	});

	jQuery( ".currentIssueDropdown" ).mouseleave(function() {
	  jQuery(this).slideToggle( "fast", function() {
	    // Animation complete.
	  });
	  jQuery(this).siblings( ".currentIssueMoreButton span" ).toggleClass('dropdownOpen');
	});



	///////////////this makes all the mags in 'current issue layout' carousels have the same height!!!!!!!////////////////////////
	////2 second delay to make sure we get the correct height for the calculation

	//////run setHeightForCurrentIssueCarousel() on resize
	var resizeTimer;
	jQuery(window).on('resize', function(e) {
	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {
	    setHeightForCurrentIssueCarousel();    
	  }, 200);
	});


	setTimeout(function(){
		setHeightForCurrentIssueCarousel();
	},2000);




/* had to go the long way. Finding the height of the container 
and subtracting from it wouldnt work on resize. 
So here we add up all the parts and pick the biggest height */
	function setHeightForCurrentIssueCarousel(){
		var heightArray = new Array();
		var newHeight;

		jQuery( ".currentIssueCarousel" ).each(function() {
				
				jQuery(this).find( ".eael-product-carousel" ).each(function() {
					//combine the heights of title, price etc
					var imageWrap = jQuery(this).find('.image-wrap').height();
					var eaelProductTitle = jQuery(this).find('.eael-product-title').height();
					var price = jQuery(this).find('.eael-product-price').height();
					var sweetSpotSpace = 30;
					var addedHeights = imageWrap + eaelProductTitle + price + sweetSpotSpace;
					
					//put them all into an array and find the biggest height
					heightArray.push(addedHeights); 
					newHeight = Math.max.apply(Math,heightArray);
				});
				//assign the new height to all the mags in that row
				jQuery(this).find(".eael-product-carousel").css({"height":newHeight});	
				//also new height to the container and some wooCommerce classes that were messing things up
				jQuery(this).css({"height":newHeight + 20});
				jQuery(this).find(".swiper-container-wrap").css({"height":newHeight + 20});
				jQuery(this).find(".eael-woo-product-carousel").css({"height":newHeight + 20});


				var magCoverAreaHeight = jQuery( ".currentIssueArea" ).height();
				if(magCoverAreaHeight < newHeight){
					jQuery( ".currentIssueArea" ).css({"height":newHeight});
				}
				else{
					jQuery( ".currentIssueArea" ).css({"height":'auto'});
				}



		});	
	}// end setHeightForCurrentIssueCarousel()




/////////////////////////////////////////////////////////////////////store page
	var $newdiv1 = $( "<div id='object1'>fuck off</div>");
	jQuery(".site-content").prepend( $newdiv1 );


















});