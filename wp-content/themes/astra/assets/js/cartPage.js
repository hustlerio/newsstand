jQuery(document).ready(function(){


	//when the + and - are clicked, add an animation showing that the price is updating
	//bc the default is hard to notice things are updating
	jQuery(".wc-block-components-quantity-selector__button, .wc-block-components-radio-control__input").click(function(){
		jQuery(".wc-block-components-totals-footer-item").css({
			"color":"#fff",
			"background-color":"#fff",
			"background-image":"url('/wp-content/themes/astra/assets/images/loading-spin.gif')",
			"background-position":"center center",
			"background-size":"auto 90%",
			"background-repeat":"no-repeat"

		});
	});


	//put things back to normal after the proce updates
	jQuery(".wc-block-formatted-money-amount").bind('DOMSubtreeModified', function(){
		jQuery(".wc-block-components-totals-footer-item").css({
			"color":"#333",
			"background-color":"#fff",
			"background-image":"url('/wp-content/themes/astra/assets/images/empty.gif')",
			"background-position":"center center",
			"background-size":"auto 90%",
			"background-repeat":"no-repeat"

		});	  
	});

});//end whole shabang()


