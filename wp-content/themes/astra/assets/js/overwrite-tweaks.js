jQuery.noConflict();
jQuery(document).ready(function(){
	
	////////////////Cant change the mobile logo. WTF? Why doesn't the theme do this in the CMS?
	//////I think they want you to buy the pro version for mobile logo editing? 

// setTimeout(function(){
// 	jQuery('.wcppec-checkout-buttons__separator').css({"display":"none"});
// 	console.log("paypal thing");
// },5000);
	



	var hostname = window.location.hostname;
	var URL = window.location.href;
	//just get rid of all the bullshit Astra puts in at the start
	jQuery(".site-header-primary-section-left").empty();
	
	var desktopLogo ="https://" +  hostname + "/wp-content/themes/astra/assets/images/newsstandLogo.png";
	
	//create an <a> and put it into the div
	var logoLink = document.createElement('a');
	logoLink.href = hostname;
	jQuery('.site-header-primary-section-left').append(logoLink);

	//create an <img> and put it into the just created <a>
	var img = jQuery('<img id="dynamic">'); //Equivalent: $(document.createElement('img'))
	img.attr('src', desktopLogo);
	img.appendTo('.site-header-primary-section-left a');
	jQuery('#dynamic').attr('title', "Hustler Newsstand");
	jQuery('#dynamic').attr('alt', "Hustler Newsstand");


	//make the clicked "Add to cart" button say "Added"
	jQuery('.ajax_add_to_cart').click(function(){
		jQuery(this).text("Added");
	});

	//make the clicked "Add to cart" button say "Added" on Product page
	jQuery('.single_add_to_cart_button').click(function(){
		jQuery(this).text("Added");
	});





////////////////////////////////////////////fix bug - make a products "added" button say "add to cart" if its removed in the side cart

	var sideCartText;

	/////establish the target text on mouseEnter() bc im not sure if you can get it on delete in the side cart
	jQuery('body').on('mouseenter', '.xoo-wsc-product', function() {
	    sideCartText = jQuery(this).find('.xoo-wsc-pname a').text();
	    sideCartText = sideCartText.replace(/ +/g, "");
	    sideCartText = sideCartText.toLowerCase();;
	    console.log("sideCartText: " + sideCartText);
	});

	/////////when you click on a delete button in the side cart
	jQuery('body').on('click', '.xoo-wsc-smr-del, .xoo-wsc-sm-right', function() {
	    //console.log("//////////////////////////////////////////////////////////CLICK: " + sideCartText);

	    //////////////////////////this affects the desktop carousel products
		jQuery(".products li").each(function(e){
			//get the title and subtitle, remove all space and make lowercase
			var title = jQuery(this).find(".styledMagTitle").text();
			title = title.replace(/ +/g, "");
			var subTitle = jQuery(this).find(".styledMagDate").text();
			subTitle = subTitle.replace(/ +/g, "");
			var productText = (title + subTitle).toLowerCase();;
		
			if(sideCartText == productText){
				//jQuery(this).css({"background-color":"orange"});
				//console.log("mobileproductText: " + productText);
				//change the button text back to "add to Cart"
				jQuery(this).find(".add_to_cart_button").text("Add to Cart");
				//remove the "added" class so the "check" graphic is removed
				jQuery(this).find(".add_to_cart_button").removeClass("added");
			}
		});

		/////////////////////////this affects the new mobile carousel products
		jQuery(".owl-item").each(function(e){
			//get the title and subtitle, remove all space and make lowercase
			var title = jQuery(this).find(".styledMagTitle").text();
			title = title.replace(/ +/g, "");
			var subTitle = jQuery(this).find(".styledMagDate").text();
			subTitle = subTitle.replace(/ +/g, "");
			var productText = (title + subTitle).toLowerCase();;
			
			if(sideCartText == productText){
				//change the button text back to "add to Cart"
				jQuery(this).find(".wpb_cart_button a").text("Add to Cart");
				//remove the "added" class so the "check" graphic is removed
				jQuery(this).find(".wpb_cart_button a").removeClass("added");
			}
		});


		////////////////////////////this is the product page MAIN Product version. no loop since theres only one
		////but only run on the product page
		if( jQuery(".desktopTitle") != undefined && jQuery(".desktopSubTitle") != undefined ){

			var singleProductTitle = jQuery(".desktopTitle").text();
			singleProductTitle = singleProductTitle.replace(/ +/g, "");
			var singleProductSubTitle = jQuery(".desktopSubTitle").text();
			singleProductSubTitle = singleProductSubTitle.replace(/ +/g, "");
			var singleProductText = (singleProductTitle + singleProductSubTitle).toLowerCase();;
			
			if(sideCartText == singleProductText){
				console.log("/////////product page: " + singleProductText);
				//change the button text back to "add to Cart"
				jQuery(".single_add_to_cart_button").text("Add to Cart");
				//remove the "added" class so the "check" graphic is removed
				jQuery(".single_add_to_cart_button").removeClass("added");
			}

		}//end if(undefined)


	});	//end click( delete button in side cart)






////////////////////////////////////////////END "added" vs "add to cart" bug/////////////////////////////////////////////////
//






	///////////////////////////////// edit the Main desktop menu so the menus appear right away 
	//jQuery('#menu-1-511f3f5e').css({"background":"orange"});
	//make 'specials' and 'more' menus positioned so the dont go off the right side of the screen
	jQuery( "#menu-1-511f3f5e li:nth-child(5)" ).find("ul").css({"right":"-60px"});
	jQuery( "#menu-1-511f3f5e li:nth-child(6)" ).find("ul").css({"right":"0"});

	jQuery('#menu-1-511f3f5e li').mouseover(function(){
		//get ID for each links <ul>
		var subMenuID = jQuery(this).find("ul").attr("id");
		//open each menu immeadiately and bypass that shitty pause
		jQuery(this).find("ul").css({"display":"block","width":"auto"});
		//check for 'specials' and 'more' buttons so menus can be positioned so they wont go off screen
		

	});//end mouseOver()

	jQuery('#menu-1-511f3f5e li').mouseout(function(){
		jQuery(this).find("ul").css({"display":"none"});
	});
	//////////////////////////////////////////////////////////////END desktop menu edit to soeed up the menus





	///////////////////////////////////////mobile search functionality/////////////////////////////////////////////////////
	jQuery(".mobileSearchButton").click(function(){
		jQuery(".mobileSearchOverlay").css({"display":"block"});
		jQuery(".mobileSearchOverlay").stop().animate({"opacity":1},"medium");
	});

	jQuery(".mobileSearchOverlay, .mobileSearchCloseWrapper, .mobileSearchCloseWrapper span, .mobileSearchCloseWrapper p").click(function(e){
		if (e.target !== this){
			return;
		}
    	
		jQuery(".mobileSearchOverlay").stop().animate({"opacity":0},"medium",function(){
			jQuery(this).css({"display":"none"});
		});
	});


	/////////////////styles the woocommerce search button
	jQuery(".woocommerce-product-search button").text('');
	


	///////////////////////////////styles the titles for the mags in carousels/////////////////////////////////////////////
	///loop through all the carousels h2s, split them in half and change their style
	//h3.pro_title is for the new sliders we're using for the mobile carousels
	jQuery(".eael-product-title h2, h3.pro_title, .woocommerce-loop-product__title").each(function(e){
		//get the h2 and split it into the title and the date
		var html = jQuery(this).html();
		var both = html.split("<br>");
		var title = both[0];
		var date = both[1];
		//console.log(date);
		//replace the html with our new version where we can style both parts separately
		var fixedStyle = "<span class='styledMagTitle'>" + title + "</span> <span class='styledMagDate'>" + date + "</span>";
		jQuery(this).html(fixedStyle);
	});





///////////////////////////////this controls ALL the dropdown menus in the magazine areas//////////////////////////////////////
//we only need the code once so i can store it here/////////
	jQuery( ".currentIssueMoreButton" ).click(function() {
	  jQuery(this).find( ".currentIssueDropdown" ).slideToggle( "fast", function() {
	    // Animation complete.
	  });
	  jQuery(this).find( "span" ).toggleClass('dropdownOpen');
	});


	jQuery( ".currentIssueMoreButton" ).mouseleave(function() {
	  jQuery(".currentIssueDropdown").slideUp( "fast", function() {
	    // Animation complete.
	  });
	  jQuery(this).find( "span" ).removeClass('dropdownOpen');
	});

	jQuery( ".currentIssueDropdown" ).mouseleave(function() {
	  jQuery(this).slideToggle( "fast", function() {
	    // Animation complete.
	  });
	  jQuery(this).siblings( ".currentIssueMoreButton span" ).toggleClass('dropdownOpen');
	});

	//little hack for a weird bug where the dropdown would go up and down when a button is clicked
	//since the buttons take you to another page, just destroy the menu on click()
	jQuery( ".currentIssueDropdown p" ).click(function() {
		jQuery( ".currentIssueDropdown" ).css({"display":"none"});
		jQuery( ".currentIssueDropdown" ).remove();
	});
	



	function featuredMagload(){
		
		//featuredMagWidget


		jQuery(".currentMagFeaturedContainer").each(function(){
			var imagesLoaded = 0;
			var oneTimeCounter = 0;

			var thisContainer = jQuery(this);
			var thisWidget = jQuery(this).find(".featuredMagWidget");

			jQuery(thisWidget).find("img").css({"opacity":0});
			
			jQuery(thisWidget).find( "img" ).each(function(){
				
				var cloneCounter = 0;
				imagesLoaded++;
				var thisCurrentImage = jQuery(this);

				if(imagesLoaded >= 1){
					
					var intervalCount = 0;

					var interval = setInterval(function(){
						intervalCount++;
						//console.log("intervalCount: " + intervalCount);
					    var lauren = jQuery(thisCurrentImage).attr("src");
					    //if the imagepath has "/uploads/", kill the interval
					    if(lauren.indexOf("/uploads/") != -1 ){
					        clearInterval(interval);
					    }

					    //do interval code here..
					    //create the new image name
					    var newImageName = jQuery(thisCurrentImage).attr("src").replace("-300x400", "");

					    //set the new image name
					    jQuery(thisCurrentImage).attr("src",newImageName);

					    //if the new image has been applied, render
					    if(newImageName.indexOf("-300x400") === -1){
					    	if(oneTimeCounter === 0){
					    		jQuery(thisCurrentImage).css({"opacity":1},"slow");
					    		
					    		jQuery(thisWidget).stop().animate({"opacity":1});
					    	
								jQuery(thisContainer).find(".astra-shop-thumbnail-wrap").append("<div class='featuredMagOverlay' style='opacity:0'></div>");
								jQuery(thisContainer).find( ".ast-loop-product__link" ).clone().addClass("clonedProductInfo").appendTo( jQuery(thisContainer).find(".featuredMagOverlay") );
								jQuery(thisContainer).find( ".ast-loop-product__link:last" ).remove();
								jQuery(thisContainer).find( ".price" ).clone().addClass("clonedProductInfo").appendTo( jQuery(thisContainer).find(".featuredMagOverlay") );
								jQuery(thisContainer).find( ".price:last" ).remove();

								setTimeout(function(){
									jQuery(thisContainer).find(".featuredMagOverlay").animate({"opacity":1});
								},500);


								oneTimeCounter++;
					    	}
					    	
					    }//end if()

					}, 200); //setInterval

				}//end if()
				
			});//end Each()
			
			//we set the height inline to prevent shifting at the start. THEN set height to auto
			//jQuery(".currentMagFeaturedContainer").css({"height":"auto"});
			
			//when the main image is loaded, remove the sliders bg spinner
			jQuery(".currentMagSliderHalf .elementor-widget-wrap").css({"background-image":"url('/wp-content/themes/astra/assets/images/empty.gif')"});
	    	


		});

	}//end featuredMagload()


		////////Featured Mag area Main pic - this replaces the -300x400 project version with the big one
		function grabBigMagImage(){
			jQuery(".currentMagFeaturedContainer").each(function(e){
				var image = jQuery(this).find("img");
				var newImageName = image.attr("src").replace("-300x400", "");
				image.attr("src",newImageName);
			});
		}


	//////////////////////////////////////////////////////BIG FEATURED MAG AREA/////////////////////////////////////////////
	////////Featured Mag area Main pic - builds the overlay area and clones the data into it
	
	function featuredMagCreate(){

		jQuery(".currentMagFeaturedContainer").each(function(e){
			jQuery(this).find(".astra-shop-thumbnail-wrap").append("<div class='featuredMagOverlay'></div>");
			jQuery(this).find( ".ast-loop-product__link" ).clone().addClass("clonedProductInfo").appendTo( jQuery(this).find(".featuredMagOverlay") );
			jQuery(this).find( ".ast-loop-product__link:last" ).remove();
			jQuery(this).find( ".price" ).clone().addClass("clonedProductInfo").appendTo( jQuery(this).find(".featuredMagOverlay") );
			jQuery(this).find( ".price:last" ).remove();
		});
		

		jQuery(".currentMagFeaturedContainer").prepend("<div class='currentIssueBanner'>Current Issue</div>");
	}

	
	//featuredMagCreate();






	featuredMagload();
	//grabBigMagImage();NOT USING ANYMORE


	///////////////this makes all the mags in 'current issue layout' carousels have the same height!!!!!!!////////////////////////
	///////////////this makes all the mags in 'current issue layout' carousels have the same height!!!!!!!////////////////////////
	///////////////this makes all the mags in 'current issue layout' carousels have the same height!!!!!!!////////////////////////
	////2 second delay to make sure we get the correct height for the calculation

	//////run setHeightForCurrentIssueCarousel() on resize
	var resizeTimer;
	jQuery(window).on('resize', function(e) {
	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {
	    //setHeightForCurrentIssueCarousel(); 
	   mobileCarouselHeightEqualize();
	    detectOrientationChange();  
	  }, 200);
	});


	jQuery(document).scroll(function() {
	    mobileCarouselHeightEqualize();
	    //jQuery(window).unbind('scroll');   
	});


	function detectOrientationChange(){
		if(window.innerHeight > window.innerWidth){
    		
    		setTimeout(function(){
    			mobileCarouselHeightEqualize();
    		},400);
		}
		if(window.innerWidth > window.innerHeight){
			
    		setTimeout(function(){
    			mobileCarouselHeightEqualize();
    		},400); 
		}
	}





	//NEW better mobile carousel using Owl SLider and shortcode
	function mobileCarouselHeightEqualize(){
		jQuery( ".mobileSlider").each(function() {
			var heightArray = new Array();
			var newHeight;
			var titleHeightArray = new Array();
			var newTitleHeight;

			jQuery(this).find(".owl-item").each(function(){
				var titleHeight = jQuery(this).find('.pro_title').height();
				titleHeightArray.push(titleHeight); 
				newTitleHeight = Math.max.apply(Math,titleHeightArray);				
			});//end li each()

			jQuery(this).find(".pro_title").css({"height":newTitleHeight});

		});
	}

	mobileCarouselHeightEqualize();






	////////////////add dvd olog on product page if theres a DVD
	var dvdAttributeText = jQuery(".woocommerce-product-attributes-item__label").text();
	var dvdAttributeText = dvdAttributeText.toLowerCase();
	var infoCell = jQuery(".magInfoCell> .elementor-widget-wrap");

	if(dvdAttributeText != null && dvdAttributeText != undefined && dvdAttributeText != ""){
		if( dvdAttributeText.indexOf("comes with dvd") != -1 ){
			//console.log(dvdAttributeText);
			//infoCell.addClass("bonusDVDcornerDisplay");
			//once we check for the "comes with dvd" text, remove it to be replaced with an image
			//jQuery(".magCoverCell .elementor-widget-wl-single-product-image").prepend("<div class='bonusDVDBanner'>BONUS DVD INSIDE</div>");
			//jQuery(".woocommerce-product-gallery__image").prepend("<div class='bonusDVDBannerBottom'></div>");
			jQuery(".mobileProductImageCell .woocommerce-product-gallery__image").prepend("<div class='bonusDVDBannerBottom'></div>");

			//jQuery(".woocommerce-product-attributes-item__label").text(" ");
		}
	}
	else{
		//console.log("has no DVD");
	}	

	



	//////////////HACK an "add to cart" button at the bottom of the carousel products
	jQuery(".eael-product-carousel").each(function(e){
		//add a link to each image by taking it from the shitty hover thing
		var link = jQuery(this).find(".view-details a").attr("href");
		var image = jQuery(this).find(".attachment-thumbnail"); 
		jQuery(image).wrap('<a href="'+ link +'"  ></a>');

		//add an "add to cart" button using the link from the shitty hover thing
		// var addToCartLink = jQuery(this).find(".add-to-cart a").attr("href");
		// var addToCartElement = jQuery(this).find(".add-to-cart");

		//jQuery(this).append("<a href='"+ addToCartLink +"' class=' myAddToCartButton'>hey now</a>");
		//jQuery(this).append("<div class='myTarget'>hey now</div>");

		//console.log(addToCartElement);

		jQuery(this).find( ".add-to-cart a" ).clone().addClass('myNewAddToCartLink').appendTo( jQuery(this) );
		
	});

	jQuery(document).on('click', '.myNewAddToCartLink', function(event) { 
	    console.log("hey now its brenda for a photoshoot");
	    jQuery(this).text("Added");
	});



	//////////////keep all mag covers in the carousels the same aspect ratio/////////////////////////////////////
	///keepAspectRatio() loops through all images in all product carousels
	///it compares their heights to a calculated expected height, based on the width of the mag
	///this works bc all mags have the same width in this layout. Pure... Genius ;)
	function keepAspectRatio(){
		jQuery(".eael-product-carousel, .eael-woo-product-carousel").each(function(e){
			var image = jQuery(this).find(".attachment-thumbnail"); 
			var magicHeightValue = 1.3333;
			var imageStartingHeight = image.height();
			var imageStartingWidth = image.width();
			var targetHeight = imageStartingWidth * magicHeightValue;

			if(imageStartingHeight > targetHeight){ 
				image.css({"height":targetHeight - 5});
			}
			//if it's way smaller, fix it too
			if(imageStartingHeight + 20 < targetHeight){ 
				image.css({"height":targetHeight - 5});
			}
		});
	}

	//keep the image aspect ratio for the big main featured mags
	function keepFeaturedAspectRatio(){
		jQuery(".featuredMagWidget").each(function(e){
			var image = jQuery(this).find(".attachment-woocommerce_thumbnail"); 
			var magicHeightValue = 1.3333;
			var imageStartingHeight = image.height();
			var imageStartingWidth = image.width();
			var targetHeight = imageStartingWidth * magicHeightValue;

			if(imageStartingHeight > targetHeight){ 
				image.css({"height":targetHeight - 5});
			}
			//if it's way smaller, fix it too
			if(imageStartingHeight + 20 < targetHeight){ 
				image.css({"height":targetHeight - 5});
			}
		});
	}


keepFeaturedAspectRatio();

	//when the page loads, keepAspectRatio()
	//keepAspectRatio();

	//because of lazyLoad(), keepAspectRatio() with a slight delay
	// setTimeout(function(){keepAspectRatio();},1000);
	// setTimeout(function(){keepAspectRatio();},2000);
	// setTimeout(function(){keepAspectRatio();},3000);	
	// setTimeout(function(){keepAspectRatio();},4000);
	// setTimeout(function(){keepAspectRatio();},5000);
	// setTimeout(function(){keepAspectRatio();},6000);


	var resizeTimerRatio;
	jQuery(window).on('resize', function(e) {
	  clearTimeout(resizeTimerRatio);
	  resizeTimerRatio = setTimeout(function() {
	   	//keepAspectRatio();
	    keepFeaturedAspectRatio();  
	  }, 200);
	});


	//some images may be hidden in the carousel, so keepAspectRatio() when you go right or left
	jQuery(".swiper-button-prev , .swiper-button-next ").click(function(e){
		//keepAspectRatio();
	});

	//because of lazyLoad(), keepAspectRatio() after any scroll
	jQuery(document).scroll(function() {
	    keepAspectRatio();
	    keepFeaturedAspectRatio();
	    //grabBigMagImage();....NOT USING ANYMORE
	    //jQuery(window).unbind('scroll');   
	});
	/////////////////////////////////////////END carousel aspect ratio/////////////////////////////////////






	///////////////////////// new Cart Plugin - mimic the click and cart count for the top nav icon ////////////////
	jQuery(document).on('click', '.newCart', function(event) { 
	    event.preventDefault(); 
	    jQuery(".xoo-wsc-basket").click(); 
	});

	////on every click(), check the item count in the cart plugin, and put the value in the top Nav version
	jQuery(document).click(function(){
		copyTheItemCount();
	});

	function copyTheItemCount(){
		var currentCartItemCount = jQuery(".xoo-wsc-items-count").text();
		jQuery(".itemCountCopy").text(currentCartItemCount);	
	}

	copyTheItemCount();



	//add buttons for the mobile owl slider on the products page
	jQuery(".mobileRelatedTitles").prepend("<button class='arrow-next'></button>");	
	jQuery(".mobileRelatedTitles").prepend("<button class='arrow-prev'></button>");	

	jQuery(document).on('click', '.arrow-prev', function(event) { 
	    event.preventDefault(); 
	    jQuery(".prev").click(); 
	});
	jQuery(document).on('click', '.arrow-next', function(event) { 
	    event.preventDefault(); 
	    jQuery(".next").click(); 
	});





	//////////////////////////////////set cart page coupon to default to open
	setTimeout(function() {
		jQuery(".wc-block-components-panel__button").click(); 
	},50);

	//<abbr class='required' title='required'>*</abbr>
	//jQuery( "#billing_phone_field" ).find('label').html("Phone <abbr class='required' title='required'>*</abbr>");


	//remove the annoying <p> in the coupon code area bc CSS couldnt target the little bastard. 
	jQuery( ".checkout_coupon p" ).first().css({"display":"none"});



	//////Uncheck the credit card radio button on the checkout page
	setTimeout(function(){
		jQuery('#payment_method_ccbill').prop("checked", false);
		//console.log(jQuery('.wc_payment_method payment_method_ccbill'));	
	},1000);


	///////change wishlist "return to shop" button
	jQuery( ".tinv-wishlist" ).find(".wc-backward").text("Continue Shopping");
	jQuery( ".tinv-wishlist" ).find(".wc-backward").attr("href","/");


	///////remove (not Nick, Logout) from the account page
	// var brenda = jQuery( ".woocommerce-MyAccount-content" ).find( "p" ).first().text();
	// jQuery( ".woocommerce-MyAccount-content" ).find( "p" ).first().css( "background-color", "red" );

	// var lauren = brenda.split('(')[0];
	// jQuery( ".woocommerce-MyAccount-content" ).find( "p" ).first().text(lauren)
	// console.log(lauren);






});////end whole shabang