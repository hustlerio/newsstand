jQuery(document).ready(function(){
	
	////////////////Cant change the mobile logo. WTF? Why doesn't the theme do this in the CMS?
	//////I think they want you to buy the pro version for mobile logo editing? 

	var hostname = window.location.hostname;
	var URL = window.location.href;
	//just get rid of all the bullshit Astra puts in at the start
	jQuery(".site-header-primary-section-left").empty();
	
	var desktopLogo ="https://" +  hostname + "/wp-content/themes/astra/assets/images/newsstandLogo.png";
	
	//create an <a> and put it into the div
	var logoLink = document.createElement('a');
	logoLink.href = 'https://woocomm.hustlernewsstand.com/';
	jQuery('.site-header-primary-section-left').append(logoLink);

	//create an <img> and put it into the just created <a>
	var img = jQuery('<img id="dynamic">'); //Equivalent: $(document.createElement('img'))
	img.attr('src', desktopLogo);
	img.appendTo('.site-header-primary-section-left a');
	jQuery('#dynamic').attr('title', "Hustler Newsstand");
	jQuery('#dynamic').attr('alt', "Hustler Newsstand");


	//make the clicked "Add to cart" button say "Added"
	jQuery('.ajax_add_to_cart').click(function(){
		jQuery(this).text("Added");
	});

	//make the clicked "Add to cart" button say "Added" on Product page
	jQuery('.single_add_to_cart_button').click(function(){
		jQuery(this).text("Added");
	});


	///////////////////////////////////////mobile search functionality/////////////////////////////////////////////////////
	jQuery(".mobileSearchButton").click(function(){
		jQuery(".mobileSearchOverlay").css({"display":"block"});
		jQuery(".mobileSearchOverlay").stop().animate({"opacity":1},"medium");
	});

	jQuery(".mobileSearchOverlay, .mobileSearchCloseWrapper, .mobileSearchCloseWrapper span, .mobileSearchCloseWrapper p").click(function(e){
		if (e.target !== this){
			return;
		}
    	
		jQuery(".mobileSearchOverlay").stop().animate({"opacity":0},"medium",function(){
			jQuery(this).css({"display":"none"});
		});
	});


	/////////////////styles the woocommerce search button
	jQuery(".woocommerce-product-search button").text('');
	


	///////////////////////////////styles the titles for the mags in carousels/////////////////////////////////////////////
	///loop through all the carousels h2s, split them in half and change their style
	jQuery(".eael-product-title h2").each(function(e){
		//get the h2 and split it into the title and the date
		var html = jQuery(this).html();
		var both = html.split("<br>");
		var title = both[0];
		var date = both[1];
		//console.log(date);
		//replace the html with our new version where we can style both parts separately
		var fixedStyle = "<span class='styledMagTitle'>" + title + "</span> <span class='styledMagDate'>" + date + "</span>";
		jQuery(this).html(fixedStyle);
	});



	/////////////////////////SPLIT titles like "ALL BUSHVolume #12" and "ALL BUSHIssue #12" on PRODUCT PAGE////////////////////////////////

	//////split the titles in half accordingly and return the results, to be styled in later functions
	function checkAllTitles(html){
		var both;
		var subTitle = "";
		if(html.indexOf("volume") != -1 ){
			both = html.split("volume");
			subTitle = "Volume " + both[1].toUpperCase();
			return {'title':both[0], 'subTitle':subTitle}
		}
		else if(html.indexOf("issue") != -1  ){
			both = html.split("issue");
			subTitle = "Issue " + both[1].toUpperCase();
			return {'title':both[0], 'subTitle':subTitle}
		}
		else if(html.indexOf("anniversary") != -1  ){
			both = html.split("anniversary");
			subTitle = "Anniversary " + both[1].toUpperCase();
		}
		else if(html.indexOf("january") != -1  ){
			both = html.split("january");
			subTitle = "January " + both[1].toUpperCase();
		}
		else if(html.indexOf("february") != -1  ){
			both = html.split("february");
			subTitle = "February " + both[1].toUpperCase();
		}
		else if(html.indexOf("march") != -1  ){
			both = html.split("march");
			subTitle = "March " + both[1].toUpperCase();
		}
		else if(html.indexOf("april") != -1  ){
			both = html.split("april");
			subTitle = "April " + both[1].toUpperCase();
		}
		else if(html.indexOf("may") != -1  ){
			both = html.split("may");
			subTitle = "May " + both[1].toUpperCase();
		}
		else if(html.indexOf("june") != -1  ){
			both = html.split("june");
			subTitle = "June " + both[1].toUpperCase();
		}
		else if(html.indexOf("july") != -1  ){
			both = html.split("july");
			subTitle = "July " + both[1].toUpperCase();
		}
		else if(html.indexOf("august") != -1  ){
			both = html.split("august");
			subTitle = "August " + both[1].toUpperCase();
		}
		else if(html.indexOf("september") != -1  ){
			both = html.split("september");
			subTitle = "September " + both[1].toUpperCase();
		}
		else if(html.indexOf("october") != -1  ){
			both = html.split("october");
			subTitle = "October " + both[1].toUpperCase();
		}
		else if(html.indexOf("november") != -1  ){
			both = html.split("november");
			subTitle = "November " + both[1].toUpperCase();
		}
		else if(html.indexOf("december") != -1  ){
			both = html.split("december");
			subTitle = "December " + both[1].toUpperCase();
		}
		else if(html.indexOf("best of") != -1  ){
			both = html.split("best of");
			subTitle = "Best Of " + both[1].toUpperCase();
		}
		else if(html.indexOf("#") != -1  ){
			both = html.split("#");
			subTitle = "# " + both[1].toUpperCase();
		}	
		else{
			return {'title':html, 'subTitle':''}

		}	
		return {'title':both[0], 'subTitle':subTitle}

	}



	//////style the product pages title
	function productPageTitleSplit() {
		//console.log("productPageTitleSplit() firing");
		var html = jQuery(".entry-title").html();
		if(html == undefined){return}
		html = html.toLowerCase();
		var finish = checkAllTitles(html);
		var title = finish.title.toUpperCase();
		var subTitle = finish.subTitle.toLowerCase();
		jQuery(".entry-title").html("<span class='productPageTitle'>" + title + "</span> <span class='productPageSubTitle'>" + subTitle + "</span>");
	}

	//productPageTitleSplit();

	//////style the woocommerce product items titles
	function woocommerceProductTitle() {
		jQuery(".woocommerce-loop-product__title").each(function(e){
			var html = jQuery(this).html();
			if(html == undefined){return}
			html = html.toLowerCase();
			var finish = checkAllTitles(html);
			var title = finish.title.toUpperCase();
			var subTitle = finish.subTitle;
			jQuery(this).html("<span class='productMagTitle'>" + title + "</span> <span class='productMagSubTitle'>" + subTitle + "</span>");
		});

	}
	woocommerceProductTitle();


	function productPageBreadCrumbSplit(){
		var htmlGrab = jQuery(".woocommerce-breadcrumb").html();
		
		if(htmlGrab != undefined && htmlGrab != "" && htmlGrab != null){

			htmlGrab = htmlGrab.toLowerCase();
			//get everything before last /
			var firstPart = htmlGrab.substr(0, htmlGrab.lastIndexOf("/"));
			//get everything after last /
			var lastSlash = htmlGrab.lastIndexOf('/');
			var titleAfterSlash = htmlGrab.substring(lastSlash + 1);
			//run our function() on everything after last /
			var finish = checkAllTitles(titleAfterSlash);
			//checkAllTitles() returns a title and subtitle
			var newTitle = finish.title.toUpperCase();
			newTitle = newTitle.replace("&NBSP;", "");
			newTitle = newTitle.replace("&nbsp;", "");
			var subtitle = finish.subTitle;
			//rebuild breadcrumb HTML
			var newBreadcrumbHTML = firstPart + "/" + newTitle + " " + subtitle;

			//console.log(newTitle);

			jQuery(".woocommerce-breadcrumb").html(newBreadcrumbHTML);
			//jQuery(".woocommerce-breadcrumb").css({"text-transform":"uppercase"});
		}

	}

	productPageBreadCrumbSplit();



///////////////////////////////this controls ALL the dropdown menus in the magazine areas//////////////////////////////////////
//we only need the code once so i can store it here/////////
	jQuery( ".currentIssueMoreButton" ).click(function() {
	  jQuery(this).find( ".currentIssueDropdown" ).slideToggle( "fast", function() {
	    // Animation complete.
	  });
	  jQuery(this).find( "span" ).toggleClass('dropdownOpen');
	});

	jQuery( ".currentIssueDropdown" ).mouseleave(function() {
	  jQuery(this).slideToggle( "fast", function() {
	    // Animation complete.
	  });
	  jQuery(this).siblings( ".currentIssueMoreButton span" ).toggleClass('dropdownOpen');
	});

	//little hack for a weird bug where the dropdown would go up and down when a button is clicked
	//since the buttons take you to another page, just destroy the menu on click()
	jQuery( ".currentIssueDropdown p" ).click(function() {
		jQuery( ".currentIssueDropdown" ).css({"display":"none"});
		jQuery( ".currentIssueDropdown" ).remove();
	});
	



	// NOT USING ANYMORE
	// function homepageBackIssuesTop(){
	// 	//on desktop, start at this height
	// 	jQuery(".backIssueHeader-container" ).css({"height":"292px"});
	// 	var count = 0;	
	// 	var big3Array = new Array();
	// 	big3Array[0] = jQuery('<img/>').attr('src', '/wp-content/themes/astra/assets/images/backissue-hustler-bg.jpg');
	// 	big3Array[1] = jQuery('<img/>').attr('src', '/wp-content/themes/astra/assets/images/backissue-bl-bg.jpg');
	// 	big3Array[2] = jQuery('<img/>').attr('src', '/wp-content/themes/astra/assets/images/backissue-taboo-bg.jpg');

	// 	for (var i = 0; i < big3Array.length; i++) {
	// 	    big3Array[i].on('load', function() {
	// 	   		//console.log("loaded");
	// 	    	count++;
	// 	    	if(count >= big3Array.length){
	// 	    		//console.log("done");
	// 	    		jQuery(".backIssueHeader-container" ).css({"height":"auto"});
	// 	    		jQuery(".backIssueHeader-container" ).css({"background-image":"url()"});
	// 	    		jQuery(".backIssueHeader-magazine" ).stop().delay(100).animate({"opacity":1},200);
	// 	    	}
	// 	    });	
	// 	}
	// }
	//homepageBackIssuesTop();









	////////this is a lazyload hack to hide the first "featured magazine" area to avoid all that ugly content shifting 
	//NOT USING ANYMORE
	// var visibleImagesAtStart = 4;
	// var imageCount = 0;
	// jQuery(".currentIssueCarousel").first().find( "img" ).each(function(){
	// 	jQuery(this).load(function(){
	// 		imageCount++;
	//         //console.log("visibleImagesAtStart: " + visibleImagesAtStart + " | " + imageCount);
	// 	    if(imageCount >= visibleImagesAtStart){
	// 	    	//console.log("DONE");
	// 	    	jQuery(".featuredMagAreaContainer, .currentMagFeaturedContainer" ).css({"height":"auto"});
	// 	    	jQuery(".featuredMagAreaContainer, .currentMagFeaturedContainer" ).stop().delay(200).animate({"opacity":1},200);
	// 	    	setHeightForCurrentIssueCarousel();
	// 	    	///this is the wrapper of the whole thing. The BG spinner is set in elementor
	// 	    	jQuery(".featuredMagAreaWrapper" ).css({"background-image":"url()"});
	// 	    }
	//     });
	// });


	function featuredMagload(){
		
		//featuredMagWidget


		jQuery(".currentMagFeaturedContainer").each(function(){
			var imagesLoaded = 0;
			var oneTimeCounter = 0;

			var thisContainer = jQuery(this);
			var thisWidget = jQuery(this).find(".featuredMagWidget");

			jQuery(thisWidget).find("img").css({"opacity":0});
			
			jQuery(thisWidget).find( "img" ).each(function(){
				
				var cloneCounter = 0;
				imagesLoaded++;
				var thisCurrentImage = jQuery(this);

				if(imagesLoaded >= 1){
					
					var intervalCount = 0;

					var interval = setInterval(function(){
						intervalCount++;
						//console.log("intervalCount: " + intervalCount);
					    var lauren = jQuery(thisCurrentImage).attr("src");
					    //if the imagepath has "/uploads/", kill the interval
					    if(lauren.indexOf("/uploads/") != -1 ){
					        clearInterval(interval);
					    }

					    //do interval code here..
					    //create the new image name
					    var newImageName = jQuery(thisCurrentImage).attr("src").replace("-300x400", "");

					    //set the new image name
					    jQuery(thisCurrentImage).attr("src",newImageName);

					    //if the new image has been applied, render
					    if(newImageName.indexOf("-300x400") === -1){
					    	if(oneTimeCounter === 0){
					    		jQuery(thisCurrentImage).css({"opacity":1},"slow");
					    		
					    		jQuery(thisWidget).stop().animate({"opacity":1});
					    	
								jQuery(thisContainer).find(".astra-shop-thumbnail-wrap").append("<div class='featuredMagOverlay' style='opacity:0'></div>");
								jQuery(thisContainer).find( ".ast-loop-product__link" ).clone().addClass("clonedProductInfo").appendTo( jQuery(thisContainer).find(".featuredMagOverlay") );
								jQuery(thisContainer).find( ".ast-loop-product__link:last" ).remove();
								jQuery(thisContainer).find( ".price" ).clone().addClass("clonedProductInfo").appendTo( jQuery(thisContainer).find(".featuredMagOverlay") );
								jQuery(thisContainer).find( ".price:last" ).remove();

								setTimeout(function(){
									jQuery(thisContainer).find(".featuredMagOverlay").animate({"opacity":1});
								},500);


								oneTimeCounter++;
					    	}
					    	
					    }//end if()

					}, 200); //setInterval

				}//end if()
				
			});//end Each()
			
			//we set the height inline to prevent shifting at the start. THEN set height to auto
			//jQuery(".currentMagFeaturedContainer").css({"height":"auto"});
			
			//when the main image is loaded, remove the sliders bg spinner
			jQuery(".currentMagSliderHalf .elementor-widget-wrap").css({"background-image":"url('/wp-content/themes/astra/assets/images/empty.gif')"});
	    	


		});

	}//end featuredMagload()


		////////Featured Mag area Main pic - this replaces the -300x400 project version with the big one
		function grabBigMagImage(){
			jQuery(".currentMagFeaturedContainer").each(function(e){
				var image = jQuery(this).find("img");
				var newImageName = image.attr("src").replace("-300x400", "");
				image.attr("src",newImageName);
			});
		}


	//////////////////////////////////////////////////////BIG FEATURED MAG AREA/////////////////////////////////////////////
	////////Featured Mag area Main pic - builds the overlay area and clones the data into it
	
	function featuredMagCreate(){

		jQuery(".currentMagFeaturedContainer").each(function(e){
			jQuery(this).find(".astra-shop-thumbnail-wrap").append("<div class='featuredMagOverlay'></div>");
			jQuery(this).find( ".ast-loop-product__link" ).clone().addClass("clonedProductInfo").appendTo( jQuery(this).find(".featuredMagOverlay") );
			jQuery(this).find( ".ast-loop-product__link:last" ).remove();
			jQuery(this).find( ".price" ).clone().addClass("clonedProductInfo").appendTo( jQuery(this).find(".featuredMagOverlay") );
			jQuery(this).find( ".price:last" ).remove();
		});
		

		jQuery(".currentMagFeaturedContainer").prepend("<div class='currentIssueBanner'>Current Issue</div>");
	}

	
	//featuredMagCreate();






	featuredMagload();
	//grabBigMagImage();NOT USING ANYMORE


	///////////////this makes all the mags in 'current issue layout' carousels have the same height!!!!!!!////////////////////////
	///////////////this makes all the mags in 'current issue layout' carousels have the same height!!!!!!!////////////////////////
	///////////////this makes all the mags in 'current issue layout' carousels have the same height!!!!!!!////////////////////////
	////2 second delay to make sure we get the correct height for the calculation

	//////run setHeightForCurrentIssueCarousel() on resize
	var resizeTimer;
	jQuery(window).on('resize', function(e) {
	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {
	    //setHeightForCurrentIssueCarousel(); 
	    equalizeHeightNewCarousel(); 
	    detectOrientationChange();  
	  }, 200);
	});


	jQuery(document).scroll(function() {
	    equalizeHeightNewCarousel();
	    //jQuery(window).unbind('scroll');   
	});


	function detectOrientationChange(){
		if(window.innerHeight > window.innerWidth){
    		equalizeHeightNewCarousel(); 
    		setTimeout(function(){
    			equalizeHeightNewCarousel(); 
    		},400);
		}
		if(window.innerWidth > window.innerHeight){
			equalizeHeightNewCarousel();
    		setTimeout(function(){
    			equalizeHeightNewCarousel(); 
    		},400); 
		}
	}


	function equalizeHeightNewCarousel(){
		jQuery( ".slick-track, .products").each(function() {
			
			var heightArray = new Array();
			var newHeight;
			var titleHeightArray = new Array();
			var newTitleHeight;

			jQuery(this).find("li").each(function() {
				//jQuery(this).find('.ast-loop-product__link').css({'background-color':'pink'});

				var titleHeight = jQuery(this).find('.ast-loop-product__link').height();
				//put all the title heights into an array and find the biggest height
				titleHeightArray.push(titleHeight); 
				newTitleHeight = Math.max.apply(Math,titleHeightArray);

			});//end li each()

			//after finding the biggest height, set it for each of this carousels mag titles
			jQuery(this).find(".ast-loop-product__link").css({"height":newTitleHeight});

		});//end slick-track each()

	}//end equalizeHeightNewCarousel()

	//equalizeHeightNewCarousel();



	//shitty old carousel but we need it on mobile
	function mobileCarouselHeightEqualize(){
		jQuery( ".currentIssueCarousel").each(function() {
			
			var heightArray = new Array();
			var newHeight;
			var titleHeightArray = new Array();
			var newTitleHeight;

			jQuery(this).find("li").each(function(){
				var titleHeight = jQuery(this).find('.eael-product-title').height();
				titleHeightArray.push(titleHeight); 
				newTitleHeight = Math.max.apply(Math,titleHeightArray);				
			});//end li each()

			jQuery(this).find(".eael-product-title").css({"height":newTitleHeight});

		});
	}

	mobileCarouselHeightEqualize();








	// var magCoverAreaHeight = jQuery( ".currentIssueArea" ).height();
	// if(magCoverAreaHeight < newHeight){
	// 	jQuery( ".currentIssueArea" ).css({"height":newHeight});
	// }
	// else{
	// 	jQuery( ".currentIssueArea" ).css({"height":'auto'});
	// }



	////////////////add dvd olog on product page if theres a DVD
	var dvdAttributeText = jQuery(".woocommerce-product-attributes-item__label").text();
	var dvdAttributeText = dvdAttributeText.toLowerCase();
	var infoCell = jQuery(".magInfoCell> .elementor-widget-wrap");

	if(dvdAttributeText != null && dvdAttributeText != undefined && dvdAttributeText != ""){
		if( dvdAttributeText.indexOf("comes with dvd") != -1 ){
			//console.log(dvdAttributeText);
			infoCell.addClass("bonusDVDcornerDisplay");
			//once we check for the "comes with dvd" text, remove it to be replaced with an image
			//jQuery(".magCoverCell .elementor-widget-wl-single-product-image").prepend("<div class='bonusDVDBanner'>BONUS DVD INSIDE</div>");
			jQuery(".magCoverCell .elementor-widget-wl-single-product-image").prepend("<div class='bonusDVDBannerBottom'></div>");
			jQuery(".mobileProductImageCell .elementor-widget-wl-single-product-image").prepend("<div class='bonusDVDBannerBottom'></div>");

			//jQuery(".woocommerce-product-attributes-item__label").text(" ");
		}
	}
	else{
		//console.log("has no DVD");
	}	

	



	//////////////HACK an "add to cart" button at the bottom of the carousel products
	jQuery(".eael-product-carousel").each(function(e){
		//add a link to each image by taking it from the shitty hover thing
		var link = jQuery(this).find(".view-details a").attr("href");
		var image = jQuery(this).find(".attachment-thumbnail"); 
		jQuery(image).wrap('<a href="'+ link +'"  ></a>');

		//add an "add to cart" button using the link from the shitty hover thing
		// var addToCartLink = jQuery(this).find(".add-to-cart a").attr("href");
		// var addToCartElement = jQuery(this).find(".add-to-cart");

		//jQuery(this).append("<a href='"+ addToCartLink +"' class=' myAddToCartButton'>hey now</a>");
		//jQuery(this).append("<div class='myTarget'>hey now</div>");

		//console.log(addToCartElement);

		jQuery(this).find( ".add-to-cart a" ).clone().addClass('myNewAddToCartLink').appendTo( jQuery(this) );
		
	});


	//////////////keep all mag covers in the carousels the same aspect ratio/////////////////////////////////////
	///keepAspectRatio() loops through all images in all product carousels
	///it compares their heights to a calculated expected height, based on the width of the mag
	///this works bc all mags have the same width in this layout. Pure... Genius ;)
	function keepAspectRatio(){
		jQuery(".eael-product-carousel, .eael-woo-product-carousel").each(function(e){
			var image = jQuery(this).find(".attachment-thumbnail"); 
			var magicHeightValue = 1.3333;
			var imageStartingHeight = image.height();
			var imageStartingWidth = image.width();
			var targetHeight = imageStartingWidth * magicHeightValue;

			if(imageStartingHeight > targetHeight){ 
				image.css({"height":targetHeight - 5});
			}
			//if it's way smaller, fix it too
			if(imageStartingHeight + 20 < targetHeight){ 
				image.css({"height":targetHeight - 5});
			}
		});
	}
	//when the page loads, keepAspectRatio()
	//keepAspectRatio();

	//because of lazyLoad(), keepAspectRatio() with a slight delay
	// setTimeout(function(){keepAspectRatio();},1000);
	// setTimeout(function(){keepAspectRatio();},2000);
	// setTimeout(function(){keepAspectRatio();},3000);	
	// setTimeout(function(){keepAspectRatio();},4000);
	// setTimeout(function(){keepAspectRatio();},5000);
	// setTimeout(function(){keepAspectRatio();},6000);


	var resizeTimerRatio;
	jQuery(window).on('resize', function(e) {
	  clearTimeout(resizeTimerRatio);
	  resizeTimerRatio = setTimeout(function() {
	   // keepAspectRatio();    
	  }, 200);
	});


	//some images may be hidden in the carousel, so keepAspectRatio() when you go right or left
	jQuery(".swiper-button-prev , .swiper-button-next ").click(function(e){
		//keepAspectRatio();
	});

	//because of lazyLoad(), keepAspectRatio() after any scroll
	jQuery(document).scroll(function() {
	    keepAspectRatio();
	    //grabBigMagImage();....NOT USING ANYMORE
	    //jQuery(window).unbind('scroll');   
	});
	/////////////////////////////////////////END carousel aspect ratio/////////////////////////////////////

	//these are for the lower featuredMags....NOT USING ANYMORE
	// setTimeout(function(){grabBigMagImage();},500);
	// setTimeout(function(){grabBigMagImage();},1000);
	// setTimeout(function(){grabBigMagImage();},2000);





	///////////////////////// new Cart Plugin - mimic the click and cart count for the top nav icon ////////////////
	jQuery(document).on('click', '.newCart', function(event) { 
	    event.preventDefault(); 
	    jQuery(".xoo-wsc-basket").click(); 
	});

	////on every click(), check the item count in the cart plugin, and put the value in the top Nav version
	jQuery(document).click(function(){
		copyTheItemCount();
	});

	function copyTheItemCount(){
		var currentCartItemCount = jQuery(".xoo-wsc-items-count").text();
		jQuery(".itemCountCopy").text(currentCartItemCount);	
	}

	copyTheItemCount();



	//add buttons for the mobile owl slider on the products page
	jQuery(".mobileRelatedTitles").prepend("<button class='arrow-next'></button>");	
	jQuery(".mobileRelatedTitles").prepend("<button class='arrow-prev'></button>");	

	jQuery(document).on('click', '.arrow-prev', function(event) { 
	    event.preventDefault(); 
	    jQuery(".prev").click(); 
	});
	jQuery(document).on('click', '.arrow-next', function(event) { 
	    event.preventDefault(); 
	    jQuery(".next").click(); 
	});

	////////NOW USED IN ELEMENTOR///////////////
	// function modelListExpand(){
	// 	var modelListClosed = true;
	// 	jQuery(".woocommerce_product_description").click(function(){
			
	// 		var targetHeight = jQuery(this).find('p').height() + 20;
			
	// 		if(modelListClosed === true){
	// 			jQuery(this).stop().animate({"height":targetHeight},'fast');
	// 			modelListClosed = false;
	// 		}
	// 		else{
	// 			jQuery(this).stop().animate({"height":"72px"},'fast');
	// 			modelListClosed = true;
	// 		}
	// 	});
	// }
	// modelListExpand();


	// jQuery(".mobileDescription, .productPageDescription").append("<div class='productDescriptionButton'>More</div>");



	// function getDescriptionHeight(){
	// 	var desktopDescriptionHeight = jQuery(".productPageDescription").find('p').height() + 20;
	// 	var mobileDescriptionHeight = jQuery(".mobileDescription").find('p').height() + 20;
	// 	var smallestDescriptionHeight = jQuery(".smallestScreenDescription").find('p').height() + 20;
		
	// 	var descriptionArray = new Array();	
	// 	descriptionArray.push(desktopDescriptionHeight); 
	// 	descriptionArray.push(mobileDescriptionHeight); 
	// 	descriptionArray.push(smallestDescriptionHeight); 
	// 	var descriptionHeight = Math.max.apply(Math,descriptionArray);		
	// 	return descriptionHeight;
	// }


	// var descriptionHeight = getDescriptionHeight();




	//////////////////////////////////set cart page coupon to default to open
	setTimeout(function() {
		jQuery(".wc-block-components-panel__button").click(); 
	},50);


	//jQuery(".woocommerce-checkout-payment").prepend("<div class='paymentOptionTitle'>Payment Options</div>"); 

	jQuery( "<div class='paymentOptionTitle'>Select your payment method</div>" ).insertBefore( jQuery( ".woocommerce-checkout-payment" ) );
	//change "Your Order" to "Your Order Summary" on cart page
	jQuery( "#order_review_heading" ).text("Your Order Summary");


});////end whole shabang