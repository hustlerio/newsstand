jQuery(document).ready(function(){


	////////////////////////////////////////get the widow width at start and onResize()
	var resizeTimer;
	var widthOfWindow;
	widthOfWindow = jQuery(window).width();
	jQuery(window).on('resize', function(e) {
	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {
	        widthOfWindow = jQuery(window).width();
	        girlListDropDown();
	  }, 200);
	});


	/////////////////////////////////////////////////determine the description height by comparing the 3 <p>s
	//theres only one on screen at any given time, hence your frustration with the bug
	function getDescriptionHeight(){
		//var desktopDescriptionHeight = jQuery(".productPageDescription").find('p').height() + 20;
		var mobileDescriptionHeight = jQuery(".mobileDescription").find(".woocommerce_product_description").height() ;
		var smallestDescriptionHeight = jQuery(".smallestScreenDescription").find(".woocommerce_product_description").height() ;
		var descriptionArray = new Array();	
		//descriptionArray.push(desktopDescriptionHeight); 
		descriptionArray.push(mobileDescriptionHeight); 
		descriptionArray.push(smallestDescriptionHeight); 
		var descriptionHeight = Math.max.apply(Math,descriptionArray);		
		
		console.log("description height:" + descriptionHeight);
		return descriptionHeight;
	}
	

	var descriptionHeight = getDescriptionHeight();;




	function girlListDropDown(){
		//since this will be fired on resize(), remove all buttons before you append one
		jQuery(".productDescriptionButton").remove();
		jQuery(".SMALLDescriptionButton").remove();

		//if youre over 1200 and the list isnt long enough, ignore the buttons and animation
		if(widthOfWindow >= 1200 && descriptionHeight <= 240){
		    jQuery(".productDescriptionButton").css({"display":"none"});
		    jQuery(".mobileDescription").find(".woocommerce_product_description").css({"height":descriptionHeight, "pointer-events":"none"});
		    console.log("1200 and no dropdown");
		}
		else if(widthOfWindow >= 1200 && descriptionHeight > 220){
			appendDescriptionButton();
			jQuery(".mobileDescription").find(".woocommerce_product_description").css({"height":72,"pointer-events":"auto"});
			console.log("1200 YES dropdown. Height:  " +  descriptionHeight);
		}

		//if youre between 600 and 1200 and theres a really short list, ignore button and animation
		else if( (widthOfWindow >= 600 && widthOfWindow < 1200) &&  descriptionHeight <= 90){
		    jQuery(".productDescriptionButton").css({"display":"none"});
		    jQuery(".mobileDescription").find(".woocommerce_product_description").css({"height":descriptionHeight, "pointer-events":"none"});
		    console.log("over 600");
		}	
		else if( (widthOfWindow >= 600 && widthOfWindow < 1200) &&  descriptionHeight > 90){
			appendDescriptionButton();
			jQuery(".mobileDescription").find(".woocommerce_product_description").css({"height":90, "pointer-events":"auto"});
			console.log("600 YES dropdown");
		}	

		//if youre between 550 and 600 and theres a really short list, ignore button and animation
		else if( (widthOfWindow >= 550 && widthOfWindow <= 600) &&  descriptionHeight <= 90){
		    jQuery(".productDescriptionButton").css({"display":"none"});
		    jQuery(".mobileDescription").find(".woocommerce_product_description").css({"height":descriptionHeight, "pointer-events":"none"});
		    console.log("over 450");
		}	
		else if( (widthOfWindow >=550 && widthOfWindow < 600) && descriptionHeight > 90){
			appendMobileDescriptionButton();
			jQuery(".mobileDescription").find(".woocommerce_product_description").css({"height":90, "pointer-events":"auto"});
			console.log("450 YES dropdown");
		}	

		//if youre between 550 and 600 and theres a really short list, ignore button and animation
		else if( widthOfWindow < 550 && descriptionHeight <= 72){
		    jQuery(".productDescriptionButton").css({"display":"none"});
		    jQuery(".mobileDescription").find(".woocommerce_product_description").css({"height":descriptionHeight, "pointer-events":"none"});
		    console.log("SMALLEST NO dropdown");
		}	

		else if( widthOfWindow < 550  && descriptionHeight > 72){
			appendSMALLDescriptionButton();
			jQuery(".smallestScreenDescription").find(".woocommerce_product_description").css({"height":72, "pointer-events":"auto"});
			console.log("SMALLEST YES dropdown");
		}	

	}//end girlListDropDown()

	girlListDropDown();

	setTimeout(function(){

	},2000);


	///////////////////////////////////////////my custom lightbox/////////////////////////////////////////////
	var lightBoxOpen = false;

	jQuery(".woocommerce-product-gallery__image, .woocommerce-product-gallery__trigger").click(function(){
		var image = jQuery(".zoomImg");
		var imagePath = jQuery(".wp-post-image").attr("src");
		var windowHeight = jQuery( window ).height();
		var windowWidth = jQuery( window ).width();
		var imageWidth = jQuery(".zoomImg").width();
		jQuery(".myLightBoxImage").attr("src",imagePath);


		if(imageWidth >= windowWidth){
			jQuery(".myLightBoxImage").css({"width":"100%","height":"auto"});
			console.log("wider than window");
		}
		else{
			jQuery(".myLightBoxImage").css({"width":"auto","height":windowHeight - 40});
			console.log("use the tall");
		}


		if(lightBoxOpen === false){
			jQuery('.productPageOverlay, .myLightBoxCloseButton').css({"display":"block"});
			jQuery('.productPageOverlay, .myLightBoxCloseButton').stop().animate({"opacity":1},"fast");
			lightBoxOpen = true;
		}
		
		console.log("i see the click:" + windowHeight);
	});//end click()


	//////if you click on the black transparent BG, cloce the moblie menu
	jQuery('.productPageOverlay, .productImageHolder, .myLightBoxCloseButton, .myLightBoxCloseButton').on('click', function(e) {
	  
	  if (e.target !== this)
	    return;

		jQuery('.productPageOverlay').stop().animate({"opacity":0},"fast",function(){
			jQuery('.productPageOverlay').css({"display":"none"});
		});	
		jQuery('.myLightBoxCloseButton').stop().animate({"opacity":0},"fast",function(){
			jQuery('.myLightBoxCloseButton').css({"display":"none"});
		});			
	  	lightBoxOpen = false;
	});












	///////////////////////////////////////////END my custom lightbox///////////////////////////////////////



	////////////////////////////////open and close description dropdown
	var modelListClosed = true;
	jQuery(".woocommerce_product_description").click(function(){
	    	if(modelListClosed === true){
				jQuery(".woocommerce_product_description").stop().animate({"height":descriptionHeight},'fast');
				
				modelListClosed = false;
				jQuery(".productDescriptionButton, .SMALLDescriptionButton").html("Less<span><img src='/wp-content/themes/astra/assets/images/mobileMenuArrowUp.png'></span>");
			}
			else{
				jQuery(".woocommerce_product_description").stop().animate({"height":"72px"},'fast');
				jQuery(".productDescriptionButton, .SMALLDescriptionButton").html("More<span><img src='/wp-content/themes/astra/assets/images/mobileMenuArrowDown.png'></span>");			
				modelListClosed = true;
			}
	});


	////when you click on the description buttons, trigger the dropdown click() attached to the woocomm container
	jQuery(document).on('click', '.productDescriptionButton', function(event) { 
	    event.preventDefault(); 
	    
	    jQuery(".mobileDescription .woocommerce_product_description").click();
	});

	jQuery(document).on('click', '.SMALLDescriptionButton', function(event) { 
	    event.preventDefault(); 
	    
	    jQuery(".smallestScreenDescription .woocommerce_product_description").click();
	});






	///////////Append the description buttons
	function appendDescriptionButton(){
		jQuery(".mobileDescription").append("<div class='productDescriptionButton'>More<span><img src='/wp-content/themes/astra/assets/images/mobileMenuArrowDown.png'></span></div>");
	}
	function appendMobileDescriptionButton(){
		console.log("mobile firing");
		jQuery(".mobileDescription").append("<div class='productDescriptionButton'>More<span><img src='/wp-content/themes/astra/assets/images/mobileMenuArrowDown.png'></span></div>");
	}
	function appendSMALLDescriptionButton(){
		jQuery(".smallestScreenDescription").append("<div class='SMALLDescriptionButton'>More<span><img src='/wp-content/themes/astra/assets/images/mobileMenuArrowDown.png'></span></div>");
	}



	//NEW better mobile carousel using Owl SLider and shortcode
	function relatedProductsHeightEqualize(){
		jQuery( "#carusel_poduct_related").each(function() {
			var heightArray = new Array();
			var newHeight;
			var titleHeightArray = new Array();
			var newTitleHeight;

			jQuery(this).find("li").each(function(){
				// jQuery(this).css({"background-color":"orange"});
				var titleHeight = jQuery(this).find('.woocommerce-loop-product__title').height();
				titleHeightArray.push(titleHeight); 
				newTitleHeight = Math.max.apply(Math,titleHeightArray);				
			});//end li each()

			jQuery(this).find(".woocommerce-loop-product__title").css({"height":newTitleHeight});

		});
	}

	relatedProductsHeightEqualize();
















});//end jQuery()





