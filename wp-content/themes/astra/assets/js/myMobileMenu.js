jQuery(document).ready(function(){

	//start all sub menus closed
	jQuery(".sub-menu").slideUp();

	jQuery( ".myMenu>.menu-main-menu-container>#menu-main-menu>li>a").click(function(e) {
		e.preventDefault();
		//get current sub menu
		var currentSubMenu = jQuery(this).parent().find(".sub-menu");
		//close all sub menus except for the current
		jQuery(".sub-menu").not(currentSubMenu).slideUp('fast');
		//toggle current sub menu
		jQuery(this).parent().find(".sub-menu").slideToggle( "fast");
		//toggle bg arrow
		jQuery(this).toggleClass( "openArrow");
		jQuery( ".myMenu>.menu-main-menu-container>#menu-main-menu>li>a").not(this).removeClass("openArrow");
	});



	///////////////////////////control the appearance of the mobile menu
	var isOpen = false;
	jQuery('#nav-icon3').click(function(){
		jQuery(this).toggleClass('open');
		
		if(isOpen === false){
			jQuery('.mobileMenuOverlay').css({"display":"block"});
			jQuery('.mobileMenuOverlay').stop().animate({"opacity":1},"fast", function(){
				jQuery('.myMenu').slideDown('fast');
			});
			isOpen = true;
		}
		else{
			jQuery('.myMenu').slideUp('fast',function(){
				jQuery('.mobileMenuOverlay').stop().animate({"opacity":0},"fast",function(){
					jQuery('.mobileMenuOverlay').css({"display":"none"});
				});	
			});
			isOpen = false;
		}

	});


	//////if you click on the black transparent BG, cloce the moblie menu
	jQuery('.mobileMenuOverlay, .scrollable').on('click', function(e) {
	  
	  if (e.target !== this)
	    return;

		jQuery('.myMenu').slideUp('fast',function(){
			jQuery('.mobileMenuOverlay').stop().animate({"opacity":0},"fast",function(){
				jQuery('.mobileMenuOverlay').css({"display":"none"});
			});	
		});
		jQuery('#nav-icon3').toggleClass('open');
	  	isOpen = false;
	});




	//hustler
	var hustlerMenu = jQuery(".myMenu .sub-menu:eq(0)");
	hustlerMenu.append("<li class='injectedLI'><a href='/hustler/' class='injectedMenuItem'> All Hustler Back Issues</a></li>");

	//bl
	var blMenu = jQuery(".myMenu .sub-menu:eq(1)");
	blMenu.append("<li class='injectedLI'><a href='/barely-legal/' class='injectedMenuItem'> All Barely Legal Back Issues</a></li>");
	
	//taboo
	var tabooMenu = jQuery(".myMenu .sub-menu:eq(2)");
	tabooMenu.append("<li class='injectedLI'><a href='/taboo/' class='injectedMenuItem'> All Taboo Back Issues</a></li>");

	//xxx
	var xxxMenu = jQuery(".myMenu .sub-menu:eq(3)");
	xxxMenu.append("<li class='injectedLI'><a href='/xxx/' class='injectedMenuItem'> All XXX Back Issues</a></li>");

	//Specials
	var specialsMenu = jQuery(".myMenu .sub-menu:eq(4)");
	specialsMenu.append("<li class='injectedLI'><a href='/specials/' class='injectedMenuItem specialPaddingFix'> All Specials Back Issues</a></li>");
	//split the specials menus links in half since there's so many
	jQuery(specialsMenu).children().css({ "width":"48%"});

	//'latin women/latin girls' is messing up the layout so change it to 'latin girls'
	jQuery(specialsMenu).children().find('a').each(function(){
		//console.log("lauren text: " + jQuery(this).text());
		var textGrab = jQuery(this).text().toLowerCase();
		
		if(textGrab.indexOf("latin women") != -1){
			jQuery(this).text("LATIN GIRLS")
		}
	});

	//more
	var moreMenu = jQuery(".myMenu .sub-menu:eq(5)");
	moreMenu.append("<li class='injectedLI'><a href='/more/' class='injectedMenuItem'> A collection of even More</a></li>");















});//end mobile menu