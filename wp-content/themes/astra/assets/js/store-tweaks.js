jQuery(document).ready(function(){


//////////////////////////////////display something specific when a search returns nothing
var hostname = window.location.hostname;
var nothingFoundImage = "https://" + hostname + "/wp-content/themes/astra/assets/images/store-headers/shop-nothing-found.jpg";
//get the 'searched term' from the URL params
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const searchTerm = urlParams.get('s')


var nothingFoundText = jQuery('.elementor-nothing-found').text();

if(nothingFoundText.indexOf("It seems we can't") != -1){
	jQuery('.searchedTerm').html("We didn't find anything for <span>" +searchTerm + "</span>" );
	jQuery('.storeHeaderImage').attr('src',nothingFoundImage);
}










});