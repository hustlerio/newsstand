<?php

/*

Plugin Name: Newsstand Reports

Description: Shows daily reports

Version: 1.0.0


*/


add_action('admin_menu', 'plugin_admin_add_page');
function plugin_admin_add_page() {
  add_options_page('Newsstand Reports', 'Newsstand Reports', 'edit_shop_orders', 'newsstand_reports', 'plugin_options_page'); //settings menu page
}

function plugin_options_page() {
	  wp_enqueue_script('jquery-ui-datepicker');
   // wp_register_style('jquery-ui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css');
    wp_enqueue_style('jquery-ui');

	$today    = new DateTime();
	$yesterday = date('Y-m-d',strtotime("-1 days"));
	if (!empty($_GET['datestart']) && !empty($_GET['dateend'])) {
		$showrange = true;
	} else {
		$showrange = false;
	}
	if ($showrange) {
		$initial_date = $_GET['datestart'];
		$end_date = $_GET['dateend'];
	} else if (empty($_GET['date'])) {
		$initial_date = $yesterday;
	} else {
		$initial_date = $_GET['date'];
	}
	$datestring = date_create($initial_date);
	if ($showrange) {
		$enddatestring = date_create($end_date);
		$nice_date = date_format($datestring,"M. d, Y").'-'.date_format($enddatestring,"M. d, Y");
	} else {
		$nice_date = date_format($datestring,"M. d, Y");
	}
	
	echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">';
    //HTML and PHP for Plugin Admin Page
    echo "<h1>Newsstand Reports</h1>";
	echo '<a href="/wp-admin/options-general.php?page=newsstand_reports" id="showyest">Show Yesterday</a>';
	echo '<br/><br/>';
	echo '<label>Pick Date</label><input type="text" class="custom_date" name="start_date" value="'.$initial_date.'"><input type="submit" id="showdatebtn" value="Show">';
	echo '<br/><br/>';
	echo '<label>Pick Date Range</label> Start: <input type="text" class="custom_start_date" name="range_start_date" value="'.$initial_date.'"> End: <input type="text" class="custom_end_date" name="range_end_date" value="'.$initial_date.'"><input type="submit" id="showrangebtn" value="Show Range">';
	
	echo '<style>
		.table2excel {
		border: 1px solid rgba(0,0,0,0.1);
		width: 100%;
		}
		.table2excel td {
		border-bottom: 1px solid rgba(0,0,0,0.1);
		}
		.table2excel tr:hover td {
		background: #e4f0fc;
		}
	#ui-datepicker-div {
		background: #fff;
    	padding: 0.7rem;
    	border: 1px solid #000;
	}
	';
	
	echo '</style>';
	



//$final_date = '2021-01-14';
if ($showrange) {
		$args = array(
 'limit' => 9999,
 'return' => 'ids',
 'date_paid' => $initial_date."...".$end_date,
 'status' => array('processing', 'completed')
	);
} else {
	$args = array(
 'limit' => 9999,
 'return' => 'ids',
 'date_paid' => $initial_date,
 'status' => array('processing', 'completed')
	);
	
}

	echo "<br><br><br><h2>Showing orders on: <strong style=\"color: blue;\">".$nice_date."</strong></h2>";
	
$query = new WC_Order_Query( $args );
$orders = $query->get_orders();
	if ($orders) {	
	echo '<table class="table2excel table2excel_with_colors" data-tableName="Test Table 1">
			<thead>
				<tr>
				<td>Order ID</td>
				
				<td>Date Paid</td>
				<td>Method</td>
				<td>Status</td>
				<td>Customer Name</td>
				<td>Product Name</td>
				<td>Product Model</td>
				<td>Product Price</td>
				<td>Quantity</td>
				<td>Order Subtotal</td>
				<td>Tax</td>
				<td>Charged Total</td>
				<td>Refunds</td>
				</tr>
			</thead>
			<tbody>';
	} else {
		echo '<p>No orders found for this date.</p>';
	}
foreach( $orders as $order_id ) {
 // ...

	$order = wc_get_order( $order_id );
	
	$customer_name = $order->get_billing_first_name().' '.$order->get_billing_last_name();
	$order_date = $order->get_date_paid();
	$order_date = $order_date->date('Y-m-d H:i:s');
	$order_subtotal = $order->get_subtotal();
	$order_subtotal = number_format( $order_subtotal, 2 );
	
	$order_refunds = $order->get_refunds();
	$refundamount = 0;
foreach( $order_refunds as $order_refund ){
    // To be sure we check if that method exist and that is not empty
	$refundamount += $order_refund->get_amount();
}

	$order_tax = $order->get_total_tax();
	$order_tax = number_format( $order_tax, 2 );

	
	$order_total = $order->get_total();

	$order_items  = $order->get_items();
	foreach( $order_items as $item_id => $item ){

	// methods of WC_Order_Item class

	// The element ID can be obtained from an array key or from:
	$item_id = $item->get_id();

	// methods of WC_Order_Item_Product class

	$item_name = $item->get_name(); // Name of the product
	$item_type = $item->get_type(); // Type of the order item ("line_item")

	$product_id = $item->get_product_id(); // the Product id
	//$wc_product = $item->get_product();    // the WC_Product object
	$product_variation_id = $item['variation_id'];

  // Check if product has variation.
  if ($product_variation_id) { 
    $product = new WC_Product($item['variation_id']);
  } else {
    $product = new WC_Product($item['product_id']);
  }

	// order item data as an array
	$item_data = $item->get_data();
	//print_r($wc_product->get_sku());
//$order_subtotal = $item_data['subtotal'];
//$order_subtotal = number_format( $order_subtotal, 2 );
		
//$order_total = $item_data['total'];
//$order_total = number_format( $order_total, 2 );
	$item_subtotal = $item_data['subtotal'];
	$item_subtotal = number_format( $item_subtotal, 2 );
		//print_r($item->get_formatted_refund_amount());
	echo '<tr>
				<td>'.$order_id.'</td>
				
				<td>'.$order_date.'</td>
				<td>'.$order->get_payment_method().'</td>
				<td>'.$order->get_status().'</td>
				<td>'.$customer_name.'</td>
				<td>'.$item_data['name'].'</td>
				<td>'.$product->get_sku().'</td>
				<td>$'.$item_subtotal.'</td>
				<td>'.$item_data['quantity'].'</td>
				<td>$'.$order_subtotal.'</td>
				<td>$'.$order_tax.'</td>
				<td>$'.$order_total.'</td>	
				';
		
		if ($refundamount > 0) { 
			$refundamount = number_format( $refundamount, 2 );
			echo '<td>(-$'.$refundamount.')</td>';
		} else {
			echo '<td></td>';
		}
		echo '</tr>';
}

	
	


}

	if ($orders) {	
		echo '</tbody>
		</table>
			
		<button class="exportToExcel">Export to XLS</button>
		
		<script>
			jQuery(function() {
				jQuery(".exportToExcel").click(function(e){
					var table = jQuery(this).prev(\'.table2excel\');
					if(table && table.length){
						var preserveColors = (table.hasClass(\'table2excel_with_colors\') ? true : false);
						jQuery(table).table2excel({
							exclude: ".noExl",
							name: "Newsstand Sales Report - '.$initial_date.'",
							filename: "Newsstand Sales - '.$initial_date.'.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});
			
		</script>
		<script src="/wp-content/plugins/newsstand_reports/table2excel.min.js"></script>
		';
	}
	echo "<script type=\"text/javascript\">
jQuery(document).ready(function($) {
$('.custom_date').datepicker({
dateFormat : 'yy-mm-dd',
maxDate: -1
});
$('.custom_start_date').datepicker({
dateFormat : 'yy-mm-dd',
maxDate: -1
});
$('.custom_end_date').datepicker({
dateFormat : 'yy-mm-dd',
maxDate: -1
});
});
jQuery('#showdatebtn').on('click', function() {
				var showdate = jQuery('.custom_date').val();
			 window.location.href = '/wp-admin/options-general.php?page=newsstand_reports&date='+showdate;
			});
jQuery('#showrangebtn').on('click', function() {
      var showstartdate = jQuery('.custom_start_date').val();
	  var showenddate = jQuery('.custom_end_date').val();
   window.location.href = '/wp-admin/options-general.php?page=newsstand_reports&datestart='+showstartdate+'&dateend='+showenddate;
  });		
		
</script>";
} // end function