﻿=== LitExtension: Shopping Cart Migration===
Contributors: litexten
Tags: shopping cart data migration, data migration, import data, transfer data, magento to woocommerce, opencart to woocommerce, prestashop to woocommerce, shopify to woocommerce, wix to woocommerce, virtuemart to woocommerce, bigcommerce to woocommerce, woocommerce to woocommerce, zen cart to woocommerce, oscommerce to woocommerce, data transfer, migrate data, data import, data migration tool, import product, store import, woocommerce migration, migrate to woocommerce, import to woocommerce, import store woocommerce, import data to woocommerce, import customer, import order, data importer, wordpress importer, store migration, data migration plugin, data migration service, website migration, migration to woocommerce, import woocommerce store, transfer data to woocommerce, product import, customer import, order import, import store to woocommerce, migrate shopping cart to woocommerce, shopping cart migration, data migration module, data migration extension, ecommerce replatforming.
Requires at least: 3.0
Tested up to: 5.8.1
Stable tag: 1.2.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The #1 data migration solution for WooCommerce. Simple and robust. 100% uptime. Highest security level. 100+ eCommerce platforms are supported.

== Description ==

**Supported WooCommerce versions: 1.1.x – 1.6.x, 2.x, 3.x**

LitExtension: Shopping Cart Data Migration to WooCommerce plugin is the optimal solution that helps you migrate your store from 100+ eCommerce platforms to WooCommerce. Within 3 simple steps, you can transfer all of your important data including products, customers, orders and other related entities automatically, accurately and securely.

During and after the migration to WooCommerce, we ensure no downtime on your current store, no data lost and a dedicated support!

### 3 simple steps to import data to WooCommerce
With the most advanced shopping cart migration solution, we ensure the highest accuracy and the least human involvement. All your data will be transferred only with a few clicks. No technical or coding skills needed!
1. Download LitExtension: Shopping Cart Data Migration to WooCommerce plugin and fill in your store information.
2. Select the data you want to migrate from your current store to WooCommerce
3. Run a Free Demo Migration (optionally) or perform your Full Migration immediately.

(All detailed instructions are provided directly on the migration tool)

### We support migration from 100+ eCommerce platforms to WooCommerce
**1. Ecommerce Platforms:**
- Migrate from WooCommerce to WooCommerce
- Migrate from Magento to WooCommerce
- Migrate from OpenCart to WooCommerce
- Migrate from PrestaShop to WooCommerce
- Migrate from Shopify to WooCommerce
- Migrate from Wix to WooCommerce
- Migrate from Virtue Mart to WooCommerce
- Migrate from BigCommerce to WooCommerce
- Migrate from Zen Cart to WooCommerce
- Migrate from osCommerce to WooCommerce

You can import data to WooCommerce from other shopping carts: Shopware, Squarespace, CS-Cart, Ecwid, X-Cart, Volusion, HikaShop, Shopp, OXID eShop, 3dCart, xt:Commerce, Pinnacle Cart, Weebly, WP eCommerce, Neto, Easy Digital Downloads and so on.

**2. Files:**
With LitExtension, you can import data to WooCommerce from files such as CSV, XML, XLS, TXT, SQL etc. The structure and relation of your entities will be preserved exactly.

**3. Custom platforms:**
Our migration tool can help you migrate all of your important data from custom platforms to WooCommerce automatically and flawlessly.

### Migrate all your valuable data from current shopping cart to WooCommerce

* Products
 - Name, SKU, Short Description, Full Description, Status, Manufacturer, Tax Class
 - Price, Special Price
 - Meta Title, Meta Description
 - Weight, Width, Height, Depth
 - Product Tags, Up-sells, Cross-sells
 - Attributes (Name, Values)
 - Variants (Name, SKU, Weight, Quantity, Images, Price, Special Price)
 - Downloadable Products (Files, Max Downloads, Expiration Date), Grouped Products (Associated Products)
 - Base Images, Thumbnail Images, Additional Images
 - Quantity, Manage Stock
* Product Categories
 - Name, Description Sort Order
 - Images
 - URLs, Meta Title, Meta Description
* Manufacturers
 - Name
- Image
* Taxes
 - Tax Class (Tax Name, Country, Tax Rate, City)
* Customers
 - First Name, Last Name, Email, Customer Group, Passwords
 - Billing Address (Company, Address 1, Address 2, Country, State, City, Zip Code, Telephone, First Name, Last Name)
 - Shipping Address (Company, Address 1, Address 2, Country, State, City, Zip Code, Telephone, First Name, Last Name)
* Orders
 - ID, Order Date, Order Status, Order Products (Name, SKU, Option), Product Price, Quantity, Discount Price, Tax Price, Total Price, Custom, Order Status, Shipping Price, Order Status History
 - Customer Name, Email, Billing Address (Company, Address 1, Address 2, Country, State, City, Zip Code, Telephone, First Name, Last Name)
 - Shipping Address (Company, Address 1, Address 2, Country, State, City, Zip Code, Telephone, First Name, Last Name)
* Coupons
 - Name, Description, Coupon Code, Coupon Date, Customer Groups, Uses Per Coupon, Uses Per Customer, Discount Amount, Coupon from date, Coupon to date
* Reviews
 - Created Date, Status, Rate, User Name, Comment, User Email
* CMS Pages
 - Title, Full Description, URLs, Description
* CMS Pages
 - Title, Full Description, URLs, Description
* Multiple Language
 - Yes
* Blogs
 - Title, Full Description, SEO URLs
* Blog Posts
 - Title, Full Description, Short Description, Tags, Created Time, SEO URLs

### Various additional options to extend your migration possibility

* Migrate customer passwords
* Migrate product and category SEO URLs
* Clear data on your WooCommerce store before migration
* Preserve Order IDs on your WooCommerce store
* Create 301 redirects on WooCommerce store after migration
* Create product variants based on combinations of options
* Strip HTML from category, product names and descriptions
* Migrate images from product, category, and blog post descriptions
* Change product quantity on your WooCommerce store
* Migrate products’ additional images
* Migrate short and full descriptions
* Migrate product SKUs
* Need more? Feel free to contact us, customization requests are supported


### The #1 plugin for migration to WooCommerce

**Guarantee your sales performance**
- 100% Uptime: LitExtension: Shopping Cart Data Migration to WooCommerce plugin ensures that your current store will operate normally during the data transfer process to receive new customers and orders. Hence, there will be no sales disruption!
- Preserve SEO Ranking: LitExtension keeps your URLs ranking on search engines, which ensures your store’s SEO performance and a seamless customer experience.
- Free & Unlimited Updating Data: You can easily import products, import customers and import orders… that recently appeared in your current store during and after the migration.

**Migrate data effectively**
With the most advanced shopping cart migration solution, we ensure the highest level of accuracy and time-efficiency for your replatforming projects. All of your important data will be completely migrated to the new WooCommerce store.

**24/7 professional support**
Our experts have more than 10 years of experience in the eCommerce industry with 150,000+ successful migration for 50,000+ customers worldwide. We are confident to help you resolve all issues during, during or after the migration process.

Please do not hesitate to contact us if you have any questions. We provide 24/7 dedicated support via various channels including live chat, emails, phone and Skype.

**Highest level of data security**
- Non-Disclosure Agreement: We do not keep your credentials or import records after the migration. Your data is guaranteed not to be disclosed or used for any other purposes rather than the migration.
- Data Access Restriction: Only authorized LitExtension experts can access the source code to handle custom migration requests.
- Payment Security: Your payment is processed by PayPal - the #1 worldwide payment gateway. Hence, LitExtension does not store any payment data.

### Pricing

**Unlimited free demo**
LitExtension: Shopping Cart Data Migration to WooCommerce plugin allows you to import 20 products, customers and orders. You can try out free demo migration to see more clearly how LitExtension works.

**Full migration**
The pricing is calculated based on the number of entities and additional options you select. You can pre-check the price using our <a href="https://litextension.com/pricing">Pricing Calculator</a>.

**30-day money back guarantee**
To provide our confidence in our services and their value to you, we provide a 30-day money back guarantee.

### Customer review
If you are still considering, you can listen to others sharing about their journey with LitExtension on <a href="https://www.trustpilot.com/review/litextension.com">Trustpilot</a>. We’re proud to have delivered 150,000+ successful migrations with 98% customer satisfaction.

==Installation==

1. Download the plugin and upload plugin files to your plugins folder, or install using WordPress built-in Add New Plugin installer.
2. Log in your WordPress admin panel, go to Plugin page, Find "LitExtension" and active the plugin.
3. Find LitExtension plugin on the left side menu. Register LitExtension and proceed with the migration setup.

== Frequently Asked Questions ==

= What do I need in order to start Migration? =
Please find details in "Installation" tab. Once you have finished installation and get to the Migration Tool UI, please follow Live instruction on the right to complete your migration.

= Can I migrate metadata to WooCommerce? = 
Yes, LitExtension supports metadata migrations. Online shop owners can transfer their meta titles, keywords and descriptions from and to WooCommerce store. In order to do that, you will have to install a free <a href="https://wordpress.org/plugins/wordpress-seo/"> WordPress SEO Plugin </a>before migration.

= How to migrate manufacturers from and to WooCommerce? = 
We supports migrating manufacturers if your WooCommerce have one of these 2 plugins installed: [Wordpress.org (free)](https://wordpress.org/plugins/woocommerce-brand/) and [WooCommerce.com (paid)](https://woocommerce.com/products/brands/). 
If you choose paid plugin to install, there will be an opportunity to migrate manufacturer images as well.

= Is Product Add-ons module supported? = 
Yes, we support Product Add-ons (https://woocommerce.com/products/product-add-ons/) as an alternative way to transfers your product options over to WooCommerce.

== Screenshots ==
1. About us
2. Install the plugin and login to connect to LitExtension
3. Setup connector on source and target store
4. Select entities and additional options to migrate
5. Data mapping
6. Migration on progress
7. Product list result
8. Category list result


== Changelog ==

= 1.0.0 =
* First commit
= 1.0.1 =
* +$5 after Demo Migration
+ Assets updated
= 1.1.0 =
* Add pages, blog posts migration to WooCommerce/Wordpress
= 1.2.0 =
* Add auto upload connector

