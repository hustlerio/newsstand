<?php

class LePrePassVolusion
{

    public function __construct()
    {

    }

    public function validatePassword($passwd, $encrypt, $user_id){
        $check = false;
        $email = $this->_getEmailCustomerById($user_id);
        if(!$email){
            return false;
        }
        $email = $email[0]['user_login'];
        $url = get_option("LEPP_URL");
        $data = array(
            'email' => $email,
            'password' => $passwd,
            'CustomerNewOld' => 'old',
        );
        $check = $this->request($url, $data);
        return $check;
    }

    protected function request($url, $data = array()){
        $option = http_build_query($data);
        $ch = curl_init($url . '/login.asp');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $option);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
        if($response && $this->checkHeader($response)){
            return true;
        }
        return false;
    }

    protected function checkHeader($header){
        preg_match('/Location:(.+)/', $header, $match);
        if ($match) {
            if (!strpos($match[1], 'shoppingcart.asp')) {
                return true;
            }
            if(!strpos($match[1], 'myaccount.asp')){
                return true;
            }
        }
        return false;
    }

    protected function _getEmailCustomerById($user_id){
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        require_once ABSPATH . 'wp-admin/includes/schema.php';
        $db = LePrePass::getGlobal('wpdb');
        $query = "SELECT `user_login` FROM `" . $db->base_prefix . "users` WHERE ID = " . $user_id . "";
        return $db->get_results($query, ARRAY_A);
    }

}