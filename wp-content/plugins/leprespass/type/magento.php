<?php


class LePrePassMagento {

    public function __construct() {
        
    }

    public function validatePassword($password, $hash) {
        $hashArr = explode(':', $hash);
        switch (count($hashArr)) {
            case 1:
                return $this->hash($password) === $hash;
            case 2:
            case 3:
                return $this->hash($hashArr[1] . $password) === $hashArr[0] || $this->hash256($hashArr[1] . $password) === $hashArr[0] || $this->getArgonHash($password, $hashArr[1]) === $hashArr[0];
        }
        return false;
    }

    public function hash($data)
    {
        return md5($data);
    }

    public function hash256($data) {
        return hash('sha256', $data);
    }
    public function getArgonHash($data, $salt = ''): string
    {
        $salt = empty($salt) ?
            random_bytes(SODIUM_CRYPTO_PWHASH_SALTBYTES) :
            substr($salt, 0, SODIUM_CRYPTO_PWHASH_SALTBYTES);

        if (strlen($salt) < SODIUM_CRYPTO_PWHASH_SALTBYTES) {
            $salt = str_pad($salt, SODIUM_CRYPTO_PWHASH_SALTBYTES, $salt);
        }

        return bin2hex(
            sodium_crypto_pwhash(
                SODIUM_CRYPTO_SIGN_SEEDBYTES,
                $data,
                $salt,
                SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE,
                SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE,
                2
            )
        );
    }
    
}
