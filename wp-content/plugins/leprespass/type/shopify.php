<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LePrePassShopify
{
   
    public function __construct() {

    }
    
    public function validatePassword($password, $hash, $user_id){
        $check = true;
        $email = $this->_getEmailCustomerById($user_id);
        if(!$email){
            return false;
        }
        $data = array(
            'form_type' => "customer_login",
            'customer[email]' => $email[0]['user_nicename'],
            'customer[password]' => $password,
            'send' => ""
        );
        $url_raw = get_option("LEPP_URL");
        $url = parse_url($url_raw);
        $url_ssl = "https://" . $url['host'];
        $check = $this->request($url_ssl, $data);
        if(!$check){
            return $check;
        }
        return $check;
    }
    /////////////////////////////////////////////
    
    protected function _checkHeader($header) {
        preg_match('/Location:(.+)/', $header, $match);
        if ($match) {
            if (!strpos($match[1], 'login')) {
                return true;
            }
        }
        return false;
    }

    protected function request($url, $data = array()) {
        $options = http_build_query($data);
        $ch = curl_init($url . '/account/login');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
        if ($response && $this->_checkHeader($response)) {
            return true;
        }
        return false;
    }

    protected function _getEmailCustomerById($userId) {
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        require_once ABSPATH . 'wp-admin/includes/schema.php';
        global $wpdb;
        $query = "SELECT user_nicename FROM " . $wpdb->base_prefix . 'users WHERE ID = ' . $userId;
        return $wpdb->get_results($query, ARRAY_A);
    }
    
}