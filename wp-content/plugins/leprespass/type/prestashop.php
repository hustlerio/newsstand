<?php


class LePrePassPrestashop {

    public function __construct() {
        
    }
    
    public function validatePassword($string, $encrypted) {
        $part = explode(":", $encrypted);
        $pass = $part[0];
        $cookie_key = isset($part[1]) ? $part[1] : '';
        if(password_verify($string, $pass)) {
            return true;
        }
        if (!$cookie_key) {
            return false;
        }
        if (md5($cookie_key . $string) == $pass) {
            return true;
        }
        if (password_verify($string, $pass)) {
            return true;
        }
        return false;
    }
    
    
}
