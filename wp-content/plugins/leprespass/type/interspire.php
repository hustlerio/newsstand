<?php

class LePrePassInterSpire
{
    public function __construct() {

    }

    public function validatePassword($passwd, $encrypt){
        $part = explode(":", $encrypt);
        if(count($part) < 2 || !$part[0] || !$part[1]){
            return false;
        }
        $pass_hash = $part[0];
        $salt = $part[1];
        $generate_pass = md5($salt.sha1($salt.$passwd));
        $generate_pass = substr($generate_pass, 0, 50);
        if($generate_pass == $pass_hash){
            return true;
        }
        return false;
    }

}