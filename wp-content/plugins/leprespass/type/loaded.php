<?php


class LePrePassLoaded {

    public function __construct() {
        
    }
    
    public function validatePassword($password, $encrypted) {
        if (strstr($encrypted, '::')) {  // sha256 hash
            $stack = explode('::', $encrypted);
            if (sizeof($stack) === 2) {
                return ( hash('sha256', $stack[1] . $password) == $stack[0] );
            }
        } else { // legacy md5 hash - will be removed in production release           
            $stack = explode(':', $encrypted);
            if (sizeof($stack) === 2) {
                return ( md5($stack[1] . $password) == $stack[0] );
            }
        }
    }
    
}
