<?php


class LePrePassOpencart {

    public function __construct() {
        
    }
    
    public function validatePassword($string, $encrypted) {
        $part = explode(":", $encrypted);
        $sha1 = $part[0];
        $salt = isset($part[1]) ? $part[1] : '';
        if (!empty($salt)) {
            if (sha1($salt . sha1($salt . sha1($string))) == $sha1) {
                return true;
            } elseif (md5($string) == $sha1) {
                return true;
            }
        } else {
            if (md5($string) == $sha1) {
                return true;
            }
        }
        return false;
    }
    
}
