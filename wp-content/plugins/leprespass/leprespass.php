<?php
/*
Plugin Name: LitExtension Customer Password Plugin
Plugin URI: http://litextension.com/
Description: A brief description of the Plugin.
Version: 1.0.0
Author: LitExtension
Author URI: http://litextension.com/
License: A "Slug" license name e.g. GPL2
*/

defined('ABSPATH') or die();

add_filter('check_password', 'lepp_authenticate', 1000000, 4);
add_filter('woocommerce_login_credentials', 'lepp_woo_authenticate', 1000000, 4);

function lepp_authenticate($check, $password, $hash, $user_id) {
    $type_folder = plugin_dir_path(__FILE__) . 'type/';
    $cartfiles = array_diff(scandir($type_folder), array('.', '..'));

    if($cartfiles && count($cartfiles)){
        foreach($cartfiles as $cart){
            $cart = str_replace (".php", '' , $cart);
            if (!$check && $model = LePrePass::getType($cart)) {
                try{
                    $validate = $model->validatePassword($password, $hash, $user_id);
                }catch (Exception $e){
                    continue;
                }
                if ($validate && $user_id) {
                    wp_set_password($password, $user_id);
                    $hash = wp_hash_password($password);
                    break;
                }
            }else{
                //echo $cart . " false <br >";
            }
        }
    }

    global $wp_hasher;
    if (empty($wp_hasher)) {
        require_once( ABSPATH . WPINC . '/class-phpass.php');
        $wp_hasher = new PasswordHash(8, true);
    }
    return $wp_hasher->CheckPassword($password, $hash);
}


function lepp_woo_authenticate($creds){

    require_once ABSPATH . 'wp-includes/pluggable.php';
    $user = get_user_by("login", $creds["user_login"]);

    lepp_authenticate(false, $creds["user_password"], $user->user_pass, $user->ID);

    return $creds;
}

class LePrePass {
    
    protected static $_instance = null;
    
    function __construct() {
        
    }
    
    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function getGlobal($name){
        global $$name;
        return $$name;
    }

    public function init() {
        register_activation_hook(__FILE__, array(&$this, 'install'));
        register_deactivation_hook(__FILE__, array(&$this, 'uninstall'));
    }
    
    public function install() {
        $this->_changePasswordLength();
        add_option('LEPP_TYPE', '');
        add_option('LEPP_URL', '');
    }
    
    public function uninstall() {
        delete_option('LEPP_TYPE');
        delete_option('LEPP_URL');
    }
    
    protected function _changePasswordLength() {
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        require_once ABSPATH . 'wp-admin/includes/schema.php';
        global $wpdb;
        $query = "ALTER TABLE " . $wpdb->base_prefix . 'users MODIFY user_pass VARCHAR(255)';
        $wpdb->query($query);
    }
    
    public static function path(){
        return plugin_dir_path(__FILE__);
    }

    public static function getType($cart){

        $name = $cart;

        if (!$name) {
            return false;
        }
        $type = null;
        $cart_path = self::path() . 'type/' . $name . '.php';
        if (file_exists($cart_path)) {
            $class = ucfirst($name);
            require_once $cart_path;
            $class_name = 'LePrePass' . $class;
            $type = new $class_name();
        }
        return $type;
    }
}

LePrePass::instance()->init();
