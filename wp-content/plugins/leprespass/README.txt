1. SETUP INSTRUCTIONS AND USAGE GUIDE

- Extract the folder leprespass into /wp-content/plugins/leprespass
- Activate the module in Wordpress backend

2. CONTACT & SUPPORT & PLUGIN REQUEST

For any question, bug report, plugin request, please drop us a message with your details info at: http://litextension.com/contacts, or email us: contact@litextension.com
We are striving to get your issue solved within 24 hours.