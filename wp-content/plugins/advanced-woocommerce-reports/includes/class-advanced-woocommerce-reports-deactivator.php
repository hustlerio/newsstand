<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 * @author     MakeWebBetter <webmaster@makewebbetter.com>
 */
class Advanced_Woocommerce_Reports_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		wp_clear_scheduled_hook( 'arfw_check_licence_daily' );
	}

}
