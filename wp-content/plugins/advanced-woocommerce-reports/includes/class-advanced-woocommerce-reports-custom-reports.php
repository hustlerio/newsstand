<?php

/**
 * Fired during plugin activation
 *
 * @link       https://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
class Advanced_Custom_Reports{

    public static function get_started(){
       
        // Custom Report Order field Data
        $arfw_custom_report_order_start_date = get_option('arfw_custom_report_order_start_date','');
        $arfw_custom_report_order_end_date = get_option('arfw_custom_report_order_end_date','');
        $arfw_custom_report_order_status = get_option('arfw_custom_report_order_status','');

        // Custom Report Product field Data
        $arfw_custom_report_product_start_date = get_option('arfw_custom_report_product_start_date','');
        $arfw_custom_report_product_end_date = get_option('arfw_custom_report_product_end_date','');

        // Custom Report Customer field Data
        $arfw_custom_report_customer_start_date = get_option('arfw_custom_report_customer_start_date','');
        $arfw_custom_report_customer_end_date = get_option('arfw_custom_report_customer_end_date','');
        ?>
        
        <div class="arfw-clearfix arwf-custom-report-wrapper">
            <!-- Error Notice -->
            <div class="arfw_notice">
                
            </div>
            <div class="notice error is-dismissible" id="arfw_empty_data" style="display:none;"><?php _e("Sorry no data found.","arf-woo");?></div>
            <div class="arwf-custom-report__tab">
                <!-- Menu Items -->
                <div class="arfw-custom-report">
                    <div id="arfw-custom-report-columns">
                        <ul>
                            <li>
                                <a href="#arfw-custom-report-order" class="current"><?php _e('Report for Order','arf-woo');?></a>
                            </li>
                            <li>
                                <a href="#arfw-custom-report-product"><?php _e('Report for Product','arf-woo');?></a>
                            </li>
                            <li>
                                <a href="#arfw-custom-report-customer"><?php _e('Report for Customer','arf-woo');?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="arwf-custom-report__tab-content">
                <!-- Custom report ORDER Section. -->
                <div id="arfw-custom-report-order" class="arfw-clearfix arfw-custom-report-order current">
                    <div class="arfw-custom-report-option-wrapper">
                        <div id="arfw-custom-report-order-period">
                            <label><?php _e('Order Period','arf-woo');?></label>
                            <select name="arfw_custom_report_order_period" id="arfw_custom_report_order_period">
                                <option value="select"> <?php _e('-- Select Order Period --','arf-woo'); ?> </option>
                                <option value="today"> <?php _e('Today','arf-woo'); ?>  </option>
                                <option value="days">  <?php _e('Last 7 Days','arf-woo'); ?> </option>
                                <option value="month"> <?php _e('This Month','arf-woo'); ?> </option>
                                <option value="year">  <?php _e('This Year','arf-woo'); ?> </option>
                                <option value="custom"><?php _e('Choose Custom Date','arf-woo'); ?> </option>
                            </select>
                        </div>
                        <div id="arfw-custom-report-order-status">
                            <label for="arfw_custom_report_order_status"><?php _e('Order Statuses','arf-woo');?></label>
                            <select class="arfw_select" name="arfw_custom_report_order_status[]" id="arfw_custom_report_order_status" multiple="multiple" data-placeholder="<?php esc_attr_e( 'Select Order Statuses', 'arf-woo' ); ?>">
                            <?php
                                $wc_order_statuses = wc_get_order_statuses();
                                if(is_array($arfw_custom_report_order_status) ){

                                    if(count($arfw_custom_report_order_status)> 0){
                                        foreach ( $arfw_custom_report_order_status as $single_status ) {

                                            if( array_key_exists( $single_status, $wc_order_statuses ) ) {
                            
                                                echo '<option value="'.$single_status. '" selected="selected">'.$wc_order_statuses[$single_status].'</option>';
                                            }
                                        }
                                    }
                                    
                                }
                            ?>
                            </select>
                        </div>
                        <div id="arfw-custom-report-date-section-order" style="display:none" class="arfw-custom-report-customer-calendar">
                            <div id="arfw-custom-report-order-start-date" class="arfw-custom-report-start-date">
                                <label for="arfw_custom_report_order_start_date"><?php _e('Start Date','arf-woo');?></label>
                                <input type="text" class="date-picker" name="arfw_custom_report_order_start_date" id="arfw_custom_report_order_start_date" placeholder="<?php _e('Start date for report','arf-woo');?>" value="<?php $arfw_custom_report_order_start_date;?>">
                            </div>
                            <div id="arfw-custom-report-order-end-date" class="arfw-custom-report-end-date">
                                <label for="arfw_custom_report_order_end_date"><?php _e('End Date','arf-woo');?></label>
                                <input type="text" class="date-picker" name="arfw_custom_report_order_end_date" id="arfw_custom_report_order_end_date" placeholder="<?php _e('End date for report','arf-woo');?>" value="<?php $arfw_custom_report_order_end_date;?>">
                            </div>
                        </div>
                    </div>
                    <div class="arfw-custom-report-order__drag-drop arfw-clearfix">
                        <div class="arfw-custom-report-order__drag arfw-clearfix">
                            <ul id="dvsource" class="drop">
                                <li class="drag" style="border:2px solid black" data-name='order_id'><?php _e('Order Id','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='order_status'><?php _e('Order Status','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='order_date'><?php _e('Order date','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='order_item_name'><?php _e('Order Item Name','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='payment_method'><?php _e('Payment Method','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='customer_ip_address'><?php _e('Customer IP Aaddress','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_first_name'><?php _e('Billing First Name','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_last_name'><?php _e('Billing Last Name','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_company'><?php _e('Billing Company','arfw-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_address_1'><?php _e('Billing Address 1','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_address_2'><?php _e('Billing Address 2','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_city'><?php _e('Billing City','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_state'><?php _e('Billing State','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_postcode'><?php _e('Billing Post Code','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_country'><?php _e('Billing Country','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_email'><?php _e('Billing Email','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='billing_phone'><?php _e('Billing Phone','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='shipping_first_name'><?php _e('Shipping First Name','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='shipping_last_name'><?php _e('Shipping Last Name','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='shipping_company'><?php _e('Shipping Company','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='shipping_address_1'><?php _e('Shipping Address 2','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='shipping_address_2'><?php _e('Shipping Address 1','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='shipping_city'><?php _e('Shipping City','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='shipping_state'><?php _e('Shipping State','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='shipping_postcode'><?php _e('Shipping Postcode','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='shipping_country'><?php _e('Shipping Country','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='order_currency'><?php _e('Currency','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='cart_discount'><?php _e('Discount','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='cart_discount_tax'><?php _e('Discount Tax','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='order_shipping'><?php _e('Shipping amount','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='order_shipping_tax'><?php _e('Shipping tax','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='order_tax'><?php _e('Order Tax','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='order_total'><?php _e('Order Total','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='product_id'><?php _e('Product Id','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='variation_id'><?php _e('Variation Id','arf-woo');?></li>
                                <!-- <li class="drag" style="border:2px solid black" data-name='line_subtotal'><?php _e('Line Subtotal','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='line_subtotal_tax'><?php _e('Line Subtotal Tax','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='line_total'><?php _e('Line Total','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='line_tax'><?php _e('Line Tax','arf-woo');?> -->
                            </li>
                            </ul>
                        </div>
                        <div class="arfw-custom-report-order__drop arfw-clearfix">
                            <ul id="dvdest" class="drop"></ul>
                        </div>
                        <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/switch.png'?>" alt="" class="arfw-switch-icon">
                    </div> 
                    <button id="arfw-generate-custom-report" class = "arfw-button"><?php _e("Generate Report","arf-woo");?></button>    
                    <div>
                        <div id="arfw-custom-report-table-order" class="arwf-custom-report-table-result">
                        </div>
                        <div class="arfw-mail-sent" style="display:none">
                            <button id="arfw_custom_report_mail"  class="arfw-mail-sent__btn"><?php _e("Send Email","arf-woo");?></button>
                        </div>
                    </div>
                </div>
                
                <!-- Custom Report PRODUCT Section -->
                <div id="arfw-custom-report-product" class="arfw-clearfix">
                    <form method="post">
                        <div class="arfw-custom-report-option-wrapper">
                            <div id="arfw-custom-report-product-period">
                                <label><?php _e('Timeline','arf-woo');?></label>
                                <select name="arfw_custom_report_product_period" id="arfw_custom_report_product_period">
                                    <option value="select"> <?php _e('-- Select Timeline --','arf-woo'); ?> </option>
                                    <option value="today"> <?php _e('Today','arf-woo'); ?> </option>
                                    <option value="days">  <?php _e('Last 7 Days','arf-woo'); ?> </option>
                                    <option value="month"> <?php _e('This Month','arf-woo'); ?> </option>
                                    <option value="year">  <?php _e('This Year','arf-woo'); ?> </option>
                                    <option value="custom"><?php _e('Choose Custom Date','arf-woo'); ?> </option>
                                </select>
                            </div>
                            
                            <div id="arfw-custom-report-checkbox-product">
                                
                                <div id="arfw_custom_report_all_product">
                                    <input type="radio" name="arfw_product_choice" value="all"><?php _e('All Products','arf-woo');?>
                                </div>

                                <div id="arfw_custom_report_particular_product">
                                    <input type="radio" name="arfw_product_choice" value="particular"><?php _e('Particular Products','arf-woo');?>
                                </div>
                            </div>
                            <div id="arfw-custom-report-select-product" style="display:none">
                                <label for="arfw_custom_report_select_product"><?php _e("Select Product","arf-woo");?></label>
                                <select name="arfw_product_name" id="arfw_product_name">
                                    <?php
                                    $all_products = Advanced_Woocommerce_Reports_Admin::arfw_get_products_list();?>
            						<option value="select"><?php _e('--Select a product--','arf-woo');?>
            						<?php
            						foreach ($all_products as $key => $value){
                                        ?>
                                        <option value="<?php echo $value->ID?>"><?php echo $value->post_title;?></option>
                                        <?php
            						}
            						?>
            					</select>
                            </div>
                            <div id="arfw-custom-report-date-section-product" style="display:none">
                                <div id="arfw-custom-report-product-start-date">
                                    <label for="arfw_custom_report_product_start_date"><?php _e('Start Date','arf-woo');?></label>
                                    <input required type="text" class="date-picker" name="arfw_custom_report_product_start_date" id="arfw_custom_report_product_start_date" placeholder="<?php _e('Start date for report','arf-woo');?>" value="<?php $arfw_custom_report_product_start_date;?>">
                                </div>
                                <div id="arfw-custom-report-product-end-date">
                                    <label for="arfw_custom_report_product_end_date"><?php _e('End Date','arf-woo');?></label>
                                    <input type="text" class="date-picker" name="arfw_custom_report_product_end_date" id="arfw_custom_report_product_end_date" placeholder="<?php _e('End date for report','arf-woo');?>" value="<?php $arfw_custom_report_product_end_date;?>">
                                </div>
                            </div>
                        </div>
                        <div class="arfw-custom-report-order__drag-drop arfw-clearfix">
                            <div class="arfw-custom-report-order__drag arfw-clearfix">
                                <ul id="dvsource_product" class="drop">
                                <li class="drag" style="border:2px solid black" data-name='id'><?php _e('Product ID','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='name'><?php _e('Product Name','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='quantity'><?php _e('Qunatity Sold','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='total'><?php _e('Revenue','arf-woo');?></li>
                                </ul>
                            </div>
                            <div class="arfw-custom-report-order__drop arfw-clearfix">
                                <ul id="dvdest_product" class="drop"></ul>
                            </div>
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/switch.png'?>" alt="" class="arfw-switch-icon">
                        </div>
                    </form>
                    <button id="arfw-generate-custom-report-product" class="arfw-button"><?php _e("Generate Report","arf-woo");?></button>    
                    <div>
                        <div id="arfw-custom-report-table-product" class="arwf-custom-report-table-result">
                        </div>
                        <div class="arfw-mail-sent" style="display:none">
                            <button id="arfw_custom_report_mail" class="arfw-mail-sent__btn"><?php _e("Send Email","arf-woo");?></button>
                        </div>
                    </div> 
                </div>

                <!-- Custom Report CUSTOMER Section -->
                <div id="arfw-custom-report-customer" class="arfw-clearfix">
                    <div class="arfw-custom-report-option-wrapper">
                        <div id="arfw-custom-report-customer-period">
                            <label><?php _e('Timeline','arf-woo');?></label>
                            <select name="arfw_custom_report_customer_period" id="arfw_custom_report_customer_period">
                                <option value="select"> <?php _e('-- Select Timeline --','arf-woo'); ?> </option>
                                <option value="today"> <?php _e('Today','arf-woo'); ?> </option>
                                <option value="days">  <?php _e('Last 7 Days','arf-woo'); ?> </option>
                                <option value="month"> <?php _e('This Month','arf-woo'); ?> </option>
                                <option value="year">  <?php _e('This Year','arf-woo'); ?> </option>
                                <option value="custom"><?php _e('Choose Custom Date','arf-woo'); ?> </option>
                            </select>
                        </div>
                        
                        <div id="arfw-custom-report-customer-option">
                            <label for="arfw_custom_report_customer_option"><?php _e('Generate Report for : ','arf-woo'); ?></label>
                            <select id="arfw_custom_report_customer_option">
                                <option value="select"><?php _e('--Please Select an Option--','arf-woo'); ?></option>
                                <option value="all"><?php _e('All Customers','arf-woo'); ?></option>
                                <option value="particular"><?php _e('Specific Customers','arf-woo'); ?></option>
                            </select>
                        </div>
                        <div id="arfw-custom-report-customer-email" style="display:none" class="arfw-custom-report-customer-calendar">
                            <label for="arfw_custom_report_customer_email"><?php _e('Customer Email : ','arf-woo'); ?></label>
                            <input type="text" id="arfw_custom_report_customer_email" placeholder="<?php _e('customer email address','arf-woo');?>">
                        </div>

                        <div id="arfw-custom-report-date-section-customer" style="display:none">
                            <div id="arfw-custom-report-customer-start-date">
                                <label for="arfw_custom_report_customer_start_date"><?php _e('Start Date','arf-woo');?></label>
                                <input required type="text" class="date-picker " name="arfw_custom_report_customer_start_date" id="arfw_custom_report_customer_start_date" placeholder="<?php _e('Start date for report','arf-woo');?>" value="<?php $arfw_custom_report_customer_start_date;?>">
                            </div>
                            <div id="arfw-custom-report-product-end-date">
                                <label for="arfw_custom_report_customer_end_date"><?php _e('End Date','arf-woo');?></label>
                                <input type="text" class="date-picker" name="arfw_custom_report_customer_end_date" id="arfw_custom_report_customer_end_date" placeholder="<?php _e('End date for report','arf-woo');?>" value="<?php $arfw_custom_report_customer_end_date;?>">
                            </div>
                        </div>
                    </div>
                    <div class="arfw-custom-report-order__drag-drop arfw-clearfix">
                        <div class="arfw-custom-report-order__drag arfw-clearfix">
                            <ul id="dvsource_customer" class="drop">
                                <li class="drag" style="border:2px solid black" data-name='FirstName'><?php _e('First Name','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='LastName'><?php _e('Last Name','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='BillingName'><?php _e('Billing Name','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='BillingEmail'><?php _e('Billing Email','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='OrderCount'><?php _e('Order Count','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='Total'><?php _e('Total Revenue','arf-woo');?></li>
                                <li class="drag" style="border:2px solid black" data-name='user_id'><?php _e('Customer/Guest','arf-woo');?></li>
                            </ul>
                        </div>
                        <div class="arfw-custom-report-order__drop arfw-clearfix">
                            <ul id="dvdest_customer"  class="drop"></ul>
                        </div>
                        <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/switch.png'?>" alt="" class="arfw-switch-icon">
                    </div>
                    <button id="arfw-generate-custom-report-customer" class="arfw-button"><?php _e('Generate Report', 'arf-woo'); ?></button>
                    <div>
                        <div id="arfw-custom-report-table-customer" class="arwf-custom-report-table-result">
                        </div>
                        <div class="arfw-mail-sent" style="display:none">
                            <button id="arfw_custom_report_mail" class="arfw-mail-sent__btn"><?php _e("Send Email","arf-woo");?></button>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="arwf_email_popup_bg" style="display:none">
            <div class="arfw_email_popup">
                <label for="arfw_email_recipient"><?php _e("Email Recipient *",'arf-woo');?></label>
                <input type="text" id="arfw_email_recipient" name="arfw_email_recipient" class="arfw_email_recipient">
                <label for="arfw_email_subject"><?php _e("Email Subject *",'arf-woo')?></label>
                <input type="text" id="arfw_email_subject" name="arfw_email_subject" class="arfw_email_subject">
                <label for="arfw_email_body"><?php _e("Email Body *","arf-woo")?></label>
                <textarea name="arfw_email_body" id="arfw_email_body" cols="30" rows="5" required="true"></textarea>
                <img id="arfw_gif" src="<?php echo plugin_dir_url( __FILE__ ).'/images/loader.gif'?>" alt="" class="arwf_loader" style="display:none">

                <img id="arfw_close" src="<?php echo plugin_dir_url( __FILE__ ).'/images/cancel.png'?>" alt="" class="arfw_close">
                <button id="arfw_email_send" ><?php _e("Send Email","arf-woo");?></button>
                
            </div>
        </div>
        
        <?php
    }
    
}