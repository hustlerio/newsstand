<?php

/**
 * Fired during plugin activation
 *
 * @link       https://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 * @author     MakeWebBetter <webmaster@makewebbetter.com>
 */
class Advanced_Woocommerce_Reports_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		$timestamp = get_option( 'arfw_pro_activated_timestamp', 'not_set' );
		
		if( 'not_set' === $timestamp ) {

			$current_time = current_time( 'timestamp' );

			$thirty_days = strtotime( '+30 days', $current_time );

			update_option( 'arfw_pro_activated_timestamp', $thirty_days );
		}

		if ( !wp_next_scheduled ( 'arfw_check_licence_daily' ) ) {

            wp_schedule_event( time(), 'daily', 'arfw_check_licence_daily' );
        } 
	}

}
