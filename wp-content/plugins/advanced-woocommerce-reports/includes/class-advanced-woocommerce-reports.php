<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 * @author     MakeWebBetter <webmaster@makewebbetter.com>
 */
class Advanced_Woocommerce_Reports {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Advanced_Woocommerce_Reports_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'ARFW_VERSION' ) ) {
			$this->version = ARFW_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'advanced-woocommerce-reports';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->arfw_define_constants();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Advanced_Woocommerce_Reports_Loader. Orchestrates the hooks of the plugin.
	 * - Advanced_Woocommerce_Reports_i18n. Defines internationalization functionality.
	 * - Advanced_Woocommerce_Reports_Admin. Defines all hooks for the admin area.
	 * - Advanced_Woocommerce_Reports_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-advanced-woocommerce-reports-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-advanced-woocommerce-reports-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-advanced-woocommerce-reports-admin.php';

		/**
		* The class responsible for showing all Advanced reports 
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-advanced-woocommerce-reports-summary.php';

		$this->loader = new Advanced_Woocommerce_Reports_Loader();

		/**
		* The class responsible for showing all Advanced reports by date.
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-advanced-woocommerce-reports-date.php';

		/**
		* The class responsible for showing all Advanced Custom reports & for mailing 
		* custom reorts.
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-advanced-woocommerce-reports-custom-reports.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-advanced-woocommerce-reports-license-activation.php';

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Advanced_Woocommerce_Reports_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Advanced_Woocommerce_Reports_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Advanced_Woocommerce_Reports_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		
		//add 'Advance' tab in WC>Reports navigation tab.
		$this->loader->add_filter('woocommerce_admin_reports', $plugin_admin, 'arfw_add_advance_report_tab');

		//Ajax 
		$this->loader->add_filter('wp_ajax_arfw_search_for_order_status', $plugin_admin, 'arfw_search_for_order_status');

		//Ajax
		$this->loader->add_filter('wp_ajax_arfw_generate_custom_report_order_query', $plugin_admin, 'arfw_generate_custom_report_order_query');
		
		//Ajax
		$this->loader->add_filter('wp_ajax_arfw_generate_custom_report_product_query', $plugin_admin, 'arfw_generate_custom_report_product_query');

		//Ajax
		$this->loader->add_filter('wp_ajax_arfw_generate_custom_report_customer_query', $plugin_admin, 'arfw_generate_custom_report_customer_query');

		//Ajax
		$this->loader->add_filter('wp_ajax_arfw_custom_report_table', $plugin_admin, 'arfw_custom_report_table');

		//Ajax
		$this->loader->add_filter('wp_ajax_arfw_send_mail', $plugin_admin, 'arfw_send_mail');
		
		//add link for settings
		$this->loader->add_filter ( 'plugin_action_links',$plugin_admin, 'arfw_report_page' , 10, 5 );

		$this->loader->add_filter ( 'admin_notices',$plugin_admin, 'arfw_license_notice');

		$this->loader->add_action( 'arfw_check_licence_daily', $plugin_admin, 'arfw_check_licence_daily' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Advanced_Woocommerce_Reports_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Define constants.
	 *
	 * @since 1.0.0
	*/
	function arfw_define_constants(){

		arfw_define( 'ARFW_URL', plugin_dir_url( __FILE__ ) . '/' );
		arfw_define( 'ARFW_VERSION', '1.0.0' );
		
	}

	/**
	 * Define constant if not already set.
	 *
	 * @param  string $name
	 * @param  string|bool $value
	 * @since 1.0.0
	*/

	function arfw_define($name, $value){
		if (!defined($name)) {
			define($name,$value);
		}
	}
	/**
	 * show admin notices.
	 * @param  string 	$message 	Message to display.
	 * @param  string 	$type    	notice type, accepted values - error/update/update-nag
	 * @since  1.0.0
	 */
	public static function arfw_notice( $message, $type='error' ) {

		$classes = "notice ";
		
		switch($type){

			default:
				$classes .= "error is-dismissible";
		} 

		$notice = '<div class="'. $classes .'">';
		$notice .= '<p>'. $message .'</p>';
		$notice .= '</div>';

		echo $notice;	
	}

	/**
	 * Reports Page Link
	 * @since    1.0.0
	 * @author  MakeWebBetter
	 * @link  https://makewebbetter.com/
	 */

	function arfw_report_page( $actions, $plugin_file ) {
	
		static $plugin;

		if ( !isset( $plugin ) ) {
	
			$plugin = plugin_basename ( __FILE__ );
		}

		if ( $plugin == $plugin_file ) {

			$settings = array (
				'report' => '<a href="' . admin_url ( 'admin.php?page=wc-reports&tab=mwb_advance&report=report_summary' ) . '">' . __ ( 'Advanced Reports', 'arf-woo' ) . '</a>',
			);

			$actions = array_merge ( $settings, $actions );
		}

		return $actions;
	}

	public static function arfw_activate_license($api_params = array()){

		$query = esc_url_raw( add_query_arg( $api_params, ARFW_LICENSE_SERVER_URL ) );
		
        $response = wp_remote_get( $query, array( 'timeout' => 20, 'sslverify' => false ) );

        if ( is_wp_error( $response ) ) {

            self::arfw_notice( __('An unexpected error occured. Please try again later.','arf-woo'), 'error' );
        }
        else {

	        $license_data = json_decode( wp_remote_retrieve_body( $response ) );

	        $message = __( 'An unexpected error occured. Please try again after sometime.', 'arf-woo' );

	        if( isset( $license_data->message ) && !empty( $license_data->message ) ) {

	        	$message = $license_data->message;
	        }

	        if( isset( $license_data->result ) && $license_data->result == 'success' ) {

	        	if( !empty( $license_data->date_expiry ) ) {

	        		update_option( "arfw_lic_expiry", $license_data->date_expiry );
	        	}

	            update_option( 'arfw_valid_license', true ); 
	            update_option( 'arfw_lic_key', $api_params['license_key'] ); 
				wp_redirect( admin_url() . 'admin.php?page=wc-reports&tab=mwb_advance' );
				
	        }
	        else {

				self::arfw_notice( $message, 'error' );
	        }
	    }
	}

	public static function arfw_verify_license($api_params = array()){

		$query = esc_url_raw( add_query_arg( $api_params, ARFW_LICENSE_SERVER_URL ) );
		
        $response = wp_remote_get( $query, array( 'timeout' => 20, 'sslverify' => false ) );

        if ( is_wp_error( $response ) ) {

            return;
        }
        else {

	        $license_data = json_decode( wp_remote_retrieve_body( $response ) );
	      	
	        if( isset( $license_data->result ) &&  $license_data->result == 'success' ) {

	            if ( isset( $license_data->status ) &&  $license_data->status == 'active' ) {

	            	update_option('arfw_valid_license', true);
	            }
	            else {

	            	delete_option('arfw_valid_license');
	            }
	        }
	        elseif( isset( $license_data->result ) && $license_data->result == 'error' && isset( $license_data->error_code ) && $license_data->error_code == 60 ) {
	        	
	        	delete_option('arfw_valid_license');
	        }
	    }
	}
}
