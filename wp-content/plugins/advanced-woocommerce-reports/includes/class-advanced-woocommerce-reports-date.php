<?php

/**
 * Fired during plugin activation
 *
 * @link       https://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
class Advanced_Reports_By_Date{

    /**
    * Generate and output reports by date
    * @since    1.0.0
    * @return   mixed report 
    */

    public static function get_advanced_report_by_date(){
       
        $arfw_end_date = get_option("arfw_end_date",date_i18n("Y-m-d"));
        $arfw_start_date = get_option("arfw_start_date",date_i18n("Y-m-d", strtotime("-1 months", strtotime(date_i18n("Y-m-d")))));
        $selected_order_statuses = get_option("arfw-order-status",array());
        //If Generate Report button is clicked

        if(isset($_POST['arfw_generate_report_by_date'])){

            unset($_POST['arfw_generate_report_by_date']);

            if(!empty($_POST)){

                foreach( $_POST as $key => $value ) {

                    if($key == 'arfw-order-status'){
                        $value = array_map( 'esc_attr', $_POST[ 'arfw-order-status' ]  );  
                    }
                    else{
                        $value = sanitize_text_field($value);
                    }
                    update_option( $key, $value );
    
                }
            }
            
            if(isset($_POST['arfw_start_date'])){
                $arfw_start_date = $_POST['arfw_start_date'];
            }
            if(isset($_POST['arfw_end_date'])){
                $arfw_end_date = $_POST['arfw_end_date'];
            }
            if(isset($_POST['arfw-order-status'])){
                $selected_order_statuses = $_POST['arfw-order-status'];
            }

            if(!isset($_POST['arfw-order-status'])){
                update_option("arfw-order-status","");
            }
            if(!empty($arfw_start_date) && !empty($arfw_end_date) && (strtotime($_POST['arfw_start_date']) > strtotime($_POST['arfw_end_date']) )){      
                $message = __("Start Date can not be greater than End Date !","arf-woo");
                Advanced_Woocommerce_Reports::arfw_notice($message,'error');
            }

            if(empty($_POST['arfw_start_date']) ){
                $message = __("Select Start Date !","arf-woo");
                Advanced_Woocommerce_Reports::arfw_notice($message,'error');
            }

            if(empty($_POST['arfw_end_date'])){
                $message = __("Select End Date !","arf-woo");
                Advanced_Woocommerce_Reports::arfw_notice($message,'error');
            }

            if(empty($_POST['arfw-order-status'])){
                $message = __("Select Order Status !","arf-woo");
               
                Advanced_Woocommerce_Reports::arfw_notice($message,'error');
            }

        }
       
        ?>
        <div class="arfw-report-by-date-by-date">
            <form method="post">
                <div class="arfw-start-date">
                    <label for="arfw_start_date"><?php _e('Start Date','arf-woo');?></label>
                    <input type="text" class="date-picker " name="arfw_start_date" id="arfw_start_date" placeholder="<?php _e('Start date for report','arf-woo');?>" value="<?php echo $arfw_start_date?>">
                </div>
                <div class="arfw-end-date">
                    <label for="arfw_end_date"><?php _e('End Date','arf-woo');?></label>
                    <input type="text" class="date-picker" name="arfw_end_date" id="arfw_end_date" placeholder="<?php _e('End date for report','arf-woo');?>" value="<?php echo $arfw_end_date?>">
                </div>
                <div class="arfw-order-status">
                    <label for="arfw-order-status"><?php _e('Order Statuses','arf-woo');?></label>
                    <select class="arfw_select" name="arfw-order-status[]" id="arfw-order-status" multiple="multiple" data-placeholder="<?php esc_attr_e( 'Select Order Statuses', 'arf-woo' ); ?>">
                        <?php
                            $wc_order_statuses = wc_get_order_statuses();
                            $by_date_order_status = get_option("arfw-order-status","");
                            if(!empty($by_date_order_status) && is_array($by_date_order_status)){
                                foreach ( $by_date_order_status as $single_status ) {

                                    if( array_key_exists( $single_status, $wc_order_statuses ) ) {
            
                                        echo '<option value="'.$single_status. '" selected="selected">'.$wc_order_statuses[$single_status].'</option>';
                                    }
                                }
                            }
                            
                        ?>
                    </select>
                </div>
                <div class="submit">
                    <button type="submit" name="arfw_generate_report_by_date" id="arfw_generate_report_by_date"><?php _e("Generate Report","arf-woo");?></button>
                </div>
            </form>
        </div>

        <div class="arfw-report-title-by-date">
            <?php if(!empty($arfw_start_date) && !empty($arfw_end_date)){
                ?>
                <p><?php echo sprintf(__('Summary From %s To %s','arf-woo'), $arfw_start_date, $arfw_end_date);?></p>
                <?php
            }
            else{
                ?>
                <p><?php echo sprintf(__('Summary From %s To %s','arf-woo'), "Choose Start Date", "Choose End Date");?></p>
                <?php
            }
            ?>
           
        </div>
        <?php
        $currency_symbol = get_woocommerce_currency_symbol();

        $sales_amount = Summary_report::get_total_sales_amount($arfw_start_date, $arfw_end_date, $selected_order_statuses);
        
        $sales_count = Summary_report::get_total_sales_count($arfw_start_date, $arfw_end_date, $selected_order_statuses);
        if(!empty($sales_amount) && !empty($sales_count)){
            $avg_sales = round($sales_amount/$sales_count,2);
        }
        else{
            $avg_sales = 0;
        }
        
        ?>
        <div id="arfw_report_summary">
            <div id="arfw_report_tabs">
                <div id="arfw_total_sales_amount" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title animate">
                            <?php _e('Total Sales','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon animate">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/commerce.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b>'.get_woocommerce_currency_symbol().'</b>';echo $sales_amount ?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_customers"  class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title animate">
                            <?php _e('Total Customers','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon animate">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/group.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b># </b>';echo Summary_report::get_total_customers($arfw_start_date, $arfw_end_date); ?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_tax_amount" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title animate">
                            <?php _e('Total Tax','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon animate">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/piggy-bank.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b>'.$currency_symbol.'</b>';echo (Summary_report::get_total_tax_amount($type="normal_tax",$arfw_start_date, $arfw_end_date, $selected_order_statuses)) + (Summary_report::get_total_tax_amount($type="shipping_tax",$arfw_start_date, $arfw_end_date, $selected_order_statuses)); ?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_coupon_amount" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <?php
                        $coupon_details = Summary_report::get_total_coupon_amount($arfw_start_date, $arfw_end_date, $selected_order_statuses);

                        foreach ($coupon_details as $key => $value) {
                            $coupon_total = $value->coupon_amount;
                            $coupon_count = $value->coupon_count;
                        }
                        ?>
                        <div class="arfw_tab_title animate">
                            <?php _e('Total Coupon','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon animate">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/voucher.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b>'.$currency_symbol.'</b>';echo round($coupon_total,2);?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_products_sold" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title animate">
                            <?php _e('Total Products','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/paid.png'?>">
                        </div>
                        <div class="arfw_tab_total">
                            <?php echo '<b># </b>';echo Summary_report::get_total_products();?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_guest_customers" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title">
                            <?php _e('Total Guest Customers','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/guest.png'?>">
                        </div>
                        <div class="arfw_tab_total">
                            <?php echo '<b># </b>';echo Summary_report::get_total_guest_customers($report_type ="by_date", $arfw_start_date, $arfw_end_date);?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_new_customers" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title">
                            <?php _e('New Customers','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/guest.png'?>">
                        </div>
                        <div class="arfw_tab_total">
                            <?php echo '<b># </b>';echo Summary_report::get_new_registered_customers($arfw_start_date, $arfw_end_date);?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_order_count" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title">
                            <?php _e('Order Count','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/countdown.png'?>">
                        </div>
                        <div class="arfw_tab_total">
                            <?php echo '<b># </b>';echo $sales_count;?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_avg_sale" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title">
                            <?php _e('Average Sales Per Order','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'images/label.png'?>">
                        </div>
                        <div class="arfw_tab_total">
                            <?php echo '<b>'.get_woocommerce_currency_symbol().'</b>';echo round($avg_sales,2);?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_shipping" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title">
                            <?php _e('Total Shipping Amount','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'images/label.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b>'.$currency_symbol.'</b>';echo (Summary_report::get_total_shipping_amount($arfw_start_date, $arfw_end_date, $selected_order_statuses)); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="arfw_report_graph_wrapper arfw_report_graph_wrapper_by_date arfw_clearfix">
            <div class="arfw_report_graph_wrapper_by_date_half">
                <h3 class="arfw_report_graph__heading"><?php _e("Top Billing Country",'arf-woo');?></h3>
                <div class="" name="arfw_billing_country_chart_by_date" id="arfw_billing_country_chart_by_date" data-country='<?php echo json_encode(Summary_report::get_report_for_billing_country($arfw_start_date, $arfw_end_date, $selected_order_statuses))?>' data-currency='<?php echo $currency_symbol; ?>'></div>
            </div>
            <div class="arfw_report_graph_wrapper_by_date_half">
                <h3 class="arfw_report_graph__heading"><?php _e("Top Products",'arf-woo');?></h3>
                <div class="" name="arfw_top_five_product_chart" id = "arfw_top_five_product_chart" data-product='<?php echo json_encode(Summary_report::get_top_products($arfw_start_date, $arfw_end_date)) ?>' data-currency='<?php echo $currency_symbol; ?>'></div>
            </div>
        </div>
        <div class="arfw_report_graph_wrapper arfw_report_graph_wrapper_by_date arfw_clearfix">
            <div class="arfw_report_graph_wrapper_by_date_full">
                <h3 class="arfw_report_graph__heading"><?php _e("Daily Report",'arf-woo');?></h3>
                <div id="arfw_report_by_month_day" name="arfw_report_by_month_day" data-day='<?php echo json_encode(Summary_report::get_report_by_month($chart_type="by_date",$arfw_start_date, $arfw_end_date, $selected_order_statuses))?>'></div>
            </div>
        </div>
        <?php
    }
}