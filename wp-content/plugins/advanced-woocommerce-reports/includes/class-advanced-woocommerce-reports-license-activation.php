<?php

/**
 * Fired during plugin activation
 *
 * @link       https://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
class License_Activation{

    public static function get_started(){
       
        $message = __( 'Please enter the license key for this product to activate it.' , 'arf-woo' );

        $msg = __('You were given a license key when you purchased this item in the confirmation email.','arf-woo');

        if(isset($_POST['arfw_activate_license'])){

            unset($_POST['arfw_activate_license']);

            $license_key = sanitize_text_field($_REQUEST['arfw_license_key']);

            $api_params = array(
                'slm_action' => 'slm_activate',
                'secret_key' => ARFW_ACTIVATION_SECRET_KEY,
                'license_key' => $license_key,
                'registered_domain' => $_SERVER['SERVER_NAME'],
                'item_reference' => urlencode( ARFW_ITEM_REFERENCE ),
                'product_reference' => 'MWBPK-14739',
            );

            Advanced_Woocommerce_Reports::arfw_activate_license($api_params);
        }
        ?>

        <div class="arfw-license-container">
            <div class="arfw-license-form-header arfw-common-header">
                <h2><?php _e("License Activation","arfw-woo") ?></h2>
                <div class="arfw-connect-form-desc">
                <p>
                    <?php echo $message ?>
                    <span>
                    <?php echo $msg ?>
                    </span>
                </p>
                </div>
            </div>
            <div class="arfw-license-body">
                <form class="arfw-license-form" action="" method="post">
                <div class="arfw-license">
                <label>
                <?php _e("License Key","arfw-woo") ?>
                </label>
                <input class="regular-text" type="text" id="arfw_license_key" name="arfw_license_key" value="<?php echo get_option('arfw_lic_key',""); ?>" >
                </div>
                <div class="arfw-license-form-submit">
                <p class="submit">
                    <input type="submit" name="arfw_activate_license" value="<?php _e("Save & Activate","arf-woo")?>" class="button-primary" />
                </p>
                </div>
                </form>
            </div>
        </div>
        <?php
    }
}