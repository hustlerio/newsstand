<?php

/**
 * Fired during plugin activation
 *
 * @link       https://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/includes
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
 class Summary_report{

    /**
	* Show different reports
    * 
    * @since 1.0.0
	*/
    public static function get_summary_report(){
        
        ?>
        <div id="arfw_report_summary">
            <div id="arfw_report_tabs">
                <div id="arfw_total_sales_amount" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title animate">
                            <?php _e('Total Sales','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/commerce.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b>'.get_woocommerce_currency_symbol().'</b>';echo self::get_total_sales_amount() ?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_customers"  class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title animate " >
                        <?php _e('Total Customers','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon animate">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/group.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b># </b>';echo self::get_total_customers($report_type="summary"); ?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_tax_amount" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title animate">
                            <?php _e('Total Tax','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon animate">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/piggy-bank.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b>'.get_woocommerce_currency_symbol().'</b>';echo (self::get_total_tax_amount($type='normal_tax')) + (self::get_total_tax_amount($type='shipping_tax')); ?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_coupon_amount" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <?php
                        $coupon_details = self::get_total_coupon_amount();

                        foreach ($coupon_details as $key => $value) {
                            $coupon_total = $value->coupon_amount;
                            $coupon_count = $value->coupon_count;
                        }
                        ?>
                        <div class="arfw_tab_title  animate">
                            <?php _e('Total Coupon','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon  animate">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/voucher.png'?>">
                        </div>
                        <div class="arfw_tab_total  animate">
                            <?php echo '<b>'.get_woocommerce_currency_symbol().'</b>';echo round($coupon_total,2);?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_products_sold" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title animate">
                            <?php _e('Total Products','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon animate">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/paid.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b># </b>';echo self::get_total_products();?>
                        </div>
                    </div>
                </div>
                <div id="arfw_total_guest_customers" class="arfw_single_report_tab">
                    <div class="arfw_single_report_tab__wrapper">
                        <div class="arfw_tab_title animate">
                            <?php _e('Total Guest Customers','arf-woo');?>    
                        </div>
                        <div class="arfw_tab_icon animate">
                            <img src="<?php echo plugin_dir_url( __FILE__ ).'/images/guest.png'?>">
                        </div>
                        <div class="arfw_tab_total animate">
                            <?php echo '<b># </b>';echo self::get_total_guest_customers($report_type = "summary");?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $currency_symbol = get_woocommerce_currency_symbol();
            ?>
            <div class="arfw_report_graph_wrapper arfw_clearfix">
                <div id="arfw_sales_report_amount" class="arfw_report_graph_wrapper_card arfw_report_graph_wrapper_card--small">
                    <div id="arfw_sales_report_amount_today_title" class="arfw_report_graph_wrapper_card__title animate">
                        <?php _e('Order Count Today','arf-woo');?>
                    </div>
                    <div id="arfw_sales_report_amount_today" class="arfw_report_graph_wrapper_card__amount">
                        <?php echo '<b>#</b>'.self::get_total_sales_count(date_i18n("Y-m-d"),date_i18n("Y-m-d")) ?>
                    </div>
                </div>
                <div id="arfw_order_status_piechart__wrapper" class="arwf_report_graph_wrapper arfw_report_graph_wrapper_card">
                    <h3 class="arfw_report_graph__heading"><?php _e('Order Status Report','arf-woo');?></h3>
                    <div name="arfw_order_status_piechart" id="arfw_order_status_piechart" data-iddd='<?php echo json_encode(self::get_order_status_count_report()) ?>' data-currency='<?php echo $currency_symbol; ?>'></div>
                </div>
                <div id="arfw_sales_report_count" class="arfw_report_graph_wrapper_card arfw_report_graph_wrapper_card--small">
                    <div id="arfw_sales_report_count_today_title" class="arfw_report_graph_wrapper_card__title animate">
                        <?php _e('Total Earning Today','arf-woo');?>
                    </div>
                    <div id="arfw_sales_report_count_today" class="arfw_report_graph_wrapper_card__amount">
                        <?php echo  '<b>'.$currency_symbol.'</b>'. self::get_total_sales_amount(date_i18n("Y-m-d"),date_i18n("Y-m-d")) ?>
                    </div>
                </div>
            </div>
            <div class="arfw_report_graph_wrapper arfw_clearfix">
                <div id="arfw_billing_country_chart__wrapper" class="arwf_report_graph_wrapper">
                    <h3 class="arfw_report_graph__heading  animate"><?php _e('Top Billing Country','arf-woo');?></h3>
                    <div name="arfw_billing_country_chart" id="arfw_billing_country_chart" data-country='<?php echo json_encode(self::get_report_for_billing_country())?>' ></div>
                </div>
                <div id="arfw_report_by_month__wrapper" class="arwf_report_graph_wrapper">
                    <h3 class="arfw_report_graph__heading  animate"><?php _e('Monthly Report','arf-woo');?></h3>
                    <div id="arfw_report_by_month" name="arfw_report_by_month" data-month='<?php echo json_encode(self::get_report_by_month($chart_type="summary"))?>'></div>
                </div>
            </div>

            <div class="arfw_report_graph_wrapper arfw_clearfix arfw-margin-none">
                <div id="arfw_top_product_chart__wrapper" class="arwf_report_graph_wrapper">
                    <h3 class="arfw_report_graph__heading animate"><?php _e('Top Products','arf-woo');?></h3>
                    <div name="arfw_top_product_chart" id = "arfw_top_product_chart" data-product='<?php echo json_encode(self::get_top_products()) ?>'></div>
                </div>
                <div id="arfw_payment_gateway_chart__wrapper" class="arwf_report_graph_wrapper">
                    <h3 class="arfw_report_graph__heading animate"><?php _e('Top Payment Gateway','arf-woo');?></h3>
                    <div id="arfw_payment_gateway_chart" name="arfw_payment_gateway_chart" data-payment='<?php echo json_encode(self::get_report_for_payment_gateway())?>' ></div>
                </div>
            </div>
        </div>
        <?php
    }
    /**
	* Fetch and show Order Status report data 
	* 
	* @since 1.0.0
	*/
    public static function get_order_status_count_report(){

        $pending_status     = 0;
        $processing_status  = 0;
        $on_hold_status     = 0;
        $completed_status   = 0;
        $cancelled_status   = 0;
        $refunded_status    = 0;
        $failed_status      = 0;

        foreach ( wc_get_order_types( 'order-count' ) as $type ) {

            $counts              = (array) wp_count_posts( $type );
            $pending_status     += isset( $counts['wc-pending'] )    ? $counts['wc-pending']    : 0;
            $processing_status  += isset( $counts['wc-processing'] ) ? $counts['wc-processing'] : 0;
            $on_hold_status     += isset( $counts['wc-on-hold'] )    ? $counts['wc-on-hold']    : 0;
            $completed_status   += isset( $counts['wc-completed'] )  ? $counts['wc-completed']  : 0;
            $cancelled_status   += isset( $counts['wc-cancelled'] )  ? $counts['wc-cancelled']  : 0;
            $refunded_status    += isset( $counts['wc-refunded'] )   ? $counts['wc-refunded']   : 0;
            $failed_status      += isset( $counts['wc-failed'] )     ? $counts['wc-failed']     : 0;
            
        }
 
        $statuses   = array();
        $statuses[] = array("name" => 'Completed'  ,  "value" => $completed_status);
		$statuses[] = array("name" => 'Processing' ,  "value" => $processing_status);
		$statuses[] = array("name" => 'On-hold'    ,  "value" => $on_hold_status);
        $statuses[] = array("name" => 'Pending'    ,  "value" => $pending_status);
		$statuses[] = array("name" => 'Cancelled'  ,  "value" => $cancelled_status);
		$statuses[] = array("name" => 'Refunded'   ,  "value" => $refunded_status);
        $statuses[] = array("name" => 'Failed'     ,  "value" => $failed_status);
        
        return $statuses;
    }
    /**
	* Fetch and show Billing Country report data 
	* 
	* @since 1.0.0
	*/
    public static function get_report_for_billing_country($start_date=NULL, $end_date=NULL, $order_satuses=array()){

        global $wpdb;

        $query = "SELECT 
            SUM(total.meta_value) as 'total', 
            COUNT(*) as 'count', 
            country.meta_value as country 
            FROM {$wpdb->prefix}posts as posts 
            LEFT JOIN  {$wpdb->prefix}postmeta as total 
            ON total.post_id=posts.ID 
            LEFT JOIN {$wpdb->prefix}postmeta as country 
            ON country.post_id=posts.ID  
            WHERE 1=1 
            AND posts.post_type ='shop_order' 
            AND total.meta_key='_order_total' 
            AND country.meta_key='_billing_country' ";

        if(!empty($order_satuses) ){

            if(count($order_satuses)>0){
                $status = implode("', '", $order_satuses);
                $query .=" AND posts.post_status IN ('{$status}')";
            }
            
        }
        if(!empty($start_date) && !empty($end_date)){

            $query .= " AND DATE(posts.post_date) BETWEEN '{$start_date}' AND '{$end_date}' ";
        }

        $query .= " GROUP BY country.meta_value 
            ORDER BY SUM(total.meta_value) 
            DESC 
            LIMIT 5";

        $query_result = $wpdb->get_results($query);
        
        $country_details = array();

        $country_details[0] = array("title" => '--' , "value" => 0);
        $country_details[1] = array("title" => '--' , "value" => 0);
        $country_details[2] = array("title" => '--' , "value" => 0);
        $country_details[3] = array("title" => '--' , "value" => 0);
        $country_details[4] = array("title" => '--' , "value" => 0);
        foreach($query_result as $key=>$value){

            
            $title = null !== self::get_country_name_by_code($value->country) ? self::get_country_name_by_code($value->country) : '--';

            $amount = null !== $value->total ? round($value->total,2) : "00";
            $country_details[$key] = array('title' => $title, 'value' => $amount);
            
        }
        return $country_details;
        
    }
    /**
	* Fetch country name from country code
	* 
	* @since 1.0.0
	*/
    public static function get_country_name_by_code($code){

        if(!empty($code)){

            $country = WC()->countries;
            $country_name = $country->countries[$code];

            if(!empty($country_name)){
                return $country_name;
            }
            else{
                return $code;
            }
        }
    }
    /**
	* Fetch and show Top Product report data 
	* 
	* @since 1.0.0
	*/
    public static function get_top_products($start_date=NULL,$end_date=NULL){

		global $wpdb;

		$top_products = "SELECT 
			item_name.order_item_name as name, 
			SUM(total.meta_value) as  total			
			FROM {$wpdb->prefix}posts as posts 
			LEFT JOIN {$wpdb->prefix}woocommerce_order_items as item_name 
			ON item_name.order_id=posts.ID 
			LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as product_id 
			ON product_id.order_item_id=item_name.order_item_id 
			LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as total 
			ON total.order_item_id=item_name.order_item_id 
			LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as quantity 
			ON quantity.order_item_id=item_name.order_item_id 
            WHERE 1 = 1 
            AND posts.post_type ='shop_order' 
            AND item_name.order_item_type ='line_item' 
            AND product_id.meta_key ='_product_id' 
            AND total.meta_key ='_line_total' 
            AND quantity.meta_key ='_qty' ";
        
        if(!empty($start_date) && !empty($end_date)){

            $top_products .= "AND date_format( posts.post_date, '%Y-%m-%d') BETWEEN '{$start_date}' AND                    '{$end_date}' ";
        }

        $top_products .= " GROUP BY product_id.meta_value
            order by SUM(total.meta_value) DESC ";
        
        
        $top_products .= " LIMIT 5";
        
        $query_result = $wpdb->get_results($top_products);
        $product_details = array();
        $product_details[0] = array("name" => "--", "total" => 0);
        $product_details[1] = array("name" => "--", "total" => 0);
        $product_details[2] = array("name" => "--", "total" => 0);
        $product_details[3] = array("name" => "--", "total" => 0);
        $product_details[4] = array("name" => "--", "total" => 0);
        if(empty($start_date) && empty($end_date)){
            
           
            $colors = array('#F9AA33','#232F34','#6100ED','#03DAC5','#442C2E');
            foreach ($query_result as $key => $value){

                $product_details[$key] = array('name' => ucfirst($value->name), 'total' => round($value->total,2), 'color' => $colors[$key]);
                
            }

            return $product_details;
        }
        else{

            $colors = array('#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01');
            foreach ($query_result as $key => $value){

                $product_details[$key] = array('name' => ucfirst($value->name), 'total' => round($value->total,2), 'color' => $colors[$key]);
                
            }

            return $product_details;
        }
        
        

        ?>
        
        <?php
    }
    /**
	* Fetch and show Sales Report by month data 
	* 
	* @since 1.0.0
	*/
    public static function get_report_by_month($chart_type, $start_date=NULL, $end_date=NULL, $order_satuses=array()){

        global $wpdb;

        if($chart_type == "summary"){
            $month_query = "SELECT date_format(posts.post_date,'%m') AS 'Month', 
                sum(meta_value) AS 'Revenue' 
                FROM {$wpdb->prefix}posts as posts 
                LEFT JOIN {$wpdb->prefix}postmeta as postmeta 
                ON posts.ID = postmeta.post_id 
                WHERE post_type='shop_order' 
                AND meta_key='_order_total' 
                AND date_format( posts.post_date, '%m') BETWEEN '01' AND '12' 
                GROUP BY YEAR (posts.post_date), 
                MONTH (posts.post_date) ";
        }
        elseif($chart_type == "by_date"){
            $month_query = "SELECT date_format(posts.post_date,'%m-%d') AS 'Day', 
            sum(meta_value) AS 'Revenue' 
            FROM {$wpdb->prefix}posts as posts 
            LEFT JOIN {$wpdb->prefix}postmeta as postmeta 
            ON posts.ID = postmeta.post_id 
            WHERE post_type='shop_order' 
            AND meta_key='_order_total' ";
            if( !empty($order_satuses)&& is_array($order_satuses)){

                if(count($order_satuses)>0){
                    $status = implode("', '", $order_satuses);
                    $month_query .=" AND posts.post_status IN ('{$status}') ";
                }
                
            }
            else{
                $month_query .=" AND posts.post_status IN ('wc-on-hold','wc-processing','wc-completed') ";
            }
            $month_query .= " AND date_format( posts.post_date, '%Y-%m-%d') BETWEEN '{$start_date}' AND '{$end_date}' 
            GROUP BY DATE (posts.post_date) 
            ";
        }
           
        $month_query= $wpdb->get_results($month_query);
       
        if($chart_type == "summary"){

            $monthy_report = array();
            $monthy_report[] = (object) array('day' => '01', 'total' => 0, 'month' => 'January', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '02', 'total' => 0, 'month' => 'February', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '03', 'total' => 0, 'month' => 'March', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '04', 'total' => 0, 'month' => 'April', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '05', 'total' => 0, 'month' => 'May', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '06', 'total' => 0, 'month' => 'June', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '07', 'total' => 0, 'month' => 'July', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '08', 'total' => 0, 'month' => 'August', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '09', 'total' => 0, 'month' => 'September', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '10', 'total' => 0, 'month' => 'October', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '11', 'total' => 0, 'month' => 'November', 'dashLengthLine' => 5);
            $monthy_report[] = (object) array('day' => '12', 'total' => 0, 'month' => 'December', 'dashLengthLine' => 5);
            
            foreach($month_query as $key => $value){

                foreach($monthy_report as $k => $v){
                    
                    if($v->day == $value->Month){
                        $v->total = round($value->Revenue,2);
                    }
                }
            }
            return $monthy_report;
        }
        else{
            $daily_report = array();

            foreach($month_query as $key => $value){

                $daily_report[] = (object) array('day' => $value->Day, 'total'=> round($value->Revenue,2),'dashLengthLine' => 5);
            }

            return $daily_report;
        }
        ?>
        <?php
    }
   
    /**
	* Fetch and show Payment Gateway report data 
	* 
	* @since 1.0.0
	*/
    public static function get_report_for_payment_gateway(){

        global $wpdb;

        $payment_gateway = "SELECT 
            title.meta_value as 'title', 
            SUM(total.meta_value) as 'total'
            FROM {$wpdb->prefix}posts as posts 
            LEFT JOIN {$wpdb->prefix}postmeta as total 
            ON total.post_id=posts.ID 
            LEFT JOIN {$wpdb->prefix}postmeta as title 
            ON title.post_id=posts.ID 
            WHERE 1=1 
            AND posts.post_type ='shop_order' 
            AND total.meta_key ='_order_total' 
            AND title.meta_key ='_payment_method_title' 
            GROUP BY title.meta_value 
            ORDER BY SUM(total.meta_value) DESC 
            LIMIT 5
            ";

        $query_result = $wpdb->get_results($payment_gateway);
        $gateway_details = array();

        $gateway_details[0] = array('gateway' => '--' , 'total' => 0);
        $gateway_details[1] = array('gateway' => '--' , 'total' => 0);
        $gateway_details[2] = array('gateway' => '--' , 'total' => 0);
        $gateway_details[3] = array('gateway' => '--' , 'total' => 0);
        $gateway_details[4] = array('gateway' => '--' , 'total' => 0);
        $colors = array('#FF0F00','#FF6600', '#FF9E01', '#FCD202', '#F8FF01');
        
        foreach ($query_result as $key => $value) {
            
            $gateway_details[$key] = array('gateway' => $value->title, 'total' => round($value->total, 2), 'color' => $colors[$key]);
        }
        return $gateway_details;
        
    }
    /**
    * Generate and output total sales amount reports by date
	* @since	1.0.0
	* @return 	mixed report 
    */
    public static function get_total_sales_amount($start_date=NULL, $end_date=NULL, $order_satuses=array()){

        global $wpdb;

        $total_sales_amount = "SELECT
            SUM(postmeta.meta_value) as 'amount_total',
            count(posts.ID) as 'count_total'
            FROM {$wpdb->prefix}posts as posts
            LEFT JOIN {$wpdb->prefix}postmeta as postmeta 
            ON postmeta.post_id = posts.ID
            WHERE postmeta.meta_key = '_order_total' 
            AND posts.post_type = 'shop_order' 
            AND postmeta.meta_value > 0 
            AND posts.post_type = 'shop_order' 
            ";
            if(!empty($order_satuses)){
                if(count($order_satuses)>0){
                    $status = implode("', '", $order_satuses);
                    $total_sales_amount .=" AND posts.post_status IN ('{$status}')";
                }
                
            }
            elseif(empty($order_satuses)){
                $total_sales_amount .=" AND posts.post_status IN ('wc-on-hold', 'wc-processing', 'wc-completed')";
            }
            if (!empty($start_date) && !empty($end_date)){
                $total_sales_amount .= " AND DATE(posts.post_date) BETWEEN '$start_date' AND '$end_date'
            ";
            }
            $total_sales_amount .= "";
            
        $query_result = $wpdb->get_var($total_sales_amount);
        $query_result = isset($query_result) ? $query_result : "0";
        return round($query_result,2);
    }
    /**
    * Generate and output total sales count reports by date
	* @since	1.0.0
	* @return 	mixed report 
    */
    public static function get_total_sales_count($start_date=NULL, $end_date=NULL, $order_satuses=array() ){

        global $wpdb;
        
        //get the sales count
        $total_sales_count ="SELECT 
            count(*) AS 'total_count',
            SUM(postmeta1.meta_value) AS 'total_amount',
            DATE(posts.post_date) AS 'group_date'
            FROM {$wpdb->prefix}posts as posts
            LEFT JOIN {$wpdb->prefix}postmeta as postmeta1 ON postmeta1.post_id = posts.ID
            WHERE post_type = 'shop_order'
            AND postmeta1.meta_key = '_order_total' ";

            if(!empty($order_satuses)){

                if(count($order_satuses)>0){
                    $status = implode("', '", $order_satuses);
                    $total_sales_count .=" AND posts.post_status IN ('{$status}')";
                }
                
            }
            elseif(empty($order_satuses)){
                $total_sales_count .=" AND posts.post_status IN ('wc-on-hold', 'wc-processing', 'wc-completed')";
            }
            if(!empty($start_date) && !empty($end_date)){
                $total_sales_count .= " AND DATE(posts.post_date) BETWEEN '$start_date'
            AND '$end_date'
            ";
            }
            $total_sales_count .= "";

        $query_result = $wpdb->get_var($total_sales_count);
        $query_result = isset($query_result) ? $query_result : "NIL";
        return $query_result;
    }
    /**
    * Generate and output total tax amount reports by date
	* @since	1.0.0
	* @return 	mixed report 
    */
    public static function get_total_tax_amount($type, $start_date=NULL, $end_date=NULL, $order_satuses=array()){

        global $wpdb;

        $total_tax_amount = "SELECT
            SUM(postmeta.meta_value) as 'amount_total',
            count(posts.ID) as 'count_total'
            FROM {$wpdb->prefix}posts as posts
            LEFT JOIN {$wpdb->prefix}postmeta as postmeta 
            ON postmeta.post_id = posts.ID
             ";
            
        if(!empty($type) && $type == 'normal_tax'){
            $total_tax_amount .= " WHERE postmeta.meta_key = '_order_tax' ";
        }
        if(!empty($type) && $type == 'shipping_tax'){
            $total_tax_amount .= " WHERE postmeta.meta_key = '_order_shipping_tax' ";
        }

        $total_tax_amount .= " AND posts.post_type = 'shop_order' 
            AND postmeta.meta_value > 0 
            AND posts.post_type = 'shop_order' 
            ";
        if(!empty($order_satuses)){

            if(count($order_satuses)>0){
                $status = implode("', '", $order_satuses);
                $total_tax_amount .=" AND posts.post_status IN ('{$status}')";
            }
           
        }
        elseif(empty($order_satuses)){
            $total_tax_amount .=" AND posts.post_status IN ('wc-on-hold', 'wc-processing', 'wc-completed')";
        }
        if(!empty($start_date) && !empty($end_date)){
            $total_tax_amount .= " AND DATE(posts.post_date) 
            BETWEEN '{$start_date}' AND '{$end_date}'
            ";
        }
            
        $query_result = $wpdb->get_var($total_tax_amount);
        $query_result = isset($query_result) ? $query_result : "NIL";
        return round($query_result,2);
    }
    /**
    * Generate and output total coupon amount reports by date
	* @since	1.0.0
	* @return 	mixed report 
    */
    public static function get_total_coupon_amount($start_date=NULL, $end_date=NULL, $order_satuses=array() ){

        global $wpdb;

        $total_coupon_amount = "SELECT				
            SUM(woocommerce_order_itemmeta.meta_value) As 'coupon_amount', 
            Count(*) AS 'coupon_count' 
            FROM {$wpdb->prefix}woocommerce_order_items as woocommerce_order_items 
            LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as woocommerce_order_itemmeta 
            ON woocommerce_order_itemmeta.order_item_id=woocommerce_order_items.order_item_id
            LEFT JOIN {$wpdb->prefix}posts as posts ON posts.ID=woocommerce_order_items.order_id
            WHERE woocommerce_order_items.order_item_type='coupon' 
            AND woocommerce_order_itemmeta.meta_key='discount_amount'
            AND posts.post_type='shop_order' ";

        if(!empty($order_satuses)){
            
            if(count($order_satuses)>0){
                $status = implode("', '", $order_satuses);
            $total_coupon_amount .=" AND posts.post_status IN ('{$status}')";
            }
            
        }    
        elseif(empty($order_satuses)){
            $total_coupon_amount .=" AND posts.post_status IN ('wc-on-hold', 'wc-processing', 'wc-completed')";
        }
        if(!empty($start_date) && !empty($end_date)){
            $total_coupon_amount .= " AND DATE(posts.post_date) BETWEEN '{$start_date}' AND '{$end_date}'";
        }
        $total_coupon_amount .= "";
        
        $query_result = $wpdb->get_results($total_coupon_amount);
        
        $query_result = isset($query_result) ? $query_result : "NIL";
        
        return $query_result;
    }
    /**
    * Generate and output total customers count reports by date
	* @since	1.0.0
	* @return 	mixed report 
    */
    public static function get_total_customers($report_type ,$start_date=NULL, $end_date=NULL){

        global $wpdb;

        $customer_query = "SELECT users.ID, posts.post_date 
            FROM {$wpdb->prefix}posts as posts 
            LEFT JOIN {$wpdb->prefix}postmeta as postmeta 
            ON postmeta.post_id = posts.ID 
            LEFT JOIN {$wpdb->prefix}users as users 
            ON users.ID = postmeta.meta_value 
            WHERE posts.post_type = 'shop_order' 
            AND postmeta.meta_key = '_customer_user' 
            AND postmeta.meta_value > 0 ";
        
        if($report_type == "by_date"){
            $customer_query .= "AND date_format( users.user_registered, '%Y-%m-%d') BETWEEN '{$end_date}' AND '{$end_date}' ";
        }
            
        $customer_query .= " GROUP BY users.ID  
        ORDER BY posts.post_date 
        desc ";
        

        $query_result = $wpdb->get_results($customer_query);
        
        $customer = isset($query_result) ? count($query_result) : "0";

        return $customer;
    }
    /**
    * Generate and output total registered customers reports by date
	* @since	1.0.0
	* @return 	mixed report 
    */
    public static function get_new_registered_customers($start_date=NULL, $end_date=NULL){

        global $wpdb;

        $new_registered_customers = "SELECT  
            customer.ID, 
            posts.post_date 
            FROM {$wpdb->prefix}posts as posts 
            LEFT JOIN {$wpdb->prefix}postmeta as postmeta 
            ON postmeta.post_id = posts.ID 
            LEFT JOIN {$wpdb->prefix}users as customer 
            ON customer.ID = postmeta.meta_value 
            WHERE posts.post_type = 'shop_order' 
            AND postmeta.meta_key = '_customer_user' 
            AND postmeta.meta_value > 0 ";
            
        if(!empty($start_date) && !empty($end_date)){
            $new_registered_customers .= " AND DATE(customer.user_registered) BETWEEN '{$start_date}' AND '{$end_date}'
            ";
        }
        $new_registered_customers .= " GROUP BY customer.ID 
            ORDER BY posts.post_date desc 
            ";

        $query_result = $wpdb->get_results($new_registered_customers);
        $query_result = count($query_result);
        return $query_result;
        
    }
    /**
    * Generate and output total guest customers reports
	* @since	1.0.0
	* @return 	mixed report 
    */
    public static function get_total_guest_customers($report_type , $start_date=NULL, $end_date=NULL){

        global $wpdb;

        $total_guest_customer = "SELECT email.meta_value AS billing_email 
            FROM {$wpdb->prefix}posts as posts 
            LEFT JOIN {$wpdb->prefix}postmeta as postmeta 
            ON postmeta.post_id = posts.ID 
            LEFT JOIN {$wpdb->prefix}postmeta as email 
            ON email.post_id = posts.ID 
            WHERE posts.post_type = 'shop_order' 
            AND postmeta.meta_key = '_customer_user' 
            AND postmeta.meta_value = 0 ";

        if($report_type == "by_date" && !empty($start_date) && !empty($end_date) ){
            $total_guest_customer .= "AND date_format( posts.post_date, '%Y-%m-%d') BETWEEN '{$start_date}' AND '{$end_date}' ";
        }
        
        $total_guest_customer .= " AND email.meta_key = '_billing_email' 
        AND LENGTH(email.meta_value)>0 
        GROUP BY email.meta_value 
        ORDER BY posts.post_date 
        desc 
        ";
            
        $query_result = $wpdb->get_results($total_guest_customer);
        $query_result = isset($query_result) ? count($query_result) : "NIL";
        return $query_result;
    }
   
    /**
    * Generate and output total sales count reports by date
	* @since	1.0.0
	* @return 	mixed report 
    */
    public static function get_total_products(){

        global $wpdb;

        $product_count = "SELECT COUNT(*)  
            FROM {$wpdb->prefix}posts as posts 
            WHERE  post_type='product' 
            AND post_status = 'publish' ";

        $result = $wpdb->get_var($product_count);

        return $result;
    }

    /**
    * Generate and output total shipping amount reports by date
    * @since    1.0.0
    * @return   mixed report 
    */
    public static function get_total_shipping_amount( $start_date=NULL, $end_date=NULL, $order_satuses=array() ) {

        global $wpdb;

        $total_shipping_amount = "SELECT
            SUM(postmeta.meta_value) as 'total_shipping',
            count(posts.ID) as 'count_total'
            FROM {$wpdb->prefix}posts as posts
            LEFT JOIN {$wpdb->prefix}postmeta as postmeta 
            ON postmeta.post_id = posts.ID
             ";
            
        $total_shipping_amount .= " WHERE postmeta.meta_key = '_order_shipping' ";
        
        $total_shipping_amount .= " AND posts.post_type = 'shop_order' 
            AND postmeta.meta_value > 0 
            ";
        if(!empty($order_satuses)){

            if(count($order_satuses)>0){
                $status = implode("', '", $order_satuses);
                $total_shipping_amount .=" AND posts.post_status IN ('{$status}')";
            }
           
        }
        elseif(empty($order_satuses)){
            $total_shipping_amount .=" AND posts.post_status IN ('wc-on-hold', 'wc-processing', 'wc-completed')";
        }
        if(!empty($start_date) && !empty($end_date)){
            $total_shipping_amount .= " AND DATE(posts.post_date) 
            BETWEEN '{$start_date}' AND '{$end_date}'
            ";
        }

        $query_result = $wpdb->get_var($total_shipping_amount);
        $query_result = isset($query_result) ? $query_result : "NIL";
        return round($query_result,2);
    }
    
}