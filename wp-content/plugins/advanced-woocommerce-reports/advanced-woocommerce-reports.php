<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://makewebbetter.com/
 * @since             1.0.0
 * @package           Advanced_Woocommerce_Reports
 *
 * @wordpress-plugin
 * Plugin Name:       Advanced WooCommerce Reports
 * Plugin URI:        https://makewebbetter.com/advanced-woocommerce-reports
 * Description:       The Advanced WooCommerce Plugin has a unique and easy User Interface, with all reports in one place. It also facilitates generation of Custom Reports which can also be send via Email.
 * Version:           1.0.2
 * Requires at least: 	4.4.0
 * Tested up to: 		5.3
 * WC requires at least:	3.0.0
 * WC tested up to: 		3.8.0
 * Author:            MakeWebBetter
 * Author URI:        https://makewebbetter.com/
 * License:           MakeWebBetter License
 * License URI:       https://makewebbetter.com/license-agreement.txt
 * Text Domain:       arf-woo
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

$arfw_activated = false;

/**
* Checking if WooCommerce is active
**/

if( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	$arfw_activated = true;
}

if( $arfw_activated ){

	/**
	* The code that runs during plugin activation.
	*/

	if( !function_exists( 'activate_advanced_woocommerce_reports' ) ) {

		function activate_advanced_woocommerce_reports() {
			require_once plugin_dir_path( __FILE__ ) . 'includes/class-advanced-woocommerce-reports-activator.php';
			Advanced_Woocommerce_Reports_Activator::activate();
		}
	}

	/**
	* The code that runs during plugin deactivation.
	*/

	if( !function_exists( 'deactivate_advanced_woocommerce_reports' ) ) {

		function deactivate_advanced_woocommerce_reports() {
			require_once plugin_dir_path( __FILE__ ) . 'includes/class-advanced-woocommerce-reports-deactivator.php';
			Advanced_Woocommerce_Reports_Deactivator::deactivate();
		}
		
	}

	register_activation_hook( __FILE__, 'activate_advanced_woocommerce_reports' );
	register_deactivation_hook( __FILE__, 'deactivate_advanced_woocommerce_reports' );

	/**
	* The core plugin class that is used to define internationalization,
	* admin-specific hooks, and public-facing site hooks.
	*/
	require plugin_dir_path( __FILE__ ) . 'includes/class-advanced-woocommerce-reports.php';

	/**
	* Define constants.
	*
	* @since 1.0.0
	*/

	function arfw_define_constants() {

		arfw_define( 'ARFW_URL', plugin_dir_url( __FILE__ ) . '/' );
		arfw_define( 'ARFW_VERSION', '1.0.2' );
		arfw_define( 'ARFW_ACTIVATION_SECRET_KEY', '59f32ad2f20102.74284991' );
		arfw_define( 'ARFW_LICENSE_SERVER_URL', 'https://makewebbetter.com' );
		arfw_define( 'ARFW_ITEM_REFERENCE', 'Advanced WooCommerce Reports' );
		
	}

	/**
	* Define constant if not already set.
	*
	* @param  string $name
	* @param  string|bool $value
	* @since 1.0.0
	*/
	function arfw_define($name, $value){
		if (!defined($name)) {
			define($name,$value);
		}
	}

	/**
	* Setting Page Link
	* @since    1.0.0
	* @author  MakeWebBetter
	* @link  https://makewebbetter.com/
	*/

	function arfw_report_link( $actions, $plugin_file ) {

		static $plugin;

		if ( !isset( $plugin ) ) {

			$plugin = plugin_basename ( __FILE__ );
		}

		if ( $plugin == $plugin_file ) {

			$settings = array (
				'report' => '<a href="' . admin_url ( 'admin.php?page=wc-reports&tab=mwb_advance&report=report_summary' ) . '">' . __ ( 'Reports', 'arf-woo' ) . '</a>',
			);

			$actions = array_merge ( $settings, $actions );
		}

		return $actions;
	}

	//add link for settings
	add_filter ( 'plugin_action_links','arfw_report_link', 10, 5 );
	/**
	* Begins execution of the plugin.
	*
	* Since everything within the plugin is registered via hooks,
	* then kicking off the plugin from this point in the file does
	* not affect the page life cycle.
	*
	* @since    1.0.0
	*/
	function run_advanced_woocommerce_reports() {
		arfw_define_constants();
		$plugin = new Advanced_Woocommerce_Reports();
		$plugin->run();
	
	}
	run_advanced_woocommerce_reports();
}
else{
	
	/**
	* Show warning message if woocommerce is not install
	* @since 1.0.0
	* @author MakeWebBetter<webmaster@makewebbetter.com>
	* @link https://www.makewebbetter.com/
	*/

	function arfw_plugin_error_notice() {

		?>
		<div class="error notice is-dismissible">
			<p><?php _e( 'WooCommerce is not activated, Please activate WooCommerce first to install Advanced WooCommerce Reports.', 'arf-woo' ); ?></p>
		</div>
		<style>
		#message{display:none;}
		</style>
		<?php 
	} 
	
	add_action( 'admin_init', 'deactivate_advanced_woocommerce_reports' );  

	
	/**
	* Call Admin notices
	* 
	* @author MakeWebBetter<webmaster@makewebbetter.com>
	* @link https://www.makewebbetter.com/
	*/ 	
	function deactivate_advanced_woocommerce_reports() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-advanced-woocommerce-reports-deactivator.php';
		Advanced_Woocommerce_Reports_Deactivator::deactivate();
	}
}
$arfw_lic_key = get_option( "arfw_lic_key", "" );
define( 'ARFW_LICENSE_KEY', $arfw_lic_key );
define( 'ARFW_BASE_FILE', __FILE__ );
$arfw_update_check = "https://makewebbetter.com/pluginupdates/advanced-woocommerce-reports/update.php";
require_once( 'advanced-woocommerce-reports-update.php' );
