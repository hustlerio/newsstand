(function( $ ) {

	'use strict';
	var ajaxUrl 			= local_msg.ajaxUrl;
	var arfwSecurity 		= local_msg.arfwSecurity;
	var select_columns 		= local_msg.select_columns;
	var select_order_status = local_msg.select_order_status;
	var select_date 		= local_msg.select_date;
	var select_period 		= local_msg.select_period;
	var select_timeline 	= local_msg.select_timeline;
	var select_all_options	= local_msg.select_all_options;
	var enter_email			= local_msg.enter_email;
	var empty_dataset		= local_msg.empty_dataset;
	var order_status_title	= local_msg.order_status_title;
	var income_in_text		= local_msg.income_in_text;
	var income_title		= local_msg.income_title;
	var total_in_text		= local_msg.total_in_text;
	var product_name_text	= local_msg.product_name_text;
	var in_text				= local_msg.in_text;
	var month_text			= local_msg.month_text;
	var payment_gateway_text= local_msg.payment_gateway_text;

	
	jQuery(document).ready(function(){
		jQuery(document).on('click','#arfw-notice-dismiss',function(){
			jQuery('.arfw_notice').html("");
		});

		var mail_send_data = '';
		//Order status pie chart data
		var order_status_data = jQuery('#arfw_order_status_piechart').data('iddd');
		var currency_symbol = jQuery('#arfw_order_status_piechart').data('currency');
		
		//Billing country chart data	
		var country_details = jQuery('#arfw_billing_country_chart').data('country');

		//Billing country chart data by date
		var country_details_by_date = jQuery('#arfw_billing_country_chart_by_date').data('country');
		var currency = jQuery('#arfw_billing_country_chart_by_date').data('currency');
		
		//Top products chart data
		var product_details = jQuery('#arfw_top_product_chart').data('product');

		//Top 5 product chart data
		var top_five_products = jQuery('#arfw_top_five_product_chart').data('product');

		//Report by month chart data
		var month_details = jQuery('#arfw_report_by_month').data('month');

		//Report by day chart data
		var day_details = jQuery('#arfw_report_by_month_day').data('day');
		
		//Paymemt gateway report chart
		var payment_details = jQuery('#arfw_payment_gateway_chart').data('payment');

		//animation		
		jQuery('.animate').jAnimate('fadeInDown');

		//Report date
		jQuery("#arfw_custom_report_order_start_date").datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true, 
			maxDate: 0
		});
		jQuery("#arfw_custom_report_order_end_date").datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true, 
			maxDate: 0
		});
		jQuery("#arfw_custom_report_product_start_date").datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true, 
			maxDate: 0
		});
		jQuery("#arfw_custom_report_product_end_date").datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true, 
			maxDate: 0
		});
		jQuery("#arfw_custom_report_customer_start_date").datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true, 
			maxDate: 0
		});
		jQuery("#arfw_custom_report_customer_end_date").datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true, 
			maxDate: 0
		});
		jQuery("#arfw_start_date").datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true, 
			maxDate: 0
		});
		jQuery("#arfw_end_date").datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true, 
			maxDate: 0
		});


		//searching order statuses on ajax request
		jQuery('.arfw_select').select2({
			ajax:{
				  url: ajaxUrl,
				  dataType: 'json',
				  delay: 200,
				  data: function (params) {
						return {
						q: params.term,
						action: 'arfw_search_for_order_status'
						};
				  },
				  processResults: function( data ) {
				  var options = [];
				  if ( data ) 
				  {
					$.each( data, function( index, text )
					{
						options.push( { id: text[0], text: text[1]  } );
					});
				  }
				  return {
					results:options
				  };
			  },
			  cache: true
		  },
		});

		//order status pie chart
		AmCharts.makeChart("arfw_order_status_piechart", {
			"type": "pie",
			"colors": ['#5793F3','#DB517C','#BC3B47','#FB9B35','#F2AD6A','#2A5673','#85C4C9'],
			"startDuration": 2,
			"theme": "light",
			"addClassNames": true,
			"outlineColor": "",
			"balloonText": "[[name]]:[[value]]",
			"innerRadius": "10%",
			"dataProvider": order_status_data,
			"valueField": "value",
			"radius": 100,
			"balloon": {
				"drop": false,
				"adjustBorderColor": false,
				"color": "#FFFFFF",
				"fontSize": 16
			},
			"responsive": {
				"enabled": true
			},
			"titles" : [{
				"text":order_status_title
			}]
		});
		
		//billing country chart
		AmCharts.makeChart("arfw_billing_country_chart", {
			"theme": "light",
			"type": "serial",
			"dataProvider": country_details,
			"color": "#000000",
			"bold": true,
			"valueAxes": [{
				"title": income_in_text+ currency_symbol
			}],
			"graphs": [{
				"balloonText": income_in_text+"[[category]]:"+currency_symbol+"[[value]]",
				"fillAlphas": 1,
				"lineAlpha": 0.2,
				"title": income_title,
				"type": "column",
				"valueField": "value",
				"fillColors": "#FF5E6D",
				"color": "#ffffff"
			}],
			"rotate": true,
			"categoryField": "title",
			"categoryAxis": {
				"gridPosition": "start",
				"fillAlpha": 0.05,
				"position": "left"
			},
			"responsive": {
				"enabled": true
			}
		});

		//billing country chart by date
		AmCharts.makeChart( "arfw_billing_country_chart_by_date", {
			"type": "pie",
			"theme": "light",
			"colors": ['#5793F3','#DB517C','#BC3B47','#FB9B35','#F2AD6A','#2A5673','#85C4C9'],
			"dataProvider": country_details_by_date,
			"valueField": "value",
			"titleField": "title",
			"balloon" : {
			 "fixedPosition":true
			},
			"responsive": {
				"enabled": true
			},
			"titleField": "title",
			"valueField": "value",
			"labelRadius": 5,

			"radius": "42%",
			"innerRadius": "60%",
			"depth3D": 10,
			"angle": 15
		});

		//top product chart
		AmCharts.makeChart("arfw_top_product_chart", {
			"theme": "light",
			"type": "serial",
			"startDuration": 2,
			"dataProvider": product_details,
			"color": "#000000",
			"valueAxes": [ {
				"axisAlpha": 0,
				"position": "left",
				"title": total_in_text+currency_symbol,
			}],
			"graphs": [{
				"balloonText": "[[category]]: <b>" + currency_symbol + "[[value]]</b>",
				"fillColorsField": "color",
				"fillAlphas": 1,
				"lineAlpha": 0.1,
				"type": "column",
				"valueField": "total",
			}],
			"depth3D": 20,
			"angle": 30,
			"chartCursor": {
				"categoryBalloonEnabled": false,
				"cursorAlpha": 0,
				"zoomable": false
			},
			"categoryField": "name",
			"categoryAxis": {
				"gridPosition": "start",
				"labelRotation": 45,
				"title": product_name_text
			},
			"responsive": {
				"enabled": true
			}
		
		});

		//top 5 product chart by date
		AmCharts.makeChart( "arfw_top_five_product_chart", {
			"type": "pie",
			"theme": "light",
			"colors": ['#5793F3','#DB517C','#BC3B47','#FB9B35','#F2AD6A','#2A5673','#85C4C9'],
			"dataProvider": top_five_products,
			"valueField": "total",
			"titleField": "name",
			"balloon":{
			 "fixedPosition":true
			},
			"responsive": {
				"enabled": true
			},
		});

		//report by month chart
		AmCharts.makeChart( "arfw_report_by_month", {
			"type": "serial",
			"addClassNames": true,
			"theme": "light",
			"autoMargins": true,
			"balloon": {
			  "adjustBorderColor": false,
			  "horizontalPadding": 10,
			  "verticalPadding": 8,
			  "color": "#ffffff"
			},
			"dataProvider": month_details,
			"valueAxes": [ {
			  "axisAlpha": 0,
			  "position": "left",
			  "title": total_in_text+currency_symbol,
			} ],
			"startDuration": 1,
			"graphs": [ {
			  "alphaField": "alpha",
			  "balloonText": "<span style='font-size:12px;'>[[title]]"+in_text+"[[category]]:<br><span style='font-size:20px;'>" + currency_symbol + "[[value]]</span> [[additional]]</span>",
			  "fillAlphas": 1,
			  "title": "Income",
			  "type": "column",
			  "valueField": "total",
			  "dashLengthField": "dashLengthColumn",
			  "fillColors": "#FF5E6D",
			  "outlineColor": ""
			}],
			"categoryField": "month",
			"categoryAxis": {
			  "gridPosition": "start",
			  "labelRotation": 45,
			  "title": month_text,
			},
			"responsive": {
				"enabled": true
			}
			
		});

		//report for top payment gateway chart
		AmCharts.makeChart("arfw_payment_gateway_chart", {
			"theme": "light",
			"type": "serial",
			"startDuration": 2,
			"dataProvider": payment_details,
			"valueAxes": [{
				"position": "left",
				"axisAlpha":0,
				"gridAlpha":0,
				"title": total_in_text+currency_symbol	
			}],
			"graphs": [{
				"balloonText": "[[category]]: <b>"+ currency_symbol+"[[value]]</b>",
				"colorField": "color",
				"fillAlphas": 0.85,
				"lineAlpha": 0.1,
				"type": "column",
				"topRadius":1,
				"valueField": "total"
			}],
			"depth3D": 40,
			"angle": 30,
			"chartCursor": {
				"categoryBalloonEnabled": false,
				"cursorAlpha": 0,
				"zoomable": false
			},
			"categoryField": "gateway",
			"fontSize": 12,
			"categoryAxis": {
				"gridPosition": "start",
				"axisAlpha":0,
				"gridAlpha":0,
				"labelRotation": 45,
				"title": payment_gateway_text
			},
			"responsive": {
				"enabled": true
			}
		
		}, 
		0);

		//report by day chart 
		AmCharts.makeChart("arfw_report_by_month_day", {
			"type": "serial",
			"theme": "light",
			"autoMargins": true,
			"dataProvider": day_details,
			"valueAxes": [ {
				"axisAlpha": 0,
				"position": "left",
				"title": total_in_text+currency,
			  } ],
			"graphs": [{
				"id":"g1",
				"balloonText": "[[category]]<br><b><span style='font-size:14px;'>"+ currency+"[[value]]</span></b>",
				"bullet": "round",
				"bulletSize": 12,
				"lineColor": "#d1655d",
				"lineThickness": 2,
				"negativeLineColor": "#637bb6",
				"type": "smoothedLine",
				"valueField": "total"
			}],
			"categoryField": "day",
			"categoryAxis": {
				"gridPosition": "start",
				"labelRotation": 45,
				"title": "Date"
			},
			"responsive": {
				"enabled": true
			}
		});

		//Drag for custom Report 
		$('.drag').draggable({
			cursor: "move",
			revert: true,
			distance: 50,
			containment: "parent",
			connectToSortable: "#dvdest",
			drop: function(event, ui) {
				$(this).find('ul').append(ui.draggable);
			}
		});
		
		//Drop for custom Report 
		$('.drop').droppable({

			hoverClass: "hoverDrop",
			tolerance: "pointer",
			drop: function(event, ui) {
				$(this).append(ui.draggable);
			}
		});
		
		//Custom Report Tab
		jQuery('#arfw-custom-report-columns a').click(function(e){
			e.preventDefault();
			var tab_id = jQuery(this).attr('href');
			jQuery('#arfw-custom-report-columns a').removeClass('current');
			jQuery(this).addClass('current');
			jQuery(this).parents('.arwf-custom-report-wrapper').find('.arwf-custom-report__tab-content > div').removeClass('current');
			jQuery(this).parents('.arwf-custom-report-wrapper').find(tab_id).addClass('current');
		});
		
		//Show/Hide Custom report date field for ORDER
		jQuery('#arfw_custom_report_order_period').on('click',function(){

			var option_value = this.value;

			if(option_value == 'custom'){
				jQuery('#arfw-custom-report-date-section-order').show();
			}
			else{
				jQuery('#arfw-custom-report-date-section-order').hide	();
			}
		});
		
		
		//Generate Custom Report for ORDER
		jQuery('#arfw-generate-custom-report').on('click',function(){
			
			var check_order = false;
			var html_order = "";

			var custom_report_elements = [];
			$('#dvdest li').each(function(i, li) {
				custom_report_elements.push($(this).data('name'));
			});

			if(custom_report_elements.length == 0){
				check_order = true;
				html_order += "<p class='notice_text' >"+select_columns+"</p>";
			}
			if(jQuery('#arfw_custom_report_order_status').val() == null){
				check_order = true;
				html_order += "<p class='notice_text' >"+select_order_status+"</p>";
			}

			if(jQuery('#arfw_custom_report_order_period').val() == 'custom'){
				if(jQuery('#arfw_custom_report_order_start_date').val() == '' || jQuery('#arfw_custom_report_order_end_date').val() == ''){
					check_order = true;
					html_order += "<p class='notice_text' >"+select_date+"</p>";
				}
			}
			

			if(jQuery('#arfw_custom_report_order_period').val() == null || jQuery('#arfw_custom_report_order_period').val() == 'select'){
				check_order = true;
				html_order += "<p class='notice_text' >"+select_period+"</p>";
			}
			if(check_order){

				$('.arfw_notice').html(html_order);
				$('.arfw_notice').slideDown();
				$('html, body').animate({
					scrollTop: $("body").offset().top

				} , 1000);
				
				$('.notice_text').delay(4000).fadeOut("slow");	
			}
			
			if(!check_order){

				var custom_report_order_status = jQuery('#arfw_custom_report_order_status').val();
				var custom_report_start_date = "";
				var custom_report_end_date = "";
				var option_value = jQuery('#arfw_custom_report_order_period').val();

				if(option_value == 'custom'){
					custom_report_start_date = jQuery('#arfw_custom_report_order_start_date').val();
					custom_report_end_date = jQuery('#arfw_custom_report_order_end_date').val();
				}

				jQuery.post(ajaxUrl,{
					action:'arfw_generate_custom_report_order_query', 
					column:custom_report_elements,
					start_date:custom_report_start_date, 
					end_date:custom_report_end_date, 
					status:custom_report_order_status, 
					option:option_value,
					arfwSecurity:arfwSecurity
				},
				function(data){
					
					if(data.length == 0){
						var btn_html = "<p class='notice_text' >"+empty_dataset+"</p>";
						
						$('.arfw_notice').html(btn_html);
						$('.notice_text').delay(4000).fadeOut("slow");	
					}
					else{
						// data=jQuery.parseJSON(data);
						mail_send_data = data;
						jQuery.post(ajaxUrl,{action:'arfw_custom_report_table', table_data:data,arfwSecurity:arfwSecurity},function(tableHtml){

							jQuery('#arfw-custom-report-table-order').html(tableHtml);
							
							$('html, body').animate({
								scrollTop: $("#arfw-custom-report-table-order").offset().top

							} , 1000);
						});	
						jQuery('.arfw-mail-sent').show();
						jQuery('#arfw_custom_report_mail').on('click',function(){
						
							jQuery('.arwf_email_popup_bg').show();
							
						});
					}
				});
			}
		});

		//Show/Hide Custom report data for PRODUCT
		jQuery('#arfw_custom_report_product_period').on('click',function(){

			var option_value = this.value;

			if(option_value == 'custom'){
				jQuery('#arfw-custom-report-date-section-product').show();
			}
			else{
				jQuery('#arfw-custom-report-date-section-product').hide	();
			}
		});

		//Custom Report Product Checkbox
		jQuery('#arfw-custom-report-checkbox-product').on('click',function(){
			var radio_button = jQuery('input[name=arfw_product_choice]:checked').val(); 

			if(radio_button == 'particular'){
				jQuery('#arfw-custom-report-select-product').show();
			}
			else{
				jQuery('#arfw-custom-report-select-product').hide();
			}
		});

		//Generate Custom Report for PRODUCT
		jQuery('#arfw-generate-custom-report-product').on('click',function(){

			var check_product = false;
			var html_product = "";

			var custom_report_elements_product = [];
			$('#dvdest_product li').each(function(i, li) {
				custom_report_elements_product.push($(this).data('name'));
			});

			if(custom_report_elements_product.length == 0){
				check_product = true;
				html_product += "<p class='notice_text' >"+select_columns+"</p>";
			}

			if(jQuery('#arfw_custom_report_product_period').val() == null || jQuery('#arfw_custom_report_product_period').val() == 'select'){
				check_product = true;
				html_product += "<p class='notice_text' >"+select_timeline+"</p>";
			}

			if(jQuery('#arfw_custom_report_product_period').val() == 'custom'){
				if(jQuery('#arfw_custom_report_product_start_date').val() == '' || jQuery('#arfw_custom_report_product_end_date').val() == ''){
					check_product = true;
					html_product += "<p> class='notice_text' "+select_date+"</p>";
				}
			}
			
			
			if(jQuery('input[name=arfw_product_choice]:checked').val() == null){
				check_product= true;
				html_product += "<p class='notice_text' >"+select_all_options+"</p>";
			}

			if(jQuery('input[name=arfw_product_choice]:checked').val() == 'particular'){

				if(jQuery('#arfw_product_name').val() == null || jQuery('#arfw_product_name').val() == 'select' ){
					check_product = true;
					html_product += "<p class='notice_text' >"+select_all_options+"</p>";
				}
			}
			
			if(check_product){

				
				$('.arfw_notice').html(html_product);
				$('.arfw_notice').slideDown();
				
				$('html, body').animate({
					scrollTop: $("body").offset().top

				} , 1000);
				$('.notice_text').delay(4000).fadeOut("slow");	
			}

			if(!check_product){

				var custom_report_start_date_product = "";
				var custom_report_end_date_product = "";
				var product_name = '';
				var timeline = jQuery('#arfw_custom_report_product_period').val();
				if(timeline == 'custom'){
					custom_report_start_date_product = jQuery('#arfw_custom_report_product_start_date').val();
					custom_report_end_date_product = jQuery('#arfw_custom_report_product_end_date').val();
				}
				if(jQuery('input[name=arfw_product_choice]:checked').val() == 'particular'){
					product_name = jQuery('#arfw_product_name').val();
				}

				jQuery.post(ajaxUrl,{
					action:'arfw_generate_custom_report_product_query',
					start_date:custom_report_start_date_product,
					end_date:custom_report_end_date_product,
					timeline:timeline,
					product_name:product_name,
					column:custom_report_elements_product,
					arfwSecurity:arfwSecurity
				},
				function(product_data)
				{
					if(product_data.length == 0){

						var prod_btn_html = "<p class='notice_text' >"+empty_dataset+"</p>";
						
						$('.arfw_notice').html(prod_btn_html);
						$('html, body').animate({
							scrollTop: $("body").offset().top

						} , 1000);
						$('.notice_text').delay(4000).fadeOut("slow");	
					}
					else{

						// product_data = jQuery.parseJSON(product_data);
						mail_send_data = product_data;
						jQuery.post(ajaxUrl,{action:'arfw_custom_report_table',table_data:product_data,arfwSecurity:arfwSecurity},function(product_table_html){

							jQuery('#arfw-custom-report-table-product').html(product_table_html);
							
							$('html, body').animate({
								scrollTop: $("#arfw-custom-report-table-product").offset().top

							} , 1000);
						});
						jQuery('.arfw-mail-sent').show();

						jQuery('.arfw-mail-sent__btn').on('click',function(){
							jQuery('.arwf_email_popup_bg').show();

						});
						
							
					}
				});
			}
		});

		//Show/Hide Custom report date field for CUSTOMER
		jQuery('#arfw_custom_report_customer_period').on('click',function(){

			var option_value = this.value;

			if(option_value == 'custom'){
				jQuery('#arfw-custom-report-date-section-customer').show();
			}
			else{
				jQuery('#arfw-custom-report-date-section-customer').hide();
			}
		});

		//Custom Report Customer Options
		jQuery('#arfw_custom_report_customer_option').on('click',function(){

			var option_value = this.value;

			if(option_value == 'particular'){
				jQuery('#arfw-custom-report-customer-email').show();
			}
			else{
				jQuery('#arfw-custom-report-customer-email').hide();
			}
		});
		
		//Generate Custom Report for CUSTOMER
		jQuery('#arfw-generate-custom-report-customer').on('click',function(){
			
			var check_customer = false;
			var html_customer = '';

			var custom_report_elements_customer = [];
			$('#dvdest_customer li').each(function(i, li) {
				custom_report_elements_customer.push($(this).data('name'));
			});

			if(custom_report_elements_customer.length == 0){
				check_customer = true;
				html_customer += "<p class='notice_text' >"+select_columns+"</p>";
			}

			if(jQuery('#arfw_custom_report_customer_period').val() == null || jQuery('#arfw_custom_report_customer_period').val() == 'select'){
				check_customer = true;
				html_customer += "<p class='notice_text' >"+select_timeline+"</p>";
			}

			if(jQuery('#arfw_custom_report_customer_period').val() == 'custom'){
				if(jQuery('#arfw_custom_report_customer_start_date').val() == '' || jQuery('#arfw_custom_report_customer_end_date').val() == ''){
					check_customer = true;
					html_customer += "<p class='notice_text' >"+select_date+"</p>";
				}
			}
			
			if(jQuery('#arfw_custom_report_customer_option').val() == null || jQuery('#arfw_custom_report_customer_option').val() == 'select'){
				check_customer = true;
				html_customer += "<p class='notice_text' >"+select_all_options+"</p>";
			}

			if(jQuery('#arfw_custom_report_customer_option').val() == 'particular' ){

				if(jQuery('#arfw_custom_report_customer_email').val() == ''){
					check_customer = true;
					html_customer += "<p class='notice_text' >"+enter_email+"</p>";
				}
				
			}

			if(check_customer){

				
				$('.arfw_notice').html(html_customer);
				$('.arfw_notice').slideDown();
				$('html, body').animate({
					scrollTop: $("body").offset().top

				} , 1000);
				$('.notice_text').delay(4000).fadeOut("slow");	
			}

			if(!check_customer){
				var customer_timeline = jQuery('#arfw_custom_report_customer_period').val();
				var customer_start_date = "";
				var customer_end_date = "";
				var customer_email = "";
				if(customer_timeline == 'custom'){
					customer_start_date = jQuery('#arfw_custom_report_customer_start_date').val();
					customer_end_date = jQuery('#arfw_custom_report_customer_end_date').val();
				}
				var customer_option = jQuery('#arfw_custom_report_customer_option').val();

				if(customer_option == 'particular'){
					customer_email = jQuery('#arfw_custom_report_customer_email').val();
				}
				jQuery.post(ajaxUrl,{
					action:'arfw_generate_custom_report_customer_query',
					customer_column:custom_report_elements_customer,
					customer_timeline:customer_timeline,
					customer_start_date:customer_start_date,
					customer_end_date:customer_end_date,
					customer_option:customer_option,
					customer_email:customer_email,
					arfwSecurity:arfwSecurity
				},function(customer_data){

					if(customer_data.length == 0){
						var btn_html = "<p class='notice_text' >"+empty_dataset+"</p>";
						
						$('.arfw_notice').html(btn_html);
						$('.notice_text').delay(4000).fadeOut("slow");	
					}
					else{

						// customer_data = jQuery.parseJSON(customer_data);
						mail_send_data = customer_data;
						jQuery.post(ajaxUrl,{action:'arfw_custom_report_table',table_data:customer_data,arfwSecurity:arfwSecurity},function(product_table_html){

							jQuery('#arfw-custom-report-table-customer').html(product_table_html);
							
							$('html, body').animate({
								scrollTop: $("#arfw-custom-report-table-customer").offset().top

							} , 1000);
						});
						jQuery('.arfw-mail-sent').show();
						jQuery('.arfw-mail-sent__btn').on('click',function(){
							jQuery('.arwf_email_popup_bg').show();
						});					
					}
					
				});
			}
		});
		jQuery('#arfw_close').on('click',function(){

			jQuery('.arwf_email_popup_bg').hide();
		});
		jQuery('#arfw_email_send').on('click',function(){
			jQuery('#arfw_close').hide();
			var email_recipient = jQuery('#arfw_email_recipient').val();
			//var email_recipient_array = email_recipient.split(',');
			var email_subject = jQuery('#arfw_email_subject').val();
			var cust_email_body = jQuery('#arfw_email_body').val();
			jQuery('#arfw_gif').show();

			jQuery.post(ajaxUrl,{action:'arfw_send_mail',data:mail_send_data, recipient:email_recipient,subject:email_subject, email_body:cust_email_body, arfwSecurity:arfwSecurity},function(mail_response){
				jQuery('#arfw_gif').hide();
				
				var btn_html = "<p class='notice_text' >"+mail_response+"</p>";
	
				$('.arfw_notice').html(btn_html);
				$('html, body').animate({
					scrollTop: $("body").offset().top

				} , 1000);
				$('.notice_text').delay(4000).fadeOut("slow");	
				jQuery('.arwf_email_popup_bg').hide();
			});
			mail_send_data = '';
		});
	});
	
})( jQuery );
 