<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Advanced_Woocommerce_Reports
 * @subpackage Advanced_Woocommerce_Reports/admin
 * @author     MakeWebBetter <webmaster@makewebbetter.com>
 */
class Advanced_Woocommerce_Reports_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/advanced-woocommerce-reports-admin.css', array(), $this->version, 'all' );

		wp_enqueue_style( 'jquery_daterange_picker_css', plugin_dir_url( __FILE__ ) . 'css/daterangepicker.css', array(), $this->version, 'all' );
		
		wp_enqueue_style( 'jquery_animate_css', plugin_dir_url( __FILE__ ) . 'css/animate.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_register_script( 'woocommerce_admin', WC()->plugin_url() . '/assets/js/admin/woocommerce_admin.js', array( 'jquery', 'jquery-blockui', 'jquery-ui-sortable', 'jquery-ui-widget', 'jquery-ui-core', 'jquery-tiptip', 'wc-enhanced-select' ), WC_VERSION );

		wp_enqueue_script( 'jquery-ui-datepicker' );

		wp_enqueue_script( 'woocommerce_select2', WC()->plugin_url() .'/assets/js/select2/select2.min.js', array( 'jquery' ), $this->version, false );

		wp_enqueue_script('jquery-ui-draggable');

		wp_enqueue_script('jquery-ui-droppable');

		wp_enqueue_script( 'arfw_amchart_script', plugin_dir_url( __FILE__ ) . 'js/amcharts.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'arfw_amchart_moment', plugin_dir_url( __FILE__ ) . 'js/moment.min.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'arfw_amchart_light', plugin_dir_url( __FILE__ ) . 'js/light.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'arfw_amchart_pie', plugin_dir_url( __FILE__ ) . 'js/pie.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'arfw_amchart_serial', plugin_dir_url( __FILE__ ) . 'js/serial.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'arfw_amchart_ammap', plugin_dir_url( __FILE__ ) . 'js/ammap.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'arfw_amchart_world_high', plugin_dir_url( __FILE__ ) . 'js/worldHigh.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'arfw_amchart_funnel', plugin_dir_url( __FILE__ ) . 'js/funnel.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'arfw_amchart_responsive', plugin_dir_url( __FILE__ ) . 'js/responsive.js', array( 'jquery','arfw_amchart_funnel' ), null, true );
		wp_enqueue_script( 'arfw_janimate', plugin_dir_url( __FILE__ ) . 'js/janimate.min.js', array( 'jquery','arfw_amchart_funnel' ), null, true );
		
		wp_enqueue_script( 'mwb_main_js', plugin_dir_url( __FILE__ ) . 'js/advanced-woocommerce-reports-admin.js', array( 'arfw_janimate','arfw_amchart_script','arfw_amchart_moment','arfw_amchart_light','arfw_amchart_pie', 'arfw_amchart_serial','arfw_amchart_world_high','arfw_amchart_ammap','arfw_amchart_funnel'), $this->version, false );
		
		wp_localize_script('mwb_main_js',
				'local_msg', array( 
				'ajaxUrl' 				=> admin_url('admin-ajax.php'),
				'arfwSecurity' 			=> wp_create_nonce( 'arfw_security'),
				'select_columns' 		=> __('Please Select column for report.','arf-woo'),
				'select_order_status' 	=> __('Please Select Order Status.','arf-woo'),
				'select_date' 			=> __('Please select date.','arf-woo'),
				'select_period'			=> __('Please select order period.','arf-woo'),
				'select_timeline'		=> __('Please select Timeline.','arf-woo'),
				'select_all_options'	=> __('Select all options.', 'arf-woo'),
				'enter_email'			=> __('Enter search email.','arf-woo'),
				'empty_dataset'			=> __('Sorry no data found !','arf-woo'),
				'order_status_title' 	=> __('Order Status','arf-woo'),
				'income_in_text'		=> __('Income in ', 'arf-woo'),
				'income_title'			=> __('Income','arf-woo'),
				'total_in_text'			=> __('Total in ','arf-woo'),
				'product_name_text'		=> __('Product Name ','arf-woo'),
				'in_text'				=> __(' in ','arf-woo'),
				'month_text'			=> __('Month','arf-woo'),
				'payment_gateway_text'	=> __('Payment Gateway ','arf-woo'),


			)
		);
		
	}
	/**
	 * Add advance reports tab.
	 *
	 * @since    1.0.0
	 * @param Array $reports  Exsisting reports nav data.
	 * @access public
	 * @return array Added section for advance report nav tab data.
	 */
	public function arfw_add_advance_report_tab($reports){

		$report_summary = array(
			'title' 		=> __('Summary', 'arf-woo'),
			'description'	=> '',
			'hide_title' 	=> true,
			'callback'		=> array('Summary_Report', 'get_summary_report'),
		);
		$rep_by_date = array(
			'title'			=> __( 'Advance Report by Date', 'arf-woo' ),
			'description'	=> '',
			'hide_title'	=> true,
			'callback'		=> array( 'Advanced_Reports_By_Date', 'get_advanced_report_by_date' ),
		);
		$rep_mail = array(
			'title'		  	=> __('Custom Report', 'arf-woo'),
			'description' 	=> '',
			'hide_title'  	=> true,
			'callback'    	=> array( 'Advanced_Custom_Reports', 'get_started')
		);
		$lic_check = array(
			'title'		  	=> __('License Activation', 'arf-woo'),
			'description' 	=> '',
			'hide_title'  	=> true,
			'callback'    	=> array( 'License_Activation', 'get_started')
		);
		
		if(self::check_lic_validity()){
			$report_array['report_summary'] = $report_summary;
			$report_array['rep_by_date'] = $rep_by_date;
			$report_array['rep_mail'] = $rep_mail;
		}
		
		if(!self::check_lic_validity()){ 

			if(self::check_lic_days() >= 0){
				$report_array['report_summary'] = $report_summary;
				$report_array['rep_by_date'] = $rep_by_date;
				$report_array['rep_mail'] = $rep_mail;
				$report_array['lic_check'] = $lic_check;
			}
			else{
				$report_array['report_summary'] = $lic_check;
			}
			
		}

		
		
		$reports['mwb_advance'] = array(
			'title'   => __( 'Advance Reports', 'arf-woo' ),
			'reports' => $report_array,
		);

		
		return $reports;
	}

	/**
	 * Reports Page Link
	 * @since    1.0.0
	 * @author  MakeWebBetter
	 * @link  https://makewebbetter.com/
	 */

	function arfw_report_page( $actions, $plugin_file ) {
	
		$plugin;

		if ( !isset( $plugin ) ) {
	
			$plugin = plugin_basename ( __FILE__ );
		}

		if ( $plugin == $plugin_file ) {

			$settings = array (
				'report' => '<a href="' . admin_url ( 'admin.php?page=wc-reports&tab=mwb_advance&report=report_summary' ) . '">' . __ ( 'Advanced Reports', 'arf-woo' ) . '</a>',
			);

			$actions = array_merge ( $settings, $actions );
		}

		return $actions;
	}

	//Search for order status --AJAX--
	public function arfw_search_for_order_status(){

		$order_statuses = wc_get_order_statuses();

		$return = array();

		if( !empty( $order_statuses ) ) {

			foreach ( $order_statuses as $status_key => $single_status ) {

				$return[] = array( $status_key, $single_status );
			}
		}
		echo json_encode( $return );

		wp_die();
	}
	
	//get $_POST data and return custom reprt for ORDER --AJAX--
	public function arfw_generate_custom_report_order_query(){

		// check the nonce sercurity.
		check_ajax_referer('arfw_security', 'arfwSecurity');

		$custom_report_elements = isset($_POST['column']) ? $_POST['column'] : array() ;
		$option = isset($_POST['option']) ? sanitize_text_field($_POST['option']) : "";
		$status = isset($_POST['status']) ? $_POST['status'] : array() ;

		if($option == 'custom'){

			$start_date = isset($_POST['start_date']) ? sanitize_text_field($_POST['start_date']) : "";
			$end_date = isset($_POST['end_date']) ? sanitize_text_field($_POST['end_date']) : "";
			

			update_option('arfw_custom_report_order_start_date',$start_date);
			update_option('arfw_custom_report_order_end_date',$end_date);
			
		}
		else{

			$start_date = '';
			$end_date = '';

			update_option('arfw_custom_report_order_start_date','');
			update_option('arfw_custom_report_order_end_date','');

		}
		update_option('arfw_custom_report_order_status',$status);
		
		$custom_report_data = self::arfw_custom_report_order($custom_report_elements, $option, $start_date, $end_date, $status);
		
		if(!empty($custom_report_data)){
			echo json_encode($custom_report_data);
		}
		else{
			echo "";
		}
		wp_die();
	}

	//Generate custom report for ORDER
	public static function arfw_custom_report_order($data, $option, $start, $end, $statuses){
		
		global $wpdb;
		
		if($option == 'today'){

			$start = date_i18n("Y-m-d");
			$end = date_i18n("Y-m-d");
		}
		elseif($option == 'days'){

			$end = date_i18n("Y-m-d");
			$start = date_i18n("Y-m-d", strtotime("-7 days"));
		}
		elseif($option == 'month'){

			$end = date_i18n("Y-m-d");
			$start = date_i18n("Y-m-d", strtotime("-1 months", strtotime($start)));
		}
		elseif($option == 'year'){

			$end = date_i18n("Y-m-d");
			$start = date_i18n("Y-m-d", strtotime("-12 months", strtotime($start)));
		}
        $custom_query = "SELECT posts.ID as order_id, 
            posts.post_status as order_status ,
            order_items.order_item_id as order_item_id ,
            date_format( posts.post_date, '%Y-%m-%d') as order_date ,
            order_items.order_item_name as order_item_name 
            FROM {$wpdb->prefix}posts as posts 
            LEFT JOIN {$wpdb->prefix}woocommerce_order_items as order_items 
            ON order_items.order_id=posts.ID 
            WHERE 1 = 1 
            AND posts.post_type ='shop_order' 
			AND order_items.order_item_type ='line_item' ";
		
		if(!empty($statuses) ){
			if(count($statuses)>0){
				$order_status = implode("', '", $statuses);
				$custom_query .= " AND posts.post_status IN ('{$order_status}')   ";
			}
			
		}
	

		if(!empty($start) && !empty($end)){

			$custom_query .= " AND date_format(posts.post_date,'%Y-%m-%d') BETWEEN '{$start}' AND 
			'{$end}' ";
		}

        $custom_query .= " ORDER BY posts.post_date DESC ";    

		$query_result = $wpdb->get_results($custom_query);

		if (!empty($query_result) && is_array($query_result)){

            foreach($query_result as $key => $value){

                $order_id = $value->order_id;

                $order_item_id = $value->order_item_id;

                $single_order_detail = self::get_order_meta($order_id);
                
                if(!empty($single_order_detail) && is_array($single_order_detail)){

                    foreach ($single_order_detail as $single_order_key => $single_order_value) {
                        
						$single_order_key = substr($single_order_key,1);

						if(in_array($single_order_key,$data)){

							$query_result[$key]->$single_order_key = $single_order_value;

						}	
                        
                    }
                }
                
                $order_item_query = "SELECT * FROM 
                    {$wpdb->prefix}woocommerce_order_itemmeta as item_meta 
                    WHERE item_meta.order_item_id = {$order_item_id}";

                $order_item_result = $wpdb->get_results($order_item_query);
                
                if(!empty($order_item_result) && is_array($order_item_result)){

                    foreach ($order_item_result as $order_item_key => $order_item_value) {
                        
						$new_key = substr($order_item_value->meta_key,1);
						
						if(in_array($new_key,$data)){

							$query_result[$key]->$new_key  = $order_item_value->meta_value;

						}
                    }
                }
            }
			$result = array();
			$table_heading = array();
			$table_heading['order_id'] 				= __('Order Id','arf-woo');
			$table_heading['order_status'] 			= __('Order Status','arf-woo');
			$table_heading['order_date'] 			= __('Order date','arf-woo');
			$table_heading['order_item_name'] 		= __('Order Item Name','arf-woo');
			$table_heading['payment_method'] 		= __('Payment Method','arf-woo');
			$table_heading['customer_ip_address']	= __('Customer IP Aaddress','arf-woo');
			$table_heading['billing_first_name'] 	= __('Billing First Name','arf-woo');
			$table_heading['billing_last_name'] 	= __('Billing Last Name','arf-woo');
			$table_heading['billing_company'] 		= __('Billing Company','arf-woo');
			$table_heading['billing_address_1'] 	= __('Billing Address 1','arf-woo');
			$table_heading['billing_address_2'] 	= __('Billing Address 2','arf-woo');
			$table_heading['billing_city'] 			= __('Billing City','arf-woo');
			$table_heading['billing_state'] 		= __('Billing State','arf-woo');
			$table_heading['billing_postcode'] 		= __('Billing Post Code','arf-woo');
			$table_heading['billing_country']	 	= __('Billing Country','arf-woo');
			$table_heading['billing_email'] 		= __('Billing Email','arf-woo');
			$table_heading['billing_phone'] 		= __('Billing Phone','arf-woo');
			$table_heading['shipping_first_name']	= __('Shipping First Name','arf-woo');
			$table_heading['shipping_last_name']	= __('Shipping Last Name','arf-woo');
			$table_heading['shipping_company'] 		= __('Shipping Company','arf-woo');
			$table_heading['shipping_address_1'] 	= __('Shipping Address 2','arf-woo');
			$table_heading['shipping_address_2'] 	= __('Shipping Address 1','arf-woo');
			$table_heading['shipping_city'] 		= __('Shipping City','arf-woo');
			$table_heading['shipping_state'] 		= __('Shipping State','arf-woo');
			$table_heading['shipping_postcode'] 	= __('Shipping Postcode','arf-woo');
			$table_heading['shipping_country'] 		= __('Shipping Country','arf-woo');
			$table_heading['order_currency'] 		= __('Currency','arf-woo');
			$table_heading['cart_discount'] 		= __('Discount','arf-woo');
			$table_heading['cart_discount_tax'] 	= __('Discount Tax','arf-woo');
			$table_heading['order_shipping'] 		= __('Shipping amount','arf-woo');
			$table_heading['order_shipping_tax'] 	= __('Shipping tax','arf-woo');
			$table_heading['order_tax'] 			= __('Order Tax','arf-woo');
			$table_heading['order_total']	 		= __('Order Total','arf-woo');
			$table_heading['product_id'] 			= __('Product Id','arf-woo');
			$table_heading['variation_id'] 			= __('Variation Id','arf-woo');
			// $table_heading['line_subtotal'] 		= __('Line Subtotal';
			// $table_heading['line_subtotal_tax'] 	= 'Line Subtotal Tax';
			// $table_heading['line_total'] 			= 'Line Total';
			// $table_heading['line_tax'] 				= 'Line Tax';
			foreach($query_result as $key => $value){

				
				foreach($value as $result_key => $result_value){

					if(in_array($result_key,$data)){

						if(array_key_exists($result_key,$table_heading)){
							$new_key_order = $table_heading[$result_key];
							
							$result[$key][$new_key_order] = $result_value;
							
						}
						
					}
				}
			}
            return $result;
		}
		else{

			$result = "";
			return $result;
		}
    }

	//Get information for particular ORDER
    public static function get_order_meta($oid){

        $details = get_post_meta($oid);
        
        $order = array();

        foreach ($details as $key => $value) {
            
			$order[$key] = $value[0];
			
		}
		
        return $order;
	}

	//Get list of products in the store
	public static function arfw_get_products_list(){

		global $wpdb;

		$query = "SELECT
			ID, post_title 
			FROM {$wpdb->prefix}posts as posts 
			WHERE posts.post_type = 'product' 
			AND posts.post_status= 'publish' 
			";
		
		$all_products = $wpdb->get_results($query);

		return $all_products;
	}
	
	//Get $_POST data and return custom reprt for PRODUCT --AJAX--
	public static function arfw_generate_custom_report_product_query(){

		// check the nonce sercurity.
		check_ajax_referer('arfw_security', 'arfwSecurity');

		$timeline 		= isset($_POST['timeline']) ? sanitize_text_field($_POST['timeline']) : "";
		$product_id 	= isset($_POST['product_name']) ? sanitize_text_field($_POST['product_name']) : "";
		$column 		= isset($_POST['column']) ? $_POST['column'] : array() ;
		$product_name 	= '';

		if(!empty($product_id)){
			$product_name 	=  wc_get_product( $product_id )->get_name();
		}
		
		if($timeline == 'custom'){
			$start_date 	= isset($_POST['start_date']) ? sanitize_text_field($_POST['start_date']) : "";
			$end_date 		= isset($_POST['end_date']) ? sanitize_text_field($_POST['end_date']) : "";

			update_option('arfw_custom_report_product_start_date',$start_date);
			update_option('arfw_custom_report_product_end_date',$end_date);
		}
		else{
			$start_date 	= '';
			$end_date 		= '';

			update_option('arfw_custom_report_product_start_date','');
			update_option('arfw_custom_report_product_end_date','');
		}

		if( !empty($product_name) && $product_name != 'select'){

			$custom_report_product = self::arfw_custom_report_product($column,$product_name,$timeline,$start_date,$end_date);
		}
		else{
			$custom_report_product = self::arfw_custom_report_product($column,'',$timeline,$start_date,$end_date);
		}
		
		if(!empty($custom_report_product)){
			echo json_encode($custom_report_product);
		}
		else{
			echo "";
		}
		
		wp_die();
	}

	//Generate cusstom report PRODUCT
	public static function arfw_custom_report_product($column_data, $product=NULL,$option=NULL,$start_date=NULL,$end_date=NULL){

		
		global $wpdb;
		if($option == 'today'){

			$start_date 	= date_i18n("Y-m-d");
			$end_date 		= date_i18n("Y-m-d");
		}
		elseif($option == 'days'){

			$end_date 		= date_i18n("Y-m-d");
			$start_date 	= date_i18n("Y-m-d", strtotime("-7 days"));
		}
		elseif($option == 'month'){

			$end_date 		= date_i18n("Y-m-d");
			$start_date 	= date_i18n("Y-m-d", strtotime("-1 months", strtotime($end_date)));
		}
		elseif($option == 'year'){

			$end_date 		= date_i18n("Y-m-d");
			$start_date 	= date_i18n("Y-m-d", strtotime("-12 months", strtotime($end_date)));
		}	
		$top_products = "SELECT 
			item_name.order_item_name as name, 
			product_id.meta_value as id,
			SUM(quantity.meta_value) as quantity ,
			SUM(total.meta_value) as  total			
			FROM {$wpdb->prefix}posts as posts 
			LEFT JOIN {$wpdb->prefix}woocommerce_order_items as item_name 
			ON item_name.order_id=posts.ID 
			LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as product_id 
			ON product_id.order_item_id=item_name.order_item_id 
			LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as total 
			ON total.order_item_id=item_name.order_item_id 
			LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as quantity 
			ON quantity.order_item_id=item_name.order_item_id 
			WHERE 1 = 1 ";

			if(!empty($product)){
				$top_products .= "AND item_name.order_item_name = '{$product}'";
			}
			$top_products .= " AND posts.post_type ='shop_order' 
				AND item_name.order_item_type ='line_item' 
				AND product_id.meta_key ='_product_id' 
				AND total.meta_key ='_line_total' 
				AND quantity.meta_key ='_qty' 
				AND date_format(posts.post_date ,'%Y-%m-%d') BETWEEN '{$start_date}' AND '{$end_date}' 
				AND posts.post_status NOT IN ('trash') 
 				GROUP BY product_id.meta_value
				order by SUM(total.meta_value) DESC
				
				";

		$query_result = $wpdb->get_results($top_products);

		$product_result = array();
		$table_heading = array();
		$table_heading['id'] 					= __('Product Id','arf-woo');
		$table_heading['name'] 					= __('Product Name','arf-woo');
		$table_heading['quantity'] 				= __('Qunatity Sold','arf-woo');
		$table_heading['total']					= __('Revenue','arf-woo');

		foreach($query_result as $key => $value){

			foreach($value as $single_key => $single_value){

				if(in_array($single_key,$column_data)){

					if(array_key_exists($single_key,$table_heading)){

						$table_key = $table_heading[$single_key];
						
						if($table_key == 'Revenue'){
							$product_result[$key][$table_key] = round($single_value,2);
						}else{
							$product_result[$key][$table_key] = $single_value;
						}
						
					}
					
				}
			}
		}
		
		return $product_result;
	}

	//Get $_POST data and return custom reprt for PRODUCT --AJAX--
	public static function arfw_generate_custom_report_customer_query(){

		$customer_column = isset($_POST['customer_column']) ? $_POST['customer_column'] : array() ;
		$customer_timeline = isset($_POST['customer_timeline']) ? $_POST['customer_timeline'] : "";

		if($customer_timeline == 'custom'){
			$customer_start_date = $_POST['customer_start_date'];
			$customer_end_date = $_POST['customer_end_date'];

			update_option('arfw_custom_report_customer_start_date',$customer_start_date);
			update_option('arfw_custom_report_customer_end_date',$customer_end_date);
		}
		else{

			$customer_start_date = '';
			$customer_end_date = '';

			update_option('arfw_custom_report_customer_start_date','');
			update_option('arfw_custom_report_customer_end_date','');
		}

		$customer_option = isset($_POST['customer_option']) ? sanitize_text_field($_POST['customer_option']) : "" ;

		if($customer_option == 'particular'){

			$customer_email = isset($_POST['customer_email']) ? sanitize_email($_POST['customer_email']) : "" ;
		}
		else{
			$customer_email = '';
		}


		$custom_report_customer = self::arfw_custom_report_customer($customer_column, $customer_email, $customer_start_date,$customer_end_date,$customer_timeline);

		if(!empty($custom_report_customer)){

			echo json_encode($custom_report_customer);
		}
		else{
			echo "";
		}
		wp_die();
	}

	//Generate cusstom report CUSTOMER
	public static function arfw_custom_report_customer($column, $email=NULL, $start_date=NULL, $end_date=NULL,$option=NULL){

		global $wpdb;
		if($option == 'today'){

			$start_date 	= date_i18n("Y-m-d");
			$end_date 		= date_i18n("Y-m-d");
		}
		elseif($option == 'days'){

			$end_date 		= date_i18n("Y-m-d");
			$start_date 	= date_i18n("Y-m-d", strtotime("-7 days"));
		}
		elseif($option == 'month'){

			$end_date 		= date_i18n("Y-m-d");
			$start_date 	= date_i18n("Y-m-d", strtotime("-1 months", strtotime($end_date)));
		}
		elseif($option == 'year'){

			$end_date 		= date_i18n("Y-m-d");
			$start_date 	= date_i18n("Y-m-d", strtotime("-12 months", strtotime($end_date)));
		}	
		$customer_query = "SELECT SUM(total_meta.meta_value) AS Total, 
			email_meta.meta_value AS BillingEmail, 
			fname_meta.meta_value AS FirstName, 
			lname_mata.meta_value AS LastName,
			CONCAT(fname_meta.meta_value, ' ',lname_mata.meta_value) AS BillingName, 
			Count(email_meta.meta_value) AS OrderCount ,
			postmeta.meta_value AS user_id
			FROM {$wpdb->prefix}postmeta AS postmeta
			LEFT JOIN {$wpdb->prefix}posts AS posts
			ON postmeta.post_id = posts.ID
			LEFT JOIN {$wpdb->prefix}postmeta as total_meta ON total_meta.post_id = posts.ID
			LEFT JOIN {$wpdb->prefix}postmeta as email_meta ON email_meta.post_id = posts.ID
			LEFT JOIN {$wpdb->prefix}postmeta as fname_meta ON fname_meta.post_id = posts.ID
			LEFT JOIN {$wpdb->prefix}postmeta as lname_mata ON lname_mata.post_id = posts.ID
			WHERE posts.post_type = 'shop_order'
			AND total_meta.meta_key='_order_total' 
			AND email_meta.meta_key='_billing_email'  
			AND fname_meta.meta_key='_billing_first_name'
			AND lname_mata.meta_key='_billing_last_name'
			AND postmeta.meta_key = '_customer_user' ";
		
		if(!empty($email)){
			$customer_query .= " AND email_meta.meta_value = '{$email}' ";
		}

		if(!empty($start_date) && !empty($end_date)){
			$customer_query .= " AND date_format(posts.post_date ,'%Y-%m-%d') BETWEEN '{$start_date}' AND '{$end_date}' ";
		}
		$customer_query .= " AND posts.post_status NOT IN ('trash') 
			GROUP BY email_meta.meta_value 
			Order By Total
			DESC";

		$query_result = $wpdb->get_results($customer_query);

		$customer_result = array();
		$table_heading = array();
		$table_heading['FirstName'] 			= __('First Name','arf-woo');
		$table_heading['LastName'] 				= __('Last Name','arf-woo');
		$table_heading['BillingName'] 			= __('Billing Name','arf-woo');
		$table_heading['BillingEmail'] 			= __('Billing Email','arf-woo');
		$table_heading['OrderCount'] 			= __('Order Count','arf-woo');
		$table_heading['Total'] 				= __('Total Revenue','arf-woo');
		$table_heading['user_id'] 				= __('Customer/Guest','arf-woo');
		foreach($query_result as $key => $value){

			foreach($value as $single_key => $single_value){

				if($single_key == 'user_id'){

					if($single_value == 0){
						$query_result[$key]->$single_key = 'Guest';
					}
					else{
						$query_result[$key]->$single_key = 'Customer';
					}
				}
				if(in_array($single_key,$column)){

					if(array_key_exists($single_key,$table_heading)){
						
						$table_key = $table_heading[$single_key];

						if($table_key == 'Total Revenue'){
							$customer_result[$key][$table_key] = round($single_value,2);
						}
						elseif($table_key == 'Customer/Guest' && $single_value == 0){
							$customer_result[$key][$table_key] = 'Guest';
						}
						elseif($table_key == 'Customer/Guest' && $single_value != 0){
							$customer_result[$key][$table_key] = 'Customer';
						}
						else{
							$customer_result[$key][$table_key] = $single_value;
						}
					}
				}
			}
		}
		
		return $customer_result;
	}
	//Generate custom report table --AJAX--
	public static function arfw_custom_report_table(){


		$currency_symbol = get_woocommerce_currency_symbol();

		$data = !empty($_POST['table_data']) ? wp_unslash( $_POST['table_data'] ) : array() ;

		$data = json_decode( $data, true );

		$html = '';

		$html .= '<table><thead><tr>';
		
		foreach($data as $k => $v){

			foreach($v as $key => $value){

				$html .= '<th>';
				$html .= $key;
				$html .= '</th>';

			}
			break;
		}
		
		$html .= '</tr></thead><tbody>';
		
		foreach($data as $k => $v){
			$html .= '<tr>';
			foreach($v as $key => $value){

				if($value == ''){
					$value = '--';
				}
				if( $key == 'Total Revenue' || 
					$key == 'Revenue' || 
					$key == 'Shipping amount'  || 
					$key == 'Shipping tax' || 
					$key == 'Order Tax' || 
					$key == 'Order Total' || 
					$key == 'Line Subtotal' || 
					$key == 'Line Subtotal Tax' || 
					$key == 'Line Total' || 
					$key == 'Line Tax' || 
					$key == 'Discount' || 
					$key == 'Discount Tax') {
					$html .= '<td><b>'.$currency_symbol.'</b>'.sanitize_text_field($value).'</td>';
				}
				elseif($key == 'Order Id'){
					$order_link = '<a href="'.admin_url('post.php?post='.sanitize_text_field($value).'&action=edit').'" target="_blank">'.$value.'</a>';
					$html .= '<td>'.$order_link.'</td>';
				}
				else{
					$html .= '<td>'.sanitize_text_field($value).'</td>';
				}
				
			}
			$html .= '</tr>';
		}
		$html .= '</tbody></table>';
		
		echo $html;
		wp_die();
		
	}

	//Send mail --AJAX--
	public static function arfw_send_mail(){

		// check the nonce sercurity.
		check_ajax_referer('arfw_security', 'arfwSecurity');

		$data = isset($_POST['data']) ? wp_unslash( $_POST['data'] ) : array();

		$data = json_decode( $data, true );

		$recipient = isset($_POST['recipient']) ? sanitize_text_field($_POST['recipient']) : "";

		$subject = isset($_POST['subject']) ? sanitize_text_field($_POST['subject']) : "";

		$email_body = isset($_POST['email_body']) ? sanitize_text_field($_POST['email_body']) : "";
		
		if (!filter_var($recipient, FILTER_VALIDATE_EMAIL)) {
			$error = __("Please check the email address.","arf-woo");
			echo $error;
			wp_die();
		}

		if(empty($subject)){

			$error = __("Please specify email subject.","arf-woo");
			echo $error;
			wp_die();
		}

		if(empty($email_body)){

			$error = __("Please specify email body text.","arf-woo");
			echo $error;
			wp_die();
		}

		if(empty($data)){
			$error = __("Fail","arf-woo");
			echo $error;
			wp_die();
		}
		$filepath = WP_CONTENT_DIR.'/uploads/mwb-advanced-report.csv';

		foreach ($data as $key=>$value){

			foreach($value as $k => $v){

				$header_row[$k] = trim($k,'"');
				
			}
			break;
		}
		$data_rows = array();
		
		foreach ($data as $key=>$value){
			$row = array();
			foreach($value as $k => $v){
				
				$v= sanitize_text_field($v);
				$row[$k] = trim($v,'"');
				
			}
			$data_rows[] = $row;
		}
		
		$fh = @fopen( $filepath, 'w' );
		$filename = 'mwb-advanced-report.csv';
		
		fputcsv( $fh, $header_row );
		foreach ( $data_rows as $data_row ) {
			fputcsv( $fh, $data_row );
		}
		fclose( $fh );

		$attach = array(WP_CONTENT_DIR.'/uploads/mwb-advanced-report.csv');
		$re = wp_mail($recipient,$subject, $email_body, ' ', $attach);
		
		if($re == 1){
			echo __("Mail Sent","arf-woo");
		}
		else{
			echo __("Fail","arf-woo");
		}

		wp_die();

	}

	public static function arfw_license_notice(){
		
		$screen = get_current_screen();
		$screnn_id = $screen->id;
		
		if($screnn_id == 'woocommerce_page_wc-reports'){
			if(!self::check_lic_validity()){

				$day_count = self::check_lic_days();
				$day_count_warning = floor( $day_count );
	
				$day_string = sprintf( _n( '%s day', '%s days', $day_count_warning, 'arf-woo' ), number_format_i18n( $day_count_warning ) );
				
				$day_string = '<span id="mwb-wocuf-day-count" >'.$day_string.'</span>';
	
				$license_page_url = admin_url('admin.php').'?page=wc-reports&tab=mwb_advance&report=lic_check';
				if($day_count_warning >=0){
	
					?>
				<div id="arfw_license_notice" class="notice notice-warning">
					<p>
						<strong><a href="<?php echo $license_page_url ;?>"><?php _e( 'Activate', 'arf-woo' ); ?></a><?php printf( __( ' the license key before %s or the plugin will become dysfunctional.', 'arf-woo' ), $day_string ); ?></strong>
					</p>
				</div>
					<?php
				}
				else{
					?>
					<div id="arfw_license_notice" class="notice notice-warning">
					<p>
						<strong><?php _e( 'Plugin trial period has expired please activate your plugin license. ', 'arf-woo' ); ?><a href="<?php echo $license_page_url ;?>"><?php _e( 'Activate Now', 'arf-woo' ); ?></a></strong>
					</p>
				</div>
				<?php
				}
				
			}
		}
		

	}

	public static function check_lic_validity(){

		$arfw_lic_key = get_option('arfw_lic_key','');
		$arfw_lic_valid = get_option('arfw_valid_license',false);
		if(!empty($arfw_lic_key) && $arfw_lic_valid){
			return true;
		}
		else{
			return false;
		}
	}

	public static function check_lic_days(){

		$thirty_days = get_option("arfw_pro_activated_timestamp", "");

		$current_time = current_time( 'timestamp' );

		$day_count = ( $thirty_days - $current_time ) / (24 * 60 * 60);
		
		return floor($day_count);
	}

	public function arfw_check_licence_daily() {
		
		$license_key = get_option( 'arfw_lic_key', false );
		
		$api_params = array(
            'slm_action' 		=> 'slm_check',
            'secret_key' 		=> ARFW_ACTIVATION_SECRET_KEY,
            'license_key' 		=> $license_key,
            'registered_domain' => $_SERVER['SERVER_NAME'],
            'item_reference' 	=> urlencode(ARFW_ITEM_REFERENCE),
            'product_reference' => 'MWBPK-14739',
        );

		Advanced_Woocommerce_Reports::arfw_verify_license($api_params);
	}

}
