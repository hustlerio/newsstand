<?php

namespace ACFWF\Models\Objects\Report_Widgets;

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

class Section_Title
{
    /**
     * Property that houses data of the report widget.
     * 
     * @since 4.3
     * @access protected
     * @var array
     */
    protected $_data = array(
        'key'        => '',
        'title_html' => '',
        'type'       => 'section_title',
    );

    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Create a new Report Widget object.
     *
     * @since 4.3
     * @access public
     * 
     * @param string $key   Section key
     * @param string $title Section title
     */
    public function __construct($key, $title)
    {
        $this->_data['key']        = $key;
        $this->_data['title_html'] = $title;
    }

    /*
    |--------------------------------------------------------------------------
    | Getter methods
    |--------------------------------------------------------------------------
     */

    /**
     * Access public report widget data.
     *
     * @since 4.3
     * @access public
     *
     * @param string $prop Model to access.
     */
    public function __get($prop)
    {
        if (array_key_exists($prop, $this->_data)) {
            return $this->_data[$prop];
        } else {
            throw new \Exception("Trying to access unknown property " . $prop . " on Abstract_Report_Widget instance.");
        }
    }

    /**
     * Get object data.
     * 
     * @since 4.3
     * @access public
     * 
     * @return array Object data.
     */
    public function get_data()
    {
        return $this->_data;
    }
}