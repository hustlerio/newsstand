<?php

/**
 * Sales report email
 *
 * Class WC_ASRE_Date_Range
 * 
 * @version       1.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directlyw

class WC_ASRE_Gmt_Date_Range {

	public $interval;

	/**
	 * The start date
	 * 
	 * @var DateTime
	 */
	public $start_date;	

	/**
	 * The end date
	 * 
	 * @var DateTime
	 */
	public $end_date;
	

	/**
	 * The constructor
	 *
	 * @param $interval
	 *
	 * @since  1.0.0
	 */
	public function __construct( $interval ) {

		// Set the interval
		$this->interval = $interval;

		// Set the dates based on interval
		$this->set_dates();
		
	}

	/**
	 * Set the correct start and end dates
	 *
	 * @since  1.0.0
	 */
	private function set_dates() {

		// Set the end date
		$this->start_date = new DateTime( date_i18n( 'Y-m-d 00:00:00' ), new DateTimeZone( wc_timezone_string() ) );
		$this->end_date = new DateTime( date_i18n('Y-m-d 23:59:59'), new DateTimeZone( wc_timezone_string() ) );
		
		// Clone end date into start date
		//$this->start_date = clone $this->end_date;
		
		// Subtract a second from end date.
		$this->end_date->modify( '-1 day' );
		
		if ( 'daily-overnight' == $this->interval || 'previous_overnight' == $this->interval ) {
			$data = asre_pro()->admin->get_email_data();
			$this->start_date = new DateTime( gmdate( 'Y-m-d' ) . $data->day_hour_start );
			$this->end_date = new DateTime( gmdate( 'Y-m-d' ) . $data->day_hour_end );
		}
		if ( 'monthly' == $this->interval || 'previous_month' == $this->interval ) {
			$this->start_date = new DateTime(gmdate('Y-m-d', strtotime('first day of previous month')) . '00:00:00' );
			$this->end_date = new DateTime(gmdate('Y-m-d', strtotime('last day of previous month')) . '23:59:59' );
		}

		// Modify start date based on interval
		switch ( $this->interval ) {
			case 'last-30-days':
				$this->start_date->modify( '-30 days' );
				break;
			case 'monthly':
				$this->start_date;
				$this->end_date;		
				break;
			case 'weekly':
				$this->start_date->modify( '-1 week' );				
				break;	
			case 'daily':
				$this->start_date->modify( '-1 day' );
				break;
			case 'daily-overnight':
				if ($data->day_hour_start >= $data->day_hour_end) {
					$this->start_date->modify( '-1 day' );
				}
				break;				
			case 'previous-last-30-days':
				$this->start_date->modify( '-60 days' );				
				$this->end_date->modify( '-30 days' );
				break;
			case 'previous_month':
				$this->start_date->modify( 'first day of previous month' );			
				$this->end_date->modify( 'last day of previous month' );
				break;
			case 'previous_week':
				$this->start_date->modify( '-2 week' );
				$this->end_date->modify( '-1 week' );				
				break;
			case 'previous_day':
				$this->start_date->modify( '-2 day' );				
				$this->end_date->modify( '-1 day' );
				break;
			case 'previous_overnight':
				$this->start_date->modify( '-1 day' );
				$this->end_date->modify( '-1 day' );
				if ($data->day_hour_start >= $data->day_hour_end) {
					$this->start_date->modify( '-1 day' );
				}
				break;
			default:
				$this->start_date->modify( '-1 day' );
				break;				
		}
		
		//date convert to gmt datetime
		$this->start_date = $this->convert_local_datetime_to_gmt($this->start_date->format('Y-m-d H:i:s'));
		$this->end_date = $this->convert_local_datetime_to_gmt($this->end_date->format('Y-m-d H:i:s'));
	}

	/**
	 * Get the end date
	 *
	 * @since  1.0.0
	 * @return DateTime
	 */
	public function get_end_date() { 
		return $this->end_date;
	}

	/**
	 * Set the end date
	 *
	 * @param DateTime $end_date
	 *
	 * @since  1.0.0
	 */
	public function set_end_date( $end_date ) {
		$this->end_date = $end_date;
	}

	/**
	 * Get the interval
	 *
	 * @since  1.0.0
	 *
	 * @return String
	 */
	public function get_interval() {
		return $this->interval;
	}

	/**
	 * Set the interval
	 *
	 * @param String $interval
	 *
	 * @since  1.0.0
	 */
	public function set_interval( $interval ) {
		$this->interval = $interval;
	}

	/**
	 * Get the start date
	 *
	 * @since  1.0.0
	 *
	 * @return DateTime
	 */	
	public function get_start_date() { 
		return $this->start_date;
	}
	
	/**
	 * Set the start date
	 *
	 * @param DateTime $start_date
	 *
	 * @since  1.0.0
	 */
	public function set_start_date( $start_date ) {
		$this->start_date = $start_date;
	}
	
	/**
	 * get the convert datetime to gmt
	 *
	 * @param DateTime $datetime_string
	 *
	 * @since  1.0.0
	 */
	public static function convert_local_datetime_to_gmt( $datetime_string ) {
		$datetime = new \DateTime( $datetime_string, new \DateTimeZone( wc_timezone_string() ) );
		$datetime->setTimezone( new \DateTimeZone( 'GMT' ) );
		return $datetime;
	}
}

