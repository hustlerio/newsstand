<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

// WC_Admin_Report is not autoloaded we manually need to load
include_once( WC()->plugin_path() . '/includes/admin/reports/class-wc-admin-report.php' );

class WC_ASRE_Report_Manager extends WC_Admin_Report {

	/**
	 * The constructor creates a WC_Admin_Reports object sets the start and end date
	 *
	 * @param WC_ESRE_Date_Range $date_range
	 *
	 * @since  1.0.0
	 */
	public function __construct( $date_range ) {
		$this->start_date = (int) $date_range->get_start_date()->format( 'U' );
		$this->end_date   = (int) $date_range->get_end_date()->format( 'U' );
	}
	
	/*
	* Growth count function
	*/
	public static function get_growth_count( $previous_count, $current_count ) {
		if ( 0 != (int) $previous_count ) {
			$percentChange = ( ( (int) $current_count - (int) $previous_count ) / (int) $previous_count ) * 100;
		} else if ( 0 != (int) $current_count && 0 == (int) $previous_count ) {
			$percentChange = 100;
		} else {
			$percentChange = null;
		}	
		return number_format($percentChange);
	}
}
