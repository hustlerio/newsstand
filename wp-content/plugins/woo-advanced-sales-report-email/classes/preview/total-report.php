<?php
/**
 * Sales report email preview
 *
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$data = wc_sales_report_email()->admin->get_data_byid( $data_array[ 'id' ] );
$date_range = new WC_ASRE_Date_Range( $data->email_interval );
$email_content = str_replace('{site_title}', get_bloginfo( 'name' ), $data->email_content );
$general_format = get_option('date_format');
$time_format = get_option('time_format');
if ( 'F j, Y' == $general_format) {
	$date_format = 'M j, Y';
} else { 
	$date_format = $general_format;
}
?>    
<html 
<?php 
if (is_rtl()) {
	echo 'dir="rtl"';
}
?>
>
	<head>
		<meta media="all" name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			<?php 
			if ( empty($data->email_subject)) {
				echo esc_html(str_replace('{site_title}', get_bloginfo( 'name' ), 'Sales Report for {site_title}' ));
			} else {
				echo esc_html($data->email_subject);
			} 
			?>
		</title>
		<?php if (is_rtl()) { ?>
		<style type="text/css">
		.report-table-widget th {
			text-align: right !important;
		}
		.growth-span {
			float:left !important;
		}
		.sales-report-email-template {
			text-align: right !important;
		}
		.report-table-widget tr td {
			text-align: right !important;
		}
		@media screen and (max-width: 500px) {
			.report-dates td {
				float: left;
				width: 100%;
				font-size: 13px !important;
				text-align: right !important;
				padding-left: 0;
			}
		}
		</style>
		<?php } else { ?>
		<style type="text/css">
		.report-table-widget th {
			text-align: left;
		}

		@media screen and (max-width: 500px) {
			.report-dates td {
				float: left;
				width: 100%;
				font-size: 13px !important;
				text-align: left !important;
				padding-left: 0;
			}
		}
		</style>
		<?php } ?>
		<style type="text/css">
		.sales-report-email-template {
			max-width: 850px;
			margin: 0 auto;
			padding: 70px 0;
			font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
			background-color: #ffffff;
			border: 1px solid #e0e0e0;
			box-shadow: 0 1px 4px rgba(0, 0, 0, 0.1);
			border-radius: 3px;
			padding: 20px 30px;
		}

		.report-summary {
			margin: 0;
			padding: 0;
			box-sizing: border-box;
		}

		.report-widget {
			width: 32.9%;
			display: inline-block;
			padding: 14px 16px;
			margin-bottom: 4px;
			margin-left: 0;
			background-color: #fafafa;
			border: 1px solid #e0e0e0;
			line-height: 1.4em;
			text-decoration: none;
			box-sizing: border-box;
		}

		.column4 .report-widget {
			width: 24.6%;
		}

		.report-widget:hover {
			background-color: #f3f4f5;
		}

		.report-summary__item-data {
			margin: 0;
			margin-bottom: 5px;
			float: left;
		}

		.report-summary__item-prev-label {
			font-size: 10px;
			color: #555d66;
			display: inline-block;
			width: 100%;
		}

		.report-summary__item-prev-value {
			font-size: 12px;
			color: #555d66;
			display: inline-block;
		}

		.col-6 {
			width: 49%;
			box-sizing: border-box;
			display: inline-block;
			margin-right: 5px;
			vertical-align: top;
		}

		.col-heading {
			margin: 0 0 10px !important;
			display: block;
			margin-bottom: 16px;
			font-size: 11px;
			text-transform: uppercase;
			color: #6c7781;
			line-height: 1.4em;
			overflow: hidden;
			white-space: nowrap;
			text-overflow: ellipsis;
		}

		.widget-value {
			margin-bottom: 4px;
			font-size: 18px;
			font-size: 1.125rem;
			font-weight: 600;
			color: #191e23;
			flex: 1 0 auto;
		}

		.report-table-widget tr td img {
			width: 50px;
			height: auto;
			vertical-align: middle;
			margin-right: 10px;
		}

		.report-table-widget {
			border-radius: 0;
			border: 1px solid #e0e0e0;
			box-shadow: none;
			font-size: 14px;
		}

		.report-table-widget tr td {
			border-top: 1px solid #e0e0e0;
			padding: 15px 10px;
			display: table-cell;
			line-height: 1.5;
			color: #333;
		}

		.report-table-widget thead tr th {
			text-align: left;
			padding: 15px 10px;
			border-radius: 0;
		}

		.report-table-widget tbody tr th {
			border-left: 1px solid #e0e0e0;
			background-color: #fff;
			padding: 15px 10px;
			border-top: 1px solid #e0e0e0;
			border-left: 1px solid #e0e0e0;
		}

		.report-table-widget tbody tr th {
			background-color: #fff;
			border-top: none;
			font-weight: 500;
			font-size: 1.1em;
			color: #333;
		}

		.growth-span {
			font-size: 80%;
			margin-left: 10px;
			line-height: 1.7;
		}

		.report-table-title.border-zero {
			border: 0;
			background: none;
		}
		
		.report-table-title {
			margin: 0;
			padding: 10px;
			font-size: 1.2em;
			font-weight: 400;
			color: #333;
			border: 1px solid #e0e0e0;
			background: #f5f5f5;
			border-bottom: 0;
		}

		.report-summary__item-data .woocommerce-summary__item-delta-icon {
			float: right;
			vertical-align: middle;
			margin-right: 3px;
			fill: currentColor;
		}

		.growth-span.arrow-down {
			background: #e05b49;
		}

		.growth-span.arrow-up {
			background: #4caf50;
		}
		.growth-span {
			color: #fff;
			padding: 5px;
			float: right;
			border-radius: 3px;
			line-height: 1;
			background: #4caf50;
		}
		
		.asre-plugin-logo {
			max-height: 50px;
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.main-title {
			border-bottom: 1px solid #e0e0e0;
		}

		.report-dates td {
			padding-left: 0;
			padding-right: 0;
		}

		.report-dates {
			width: 100%;
			vertical-align: top;
			margin-bottom: 10px;
			font-size: 14px;
		}

		@media only screen and (max-width: 1149px) {
			.report-dates td {
				font-size: 15px;
			}
			.report-widget {
				width: 32.85%;
			}
		}

		@media only screen and (max-width: 768px) {
			.report-dates td {
				font-size: 15px;
			}
			.col-heading {
				margin: 5px 0 20px !important;
				font-size: 1em !important;
			}
			.report-widget {
				width: 49% !important;
			}
			.report-table-widget {
				width: 100% !important;
			}
			.widget-value {
				font-size: 1.8em !important;
			}
			.report-summary__item-data {
				margin-bottom: 20px !important;
			}
			.growth-span {
				font-size: 100% !important;
			}
			.growth-span img {
				width: 18px !important
			}
			.report-summary__item-prev-label,
			.report-summary__item-prev-value {
				font-size: 1em !important;
				margin-bottom: 5px !important;
			}
		}

		@media only screen and (max-width: 650px) {
			.report-widget {
				width: 49% !important;
			}
			td.current-date {
				float: left;
			}
			td.previous-date {
				float: left !important;
			}
			p.col-heading {
				font-size: 0.8em !important;
			}
		}

		@media only screen and (max-width: 550px) {
			.sales-report-email-template {
				padding: 10px !important;
			}
			.report-widget {
				width: 100% !important;
			}
			.report-table-widget {
				width: 100% !important;
			}
			.col-heading {
				margin: 10px 0 15px !important;
				font-size: 0.7em !important;
			}
			.widget-value {
				font-size: 1.5em !important;
			}
			.report-summary__item-data {
				margin-bottom: 10px !important;
			}
			.growth-span {
				font-size: 80% !important;
			}
			.growth-span img {
				width: 15px !important
			}
			.report-summary__item-prev-label,
			.report-summary__item-prev-value {
				font-size: 1em !important;
			}
		}
		</style>
	</head>
	<body>
		<div class="sales-report-email-template">
			<div class="main-title">
				<img src="<?php echo esc_url(apply_filters( 'asre_branding_logo_url', plugin_dir_url(__FILE__) . 'image/sales-report-email-logo.png', $data )); ?>" width="255" height="50" class="asre-plugin-logo" style="display: block;padding-top: 10px;padding-bottom: 10px;" alt="" >
			</div>
			<p style="padding: 20px 0 10px; margin: 0; font-size: 14px;">
				<?php 
				if (empty($email_content)) {
					$email_content = 'Hi there. Please find your sales report for {site_title} below.';  
					printf( esc_html( str_replace('{site_title}', get_bloginfo( 'name' ), $email_content), 'woocommerce-advanced-sales-report-email' ), esc_html($data->email_interval) ); 
				} else {
					echo esc_html($email_content);
				}
				?>
			</p>
			<table class="report-dates" cellspacing="0" cellpadding="6" style="">
				<tr>
					<td class="current-date"> 
						<?php esc_html_e('Report dates', 'woocommerce-advanced-sales-report-email'); ?> : 
						<strong><?php echo esc_html($date_range->get_start_date()->format($date_format)); ?>
						<?php 
						if ( 'daily-overnight' == $data->email_interval ) {
							echo esc_html('(' . $date_range->get_start_date()->format($time_format) . ') ');
						} 
						?>
						- 
						<?php
						if ( 'daily-overnight' == $data->email_interval ) {
							echo esc_html($date_range->get_end_date()->modify( '+1 day' )->format($date_format));
						} else {
							echo esc_html($date_range->get_end_date()->format($date_format));
						}
						if ( 'daily-overnight' == $data->email_interval ) {
							echo esc_html('(' . $date_range->get_end_date()->format($time_format) . ') ');
						}
						?>
						</strong>
						<?php
						// callback do_action hook for email previous date
						do_action( 'asre_email_content_previous_date_hook', $date_range, $data, $data_array, $date_format, $time_format );
						?>
					</td>
				</tr>
			</table>
			<?php 
			$i = 0;
			$j = 0;
			if ( 'yes' == $data->display_total_sales || '1' == $data->display_total_sales ) {
				$i++;
			}
			if ( 'yes' == $data->display_coupon_used || '1' == $data->display_coupon_used ) {
				$i++;
			}
			if ( 'yes' == $data->display_total_refunds || '1' == $data->display_total_refunds ) {
				$i++;
			}
			if (wc_tax_enabled()) {
				if ( 'yes' == $data->display_total_tax || '1' == $data->display_total_tax ) {
					$i++;
				}
			}
			if ( 'yes' == $data->display_total_shipping || '1' == $data->display_total_shipping ) {
				$i++;
			}
			if ( 'yes' == $data->display_net_revenue || '1' == $data->display_net_revenue ) {
				$i++;
			}
			if ( 'yes' == $data->display_total_orders || '1' == $data->display_total_orders ) {
				$i++;
			}
			if ( 'yes' == $data->display_total_items || '1' == $data->display_total_items ) {
				$i++;
			}
			if ( 'yes' == $data->display_signups || '1' == $data->display_signups ) {
				$i++;
			}
			// callback apply_filters hook for email count avg data
			$i = apply_filters( 'asre_email_content_count_avg_data_hook', $i, $data );

			// callback apply_filters hook for email count data
			$j = apply_filters( 'asre_email_content_count_data_hook', $j, $data );

			$column4 = array( 4, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
			?>
			<?php if ( $i > 0 ) { ?>
				<h3 class="report-table-title border-zero"><?php esc_html_e('Report Totals', 'woocommerce-advanced-sales-report-email'); ?></h3>
			<?php } ?>
			<ul class="report-summary has-6-items 
				<?php 
				if (in_array( $i, $column4) || $i > '11' || $j > '11') {
					echo 'column4';
				} 
				?>
				">
				<?php if ( 'yes' == $data->display_total_sales || '1' == $data->display_total_sales ) { ?>
					<li class="report-widget">
						<p class="col-heading"><strong><?php esc_html_e('Gross Revenue', 'woocommerce-advanced-sales-report-email'); ?></strong></p>
						<p class="report-summary__item-data">
							<span class="widget-value">
							<?php 
							if (empty($data_array['total_sales'])) { 
								echo '$0.00';
							} else {
								echo wp_kses_post(wc_price($data_array['total_sales']));
							}
							?>
							</span>
						</p>
						<?php
						// callback do_action hook for previous total sales hook
						do_action( 'asre_previous_total_sales_hook', $data, $date_range, $data_array );
						?>
					</li>
				<?php } ?>
				<?php if ( 'yes' == $data->display_coupon_used || '1' == $data->display_coupon_used ) { ?>
					<li class="report-widget">
						<p class="col-heading"><strong><?php esc_html_e('Coupons', 'woocommerce'); ?></strong></p>
						<p class="report-summary__item-data"><span class="widget-value">
							<?php 
							if (empty($data_array['coupon_used'])) {
								echo '$0.00';
							} else {
								echo wp_kses_post(wc_price($data_array['coupon_used']));
							}
							?>
							</span>
						</p>
						<?php
						// callback do_action hook for previous total coupon hook
						do_action( 'asre_previous_total_coupon_hook', $data, $date_range, $data_array );
						?>
					</li>
				<?php } ?>
				<?php if ( 'yes' == $data->display_total_refunds || '1' == $data->display_total_refunds ) { ?>
					<li class="report-widget">
						<p class="col-heading"><strong><?php esc_html_e('Refunds', 'woocommerce'); ?></strong></p>
						<p class="report-summary__item-data"><span class="widget-value">
							<?php 
							if (empty($data_array['total_refunds'])) {
								echo '$0.00';
							} else {
								echo wp_kses_post(wc_price($data_array['total_refunds']));
							}
							?>
							</span>
						</p>
						<?php
						// callback do_action hook for previous total refund hook
						do_action( 'asre_previous_total_refund_hook', $data, $date_range, $data_array );
						?>
					</li>
				<?php } ?>
				<?php if (wc_tax_enabled()) { ?>
					<?php if ( 'yes' == $data->display_total_tax || '1' == $data->display_total_tax ) { ?>
						<li class="report-widget">
							<p class="col-heading"><strong><?php esc_html_e('Taxes', 'woocommerce'); ?></strong></p>
							<p class="report-summary__item-data"><span class="widget-value">
								<?php 
								if (empty($data_array['total_taxes'])) {
									echo '$0.00';
								} else {
									echo wp_kses_post(wc_price($data_array['total_taxes']));
								}
								?>
								</span>
							</p>
							<?php
							// callback do_action hook for previous total taxes hook
							do_action( 'asre_previous_total_taxes_hook', $data, $date_range, $data_array );
							?>
						</li>
					<?php } ?>
				<?php } ?>
				<?php if ( 'yes' == $data->display_total_shipping || '1' == $data->display_total_shipping ) { ?>
					<li class="report-widget">
						<p class="col-heading"><strong><?php esc_html_e('Shipping', 'woocommerce'); ?></strong></p>
						<p class="report-summary__item-data"><span class="widget-value">
							<?php 
							if (empty($data_array['total_shipping'])) {
								echo '$0.00';
							} else {
								echo wp_kses_post(wc_price($data_array['total_shipping']));
							}
							?>
							</span>
						</p>
						<?php
						// callback do_action hook for previous total shipping hook
						do_action( 'asre_previous_total_shipping_hook', $data, $date_range, $data_array );
						?>
					</li>
				<?php } ?>
				<?php if ( 'yes' == $data->display_net_revenue || '1' == $data->display_net_revenue ) { ?>
					<li class="report-widget">
						<p class="col-heading"><strong><?php esc_html_e('Net Revenue', 'woocommerce-advanced-sales-report-email'); ?></strong></p>
						<p class="report-summary__item-data"><span class="widget-value">
							<?php 
							if (empty($data_array['net_revenue'])) {
								echo '$0.00';
							} else {
								echo wp_kses_post(wc_price($data_array['net_revenue']));
							}
							?>
							</span>
						</p>
						<?php
						// callback do_action hook for previous total net revenue hook
						do_action( 'asre_previous_total_net_revenue_hook', $data, $date_range, $data_array);
						?>
					</li>
				<?php } ?>
				<?php if ( 'yes' == $data->display_total_orders || '1' == $data->display_total_orders ) { ?>
					<li class="report-widget">
						<p class="col-heading"><strong>
						<?php esc_html_e('Orders', 'woocommerce'); ?>
						</strong> </p>
						<p class="report-summary__item-data"><span class="widget-value">
							<?php 
							if (empty($data_array['total_orders'])) {
								echo '0';
							} else {
								echo wp_kses_post($data_array['total_orders']);
							}
							?>
							</span>
						</p>
						<?php
						// callback do_action hook for previous total order hook
						do_action( 'asre_previous_total_order_hook', $data, $date_range, $data_array );
						?>
					</li>
				<?php } ?>
				<?php if ( 'yes' == $data->display_total_items || '1' == $data->display_total_items ) { ?>
					<li class="report-widget">
						<p class="col-heading"><strong><?php esc_html_e('Items Sold', 'woocommerce-advanced-sales-report-email'); ?></strong></p>
						<p class="report-summary__item-data"><span class="widget-value">
							<?php 
							if (empty($data_array['total_items'])) {
								echo '$0.00';
							} else {
								echo wp_kses_post($data_array['total_items']);
							}
							?>
							</span>
						</p>
						<?php
						// callback do_action hook for previous total item hook
						do_action( 'asre_previous_total_item_hook', $data, $date_range, $data_array );
						?>
					</li>
				<?php } ?>
				<?php if ( 'yes' == $data->display_signups || '1' == $data->display_signups ) { ?>
					<li class="report-widget">
						<p class="col-heading"><strong><?php esc_html_e('New Customers', 'woocommerce-advanced-sales-report-email'); ?></strong></p>
						<p class="report-summary__item-data"><span class="widget-value">
							<?php 
							if (empty($data_array['total_signups'])) {
								echo '$0.00';
							} else {
								echo wp_kses_post($data_array['total_signups']->get_value());
							}
							?>
							</span>
						</p>
						<?php
						// callback do_action hook for previous total signups hook
						do_action( 'asre_previous_total_signups_hook', $data, $date_range, $data_array );
						?>
					</li>
				<?php } ?>
				<?php 
				// callback do_action hook for email content preview
				do_action( 'asre_email_content_total_preview_hook', $data, $date_range, $data_array, $j); 
				?>
			</ul>
		<br>
		<?php 
		if ( 'yes' == $data->display_top_sellers || '1' == $data->display_top_sellers ) {
			echo wp_kses_post($data_array['top_sellers']->get_value());
		} 
		if ( '1' == $data->display_top_categories ) { 
			echo wp_kses_post($data_array['top_categories']->get_value());
		}
		// callback do_action hook for email content preview
		do_action( 'asre_email_content_detail_preview_hook', $data, $date_range, $data_array );
		?>
		</div>
	</body>
</html>
