<?php
/**
 * Sales report email
 *
 * Class WC_ASRE_Admin_Report
 * 
 * @version       1.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class WC_ASRE_Admin_Report {

	public function __construct() {
		$this->init();
	}
	public function init() {
		add_filter( 'woocommerce_reports_get_order_report_query', array( $this, 'reports_get_order_report_query' ) );
	}
	public function reports_get_order_report_query( $query ) {
		$currency_rates = $this->get_currency_rates();

		if ( $currency_rates ) {
				
			$change = false;
		
			$fields = array(
				' meta__order_total.meta_value', 
				' meta__order_shipping.meta_value', 
				' meta__order_tax.meta_value', 
				' meta__order_shipping_tax.meta_value', 
				' meta__refund_amount.meta_value ',
				' order_item_meta_discount_amount.meta_value',
				' order_item_meta__line_total.meta_value',
				'parent_meta__order_total.meta_value',
				'parent_meta__order_shipping.meta_value',
				'parent_meta__order_tax.meta_value',
				'parent_meta__order_shipping_tax.meta_value'
			);
			
			foreach ( $fields as $field ) {
				
				if ( strpos( $query['select'], $field ) !== false ) {
					
					$case_ex = $this->caseex( $field, $currency_rates );
					$query['select'] = str_replace( $field, $case_ex, $query['select'] );
					$change = true;
				}
			}
			
			if ( $change ) {
				$query['join'] .= $this->join_order_currency();
			}
		}
		return $query;
	}
	public function get_currency_rates() {
		return get_option( 'wc_esre_curr_rate' );
	}
	public function caseex( $field, $currency_rates ) {
		
		$case_ex = ' CASE meta__order_currency.meta_value ';
		foreach ( $currency_rates as $currency => $rate ) {
			$case_ex .= "WHEN '{$currency}' THEN ( {$field} / ({$rate})) ";
		}
		$case_ex .= "ELSE {$field} END ";
		
		return $case_ex;
	}
	public function join_order_currency( $post_alias = false, $join_type = 'INNER' ) {											
		global $wpdb;

		$post_alias = $post_alias ? $post_alias : 'posts';
		return ' ' . $join_type . " JOIN {$wpdb->postmeta} AS meta__order_currency ON ( {$post_alias}.ID = meta__order_currency.post_id AND meta__order_currency.meta_key = '_order_currency' ) ";
	}
}
