<section id="asre_content1" class="asre_tab_section <?php if ( 'list' !== $tab ) { echo 'asre_report_form_section'; } ?>">
	<div class="asre_tab_inner_container">
		<?php if ( 'list' === $tab ) : ?>
		<?php
		wc_sales_report_email()->admin->update_email_enable_options_callback();
		$data = WC_ASRE_Admin::get_data();
			?>
		<h1 class="tab_main_heading"><?php esc_html_e( 'Reports', 'woocommerce' ); ?></h1>
		<table class="wp-list-table widefat fixed posts">
			<thead>
				<tr>
					<th scope="col" id="action" class="manage-column column-amount" style="width: 60px;">
						<?php esc_html_e( 'Status', 'woocommerce-advanced-sales-report-email' ); ?>
					</th>
					<th scope="col" id="name" class="manage-column column-type" style="">
						<?php esc_html_e( 'Title', 'woocommerce' ); ?>
					</th>
					<th scope="col" id="action" class="manage-column column-amount" style="">
						<?php esc_html_e( 'Next Run Date', 'woocommerce-advanced-sales-report-email' ); ?>
					</th>
					<th scope="col" id="action" class="manage-column column-amount" style="width: 110px;">
						<?php esc_html_e( 'Actions', 'woocommerce-advanced-sales-report-email' ); ?>
					</th>
				</tr>
			</thead>
			<tbody id="the_list">
				<?php if ( empty( $data ) ) : ?>
					<tr scope="row">
						<td colspan="4"><?php esc_html_e( 'No Reports', 'woocommerce-advanced-sales-report-email' ); ?></td>
					</tr>
				<?php else : ?>	
				<?php foreach ( class_exists( 'Sales_Report_Email_PRO_Add_on' ) ? $data : array_slice($data, 0, 1) as $w_data ) : ?>
					<tr scope="row" class="<?php echo esc_html($w_data->email_enable) ? 'active' : 'inactive'; ?>" value="<?php echo esc_html($w_data->id); ?>">
						<td>
						<?php 
						if ( $w_data->email_enable ) {
							$checked = 'checked';
						} else {
							$checked = '';
						}
						
						if (isset($array['disabled']) && true == $array['disabled'] ) {
							$disabled = 'disabled';
							$checked = '';
						} else {
							$disabled = '';
						}
						?>
						<span class="tgl-btn-parent" style="">
							<input type="hidden" name="email_enable" value="0">
							<input type="checkbox" id="email_enable_<?php echo esc_html($w_data->id); ?>" name="email_enable" data-id="<?php echo esc_html($w_data->id); ?>" class="tgl tgl-flat-sre enable_status_list" <?php echo esc_html($checked); ?> value="<?php echo esc_html($w_data->email_enable); ?>" <?php echo esc_html($disabled); ?>/>
							<label class="tgl-btn" for="email_enable_<?php echo esc_html($w_data->id); ?>"></label>
						</span>
						</td>
						<td class="post-title column-title">
							<strong style="margin:0">
								<?php if (empty($w_data->report_name)) { ?>
								<a class="row-title" href="admin.php?page=<?php echo esc_html($this->screen_id); ?>&amp;tab=edit&amp;id=<?php echo esc_html($w_data->id); ?>">
									<?php echo esc_html(stripslashes( '(no title)' )); ?>
								</a>
								<?php } else { ?>
								<a class="row-title" href="admin.php?page=<?php echo esc_html($this->screen_id); ?>&amp;tab=edit&amp;id=<?php echo esc_html($w_data->id); ?>">
									<?php echo esc_html(stripslashes( $w_data->report_name )); ?>
								</a>
								<?php } ?>
							</strong>
						</td>
						<td>
						<?php
						if ( '1' == $w_data->email_enable ) { 
							esc_html_e( $this->next_run_date($w_data), 'zorem-csv' );
						}
						?>
						</td>
						<td class="post-title column-title">
							<span class="edit">
								<a href="admin.php?page=<?php echo esc_html($this->screen_id); ?>&amp;tab=edit&amp;id=<?php echo esc_html($w_data->id); ?>"><?php esc_html_e( 'Edit', 'woocommerce' ); ?></a>
							</span>
							<span class="trash">
								<a onclick="return confirm( 'Are you sure you want to delete this entry?' );" href="admin.php?page=<?php echo esc_html($this->screen_id); ?>&amp;action=delete&amp;id=<?php echo esc_html($w_data->id); ?>">
									<?php esc_html_e( 'Delete', 'woocommerce' ); ?>
								</a>
							</span>
						</td>
					</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>

		</table>
		<?php if ( !class_exists( 'Sales_Report_Email_PRO_Add_on' ) ) { ?>
		<div class="report-tab asre-edit asre-btn">
			<a <?php if ( ( count($data) >= 1 )) { ?>
			onclick="return confirm( 'You need to purchase of Sales Report Email PRO Add-on plugin' );" 
			<?php } ?>
			<?php if ( ( count($data) == '0' ) ) { ?>
			href="admin.php?page=<?php echo esc_html($this->screen_id); ?>&tab=edit&id=0"
			<?php } ?> class="button-primary create_new_report <?php echo ( 'edit' === $tab ) ? 'nav-tab-active' : ''; ?>">
				  <?php esc_html_e( 'Add Report', 'woocommerce-advanced-sales-report-email' ); ?>
			</a>
		</div> 
		<?php } else { ?>
		<div class="report-tab asre-edit asre-btn">
			<a href="admin.php?page=<?php echo esc_html($this->screen_id); ?>&tab=edit&id=0" class="button-primary create_new_report <?php echo ( 'edit' === $tab ) ? 'nav-tab-active' : ''; ?>">
				<?php esc_html_e( 'Add Report', 'woocommerce-advanced-sales-report-email' ); ?>
			</a>
		</div> 
		<?php } ?>
		<br/>
		<?php elseif ( 'edit' === $tab ) : ?>
		<h1 class="tab_main_heading"><a href="admin.php?page=woocommerce-advanced-sales-report-email&tab=list" style="text-decoration:none;color:#e05b49;"><?php esc_html_e( 'Reports', 'sales-report-email-pro' ); ?></a> > <?php esc_html_e( 'Edit Report', 'sales-report-email-pro' ); ?></h1>
		<form method="post" id="workflow_form" action="" enctype="multipart/form-data">
			<input type="hidden" name="id" id="id" value="<?php echo esc_attr($this->data->id); ?>">
			<div class="asre_edit_inner_container">
				<div id="side-sortables" class="meta-box-sortables ui-sortable" style="">
					<div id="toggle_box" class="toggle-box postbox general">
						<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text"></span><span class="toggle-indicator" aria-hidden="true"></span></button>
						<h2 class="hndle ui-sortable-handle" style="margin:0;"><span><?php esc_html_e(  'Report Options', 'woocommerce' ); ?></span></h2>
						<div class="inside">
							<?php $this->get_html( $this->asre_general_data() ); ?>
						</div>
					</div>
					<div id="toggle_box" class="toggle-box postbox display-options total-items">
						<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text"></span><span class="toggle-indicator" aria-hidden="true"></span></button>
						<h2 class="hndle ui-sortable-handle" style="margin:0;"><span><?php esc_html_e(  'Report Totals', 'woocommerce-advanced-sales-report-email' ); ?></span></h2>
						<div class="inside total-items-list mdl-grid">
							<?php $this->get_html( $this->asre_display_total_option_data() ); ?>
						</div>
					</div>
					<?php 
					/*
					* callback do_action for subscription total html
					*/
					do_action( 'asre_subscription_total_html_data' ); 
					if (!class_exists( 'Sales_Report_Email_PRO_Add_on' )) { 
						?>
						<div id="toggle_box" class="toggle-box postbox display-options total-items">
							<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text"></span><span class="toggle-indicator" aria-hidden="true"></span></button>
							<h2 class="hndle ui-sortable-handle" style="margin:0;"><span><?php esc_html_e(  'Subscription Totals', 'woocommerce-subscriptions' ); ?></span></h2>
							<div class="inside total-items-list mdl-grid" style="padding: 6px;background: #f7fafc;">
								<?php wc_sales_report_email()->admin->get_html( wc_sales_report_email()->admin->asre_display_total_subscription_data() ); ?>
							</div>
						</div>
					<?php } ?>
						<div id="toggle_box" class="toggle-box postbox display-options breackdown-items">
							<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text"></span><span class="toggle-indicator" aria-hidden="true"></span></button>
							<h2 class="hndle ui-sortable-handle" style="margin:0;"><span><?php esc_html_e(  'Report Details', 'woocommerce-advanced-sales-report-email' ); ?></span></h2>
							<div class="inside breackdown-items-list mdl-grid">
								<?php $this->get_html( $this->asre_display_detail_option_data() ); ?>
							</div>
						</div>
					<?php 
					/*
					* callback do_action for subscription details html
					*/
					do_action( 'asre_subscription_details_html_data' );
					if (!class_exists( 'Sales_Report_Email_PRO_Add_on' )) { 
						?>
					<div id="toggle_box" class="toggle-box postbox display-options breackdown-items">
						<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text"></span><span class="toggle-indicator" aria-hidden="true"></span></button>
						<h2 class="hndle ui-sortable-handle" style="margin:0;"><span><?php esc_html_e(  'Subscription Details', 'woocommerce-advanced-sales-report-email' ); ?></span></h2>
						<div class="inside breackdown-items-list mdl-grid" style="padding: 6px;background: #f7fafc;">
							<?php wc_sales_report_email()->admin->get_html( wc_sales_report_email()->admin->asre_display_detail_subscription_data() ); ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="asre_edit_admin_sidebar">
				<div id="side-sortables" class="meta-box-sortables ui-sortable" style="">
					<div id="toggle_box" class="toggle-box postbox status">
						<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text"></span><span class="toggle-indicator" aria-hidden="true"></span></button>
						<h2 class="hndle ui-sortable-handle" style="margin:0;"><span><?php esc_html_e(  'Status', 'woocommerce' ); ?></span></h2>
						<div class="inside">
						<?php 
						$this->get_html( $this->asre_status_data() ); 
						if ($this->data->id > 0) { 
							$newDate = gmdate('M d, Y g:iA', strtotime($this->next_run_date($data)));
							?>
							<table class="form-table cron_run_date">
								<tbody>
									<tr valign="top">
										<td>
											<div><?php esc_html_e( 'Next Run Date', 'woocommerce-advanced-sales-report-email' ); ?>: <b><?php esc_html_e( $newDate); ?></b>
												<span style="font-size:13px;">Time zone: <strong><?php echo esc_html(get_option('timezone_string')); ?></strong></span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						<?php } ?>
						<div class="submit asre-delete asre-btn">
							<?php if ($this->data->id > 0) { ?>
							<a onclick="return confirm( 'Are you sure you want to delete this entry?' );" href="admin.php?page=<?php echo esc_html($this->screen_id); ?>&amp;action=delete&amp;id=<?php echo esc_html($this->data->id); ?>">
								<?php esc_html_e( 'delete report', 'woocommerce-advanced-sales-report-email' ); ?>
							</a>
							<?php } ?>
							<div class="spinner workflow_spinner" style="float:none"></div>
							<button name="save" class="button-primary woocommerce-save-button" type="submit" value="Save changes">
							<?php 
							if ( isset($_GET['id']) && 0 == $_GET['id'] ) {  
								esc_html_e(  'Save', 'woocommerce' ); 
							} else {  
								esc_html_e(  'Update', 'woocommerce' ); 
							}
							?>
							</button>
							<div class="success_msg workflow_success" style="display:none;">Report saved successfully.</div>
							<div class="error_msg workflow_error" style="display:none;">Please refresh the page.</div>
							<div class="error_msg invalid_license" style="display:none;">You have invalid license.</div>
							<?php $ajax_nonce = wp_create_nonce( 'asre_nonce_' . $this->data->id ); ?>
							<input type="hidden" name="asre_nonce_verify" id="asre_nonce_verify" value="<?php echo esc_html($ajax_nonce); ?>"/>
						</div>
						</div>
					</div>
					<div id="toggle_box" class="toggle-box postbox actions">
						<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text"></span><span class="toggle-indicator" aria-hidden="true"></span></button>
						<h2 class="hndle ui-sortable-handle" style="margin:0;"><span><?php esc_html_e(  'Actions', 'woocommerce' ); ?></span></h2>
						<div class="inside">
						<table id="preview_test_table" class="form-table asre-edit asre-btn">
							<tbody>
								<tr valign="top">
									<td>
										<button type="button" style="" class="cbr-show-modal button-primary" <?php if ( isset($_GET['id']) && 0 == $_GET['id'] ) { echo 'disabled style="cursor: not-allowed;"'; } ?>><?php esc_html_e( 'Preview report', 'woocommerce-advanced-sales-report-email' ); ?> <span class="dashicons dashicons-welcome-view-site" style="vertical-align:middle; margin-left: 5px;"></span></button>
										<button type="button" id="asre_sales_report_test_mail" name="asre_sales_report_test_mail" style="" class="button-primary" <?php if ( isset($_GET['id']) && 0 == $_GET['id'] ) { echo 'disabled style="cursor: not-allowed;"'; } ?>><?php esc_html_e( 'Send a test email   ', 'woocommerce-advanced-sales-report-email' ); ?></button>
									</td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>
					<div id="toggle_box" class="toggle-box postbox schedule">
							<button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text"></span><span class="toggle-indicator" aria-hidden="true"></span></button>
							<h2 class="hndle ui-sortable-handle" style="margin:0;"><span><?php esc_html_e(  'Schedule', 'woocommerce' ); ?></span></h2>
						<div class="inside">
						<table id="schadule_option" class="form-table asre-edit asre-btn">
							<tbody>
								<tr valign="top">
									<td>
										<?php $this->get_html( $this->asre_schadule_data() ); ?>
										<?php if (!class_exists( 'Sales_Report_Email_PRO_Add_on' )) { ?>
											<span class="display_previous_period"></span>
										<?php } ?>
										<?php $this->get_html( $this->asre_hour_option_data() ); ?>
									</td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
		</form>
		<?php endif; ?>
	</div>
</section>
<?php if ('edit' == $tab) { ?>
<dialog class="mdl-dialog" style="width: 999px;height: 600px;">
	<div class="mdl-dialog__content" style="padding: 0 !important;">
		<iframe id="iframe" style="width: 100%;height: 585px;" src="<?php echo esc_url(admin_url('admin-ajax.php')); ?>?action=preview_asre_page&amp;id=<?php echo esc_html($data->id); ?>"></iframe>
	</div>
	<div class="mdl-dialog__actions mdl-dialog__actions--full-width">
		<span style="font-size:35px;position: absolute;top: 0;right: 0px;"class="dashicons dashicons-dismiss close"></span>
	</div>
</dialog>
<div class="addon_inner_section" style="width: 500px;border: 1px solid #e0e0e0;padding: 10px 20px 20px;border-radius: 3px;display:none;">
	<div class="col asre-features-list asre-btn">
		<h1 class="plugin_title"><?php echo wp_kses_post('Upgrade to PRO!'); ?></h1>
		<ul>
			<li>One Year of Updates & Support</li>
			<li>Schedule multiple email reports</li>
			<li>Schedule partial daily report by hours and overnight</li>
			<li>Additional Report Totals – Average order value, Average Daily Sales, Average Items per order</li>
			<li>Additional Report Details – Sales By Coupons, Orders By Status, Orders By Payment Method</li>
			<li>Subscriptions Totals – Active Subscriptions, Subscriptions signups revenue, Subscription Switch, Resubscribe, Cancellation...</li>
			<li>Subscriptions details – Subscriptions by status (total), Subscriptions by status</li>
		</ul>
		<a href="https://www.zorem.com/product/sales-report-email-for-woocommerce/?utm_source=wp-admin&utm_medium=SRE&utm_campaign=add-ons" class="install-now button-primary pro-btn" target="blank" style="width: 100%;text-align: center;">UPGRADE TO PRO ></a>	
	</div>
</div>
<div id="" class="popupwrapper sre-features-popup" style="display:none;">
	<div class="popuprow" style="width: 500px;border: 1px solid #e0e0e0;padding: 20px;border-radius: 3px;">
		<div class="col asre-features-list asre-btn">
			<h1 class="plugin_title"><?php echo wp_kses_post('Upgrade to PRO!'); ?></h1>
			<ul>
				<li>One Year of Updates & Support</li>
				<li>Schedule multiple email reports</li>
				<li>Schedule partial daily report by hours and overnight</li>
				<li>Additional Report Totals – Average order value, Average Daily Sales, Average Items per order</li>
				<li>Additional Report Details – Sales By Coupons, Orders By Status, Orders By Payment Method</li>
				<li>Subscriptions Totals – Active Subscriptions, Subscriptions signups revenue, Subscription Switch, Resubscribe, Cancellation...</li>
				<li>Subscriptions details – Subscriptions by status (total), Subscriptions by status</li>
			</ul>
			<a href="https://www.zorem.com/product/sales-report-email-for-woocommerce/?utm_source=wp-admin&utm_medium=SRE&utm_campaign=add-ons" class="install-now button-primary pro-btn" target="blank" style="width: 100%;text-align: center;">UPGRADE TO PRO ></a>	
		</div>
	</div>
	<div class="popupclose"></div>
</div>
<script>
	var dialog = document.querySelector('dialog');
	var showModalButton = document.querySelector('.cbr-show-modal');
	if (! dialog.showModal) {
	  dialogPolyfill.registerDialog(dialog);
	}
	showModalButton.addEventListener('click', function() {
	  document.querySelector(".asre-sales-report-email-setting").style.overflowY  = "hidden";
	  dialog.showModal();
	  setInterval(document.getElementById('iframe').contentWindow.location.reload(), 1500);
	});
	dialog.querySelector('.close').addEventListener('click', function() {
	  document.querySelector(".asre-sales-report-email-setting").style.overflowY  = "unset";
	  dialog.close();
	});
</script>
<?php } ?>
