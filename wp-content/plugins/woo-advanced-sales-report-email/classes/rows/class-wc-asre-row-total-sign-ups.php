<?php

if ( !defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_ASRE_Row_Total_Sign_Ups extends WC_ASRE_Report_Row {

	/**
	 * The constructor
	 *
	 * @param $date_range
	 *
	 * @since  1.0.0
	 */
	public function __construct( $date_range ) {
		parent::__construct( $date_range, 'total-sign-ups', __( 'Total Sign Ups' ) );
	}

	/**
	 * Prepare the data
	 *
	 * @since  1.0.0
	 */
	public function prepare() {

		$users_query = new WP_User_Query(
			array(
				'role' => 'customer',
				'number' => -1,
				'date_query' => array(
					array(
						'after' => $this->get_date_range()->get_start_date()->format( 'Y-m-d H:i:s' ),
						'before' => $this->get_date_range()->get_end_date()->format( 'Y-m-d H:i:s' ),
					)
				)
			)
		);

		$this->set_value( $users_query->total_users );
	}

}
