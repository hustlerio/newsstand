<?php

if ( !defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_ASRE_Row_Top_Category extends WC_ASRE_Report_Row {

	/**
	 * The constructor
	 *
	 * @param $date_range
	 *
	 * @since  1.0.0
	 */
	public function __construct( $date_range ) {
		parent::__construct( $date_range, 'top-categories', __( 'Top Selling Categories' ) );
	}
	
	/**
	 * Prepare the data
	 *
	 * @since  1.0.0
	 */
	public function prepare() {
		
		// Get setting data
		$data = wc_sales_report_email()->admin->get_email_data();
		$limit_row = array();
		$limit_row = $data->display_top_categories_row;
		
		$start_date = $this->get_date_range()->get_start_date()->format( 'Y-m-d H:i:s' );
		$end_date = $this->get_date_range()->get_end_date()->format( 'Y-m-d H:i:s' );
		
		$categories_data_store = new \Automattic\WooCommerce\Admin\API\Reports\Categories\DataStore();
		$top_categories       = $limit_row > 0 ? $categories_data_store->get_data(
			apply_filters(
				'woocommerce_analytics_categories_query_args',
				array(
					'orderby'       => 'items_sold',
					'order'         => 'desc',
					'before'       	=> $end_date,
					'after'        	=> $start_date,
					'per_page'      => $limit_row,
					'extended_info' => true,
				)
			)
		)->data : array();
		
		ob_start(); ?>
		<?php if ( is_array($top_categories) && 0 != count( $top_categories ) ) { ?>
		<h3 class="report-table-title"><?php esc_html_e('Top Selling Categories', 'sales-report-email-pro'); ?></h3>
		<table class="report-table-widget" cellspacing="0" cellpadding="6" style="width: 100%;vertical-align:top;">
		<tbody>	
			<tr>
				<th style="text-align: left;border-left: 0;"><?php esc_html_e('Category Name', 'woocommerce'); ?></th>
				<th style="width: 25%;"><?php esc_html_e('Quantity', 'woocommerce'); ?></th>
				<th style="width: 25%;"><?php esc_html_e('Total Amount', 'sales-report-email-pro'); ?></th>
			</tr>
			<?php if ( is_array($top_categories) && count( $top_categories ) == 0 ) { ?>
				<tr>					
					<td colspan="3" style="text-align: left;"><?php esc_html_e( 'data not available.', 'sales-report-email-pro' ); ?></td>
				</tr>
			<?php } ?>
			<?php foreach (array_slice($top_categories, 0, $limit_row) as $top_category) { ?>
				<?php $category_name = isset( $top_category['extended_info'] ) && isset( $top_category['extended_info']['name'] ) ? $top_category['extended_info']['name'] : ''; ?>
					<tr>
						<td style="text-align: left;"><?php echo wp_kses_post($category_name); ?></td>
						<td><?php echo wp_kses_post($top_category['items_sold']); ?></td>
						<td><?php echo wp_kses_post(wc_price($top_category['net_revenue'])); ?></td>
					</tr>
			<?php } ?>				
		</tbody>
		</table>
		<?php
		}
		$value = ob_get_contents();
		ob_end_clean();
		$this->set_value( $value );
	}
	
	public function category_shorts( $a, $b ) {
		return $a['total_sale_counts'] < $b['total_sale_counts'];
	}

}
