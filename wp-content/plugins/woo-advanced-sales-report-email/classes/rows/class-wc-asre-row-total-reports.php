<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_ASRE_Row_Total_Reports extends WC_ASRE_Report_Row {

	/**
	 * The constructor
	 *
	 * @param $date_range
	 *
	 * @since  1.0.0
	 */
	public function __construct( $date_range ) {
		parent::__construct( $date_range, 'total-reports', __( 'Reports in this period' ) );
	}

	/**
	 * Prepare the data
	 *
	 * @since  1.0.0
	 */
	public function prepare() {
		
		$Interval = array(
			'daily' => 'day',
			'previous_day' => 'day',
			'daily-overnight' => 'day',
			'previous_overnight' => 'day',
			'weekly' => 'week',
			'previous_week' => 'week',
			'monthly' => 'month',
			'previous_month' => 'month',
			'last-30-days' => 'month',
			'previous-last-30-days' => 'month',
		);
		
		$args = array(
			'before' 	=> $this->get_date_range()->get_end_date()->format( 'Y-m-d H:i:s' ),
			'after'  	=> $this->get_date_range()->get_start_date()->format( 'Y-m-d H:i:s' ),
			'interval' 	=> $Interval[$this->get_date_range()->get_interval()],
		);
		
		$reports = new \Automattic\WooCommerce\Admin\API\Reports\Revenue\Query( $args );
		$mydata = $reports->get_data();

		// Set the value
		$this->set_value( $mydata->totals );

	}
}
