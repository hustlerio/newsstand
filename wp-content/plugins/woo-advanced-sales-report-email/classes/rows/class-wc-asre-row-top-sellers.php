<?php

if ( !defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_ASRE_Row_Top_Sellers extends WC_ASRE_Report_Row {

	/**
	 * The constructor
	 *
	 * @param $date_range
	 *
	 * @since  1.0.0
	 */
	public function __construct( $date_range ) {
		parent::__construct( $date_range, 'top-sellers', 'Top Selling Products' );
	}

	/**
	 * Prepare the data
	 *
	 * @since  1.0.0
	 */
	public function prepare() {
		
		// Get setting data
		$data = wc_sales_report_email()->admin->get_email_data();
		$limit_row = array();
		$limit_row = $data->display_top_sellers_row;
		
		$start_date = $this->get_date_range()->get_start_date()->format( 'Y-m-d H:i:s' );
		$end_date = $this->get_date_range()->get_end_date()->format( 'Y-m-d H:i:s' );
		
		$products_data_store = new \Automattic\WooCommerce\Admin\API\Reports\Products\DataStore();
		$top_sellers       = $limit_row > 0 ? $products_data_store->get_data(
			apply_filters(
				'woocommerce_analytics_products_query_args',
				array(
					'orderby'       => 'items_sold',
					'order'         => 'desc',
					'before'       => $end_date,
					'after'        => $start_date,
					'per_page'      => $limit_row,
					'extended_info' => true,
				)
			)
		)->data : array();
		
		ob_start();
		if ( is_array($top_sellers) && 0 != count( $top_sellers ) ) { 
			?>
			<h3 class="report-table-title"><?php esc_html_e('Top Selling Products', 'sales-report-email-pro'); ?></h3>        
			<table class="report-table-widget" cellspacing="0" cellpadding="6" style="width: 100%;vertical-align:top;">
				<tbody class="report-table-list">	
					<tr>
						<th style="text-align: left;border-left: 0;"><?php esc_html_e('Product Name', 'woocommerce'); ?></th>
						<th style="width: 25%;"><?php esc_html_e('Quantity', 'sales-report-email-pro'); ?></th>
						<th style="width: 25%;"><?php esc_html_e('Total Amount', 'woocommerce'); ?></th>
					</tr>
					<?php if ( is_array($top_sellers) && 0 == count( $top_sellers ) ) { ?>
						<tr>
							<td colspan="3" style="text-align: left;"><?php esc_html_e( 'data not available.', 'sales-report-email-pro' ); ?></td>
						</tr>
					<?php 
					} else { 
						foreach (array_slice($top_sellers, 0, $limit_row) as $top_seller) {
							$name = $top_seller['extended_info']['name'] ? wp_kses_post($top_seller['extended_info']['name'] ) : '';
							$sku = $top_seller['extended_info']['sku'] ? ' (' . wp_kses_post($top_seller['extended_info']['sku'] ) . ')' : '';
							?>
							<tr>					
								<td style="text-align: left;"><?php echo $name . $sku ; ?></td>
								<td><?php echo wp_kses_post($top_seller['items_sold']); ?></td>
								<td><?php echo wp_kses_post(wc_price($top_seller['net_revenue'])); ?></td>
							</tr>		
						<?php } } ?>		
				</tbody>
			</table><br>
		<?php
		}
		$value = ob_get_contents();
		ob_end_clean();

		$this->set_value( $value );
	}

}
