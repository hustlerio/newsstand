<?php
/**
 * Sales report email
 *
 * Class WC_ASRE_Admin
 * 
 * @version       1.0.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_ASRE_Admin { 

	/**
	 * Instance of this class.
	 *
	 * @var object Class Instance
	 */
	private static $instance;
	
	
	/**
	 * Get the class instance
	 *
	 * @return WC_ASRE_Admin
	*/
	public static function get_instance() {

		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
	
	
	/**
	 * Initialize the main plugin function
	 * 
	 * @since  1.0
	*/
	public function __construct() {
		$this->init();
	}
	
	/*
	 * init function
	 *
	 * @since  1.0
	*/
	public function init() {
		
		//adding hooks
		
		global $wpdb;
		$this->table = $wpdb->prefix . 'asre_sales_report';
		$this->screen_id = 'woocommerce-advanced-sales-report-email';
		
		//callback for admin menu register		
		add_action('admin_menu', array( $this, 'register_woocommerce_menu' ), 99 );
		
		// Handle the enable/disable/delete actions.
		add_action( 'admin_init', array( $this, 'data_toggle_callback' ) );
		
		// save/update of report settings hook
		add_action( 'wp_ajax_report_data_update', array( $this, 'data_update_callback' ) );
		
		// enable toggle in report list hook
		add_action( 'wp_ajax_enable_toggle_data_update', array( $this, 'update_enable_toggle_callback' ) );
		
		// cron run date update in report setting hook
		add_action( 'wp_ajax_cron_run_date_update', array( $this, 'update_cron_run_date_callback' ) );
		
		//ajax call to get preview of sales report
		add_action( 'wp_ajax_preview_asre_page', array( $this, 'run_preview_func') );
		
		// Cron hook
		add_action( WC_ASRE_Cron_Manager::CRON_HOOK, array( $this, 'cron_email_callback' ) );
		
		//ajax call to send test mail 
		add_action( 'wp_ajax_send_test_sales_email', array( $this, 'send_test_sales_email_func' ) );
		
		// Hook for add admin body class in settings page
		add_filter( 'admin_body_class', array( $this, 'asre_post_admin_body_class' ), 100, 1 );
	}
	
	/*
	* plugin file directory function
	*/	
	public function plugin_dir_url() {
		return plugin_dir_url( __FILE__ );
	}
	
	/*
	* add unique body class
	*/
	public function asre_post_admin_body_class( $body_class ) {
		
		if (!isset($_GET['page'])) {
			return $body_class;
		}
		if ( 'woocommerce-advanced-sales-report-email' == $_GET['page'] ) {
			$body_class .= ' asre-sales-report-email-setting ';
		}

		return $body_class;
	}
	
	/*
	* Admin Menu add function
	* WC sub menu
	*/
	public function register_woocommerce_menu() {
		add_submenu_page( 'woocommerce', 'Sales Report Email', 'Sales Report Email', 'manage_options', 'woocommerce-advanced-sales-report-email', array( $this, 'woocommerce_sales_report_page_callback' ) ); 
	}
	
	/*
	 * get_zorem_pluginlist
	 * 
	 * return array
	*/
	public function get_zorem_pluginlist() {
		
		if ( !empty( $this->zorem_pluginlist ) ) {
			return $this->zorem_pluginlist;
		}

		if ( empty($this->zorem_pluginlist) ) {
			
			$response = wp_remote_get( 'https://www.zorem.com/wp-json/pluginlist/v1/' );
			
			if ( is_array( $response ) && ! is_wp_error( $response ) ) {
				$body    = $response['body']; // use the content
				$plugin_list = json_decode( $body );
				set_transient( 'zorem_pluginlist', $plugin_list, 60*60*24 );
				$this->zorem_pluginlist = $plugin_list;
			} else {
				$this->zorem_pluginlist = array();
			}
		}
		return $this->zorem_pluginlist;
	}
	
	
	/*
	* callback for Sales Report Email page
	*/
	public function woocommerce_sales_report_page_callback() {	
		
		global $wpdb;

		// Check the user capabilities
		if ( ! current_user_can( 'manage_woocommerce' ) ) {
			wp_die( esc_html( 'You do not have sufficient permissions to access this page.', 'woocommerce-cart-notices' ) );
		}

		$tab = isset( $_GET['tab'] ) ? sanitize_text_field($_GET['tab']) : 'list';

		if ( 'list' === $tab ) {
			
			$data = $this->get_data();

		} elseif ( 'edit' === $tab ) {

			$id = isset( $_GET['id'] ) ? sanitize_text_field($_GET['id']) : '';
			$data = $this->get_data_byid( $id );
			
			if ( !is_object( $data ) ) {
			  $data = new stdClass();
			  $data->data = array(); 
			}         
			
			
			$this->data = $data;

			if ( ! $data && '0' != $id ) {
				wp_die( 'The requested data could not be found!', 'woocommerce-cart-notices' );
			}
			if ( '0' == $id ) {
				$this->data->id = '0';	
			}

		}
		?>
			<div class="zorem-layout__header">
				<img class="zorem-layout__header-logo" src="<?php echo wc_sales_report_email()->plugin_dir_url(__FILE__) . 'assets/images/sre-logo.png';?>">			
				<div class="woocommerce-layout__activity-panel">
					<div class="woocommerce-layout__activity-panel-tabs">
						<button type="button" id="activity-panel-tab-help" class="components-button woocommerce-layout__activity-panel-tab">
							<span class="dashicons dashicons-editor-help"></span>
							Help 
						</button>
					</div>
					<div class="woocommerce-layout__activity-panel-wrapper">
						<div class="woocommerce-layout__activity-panel-content" id="activity-panel-true">
							<div class="woocommerce-layout__activity-panel-header">
								<div class="woocommerce-layout__inbox-title">
									<p class="css-activity-panel-Text">Documentation</p>            
								</div>								
							</div>
							<div>
								<ul class="woocommerce-list woocommerce-quick-links__list">
									<li class="woocommerce-list__item has-action">
										<?php
										$support_link = 'https://wordpress.org/support/plugin/woo-advanced-sales-report-email/#new-topic-0' ;
										?>
										<a href="<?php echo esc_url( $support_link ); ?>" class="woocommerce-list__item-inner" target="_blank" >
											<div class="woocommerce-list__item-before">
												<img src="<?php echo wc_sales_report_email()->plugin_dir_url(__FILE__) . 'assets/images/get-support-icon.svg';?>">	
											</div>
											<div class="woocommerce-list__item-text">
												<span class="woocommerce-list__item-title">
													<div class="woocommerce-list-Text">Get Support</div>
												</span>
											</div>
											<div class="woocommerce-list__item-after">
												<span class="dashicons dashicons-arrow-right-alt2"></span>
											</div>
										</a>
									</li>            
									<li class="woocommerce-list__item has-action">
										<a href="https://www.zorem.com/docs/sales-report-email-for-woocommerce/?utm_source=wp-admin&utm_medium=SREDOCU&utm_campaign=add-ons" class="woocommerce-list__item-inner" target="_blank">
											<div class="woocommerce-list__item-before">
												<img src="<?php echo wc_sales_report_email()->plugin_dir_url(__FILE__) . 'assets/images/documentation-icon.svg';?>">
											</div>
											<div class="woocommerce-list__item-text">
												<span class="woocommerce-list__item-title">
													<div class="woocommerce-list-Text">Documentation</div>
												</span>
											</div>
											<div class="woocommerce-list__item-after">
												<span class="dashicons dashicons-arrow-right-alt2"></span>
											</div>
										</a>
									</li>
									<li class="woocommerce-list__item has-action">
										<a href="https://www.zorem.com/product/sales-report-email-for-woocommerce/?utm_source=wp-admin&utm_medium=SRE&utm_campaign=add-ons" class="woocommerce-list__item-inner" target="_blank">
											<div class="woocommerce-list__item-before">
												<img src="<?php echo wc_sales_report_email()->plugin_dir_url(__FILE__) . 'assets/images/upgrade.svg';?>">
											</div>
											<div class="woocommerce-list__item-text">
												<span class="woocommerce-list__item-title">
													<div class="woocommerce-list-Text">Upgrade To Pro</div>
												</span>
											</div>
											<div class="woocommerce-list__item-after">
												<span class="dashicons dashicons-arrow-right-alt2"></span>
											</div>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div class="woocommerce asre_admin_layout">
				<div class="asre_admin_content">
					<div class="tab-row" <?php if ( isset($_GET['tab']) && 'edit' == $_GET['tab'] ) { ?>
						style="display:none;"
					<?php } ?>>
					<input id="asre_tab1" type="radio" name="tabs" class="asre_tab_input" data-tab="list" checked>
					<a for="asre_tab1" href="admin.php?page=<?php echo esc_html($this->screen_id); ?>&amp;tab=list" class="asre_tab_label first_label <?php echo ( 'list' === $tab ) ? 'nav-tab-active' : ''; ?>"><?php esc_html_e('Reports', 'woocommerce'); ?></a>
					<?php 
						//callback do_action for license tab
						do_action( 'asre_tab2_data_array' ); 
					?>
					<input id="asre_tab4" type="radio" name="tabs" class="asre_tab_input" data-tab="add-ons" 
					<?php 
					if ( isset($_GET['tab']) && ( 'add-ons' == $_GET['tab'] ) ) { 
						echo 'checked'; 
					} 
					?> 
					>
					<a for="asre_tab4" href="admin.php?page=<?php echo esc_html($this->screen_id); ?>&amp;tab=add-ons" class="asre_tab_label <?php echo ( 'list' === $tab ) ? 'nav-tab-active' : ''; ?>"><?php esc_html_e('Go Pro', 'sales-report-email-pro-addon'); ?></a>
					</div>
					<div class="menu_devider"></div>
					<?php
					if (  'list' == $tab || 'edit' == $tab ) { 
						require_once( 'views/asre_reports_tab.php' ); 
					}
					?>
					<?php
					if (  'add-ons' == $tab ) { 
						require_once( 'views/asre_addons_tab.php' );
					}
					?>
					<?php 
						//callback do_action for license tab content
						do_action( 'asre_license_tab_content_data_array' ); 
					?>
				</div>
			</div>
	<?php		
	}
	
	
	/*
	* get all data 
	*/
	public function get_data() {
		global $wpdb;

		// Avoid database table not found errors when plugin is first installed
		// by checking if the plugin option exists
		if ( empty( $this->data ) ) {
			$this->data = array();

			$wpdb->hide_errors();
			
			$results = $wpdb->get_results( "SELECT * FROM {$this->table} ORDER BY id DESC" ); //ORDER BY name ASC
			
			if ( ! empty( $results ) ) {
				
				foreach ( $results as $key => $result ) {
					$results[ $key ]->email_enable = maybe_unserialize( $results[ $key ]->email_enable );
					$results[ $key ]->report_name = maybe_unserialize( $results[ $key ]->report_name );
					$results[ $key ]->email_interval = maybe_unserialize( $results[ $key ]->email_interval );
					$results[ $key ]->email_recipients = maybe_unserialize( $results[ $key ]->email_recipients );
				}

				$this->data = $results;
			}
		}
		return $this->data;
	}
	
	
	/*
	* get data by id
	*/
	public function get_data_byid( $id ) {
		global $wpdb;
		$results = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$this->table} WHERE id = %d", $id ) );
		if ( ! empty( $results ) ) {
			$results->email_enable = maybe_unserialize( $results->email_enable );
			$results->report_name = maybe_unserialize( $results->report_name );
			$results->email_interval = maybe_unserialize( $results->email_interval );
			$results->email_recipients = maybe_unserialize( $results->email_recipients );
		}
		return $results;
	}
	
	/*
	* get next cron run date in report list column
	*/	
	public function next_run_date( $data ) {
		$hrtime = $data->email_send_time ;

		$week = array(
			esc_html( 'Sunday', 'woocommerce' ),
			esc_html( 'Monday', 'woocommerce' ),
			esc_html( 'Tuesday', 'woocommerce' ),
			esc_html( 'Wednesday', 'woocommerce' ),
			esc_html( 'Thursday', 'woocommerce' ),
			esc_html( 'Friday', 'woocommerce' ),
			esc_html( 'Saturday', 'woocommerce' ),
		);
		$run = $data->email_interval;
		
		if ( 'monthly' == $run ) {
			$select_day_for_month = $data->email_select_month;
			if ( gmdate('j') == $select_day_for_month && current_time( 'timestamp' ) < strtotime(gmdate('Y-m-d ' . $hrtime)) ) {
				return gmdate('Y-m-d ' . $hrtime);
			} else {
				$next15th = mktime( 0, 0, 0, gmdate( 'n' ) + ( gmdate( 'j' ) >= $select_day_for_month ), $select_day_for_month );
				return gmdate('Y-m-d ' . $hrtime, $next15th);
			}
		}
		
		if ( 'weekly' == $run ) {
			$select_day_for_week = $data->email_select_week;
			if ( gmdate('w') == $select_day_for_week && current_time( 'timestamp' ) < strtotime(gmdate('Y-m-d ' . $hrtime)) ) {
				return gmdate('Y-m-d ' . $hrtime);
			} else {
				return gmdate('Y-m-d ' . $hrtime, strtotime('next ' . $week[$select_day_for_week]));
			}
		}
		
		if ( 'daily' == $run || 'daily-overnight' == $run ) {
			if ( current_time( 'timestamp' ) < strtotime(gmdate('Y-m-d ' . $hrtime)) ) {
				//today
				$datetime = new DateTime();
				return $datetime->format('Y-m-d ' . $hrtime);
			} else {
				$datetime = new DateTime('tomorrow');
				return $datetime->format('Y-m-d ' . $hrtime);
			}
		}

	}
	
	/**
	 * Handle the enable/disable/delete actions.
	 *
	 * @since 1.0
	*/
	public function data_toggle_callback() {
		global $wpdb;

		// If on the WC Email reports screen & the current user can manage WooCommerce, continue.
		if ( isset( $_GET['page'] ) && $this->screen_id === $_GET['page'] && current_user_can( 'manage_woocommerce' ) ) {

			$action = isset( $_GET['action'] ) ? sanitize_text_field($_GET['action']) : false;

			// If no action or cart notice ID are set, bail.
			if ( ! $action || ! isset( $_GET['id'] ) ) {
				return;
			}

			$id = (int) $_GET['id'];

			if ( 'enable' === $action ) {

				$wpdb->query( $wpdb->prepare( "UPDATE {$this->table} SET email_enable=true WHERE id = %d", $id ) );

				wp_redirect( esc_url_raw( add_query_arg( array( 'page' => $this->screen_id, 'result' => 'enabled' ), 'admin.php' ) ) );
				exit;

			} elseif ( 'disable' === $action ) {

				$wpdb->query( $wpdb->prepare( "UPDATE {$this->table} SET email_enable=false WHERE id = %d", $id ) );

				wp_redirect( esc_url_raw( add_query_arg( array( 'page' => $this->screen_id, 'result' => 'disabled' ), 'admin.php' ) ) );
				exit;

			} elseif ( 'delete' === $action ) {

				$wpdb->query( $wpdb->prepare( "DELETE FROM {$this->table} WHERE id = %d", $id ) );

				wp_redirect( esc_url_raw( add_query_arg( array( 'page' => $this->screen_id, 'result' => 'deleted' ), 'admin.php' ) ) );

				$cron_manager = new WC_ASRE_Cron_Manager();
				$cron_manager->remove_cron($id);
				exit;
			}
		}
	}
	
	/*
	* save/update existing entry
	*/
	public function data_update_callback() {
		
		$id = isset( $_POST['id'] ) ? sanitize_text_field($_POST['id']) : '';
		
		check_ajax_referer( 'asre_nonce_' . sanitize_text_field($id), 'asre_nonce_verify' );
		
		global $wpdb;
		$data = $this->get_data();
		
		if ('0' == $id) {
			//if 1 or more record exist and licence not found
			if ( !class_exists( 'Sales_Report_Email_PRO' ) && count($data) > 1 ) {
				$array = array(
					'status' => 'fail',
					'msg' => 'you have not pro plguin',
				);
				echo json_encode($array);
				die();
			}
			
			$data = array(
				'report_name' => isset( $_POST['report_name'] ) ? sanitize_text_field($_POST['report_name']) : '',
			);
			$wpdb->insert( $this->table, $data );
			$id = $wpdb->insert_id;
		}
			
		//get form field
		$form_data_1 = $this->asre_filter_widget_data();
		$form_data_2 = $this->asre_general_data();
		$form_data_3 = $this->asre_schadule_data();
		$form_data_4 = $this->asre_status_data();
		$form_data_5 = $this->asre_report_option_data();
		$form_data_6 = $this->asre_display_total_option_data();
		$form_data_7 = $this->asre_display_detail_option_data();
		$form_data_8 = $this->asre_display_total_subscription_data();
		$form_data_9 = $this->asre_display_detail_subscription_data();
		$form_data_10 = $this->asre_hour_option_data();
		
		//data to be saved
		$data = array();
		foreach ( (array) $form_data_1 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key]) && !empty($_POST[$key])) {				
				$data[$key] = serialize(wc_clean($_POST[$key]));
			} else {
				$data[$key] = serialize(array());
			}
		}

		foreach ( (array) $form_data_2 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key])) {
				$data[$key] = sanitize_text_field($_POST[$key]);
			}
		}
		foreach ( (array) $form_data_3 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key])) {
				$data[$key] = sanitize_text_field($_POST[$key]);
			}
		}
		foreach ( (array) $form_data_4 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key])) {
				$data[$key] = sanitize_text_field($_POST[$key]);
			}
		}
		foreach ( (array) $form_data_5 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key])) {
				$data[$key] = sanitize_text_field($_POST[$key]);
			}
		}
		foreach ( (array) $form_data_6 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key])) {
				$data[$key] = sanitize_text_field($_POST[$key]);
			}
		}
		
		foreach ( (array) $form_data_7 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key])) {
				$data[$key] = sanitize_text_field($_POST[$key]);
				$data['display_top_sellers_row'] = isset( $_POST['display_top_sellers_row'] ) ? sanitize_text_field($_POST['display_top_sellers_row']) : '';
				$data['display_top_categories_row'] = isset( $_POST['display_top_categories_row'] ) ? sanitize_text_field($_POST['display_top_categories_row']) : '';
				if ( class_exists( 'Sales_Report_Email_PRO' ) ) {
					$data['display_sales_by_billing_city_row'] = isset( $_POST['display_sales_by_billing_city_row'] ) ? sanitize_text_field($_POST['display_sales_by_billing_city_row']) : '';
					$data['display_sales_by_shipping_city_row'] = isset( $_POST['display_sales_by_shipping_city_row'] ) ? sanitize_text_field($_POST['display_sales_by_shipping_city_row']) : '';
					$data['display_sales_by_billing_state_row'] = isset( $_POST['display_sales_by_billing_state_row'] ) ? sanitize_text_field($_POST['display_sales_by_billing_state_row']) : '';
					$data['display_sales_by_shipping_state_row'] = isset( $_POST['display_sales_by_shipping_state_row'] ) ? sanitize_text_field($_POST['display_sales_by_shipping_state_row']) : '';
					$data['display_sales_by_billing_country_row'] = isset( $_POST['display_sales_by_billing_country_row'] ) ? sanitize_text_field($_POST['display_sales_by_billing_country_row']) : '';
					$data['display_sales_by_shipping_country_row'] = isset( $_POST['display_sales_by_shipping_country_row'] ) ? sanitize_text_field($_POST['display_sales_by_shipping_country_row']) : '';
					$data['display_sales_by_coupons_row'] = isset( $_POST['display_sales_by_coupons_row'] ) ? sanitize_text_field($_POST['display_sales_by_coupons_row']) : '';
				}
			}
		}
		foreach ( (array) $form_data_8 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key])) {
				$data[$key] = sanitize_text_field($_POST[$key]);
			}
		}
		
		foreach ( (array) $form_data_9 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key])) {
				$data[$key] = sanitize_text_field($_POST[$key]);
			}
		}
		
		foreach ( (array) $form_data_10 as $key => $val ) {
			$type = $val['type'];
			
			if ( 'title' == $type ) {
				continue;
			}
			if (isset($_POST[$key])) {
				$data[$key] = sanitize_text_field($_POST[$key]);
			}
		}
		
		//check column exist
		$tabledata = $wpdb->get_row( sprintf("SELECT * FROM %s LIMIT 1", $this->table) );
		foreach ( (array) $data as $key1 => $val1  ) {
			if ( 'email_enable' == $key1 ) {
				continue;
			}
			if ( 'report_name' == $key1 ) {
				continue;
			}
			if (!isset($tabledata->$key1)) {
				$wpdb->query( sprintf( 'ALTER TABLE %s ADD %s text NOT NULL', $this->table, $key1 ) );
			}
		}
		
		
		$array = array();
	
		$where = array(
			'id' => $id,
		);
		
		$result = $wpdb->update( $this->table, $data, $where );
		
		$this->reset_cron($id);
		
		$array = array(
			'status' => 'success',
			'id' => $id,
			'asre_nonce_verify' => wp_create_nonce( 'asre_nonce_' . $id ), 
		);
		echo json_encode($array);
		die();
	}
	
	/*
	* if (pro not exist) update enable setting of more reports existing entry
	*/	
	public function update_email_enable_options_callback() {
		
		if ( class_exists( 'Sales_Report_Email_PRO' ) ) {
			return;
		}
		
		global $wpdb;
		
		$results = $wpdb->get_results( "SELECT * FROM {$this->table} WHERE ID != (SELECT MAX(ID) FROM {$this->table}) ORDER BY id DESC" );//ORDER BY name ASC

		foreach ($results as $data_list) {
			$data = array(
				'email_enable' => '0',
			);
			$where = array(
				'id' => $data_list->id,
			);
			$result = $wpdb->update( $this->table, $data, $where );
		}
	
	}
	
	/*
	* update report enable toggle of existing entry
	*/	
	public function update_enable_toggle_callback() {
		
		$nonce = isset( $_POST['nonce'] ) ? sanitize_text_field($_POST['nonce']) : '';
		if ( ! wp_verify_nonce( $nonce, 'asre-ajax-nonce' ) ) {
			die();
		}
		
		global $wpdb;
		$id = isset( $_POST['ID'] ) ? sanitize_text_field($_POST['ID']) : '';
				
		if ( isset($_POST['check']) && 'true' == $_POST['check'] ) {
			$check = 1;	
		} else {
			$check = 0;		
		}

		$array = array();
		$data = array(
			'email_enable' => $check,
		);
		$where = array(
			'id' => $id,
		);

		$result = $wpdb->update( $this->table, $data, $where );

		$array = array(
			'status' => 'success',
			'id' => $id,
		);

		echo json_encode($data);
		die();
	}
	
	/*
	* update cron run date callback
	*/	
	public function update_cron_run_date_callback() {
		
		$nonce = isset( $_POST['nonce'] ) ? sanitize_text_field($_POST['nonce']) : '';
		if ( ! wp_verify_nonce( $nonce, 'asre-ajax-nonce' ) ) {
			die();
		}
		
		$TIME = isset( $_POST['TIME'] ) ? sanitize_text_field($_POST['TIME']) : '';
		$hrtime = $TIME;

		$week = array(
			esc_html( 'Sunday', 'woocommerce' ),
			esc_html( 'Monday', 'woocommerce' ),
			esc_html( 'Tuesday', 'woocommerce' ),
			esc_html( 'Wednesday', 'woocommerce' ),
			esc_html( 'Thursday', 'woocommerce' ),
			esc_html( 'Friday', 'woocommerce' ),
			esc_html( 'Saturday', 'woocommerce' ),
		);

		$INTERVAL = isset( $_POST['INTERVAL'] ) ? sanitize_text_field($_POST['INTERVAL']) : '';
		$MONTH = isset( $_POST['MONTH'] ) ? sanitize_text_field($_POST['MONTH']) : '';
		$WEEK = isset( $_POST['WEEK'] ) ? sanitize_text_field($_POST['WEEK']) : '';
		$run = $INTERVAL;

		if ( 'monthly' == $run ) {
			$select_day_for_month = $MONTH;
			if ( gmdate('j') == $select_day_for_month && current_time('timestamp') < strtotime(gmdate('Y-m-d ' . $hrtime)) ) {
				$NextRunDate = gmdate('Y-m-d ' . $hrtime);
			} else {
				$next15th = mktime( 0, 0, 0, gmdate( 'n' ) + ( gmdate( 'j' ) >= $select_day_for_month ), $select_day_for_month ); 
				$NextRunDate = gmdate('Y-m-d ' . $hrtime, $next15th);
			}
		}

		if ( 'weekly' == $run || 'last-30-days' == $run ) {
			$select_day_for_week = $WEEK;
			if ( gmdate('w') == $select_day_for_week && current_time('timestamp') < strtotime(gmdate('Y-m-d ' . $hrtime)) ) {
				$NextRunDate = gmdate('Y-m-d ' . $hrtime);
			} else {
				$NextRunDate = gmdate('Y-m-d ' . $hrtime, strtotime('next ' . $week[$select_day_for_week]));
			}
		}

		if ( 'daily' == $run || 'daily-overnight' == $run ) {
			if ( current_time('timestamp') < strtotime(gmdate('Y-m-d ' . $hrtime)) ) {
				//today
				$datetime = new DateTime();
				$NextRunDate = $datetime->format('Y-m-d ' . $hrtime);
			} else {
				$datetime = new DateTime('tomorrow');
				$NextRunDate = $datetime->format('Y-m-d ' . $hrtime);
			}
		}
		$newDate = gmdate('M d, Y g:iA', strtotime($NextRunDate));

		$array = array(
			'NextRunDate'	=> $newDate,
			'interval' => $INTERVAL,
			'sendTime' => $TIME,
			'week' => $WEEK,
			'month' => $MONTH,
		);

		echo json_encode($array);
		die();
	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_filter_widget_data() {
		
		//setting fields array

	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_general_data() {
		$form_data = array(
		   'report_name' => array(
				'type'		=> 'text',
				'title'		=> esc_html( 'Report Title', 'woocommerce' ),
				'show'		=> true,
				'class'     => '',
				'tooltip'	=> esc_html( 'the name of the report for internal use', 'woocommerce-advanced-sales-report-email' ),
				'placeholder' => esc_html( 'Add title', 'woocommerce-advanced-sales-report-email' ),
			),
		  'email_recipients' => array(
			  'type'		=> 'text',
			  'title'		=> esc_html( 'Recipient(s)', 'woocommerce' ),
			  'tooltip'      => '',
			  'show'		=> true,
			  'class'     => 'general',
			  'placeholder' => esc_html( 'E.g. {customer.email}, admin@example.org' ),
			  'tooltip'     => esc_html( 'enter the recipients email addresses, separated by commas.', 'woocommerce-advanced-sales-report-email'),
		  ),		
		  'email_subject' => array(
			  'type'		=> 'text',
			  'title'		=> esc_html( 'Subject', 'woocommerce' ),				
			  'show'		=> true,
			  'class'     => 'general',
			  'placeholder' => esc_html( 'Sales Report for {site_title}.', 'woocommerce-advanced-sales-report-email' ),
			  'tooltip'     => esc_html( 'available Placeholders: {site_title}', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'email_content' => array(
			  'type'		=> 'textarea',
			  'title'		=> esc_html( 'Email Content', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> true,
			  'class'     => 'general',
			  'placeholder' => esc_html( 'Hi there. Please find your sales report for {site_title} below.', 'woocommerce-advanced-sales-report-email' ),
			  'tooltip'     => esc_html( 'available Placeholders: {site_title}', 'woocommerce-advanced-sales-report-email'),
		  ),	
		);
		return $form_data;
	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_schadule_data() {
		
		$send_time_array = array();
				
		for ( $hour = 0; $hour < 24; $hour++ ) {
			for ( $min = 0; $min < 60; $min = $min + 30 ) {
				$this_time = gmdate( 'H:i', strtotime( "2014-01-01 $hour:$min" ) );
				$send_time_array[ $this_time ] = $this_time;
			}			
		}	
		
		$month = array(
			'1'=>'1', '5'=>'5','10'=>'10', '15'=>'15', '20'=>'20', '25'=>'25', '30'=>'30'
		);
		
		$week = array(
			esc_html( 'Sunday', 'woocommerce' ),
			esc_html( 'Monday', 'woocommerce' ),
			esc_html( 'Tuesday', 'woocommerce' ),
			esc_html( 'Wednesday', 'woocommerce' ),
			esc_html( 'Thursday', 'woocommerce' ),
			esc_html( 'Friday', 'woocommerce' ),
			esc_html( 'Saturday', 'woocommerce' ),
		);
		if (class_exists( 'Sales_Report_Email_PRO' )) {
			$interval = array(
				'daily'   => esc_html( 'Daily', 'woocommerce' ),
				'daily-overnight'   => esc_html( 'Daily Flex Overnight', 'woocommerce' ),
				'weekly'  => esc_html( 'Weekly', 'woocommerce' ),
				'monthly' => esc_html( 'Monthly', 'woocommerce' ),
				'last-30-days' => esc_html( 'Last 30 Days', 'woocommerce' )
			);
		} else {
			$interval = array(
				'daily'   => esc_html( 'Daily', 'woocommerce' ),
				'weekly'  => esc_html( 'Weekly', 'woocommerce' ),
				'monthly' => esc_html( 'Monthly', 'woocommerce' ),
				'last-30-days' => esc_html( 'Last 30 Days', 'woocommerce' )
			);
		}
		
		
		$form_data = array(
		   'email_interval' => array(
			  'type'		=> 'dropdown',
			  'title'		=> esc_html( 'Interval', 'woocommerce-advanced-sales-report-email' ),		
			  'options'   => $interval,		
			  'show'		=> true,
			  'disabled'  	=> false,
			  'class'     => 'report-dropdown',
			  'tooltip'     => esc_html( 'The frequency of which the report should be sent.', 'woocommerce-advanced-sales-report-email'),
		  ),		

		  'email_select_week' => array(
			  'type'		=> 'dropdown',
			  'title'		=> esc_html( 'Day of Week', 'woocommerce-advanced-sales-report-email' ),
			  'tooltip'      => '',
			  'show'		=> true,
			  'disabled'  	=> false,
			  'options'   => $week,
			  'class'     => 'report-dropdown',
			  'placeholder' => esc_html( '', 'woocommerce-advanced-sales-report-email' ),
			  'tooltip'     => esc_html( 'Day of the week to send the report email.', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'email_select_month' => array(
			  'type'		=> 'dropdown',
			  'title'		=> esc_html( 'Day of Month', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'options'   => $month,
			  'class'     => 'report-dropdown',
			  'placeholder' => esc_html( '', 'woocommerce-advanced-sales-report-email' ),
			  'tooltip'     => esc_html( 'the day on the month to send the report email.', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'email_send_time' => array(
			  'type'		=> 'dropdown',
			  'title'		=> esc_html( 'Send Report At', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'class'     => 'report-dropdown',
			  'options'   => $send_time_array,
			  'placeholder' => esc_html( '', 'woocommerce-advanced-sales-report-email' ),
			  'tooltip'     => esc_html( 'the time of day to send out the report email.', 'woocommerce-advanced-sales-report-email'),
		  ),
		);
		return $form_data;
	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_status_data() {
		$form_data = array(
			'email_enable' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'Enable/Disable', 'woocommerce' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'block-options'	=> false,
			  'class'     => 'report-status',
			  'tooltip'     => esc_html( 'Enable this option to enable this email notification.', 'woocommerce-advanced-sales-report-email'),
			),
		);
		return $form_data;
	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_display_total_option_data() {

		$tax_field	 = array();
		if (!wc_tax_enabled()) {
			$tax_field = 'true';
		}
		
		$PRO = array();
		if ( !class_exists( 'Sales_Report_Email_PRO' ) ) {
			$PRO = true; 
		}
		
		$form_data = array(
		  'display_total_sales' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'Gross Revenue', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'block-options'  	=> true,
			  'class'     => '',
			  'tooltip'     => esc_html( 'sum of all orders including shipping & taxes with refunds taken off.', 'woocommerce-advanced-sales-report-email'),
		  ),
		   'display_coupon_used' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'Coupons', 'woocommerce' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'block-options'  	=> true,
			  'class'     => '',
			  'tooltip'     => esc_html( 'Total discounts with coupons.', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'display_total_refunds' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'Refunds', 'woocommerce' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'block-options'  	=> true,
			  'class'     => '',
			  'tooltip'     => esc_html( 'Total Refunds during the report period.', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'display_total_tax' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'Taxes', 'woocommerce' ),				
			  'show'		=> true,
			  'disabled'  	=> $tax_field,
			  'block-options'  	=> true,
			  'class'     => '',
			  'tooltip'     => esc_html( 'Total tax charges during the report period.', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'display_total_shipping' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'Shipping', 'woocommerce' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'block-options'  	=> true,
			  'class'     => '',
			  'tooltip'     => esc_html( 'Total shipping charges during the report period.', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'display_net_revenue' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'Net Revenue', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'block-options'  	=> true,
			  'class'     => '',
			  'tooltip'     => esc_html( 'sum of all orders, with refunds, shipping & taxes taken off.', 'woocommerce-advanced-sales-report-email'),
		  ),
		   'display_total_orders' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'Orders', 'woocommerce' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'block-options'  	=> true,
			  'class'     => '',
			  'tooltip'     => esc_html( 'Total count of orders in status Processing/Complete.', 'woocommerce-advanced-sales-report-email'),
		  ),
		   'display_total_items' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'Items Sold', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'block-options'  	=> true,
			  'class'     => '',
			  'tooltip'     => esc_html( 'Total items sold during the report period.', 'woocommerce-advanced-sales-report-email'),
		  ),
		   'display_signups' => array(
			  'type'		=> 'checkbox',
			  'title'		=> esc_html( 'New Customers', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> true,
			  'disabled'  	=> false,
			  'block-options'  	=> true,
			  'class'     => '',
			  'tooltip'     => esc_html( 'Total number of new signups during the report period.', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'display_average_order_value' => array(
			  'type'		=> 'PRO-Block',
			  'title'		=> esc_html( 'Avg Order Value', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> $PRO,
			  'disabled'  	=> true,
			  'block-options'  	=> true,
			  'class'     => 'pro-feature',
			  'tooltip'     => esc_html( 'Average Order Value during the report period.', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'display_average_daily_sales' => array(
			  'type'		=> 'PRO-Block',
			  'title'		=> esc_html( 'Avg Daily Sales', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> $PRO,
			  'disabled'  	=> true,
			  'block-options'  	=> true,
			  'class'     => 'pro-feature',
			  'tooltip'     => esc_html( 'Average Daily Sales during the report period.', 'woocommerce-advanced-sales-report-email'),
		  ),
		  'display_average_daily_items' => array(
			  'type'		=> 'PRO-Block',
			  'title'		=> esc_html( 'Avg Items per order', 'woocommerce-advanced-sales-report-email' ),				
			  'show'		=> $PRO,
			  'disabled'  	=> true,
			  'block-options'  	=> true,
			  'class'     => 'pro-feature',
			  'tooltip'     => esc_html( 'Average Items per order during the report period.', 'woocommerce-advanced-sales-report-email'),
		  )
		);
		$form_data = apply_filters( 'asre_display_total_option_data_array', $form_data );
		return $form_data;
	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_display_detail_option_data() {
		
		$PRO = array();
		if ( !class_exists( 'Sales_Report_Email_PRO' ) ) {
			$PRO = true;
		}

		$form_data = array(
			'display_top_sellers' => array(
				'type'		=> 'checkbox',
				'title'		=> esc_html( 'Top Selling Products', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> true,
				'disabled'  	=> false,
				'block-options'	=> true,
				'class'     => '',
				'breackdown' => true,
				'tooltip'     => esc_html( 'product name, quantity, amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
		   'display_top_categories' => array(
				'type'		=> 'checkbox',
				'title'		=> esc_html( 'Top Selling Categories', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> true,
				'disabled'  	=> false,
				'block-options'	=> true,
				'class'     => '',
				'breackdown' => true,
				'tooltip'     => esc_html( 'category name, quantity, amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_sales_by_billing_city' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Sales By Billing City', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'breackdown' => $PRO,
				'tooltip'     => esc_html( 'City, orders count, total amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_sales_by_shipping_city' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Sales By Shipping City', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'breackdown' => $PRO,
				'tooltip'     => esc_html( 'City, orders count, total amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_sales_by_billing_state' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Sales By Billing State', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'breackdown' => $PRO,
				'tooltip'     => esc_html( 'State, orders count, total amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_sales_by_shipping_state' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Sales By Shipping State', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'breackdown' => $PRO,
				'tooltip'     => esc_html( 'State, orders count, total amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_sales_by_billing_country' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Sales By Billing Country', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'breackdown' => $PRO,
				'tooltip'     => esc_html( 'country, orders count, total amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_sales_by_shipping_country' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Sales By Shipping Country', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'breackdown' => $PRO,
				'tooltip'     => esc_html( 'country, orders count, total amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_sales_by_coupons' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Sales By Coupons', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'breackdown' => $PRO,
				'tooltip'     => esc_html( 'coupon, quantity used and total discount amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_order_status' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Orders By Status', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'tooltip'     => esc_html( 'orders Status, order count, total amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_payment_method' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Orders By Payment Method', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'tooltip'     => esc_html( 'payment method, order count, total amount during the report period.', 'woocommerce-advanced-sales-report-email'),
			)
		);
		$form_data = apply_filters( 'asre_display_detail_option_data_array', $form_data );
		return $form_data;
	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_report_option_data() {
		$form_data = array(
			
			// callback more settings field
			
		);
		$form_data = apply_filters( 'asre_report_option_data_array', $form_data );
		return $form_data;
	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_hour_option_data() {
		$form_data = array(
			
			// callback more settings field
			
		);
		$form_data = apply_filters( 'asre_hours_option_data_array', $form_data );
		return $form_data;
	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_display_total_subscription_data() {
		
		$PRO = array();
		if ( !class_exists( 'Sales_Report_Email_PRO' ) ) {
			$PRO = true;
		}

		$form_data = array(
		'display_active_subscriptions' => array(
			'type'		=> 'PRO-Block',
			'title'		=> esc_html( 'Active Subscriptions', 'woocommerce-subscriptions' ),				
			'show'		=> $PRO,
			'disabled'  	=> true,
			'block-options'  	=> true,
			'class'     => 'pro-feature',
			'tooltip'     => esc_html( 'Total number of active subscriptions during the report period.', 'woocommerce-advanced-sales-report-email'),
		),
		'display_signup_subscriptions' => array(
			'type'		=> 'PRO-Block',
			'title'		=> esc_html( 'Subscriptions signups', 'woocommerce-subscriptions' ),				
			'show'		=> $PRO,
			'disabled'  	=> true,
			'block-options'  	=> true,
			'class'     => 'pro-feature',
			'tooltip'     => esc_html( 'Total number of subscriptions signups during the report period.', 'woocommerce-advanced-sales-report-email'),
		),
		'display_signup_revenue' => array(
			'type'		=> 'PRO-Block',
			'title'		=> esc_html( 'Signup Revenue', 'woocommerce-advanced-sales-report-email' ),				
			'show'		=> $PRO,
			'disabled'  	=> true,
			'block-options'  	=> true,
			'class'     => 'pro-feature',
			'tooltip'     => esc_html( 'Total signup revenue during the report period.', 'woocommerce-advanced-sales-report-email'),
		),
		'display_renewal_subscriptions' => array(
			'type'		=> 'PRO-Block',
			'title'		=> esc_html( 'Subscription Renewal', 'woocommerce-subscriptions' ),				
			'show'		=> $PRO,
			'disabled'  	=> true,
			'block-options'  	=> true,
			'class'     => 'pro-feature',
			'tooltip'     => esc_html( 'Total number of subscriptions renewal during the report period.', 'woocommerce-advanced-sales-report-email'),
		),
		'display_renewal_revenue' => array(
			'type'		=> 'PRO-Block',
			'title'		=> esc_html( 'Renewal Revenue', 'woocommerce-advanced-sales-report-email' ),				
			'show'		=> $PRO,
			'disabled'  	=> true,
			'block-options'  	=> true,
			'class'     => 'pro-feature',
			'tooltip'     => esc_html( 'Total renewal revenue during the report period.', 'woocommerce-advanced-sales-report-email'),
		),
		'display_switch_subscriptions' => array(
			'type'		=> 'PRO-Block',
			'title'		=> esc_html( 'Subscription Switch', 'woocommerce-subscriptions' ),				
			'show'		=> $PRO,
			'disabled'  	=> true,
			'block-options'  	=> true,
			'class'     => 'pro-feature',
			'tooltip'     => esc_html( 'Total number of subscriptions switch during the report period.', 'woocommerce-advanced-sales-report-email'),
		),
		'display_switch_revenue' => array(
			'type'		=> 'PRO-Block',
			'title'		=> esc_html( 'Switch Revenue', 'woocommerce-advanced-sales-report-email' ),				
			'show'		=> $PRO,
			'disabled'  	=> true,
			'block-options'  	=> true,
			'class'     => 'pro-feature',
			'tooltip'     => esc_html( 'Total switch revenue during the report period.', 'woocommerce-advanced-sales-report-email'),
		),
		'display_resubscribe_subscriptions' => array(
			'type'		=> 'PRO-Block',
			'title'		=> esc_html( 'Subscription Resubscribe', 'woocommerce-subscriptions' ),				
			'show'		=> $PRO,
			'disabled'  	=> true,
			'block-options'  	=> true,
			'class'     => 'pro-feature',
			'tooltip'     => esc_html( 'Total number of subscriptions resubscribe during the report period.', 'woocommerce-advanced-sales-report-email'),
		),
		'display_resubscribe_revenue' => array(
			'type'		=> 'PRO-Block',
			'title'		=> esc_html( 'Resubscribe Revenue', 'woocommerce-advanced-sales-report-email' ),				
			'show'		=> $PRO,
			'disabled'  	=> true,
			'block-options'  	=> true,
			'class'     => 'pro-visible',
			'tooltip'     => esc_html( 'Total resubscribe revenue during the report period.', 'woocommerce-advanced-sales-report-email'),
		),
		);
		$form_data = apply_filters( 'asre_display_total_subscription_data_array', $form_data );
		return $form_data;
	}
	
	/*
	* callback for settings fields Sales report email
	*/
	public function asre_display_detail_subscription_data() {
		
		$PRO = array();
		if ( !class_exists( 'Sales_Report_Email_PRO' ) ) {
			$PRO = true;
		}
		
		$form_data = array( 
			'display_total_subscriber' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Subscriptions By Status (Total)', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'tooltip'     => esc_html( 'New, Cancelled, Pending Cancellations, etc.', 'woocommerce-advanced-sales-report-email'),
			),
			'display_period_subscriber' => array(
				'type'		=> 'PRO-Block',
				'title'		=> esc_html( 'Subscriptions By Status (Period)', 'woocommerce-advanced-sales-report-email' ),				
				'show'		=> $PRO,
				'disabled'  	=> true,
				'block-options'	=> true,
				'class'     => 'pro-feature',
				'tooltip'     => esc_html( 'New, Cancelled, Pending Cancellations, etc.', 'woocommerce-advanced-sales-report-email'),
			),
		);
		$form_data = apply_filters( 'asre_display_detail_subscription_data_array', $form_data );
		return $form_data;
	}

	/*
	* get html of fields
	*/
	public function get_html( $arrays ) {

		$checked = '';
		
		foreach ( (array) $arrays as $id => $array ) {

			if ($array['show']) {
				$placeholder = !empty($array['placeholder']) ? $array['placeholder'] : '';
				$data_id = !empty($this->data->$id) ? $this->data->$id : '';
				if ( 'report-title' == $array['class'] ) {
					?>
				<div id="titlewrap">
					<label class="screen-reader-text" id="<?php echo esc_html($id); ?>" for="title">Add title</label>
					<input type="text" name="<?php echo esc_html($id); ?>" size="30" value="<?php echo esc_html($data_id); ?>" id="<?php echo esc_html($id); ?>" spellcheck="true" autocomplete="off" placeholder="<?php echo esc_html($placeholder); ?>">
				</div>				
				<?php } elseif ( 'textarea' == $array['type'] ) { ?>
				<table class="form-table <?php echo esc_html($id); ?>">
					<tbody>
						<th colspan="2"><label><?php echo esc_html($array['title']); ?></label></th>
						<td class="forminp"> 
							<fieldset>
							<textarea rows="3" cols="20" class="input-text regular-input textarea" type="textarea" name="<?php echo esc_html($id); ?>" id="<?php echo esc_html($id); ?>" style="" placeholder="<?php echo esc_html($placeholder); ?>"><?php echo esc_html($data_id); ?></textarea>
							</fieldset>
							<?php if ( isset($array['tooltip']) ) { ?>
								<span class="" style="font-size: 12px;"><?php echo esc_html($array['tooltip']); ?></span>
							<?php } ?>
						</td>
					</tbody>
				</table>
				<?php }	elseif ( 'text' == $array['type'] ) { ?>
				<table class="form-table <?php echo esc_html($id); ?>">
					<tbody>
						<th colspan="2"><label><?php echo esc_html($array['title']); ?></label></th>
						<td class="forminp"> 
							<fieldset>
								<input class="input-text regular-input" type="text" name="<?php echo esc_html($id); ?>" id="<?php echo esc_html($id); ?>" style="" value="<?php echo esc_html($data_id); ?>" placeholder="<?php echo esc_html($placeholder); ?>">
							</fieldset>
							<?php if ( isset($array['tooltip']) ) { ?>
								<span class="" style="font-size: 12px;"><?php echo esc_html($array['tooltip']); ?></span>
							<?php } ?>
						</td>
					</tbody>
				</table>
				<?php } elseif ( 'dropdown' == $array['type'] && 'hour' != $array['class'] ) { ?>
				<?php 
					if (isset($array['disabled']) && true == $array['disabled'] ) {
						$disabled = 'disabled';
					} else {
						$disabled = '';
					}
					if ( isset($array['multiple'] ) ) {
						$multiple = 'multiple';
						$field_id = $array['multiple'];
					} else {
						$multiple = '';
						$field_id = $id;
					} 
					?>
				<table class="form-table <?php echo esc_html($id); ?>">
				  <tbody>
						<th colspan="2"><label><?php echo esc_html($array['title']); ?></label></th>
						<td class="forminp">
							<select class="select select2" id="<?php echo esc_html($id); ?>" name="<?php echo esc_html($field_id); ?>" <?php echo esc_html($multiple); ?> <?php echo esc_html($disabled); ?>>
								<?php foreach ( (array) $array['options'] as $key => $val ) { ?>
								<?php
									$selected = '';
									if( $id == 'email_send_time' && empty($this->data->$id) && $key == '08:00' ) {
										$selected = 'selected';
									} else {
										if ( isset($array['multiple']) ) {
											if (in_array($key, (array) unserialize($this->data->$id) )) {
												$selected = 'selected';
											}
										} else {
											if ( isset($this->data->$id) && $this->data->$id == (string) $key ) {
												$selected = 'selected';
											}
										}
									}
									?>
								<option value="<?php echo esc_html($key); ?>" <?php echo esc_html($selected); ?> ><?php echo esc_html($val); ?></option>
								<?php } ?>
							</select>
						</td>
				  </tbody>
				</table>
				<?php } elseif ( 'hour' == $array['class'] ) { ?>
				<?php
					if ( isset($array['multiple']) ) {
						$multiple = 'multiple';
						$field_id = $array['multiple'];
					} else {
						$multiple = '';
						$field_id = $id;
					}
					?>
				<?php if ( 'title' == $array['type'] ) { ?>
				<table class="form-table daily-overnight <?php echo esc_html($id); ?>">
					<tbody>
						<th colspan="2" style="padding: 0 10px;"><label><?php echo esc_html($array['title']); ?></label></th>
					</tbody>
				 </table>
				<?php } else { ?>
				<div colspan="2" class="daily-overnight schadule-overnight"><label><?php echo esc_html(esc_html($array['title'])); ?></label>
					<select class="select select2" id="<?php echo esc_html($field_id); ?>" name="<?php echo esc_html($id); ?>" <?php echo esc_html($multiple); ?>><?php foreach ( (array) $array['options'] as $key => $val ) { ?>
						<?php
						$selected = '';
						if ( isset($array['multiple']) ) {
							if (in_array($key, (array) $this->data->$field_id )) { 
								$selected = 'selected';
							}
						} else {
							if ( isset($this->data->$id) && $this->data->$id == (string) $key ) { 
								$selected = 'selected';
							}
						}
						?>
						<option value="<?php echo esc_html($key); ?>" <?php echo esc_html($selected); ?> ><?php echo esc_html($val); ?></option>
						<?php } ?>
					</select>
				</div>
				<?php } ?>
				<?php } elseif ( 'checkbox' == $array['type'] && true == $array['show'] && true != $array['block-options'] ) { ?>
				
				<table class="form-table">
					<tbody>
						<tr>
							<th colspan="2"><label><?php echo esc_html($array['title']); ?></label>
								<?php 
								if ( isset($array['breackdown']) && true == $array['breackdown'] ) { 
								$show_rows = array(
									'5'=>'5','10'=>'10', '20'=>'20', '100'=>'All'
								);	
									if ( isset($array['multiple']) ) {
										$multiple = 'multiple';
										$field_id = $array['multiple'];
									} else {
										$multiple = '';
										$field_id = $id;
									}
									?>
								<span class="row-label">
									<select class="select select2 breackdown-options" id="<?php echo esc_html($field_id); ?>_row" name="<?php echo esc_html($id); ?>_row" <?php echo esc_html($multiple); ?> ><?php foreach ((array) $show_rows as $key => $val ) { ?>
									<?php
									$row_id = $id . '_row';
									$selected = '';
										if ( $this->data->$row_id == (string) $key ) {
											$selected = 'selected';
										}
										?>
									<option value="<?php echo esc_html($key); ?>" <?php echo esc_html($selected); ?> ><?php echo esc_html($val); ?></option>
									<?php } ?>
									</select>
								</span>
								<?php } ?>
							</th>
							<td class="forminp checkbox"> 
							<?php
							if ( !empty($this->data->$id) ) {
								$checked = 'checked';
							} else {
								$checked = '';
							}
							
							if (isset($array['disabled']) && true == $array['disabled']) {
								$disabled = 'disabled';
								$checked = '';
							} else {
								$disabled = '';
							} 
							?>
							<span class="tgl-btn-parent" style="">
								<input type="hidden" name="<?php echo esc_html($id); ?>" value="0"/>
								<input type="checkbox" id="<?php echo esc_html($id); ?>" name="<?php echo esc_html($id); ?>" class="tgl tgl-flat-sre checkbox-slide" <?php echo esc_html($checked); ?> value="1" <?php echo esc_html($disabled); ?> />
								<label class="tgl-btn" for="<?php echo esc_html($id); ?>"></label>
							</span>
							</td>
						</tr>
					</tbody>
				</table>
				<?php } elseif ( 'checkbox-square' == $array['type'] && true == $array['show'] && true != $array['block-options']  ) { ?>
				<table class="form-table <?php echo esc_html($id); ?>">
					<tbody>
						<tr>
							<th colspan="2"><label>
								<?php
								if ( !empty($this->data->$id) ) {
									$checked = 'checked';
								} else {
									$checked = '';
								}
								
								if (isset($array['disabled']) && true == $array['disabled']) {
									$disabled = 'disabled';
									$checked = '';
								} else {
									$disabled = '';
								}
								?>
								<div class="checkbox-square">
								<input type="hidden" name="<?php echo esc_html($id); ?>" value="0"/>
								<input type="checkbox" id="<?php echo esc_html($id); ?>" name="<?php echo esc_html($id); ?>" class="mdl-switch__input checkbox-square" <?php echo esc_html($checked); ?> value="1" <?php echo esc_html($disabled); ?> /><?php echo esc_html($array['title']); ?></label>
								</div>
							  </th>
						</tr>
					</tbody>
				</table>
				<?php } elseif ( 'checkbox-branding' == $array['type'] && true == $array['show'] && true != $array['block-options']  ) { ?>
				<table class="form-table <?php echo esc_html($id); ?>">
						<tbody>
						<tr>
							<th colspan="2"><label>
								<?php
								if ( !empty(get_option($id)) ) {
									$checked = 'checked';
								} else {
									$checked = '';
								}
								
								if (isset($array['disabled']) && true == $array['disabled']) {
									$disabled = 'disabled';
									$checked = '';
								} else {
									$disabled = '';
								} 
								?>
								<div class="checkbox-square">
									<input type="hidden" name="<?php echo esc_html($id); ?>" value="0"/>
									<input type="checkbox" id="<?php echo esc_html($id); ?>" name="<?php echo esc_html($id); ?>" class="mdl-switch__input checkbox-square" <?php echo esc_html($checked); ?> value="1" <?php echo esc_html($disabled); ?> /><?php echo esc_html($array['title']); ?></label>
								</div>
							</th>
						</tr>
					</tbody>
				</table>
				<?php } elseif ( 'checkbox' == $array['type'] && true == $array['block-options'] ) { ?>
				<?php
					if ( !empty($this->data->$id) && true != $array['disabled'] ) {
						$checked = 'checked';
						$addClass = 'item-bg';
					} else {
						$checked = '';
						$addClass = '';
					}
					
					if (isset($array['disabled']) && true == $array['disabled']) {
						$disabled = 'disabled';
						$checked = '';
					} else {
						$disabled = '';
					}
					?>
				<div class="mdl-cell mdl-cell--3-cell total-item <?php echo esc_html($addClass); ?>">
					<div class="row1">
					<span class="total-item-title"><?php echo esc_html($array['title']); ?></span>
					<?php
					if (isset($array['breackdown']) && true == $array['breackdown']) { 
					$show_rows = array(
						'5'=>'5','10'=>'10', '20'=>'20', '100'=>'All'
					);	
						if ( isset($array['multiple']) ) {
							$multiple = 'multiple';
							$field_id = $array['multiple'];
						} else {
							$multiple = '';
							$field_id = $id;
						}
						?>
					<select class="select select2 breackdown-options" id="<?php echo esc_html($field_id); ?>_row" name="<?php echo esc_html($id); ?>_row" <?php echo esc_html($multiple); ?> >
					<?php foreach ( (array) $show_rows as $key => $val ) { ?>
						<?php
							$row_id = $id . '_row';
							$selected = '';
							if ( isset($this->data->$row_id) && $this->data->$row_id == (string) $key ) {
								$selected = 'selected';
							}
							?>
						<option value="<?php echo esc_html($key); ?>" <?php echo esc_html($selected); ?> ><?php echo esc_html($val); ?></option>
					<?php } ?>
					</select>
					<?php } ?>
					<?php /*if (isset($array['tooltip']) ) { ?>
						<p class="total-item-desc"><?php echo esc_html($array['tooltip']); ?></p>
					<?php }*/ ?>		
					</div>
					<div class="row2">
						<div class="checkbox-options">
						<span class="tgl-btn-parent" style="">
							<input type="hidden" name="<?php echo esc_html($id); ?>" value="0"/>
							<input type="checkbox" id="<?php echo esc_html($id); ?>" name="<?php echo esc_html($id); ?>" class="tgl tgl-flat-sre checkbox-slide" <?php echo esc_html($checked); ?> value="1" <?php echo esc_html($disabled); ?> />
							<label class="tgl-btn <?php echo esc_html($disabled); ?>" for="<?php echo esc_html($id); ?>"></label>
						</span>
						</div>
						<span class="woocommerce-help-tip tipTip" title="<?php echo esc_html($array['tooltip']); ?>"></span>
						<?php /*if ( ( 'pro-feature' == $array['class'] ) ) { ?>
							<span class="item-total pro-label">PRO</span>
						<?php } */?>
					</div>
				</div>
				<?php } elseif ( 'PRO-Block' == $array['type'] ) { ?>
				<?php 
					if ( !empty($this->data->$id) && true != $array['disabled'] ) {
						$checked = 'checked';
						$addClass = 'item-bg';
					} else {
						$checked = '';
						$addClass = '';
					}
					if ( isset($array['disabled']) && true == $array['disabled'] ) {
						$disabled = 'disabled';
						$checked = '';
					} else {
						$disabled = '';
					}
					?>
				<div class="mdl-cell mdl-cell--3-cell total-item <?php echo esc_html($addClass); ?>">
					<div class="row1">
					<span class="total-item-title"><?php echo esc_html($array['title']); ?></span>
					<?php
					if (isset($array['breackdown']) && true == $array['breackdown']) { 
					$show_rows = array(
						'5'=>'5','10'=>'10', '20'=>'20', '100'=>'All'
					);	
						if ( isset($array['multiple']) ) {
							$multiple = 'multiple';
							$field_id = $array['multiple'];
						} else {
							$multiple = '';
							$field_id = $id;
						}
						?>
					<select class="select select2 breackdown-options" id="<?php echo esc_html($field_id); ?>_row" name="<?php echo esc_html($id); ?>_row" <?php echo esc_html($multiple); ?> >
					<?php foreach ( (array) $show_rows as $key => $val ) { ?>
						<?php
							$row_id = $id . '_row';
							$selected = '';
							if ( isset($this->data->$row_id) && $this->data->$row_id == (string) $key ) {
								$selected = 'selected';
							}
							?>
						<option value="<?php echo esc_html($key); ?>" <?php echo esc_html($selected); ?> ><?php echo esc_html($val); ?></option>
					<?php } ?>
					</select>
					<?php } ?>
					<?php /*if (isset($array['tooltip']) ) { ?>
						<p class="total-item-desc"><?php echo esc_html($array['tooltip']); ?></p>
					<?php }*/ ?>		
					</div>
					<div class="row2">
						<div class="checkbox-options">
						<span class="tgl-btn-parent" style="">
							<input type="hidden" name="<?php echo esc_html($id); ?>" value="0"/>
							<input type="checkbox" id="<?php echo esc_html($id); ?>" name="<?php echo esc_html($id); ?>" class="tgl tgl-flat-sre checkbox-slide" <?php echo esc_html($checked); ?> value="1" <?php echo esc_html($disabled); ?> />
							<label class="tgl-btn <?php echo esc_html($disabled); ?>" for="<?php echo esc_html($id); ?>"></label>
						</span>
						</div>
						<span class="woocommerce-help-tip tipTip" title="<?php echo esc_html($array['tooltip']); ?>"></span>
						<?php /*if ( ( 'pro-feature' == $array['class'] ) ) { ?>
							<span class="item-total pro-label">PRO</span>
						<?php } */?>
					</div>
				</div>
				<?php }	elseif ( 'media' == $array['type'] ) { ?>
				<table class="form-table">
					<tbody>
						<tr>
							<th colspan="2"><label><?php echo esc_html($array['title']); ?></label></th>
							<td class="forminp"> 
								<fieldset>
									<div class="media-botton">
										<input id="asre_upload_image_button" type="button" class="button" value="<?php esc_html_e( 'Select image' , 'default'); ?>" />
										<input type="hidden" name="<?php echo esc_html($id); ?>" class='<?php echo esc_html($id); ?> textfield-media' placeholder='Upload Image' value='
										<?php 
										if (!empty(get_option($id))) {
											echo esc_html(get_option($id));
										} 
										?>
										' id="<?php echo esc_html($id); ?>"/>
										<input type='hidden' name='asre_image_id' class='asre_image_id' placeholder="Image" value='' id='asre_image_id' style=""/>
									</div>
									<?php if ( !empty(get_option($id)) ) { ?>
										<div class="asre-image-placeholder" style="display:none;">No File Selected</div>
										<div class="thumbnail asre-thumbnail-image">				
											<img src="<?php echo esc_url(get_option($id)); ?>" id="asre_thumbnail" draggable="false" alt="">
											<span id="remove_btn" class="dashicons dashicons-dismiss"></span>
										</div>
									<?php } else { ?>
									<div class="asre-image-placeholder" style="display:block;">No File Selected</div>
										<div class="thumbnail asre-thumbnail-image" style="display:none;">			
											<img src="" draggable="false" id="asre_thumbnail" alt=""/>
											<span id="remove_btn" class="dashicons dashicons-dismiss"></span>
										</div>
									<?php } ?>
								</fieldset>
								<?php if ( isset($array['tooltip']) ) { ?>
									<span class="" style="font-size: 12px;"><?php echo esc_html($array['tooltip']); ?></span>
								<?php } ?>
							</td>
						</tr>
					</tbody>
				</table>
				<?php 
				}
			}
		}
	}
	
	/*
	* set email data for sales report
	*/
	public function set_email_data( $data ) {
		$this->email_data = $data;
	}
	
	/*
	* retune email data for sales report
	*/
	public function get_email_data() {
		return $this->email_data;
	}
	
	/*
	* get email content data for sales report
	*/
	public function email_content( $id ) {
		$data = $this->get_data_byid( $id );
		$this->set_email_data( $data );

		$interval = $data->email_interval;
		
		// Create the date range object
		$date_range = new WC_ASRE_Date_Range( $interval );
		$gmt_date_range = new WC_ASRE_Gmt_Date_Range( $interval );
		
		// Modify start date based on interval
		switch ( $interval ) {
			case 'last-30-days':
				$previous_date_range = new WC_ASRE_Date_Range( 'previous-last-30-days' );
				break;
			case 'monthly':
				$previous_date_range = new WC_ASRE_Date_Range( 'previous_month' );
				break;
			case 'weekly':				
				$previous_date_range = new WC_ASRE_Date_Range( 'previous_week' );
				break;
			case 'daily-overnight':				
				$previous_date_range = new WC_ASRE_Date_Range( 'previous_overnight' );
				break;
			case 'daily':
			default:
				$previous_date_range = new WC_ASRE_Date_Range( 'previous_day' );
				break;
		}
		
		switch ( $interval ) {
			case 'last-30-days':
				$previous_gmt_date_range = new WC_ASRE_Gmt_Date_Range( 'previous-last-30-days' );
				break;
			case 'monthly':
				$previous_gmt_date_range = new WC_ASRE_Gmt_Date_Range( 'previous_month' );
				break;
			case 'weekly':				
				$previous_gmt_date_range = new WC_ASRE_Gmt_Date_Range( 'previous_week' );
				break;
			case 'daily-overnight':				
				$previous_gmt_date_range = new WC_ASRE_Gmt_Date_Range( 'previous_overnight' );
				break;
			case 'daily':
			default:
				$previous_gmt_date_range = new WC_ASRE_Gmt_Date_Range( 'previous_day' );
				break;
		}
			
		$rows = array();
		$total_signups = array();
		$total_orders = array();
		$total_taxes = array();
		$total_items = array();
		$total_shipping = array();
		$total_sales = array();
		$total_refunds = array();
		$previous_total_refunds  = array();
		$top_sellers = array();
		$coupon_used = array();
		$previous_period = array();
		$growth_report = array();
		$top_categories = array();
		$previous_coupon_used = array();
		$previous_total_taxes = array();
		$previous_total_shipping = array();
		$previous_total_orders = array();
		$signup_growth = 0;
		$refund_growth = 0;
		$orders_growth = 0;
		$items_growth = 0;
		$sales_growth = 0;
		$taxes_growth = 0;
		$shipping_growth =0;
		$coupon_growth = 0;
		$previous_total_signups  = array();
		$previous_total_items  = array();
		$previous_total_sales  = array();
		
		//total signups
		$total_signups = new WC_ASRE_Row_Total_Sign_Ups( $gmt_date_range ); 			
		$previous_total_signups = new WC_ASRE_Row_Total_Sign_Ups( $previous_gmt_date_range );
		$signup_growth = WC_ASRE_Report_Manager::get_growth_count( $previous_total_signups->get_value(), $total_signups->get_value() );

		//total refunds
		$get_total_refunds = new WC_ASRE_Row_Total_Reports( $date_range );
		$total_refunds = $get_total_refunds->get_value()->refunds;
		$get_previous_total_refunds = new WC_ASRE_Row_Total_Reports( $previous_date_range );
		$previous_total_refunds = $get_previous_total_refunds->get_value()->refunds;
		$refund_growth = WC_ASRE_Report_Manager::get_growth_count( $previous_total_refunds, $total_refunds );
		
		//total orders
		$get_total_orders = new WC_ASRE_Row_Total_Reports( $date_range );
		$total_orders = $get_total_orders->get_value()->orders_count;
		$get_previous_total_orders = new WC_ASRE_Row_Total_Reports( $previous_date_range );
		$previous_total_orders = $get_previous_total_orders->get_value()->orders_count;
		$orders_growth = WC_ASRE_Report_Manager::get_growth_count( $previous_total_orders, $total_orders );
		
		//total items
		$get_total_items = new WC_ASRE_Row_Total_Reports( $date_range );
		$total_items = $get_total_items->get_value()->num_items_sold;
		$get_previous_total_items = new WC_ASRE_Row_Total_Reports( $previous_date_range );
		$previous_total_items = $get_previous_total_items->get_value()->num_items_sold;
		$items_growth = WC_ASRE_Report_Manager::get_growth_count( $previous_total_items, $total_items );
		
		//total salses
		$get_total_sales = new WC_ASRE_Row_Total_Reports( $date_range  );
		$total_sales = $get_total_sales->get_value()->total_sales;
		$get_previous_total_sales = new WC_ASRE_Row_Total_Reports( $previous_date_range  );
		$previous_total_sales = $get_previous_total_sales->get_value()->total_sales;
		$sales_growth = WC_ASRE_Report_Manager::get_growth_count( $previous_total_sales, $total_sales );
		
		if ( wc_tax_enabled() ) {	
			//total taxes
			$get_total_taxes = new WC_ASRE_Row_Total_Reports( $date_range );
			$total_taxes = $get_total_taxes->get_value()->taxes;
			$get_previous_total_taxes = new WC_ASRE_Row_Total_Reports( $previous_date_range );
			$previous_total_taxes = $get_previous_total_taxes->get_value()->taxes;
			$taxes_growth = WC_ASRE_Report_Manager::get_growth_count( $previous_total_taxes, $total_taxes );
			
		}
		
		//net revenue
		$get_net_revenue = new WC_ASRE_Row_Total_Reports( $date_range );
		$net_revenue = $get_net_revenue->get_value()->net_revenue;
		$get_previous_net_revenue = new WC_ASRE_Row_Total_Reports( $previous_date_range );
		$previous_net_revenue = $get_previous_net_revenue->get_value()->net_revenue;
		$net_revenue_growth = WC_ASRE_Report_Manager::get_growth_count( $previous_net_revenue, $net_revenue );			
		
		//total shipping
		$get_total_shipping = new WC_ASRE_Row_Total_Reports( $date_range );
		$total_shipping = $get_total_shipping->get_value()->shipping;
		$get_previous_total_shipping = new WC_ASRE_Row_Total_Reports( $previous_date_range );
		$previous_total_shipping = $get_previous_total_shipping->get_value()->shipping;
		$shipping_growth = WC_ASRE_Report_Manager::get_growth_count( $previous_total_shipping, $total_shipping );
		
		//total coupons
		$get_coupon_used = new WC_ASRE_Row_Total_Reports( $date_range );
		$coupon_used = $get_coupon_used->get_value()->coupons;
		$get_previous_coupon_used = new WC_ASRE_Row_Total_Reports( $previous_date_range );
		$previous_coupon_used = $get_previous_coupon_used->get_value()->coupons;
		$coupon_growth = WC_ASRE_Report_Manager::get_growth_count( $previous_coupon_used, $coupon_used );
		
		//top seller
		$top_sellers = new WC_ASRE_Row_Top_Sellers( $date_range  );

		//top categories
		$top_categories = new WC_ASRE_Row_Top_Category( $date_range  );
		
		$data_array = array(
			'id' => $id,		
			'interval'  => $interval,
			'previous_date_range' => $previous_date_range,
			'rows'  => $rows,
			'total_signups'  => $total_signups,
			'previous_total_signups' => $previous_total_signups,
			'total_orders'  => $total_orders,
			'total_taxes'  => $total_taxes,
			'net_revenue'  => $net_revenue,
			'previous_total_orders' => $previous_total_orders,
			'coupon_growth' => round((int) $coupon_growth),
			'signup_growth' => round((int) $signup_growth),
			'refund_growth'	=> round((int) $refund_growth),
			'net_revenue_growth'	=> round((int) $net_revenue_growth),
			'taxes_growth' => wc_tax_enabled() ? round((int) $taxes_growth) : '',
			'shipping_growth' => round((int) $shipping_growth),
			'orders_growth' => round((int) $orders_growth),
			'total_items'  => $total_items,
			'previous_total_items' => $previous_total_items,
			'items_growth' => round((int) $items_growth),
			'total_shipping'  => $total_shipping,
			'total_sales' => $total_sales,
			'total_refunds' => $total_refunds,
			'previous_total_sales' => $previous_total_sales,
			'previous_total_refunds' => $previous_total_refunds,
			'previous_net_revenue' => $previous_net_revenue,
			'sales_growth' => round((int) $sales_growth),
			'top_sellers' => $top_sellers,
			'previous_period' => $previous_period,
			'growth_report' => $growth_report,
			'top_categories' => $top_categories,
			'coupon_used' => $coupon_used,
			'previous_coupon_used' => $previous_coupon_used,
			'previous_total_taxes' => $previous_total_taxes,
			'previous_total_shipping' => $previous_total_shipping,
		);
		
		$data_array = apply_filters( 'asre_email_content_data_array', $data_array, $data, $date_range, $previous_date_range );
		
		ob_start();
		wc_get_template(
			'total-report.php',
			array(
				'data_array' => $data_array,
				'data' => $data,
			),
			'woocommerce-advanced-sales-report/', 
			wc_sales_report_email()->get_plugin_path() . '/classes/preview/'
		);
		
		$message = ob_get_clean();
		return $message;
		
	}
	
	/*
	* Run preview sales report email 
	*/
	public function run_preview_func() {
		
		$id = isset($_GET['id']) ? sanitize_text_field($_GET['id']) : '';
		$report_preview = $this->email_content( $id );
		echo $report_preview;
		exit();
	}
	
	/**
	 *	Method triggered on saving admin sales report email settings
	 *	This is to make sure the time_sent parameter gets changed in the sheduled event.
	 *
	 * @since  1.1.0
	*/
	public function reset_cron( $id ) {
		$cron_manager = new WC_ASRE_Cron_Manager();
		$cron_manager->remove_cron($id);
		$cron_manager->setup_cron($id);
	}
	
	/**
	 * Method triggered on Cron run.
	 * This method will create a WC_SRE_Sales_Report_Email object and call trigger method.
	 *
	 * @since  1.0.0
	*/
	public function cron_email_callback( $id ) {
		
		$data = $this->get_data_byid( $id );
		if (isset($data)) {
			// Check if extension is active
			$enabled = $data->email_enable;
			if ( '0' == $enabled ) {
				return;
			}
		}

		// Check if an email should be send
		$interval = $data->email_interval;
		$selected_w_day = $data->email_select_week;
		$selected_m_day = $data->email_select_month;
		$now        = new DateTime( null, new DateTimeZone( wc_timezone_string() ) );
		$send_today = false;

		switch ( $interval ) {
			case 'last-30-days':
				// Send monthly reports on the selected day of the month
				if ( $selected_m_day == (int) $now->format( 'j' ) ) {
					$send_today = true;
				}
				break;
			case 'monthly':
				// Send monthly reports on the selected day of the month
				if ( $selected_m_day == (int) $now->format( 'j' ) ) {
					$send_today = true;
				}
				break;
			case 'weekly':
				// Send weekly reports on selected day of week
				if ( $selected_w_day == (int) $now->format( 'w' ) ) {
					$send_today = true;
				}
				break;
			case 'daily':
				// Send everyday if the interval is daily
				$send_today = true;
				break;
			case 'daily-overnight':
				// Send everyday if the interval is daily overnight
				$send_today = true;
				break;
		}

		// Check if we need to send an email today
		if ( true !== $send_today ) {
			return;
		}

		$wc_emails      = WC_Emails::instance();
		$emails         = $wc_emails->get_emails();	
		$mailer = WC()->mailer();
		$sent_to_admin = false;
		$plain_text = false;
		$email = '';
		
		$message = $this->email_content( $id );
		
		$email_heading = $data->email_subject;
		$subject_email = $data->email_subject;
		
		if (empty($subject_email)) {
			$subject_email = 'Sales Report for {site_title}';	
		}
		
		$subject = str_replace('{site_title}', get_bloginfo( 'name' ), $subject_email );
		
		// create a new email
		$email = new WC_Email();
		$headers = "Content-Type: text/html\r\n";
		add_filter( 'wp_mail_from', array( $this, 'get_from_address' ) );
		add_filter( 'wp_mail_from_name', array( $this, 'get_from_name' ) );

		$recipients = $data->email_recipients;
		$recipients = explode(',', $recipients);
		
		$logger = wc_get_logger();
		if ($recipients) {
			foreach ($recipients as $recipient) {
				$bool = wp_mail( $recipient, $subject, $message, $email->get_headers());
				if ('1' == $bool) { 
					$bool = 'Success';
				} else {
					$bool = 'Fail';
				}
				$logger->info( 'Report: ' . $data->report_name, array( 'source' => 'sre-log' ) );
				$logger->info( 'Email: ' . $recipient, array( 'source' => 'sre-log' ) );
				$logger->info( 'Status: ' . $bool, array( 'source' => 'sre-log' ) );
			}
		}		
	}
	
	
	/**
	 * Send test mail.
	 * This method will create a WC_ASRE_Sales_Report_Email object and call trigger method.
	 *
	 * @since  1.0.0
	*/
	public function send_test_sales_email_func() {
		
		$id = isset($_GET['id']) ? (int) $_GET['id'] : '';
		$data = $this->get_data_byid( $id );

		$wc_emails      = WC_Emails::instance();
		$emails         = $wc_emails->get_emails();	
		$mailer 		= WC()->mailer();
		$sent_to_admin 	= false;
		$plain_text 	= false;
		$email 			= '';
		$message 		= $this->email_content( $id );
		$email_heading 	= $data->email_subject;
		
		$subject_email 	= $data->email_subject;
		
		if (empty($subject_email)) {
			$subject_email = 'Sales Report for {site_title}';	
		}
		
		$subject = str_replace('{site_title}', get_bloginfo( 'name' ), 'Test ' . $subject_email );

		// create a new email
		$email 		= new WC_Email();
		$headers 	= "Content-Type: text/html\r\n";
		add_filter( 'wp_mail_from', array( $this, 'get_from_address' ) );
		add_filter( 'wp_mail_from_name', array( $this, 'get_from_name' ) );

		$recipients = $data->email_recipients;
		$recipients = explode( ',', $recipients );
		
		$logger = wc_get_logger();
		if ($recipients) {
			foreach ( $recipients as $recipient) {
				$bool = wp_mail( $recipient, $subject, $message, $email->get_headers());
				if ('0' == $bool) {
					$bool = 'Success';
				} else {
					$bool = 'Fail';
				}
				$logger->info( 'Report: ' . $data->report_name, array( 'source' => 'sre-log' ) );
				$logger->info( 'Email: ' . $recipient, array( 'source' => 'sre-log' ) );
				$logger->info( 'Status: ' . $bool, array( 'source' => 'sre-log' ) );
			}
		}	
	}
	
	/**
	 * Get the from name for outgoing emails.
	 *
	 * @return string
	 */
	public function get_from_name() {
		$from_name = apply_filters( 'woocommerce_email_from_name', get_option( 'woocommerce_email_from_name' ), $this );
		return wp_specialchars_decode( esc_html( $from_name ), ENT_QUOTES );
	}

	/**
	 * Get the from address for outgoing emails.
	 *
	 * @return string
	 */
	public function get_from_address() {
		$from_address = apply_filters( 'woocommerce_email_from_address', get_option( 'woocommerce_email_from_address' ), $this );
		return sanitize_email( $from_address );
	}
	
	/*
	* get value of sales growth
	*/
	public static function growth_html( $growth_count ) {
		ob_start();
		?>
		<span class="growth-span 
		<?php 
		if ($growth_count>0) { 
			echo'arrow-up'; 
		} 
		if ( $growth_count<0 ) {
			echo'arrow-down'; 
		} 
		?>
		">
		<?php if ($growth_count > 0) { ?>
		<img src="<?php echo esc_url(plugin_dir_url( dirname( __FILE__ ) ) . 'classes/preview/image/arrow-green.png'); ?>">
		<?php echo esc_html($growth_count); ?>%<?php } ?>
		<?php if ($growth_count < 0) { ?>
		<img src="<?php echo esc_url(plugin_dir_url( dirname( __FILE__ ) ) . 'classes/preview/image/arrow-red.png'); ?>">
		<?php echo esc_html($growth_count); ?>%<?php } ?>
		<?php if (0 == $growth_count) { ?>
		<img src="<?php echo esc_url(plugin_dir_url( dirname( __FILE__ ) ) . 'classes/preview/image/arrow.png'); ?>">
		<?php echo esc_html($growth_count); ?>%<?php } ?>
		</span> 
		<?php
		$message = ob_get_clean();
		return $message;
	}
	
}
