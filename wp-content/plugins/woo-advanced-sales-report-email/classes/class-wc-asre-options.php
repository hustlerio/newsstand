<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_ASRE_Options {

	const OPTION_NAME = 'woocommerce_advanced_sales_report_email_settings';

	/**
	 * Get the options
	 *
	 * @since  1.0.0
	 * @static
	 *
	 * @return array
	 */
	private static function get_options() {
		$admin_email = get_option('admin_email');
		return wp_parse_args( get_option( self::OPTION_NAME, array() ), array( 'enabled' => 'yes', 'recipients' => $admin_email , 'interval' => 'daily', 'send_time' => '03:00' ) );
	}

	/**
	 * Check if Sales Report Email extension is enabled
	 *
	 * @since  1.0.0
	 * @static
	 *
	 * @return mixed|void
	 */
	public static function is_enabled() {

		// Get the ASRE options
		$asre_options = self::get_options();

		/**
		 * Filter: 'wc_advanced_sales_report_email_enabled' - Allow altering if sales report email is enabled
		 *
		 * @api bool $enabled Enabled state
		 */

		return apply_filters( 'wc_advanced_sales_report_email_enabled', ( ( 'yes' == $asre_options['enabled'] ) ? true : false ) );
	}

	/**
	 * Get the advanced sales report recipients
	 *
	 * @since  1.0.0
	 * @static
	 *
	 * @return String
	 */
	public static function get_recipients() {

		// Get the ASRE options
		$asre_options = self::get_options();

		/**
		 * Filter: 'wc_advanced_sales_report_email_recipients' - Allow altering sales report email recipients
		 *
		 * @api string $recipients The recipients
		 */

		return apply_filters( 'wc_advanced_sales_report_email_recipients', $asre_options['recipients'] );
	}

	/**
	 * Get the interval option
	 *
	 * @since  1.0.0
	 * @static
	 *
	 * @return string
	 */
	public static function get_interval() {

		// Get the ASRE options
		$asre_options = self::get_options();

		/**
		 * Filter: 'wc_advanced_sales_report_email_interval' - Allow altering sales report email interval
		 *
		 * @api string $interval The interval, possible values: daily, weekly, monthly. Default: daily.
		*/
		return apply_filters( 'wc_advanced_sales_report_email_interval', $asre_options['interval'] );
	}

	/**
	 * Get the send time option
	 *
	 * @since  1.0.0
	 * @static
	 *
	 * @return string
	 */
	public static function get_send_time() {

		// Get the ASRE options
		$asre_options = self::get_options();

		/**
		 * Filter: 'wc_advanced_sales_report_email_send_time' - Allow altering sales report email send time
		 *
		 * @api string $send_time The time the email is sent, example values: '14:15'.
		 */

		return apply_filters( 'wc_advanced_sales_report_email_send_time', $asre_options[ 'send_time' ] );
	}
	
	/**
	 * Get admin settings data
	*/
	public static function get_data() {
		$admin_email = get_option('admin_email');
		return wp_parse_args( get_option( self::OPTION_NAME, array() ), array( 'enabled' => 'yes', 'recipients' => $admin_email , 'interval' => 'daily', 'send_time' => '03:00' ) );
	}
}
