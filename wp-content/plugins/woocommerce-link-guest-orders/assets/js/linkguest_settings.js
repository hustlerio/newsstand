(function($) {
	'use strict';

	const LinkGuestAdminJS = {

		/**
		 * Start the engine.
		 *
		 * @since 2.0.0
		 */
		init: function() {

			// Document ready
			$( document ).ready( LinkGuestAdminJS.ready );

			// Page load
			$( window ).on( 'load', LinkGuestAdminJS.load );

			// Document ready
			$( document ).on( 'LinkGuestjsReady', LinkGuestAdminJS.start );

			// register events and hooks
			LinkGuestAdminJS.bindEvents();
		},
		/**
		 * Document ready.
		 *
		 * @since 2.0.0
		 */
		ready: function() {

		},
		/**
		 * Page load.
		 *
		 * @since 2.0.0
		 */
		load: function() {

		},

		start: function() {
			// Set user identifier
			$( document ).trigger( 'LinkGuestAdminJSStarted' );
		},

		// --------------------------------------------------------------------//
		// Binds
		// --------------------------------------------------------------------//
		/**
		 * Events bindings.
		 *
		 * @since 2.0.0
		 */
		bindEvents: function() {
			
			$(document).on('click', '#linkguest_button_verify_license', LinkGuestAdminJS.verifyLicense);
			$(document).on('click', '#linkguest_button_deactivate_license', LinkGuestAdminJS.deactivateLicense);
			$(document).on('click', '#linkguest_button_link_orders', LinkGuestAdminJS.link_orders);
		},

		verifyLicense: function(event) {
			event.preventDefault();

			var license = $('#linkguest_key_license').val();

			$('#linkguest_message_license').empty();
			$('#linkguest_loading_license').empty().append('<img src="' + linkguest_vars.url_loading + '" alt="loading" />');
			$('#linkguest_key_license').attr('disabled', 'disabled');
			$('#linkguest_button_verify_license').attr('disabled', 'disabled');


			$.ajax({
				url : linkguest_vars.url_ajax,
				dataType: 'json',
				type: 'POST',
				data: {
					action 	: 'linkguest_verify_license',
					license : license,
					wpnonce : linkguest_vars.nonce
				},
				success: function (response) {

					console.log(response);

					if( response.success == true ) {
						$('#linkguest_message_license').append('<span style="color:green;"><img src="' + linkguest_vars.url_success + '" alt="yes" />' + response.data + '<span>');
					}
					else
						$('#linkguest_message_license').append('<span style="color:red;"><img src="' + linkguest_vars.url_failure + '" alt="no" />' + response.data + '</span>');

					$('#linkguest_loading_license').empty();
					$('#linkguest_key_license').removeAttr('disabled');
					$('#linkguest_button_verify_license').removeAttr('disabled');
				}
			});
		},

		deactivateLicense:  function(event) {
			event.preventDefault();

			$('#linkguest_message_license').empty();
			$('#linkguest_loading_license').empty().append('<img src="' + linkguest_vars.url_loading + '" alt="loading" />');
			$('#linkguest_key_license').attr('disabled', 'disabled');
			$('#linkguest_button_verify_license').attr('disabled', 'disabled');
			$('#linkguest_button_deactivate_license').attr('disabled', 'disabled');


			$.ajax({
				url : linkguest_vars.url_ajax,
				dataType: 'json',
				type: 'POST',
				data: { action: 'linkguest_deactivate_license', wpnonce : linkguest_vars.nonce },
				success: function (response) {

					if( response.success == true ) {
						$('#linkguest_message_license').append('<span style="color:green;"><img src="' + linkguest_vars.url_success + '" alt="yes" />' + response.data + '<span>');
						$('#linkguest_key_license').val('');
					}
					else
						$('#linkguest_message_license').append('<span style="color:red;"><img src="' + linkguest_vars.url_failure + '" alt="no" />' + response.data + '</span>');

					$('#linkguest_loading_license').empty();
					$('#linkguest_key_license').removeAttr('disabled');
					$('#linkguest_button_verify_license').removeAttr('disabled');
					$('#linkguest_button_deactivate_license').removeAttr('disabled');
				}
			});
		},

		link_orders: function(event) {
			event.preventDefault();

			const $button 	= $(this),
				$loading 	= $('#linkguest_loading_pending'),
				$table		= $('.linkguest_stats'),
				$torders 	= $('#total_orders'),
				$tlinks 	= $('#total_links');

			let torders 	= parseInt( $button.data('total-orders'), 10 ),
				tlinks 		= parseInt( $button.data('total-links'), 10 );

			$button.attr('disabled', 'disabled');
			$loading.empty().append('<img src="' + linkguest_vars.url_loading + '" alt="loading" />');

			$.ajax({
				url : linkguest_vars.url_ajax,
				dataType: 'json',
				type: 'POST',
				data: {
					action: 'linkguest_link_orders',
					wpnonce : linkguest_vars.nonce
				},
				success: function (response) {

					$table.find('.linkguest_stats_message').remove();

					if( response.success == true ) {

						// Calculate the new values
						torders = torders - parseInt(response.data.counter,10);
						tlinks 	= tlinks - parseInt(response.data.counter,10);

						// Updating the value on the table
						$torders.html(torders);
						$tlinks.html(tlinks);

						// Updating the attributes
						$button.data('total-orders', torders);
						$button.data('total-links', tlinks);

						$table.append('<tr class="linkguest_stats_message"><td colspan="2"><span style="color:green;"><img src="' + linkguest_vars.url_success + '" alt="yes" />' + response.data.msg + '<span></td></tr>');
					}
					else
						$table.append('<tr class="linkguest_stats_message"><td colspan="2"><span style="color:red;"><img src="' + linkguest_vars.url_failure + '" alt="no" />' + response.data.msg + '</span></td></tr>');

					if( tlinks > 0 )
						$button.removeAttr('disabled');

					$loading.empty();
				}
			});
		}
	};

	// Initialize.
	LinkGuestAdminJS.init();

	// Add to global scope.
	window.linkguestadmin = LinkGuestAdminJS;

})(jQuery);