<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class LinkGuest {

	/**
	 * Plugin Instance
	 */
	protected static $_instance = null;

	/**
	 * Admin Instance
	 */
	public $admin;

	/**
	 * Public Instance
	 */
	public $public;

	/**
	 * License Instance
	 */
	public $license;

	/**
	 * Ajax Instance
	 */
	public $ajax;


	/**
	 * Ensures only one instance is loaded or can be loaded.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'shiparea' ), '2.1' );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'shiparea' ), '2.1' );
	}


	function __construct() {

		$this->load_dependencies();
		$this->set_locale();
		$this->set_objects();

		//add_action( 'plugins_loaded', [ $this, 'updater' ] );
	}

	function load_dependencies() {

		require_once LINKGUEST_PLUGIN_DIR . 'includes/functions.php';
		require_once LINKGUEST_PLUGIN_DIR . 'includes/class-linkguest-i18n.php';
		require_once LINKGUEST_PLUGIN_DIR . 'includes/class-linkguest-public.php';
		require_once LINKGUEST_PLUGIN_DIR . 'includes/class-linkguest-ajax.php';
		

		if( is_admin() ) {
			require_once LINKGUEST_PLUGIN_DIR . 'includes/admin/class-linkguest-license.php';
			require_once LINKGUEST_PLUGIN_DIR . 'includes/admin/class-linkguest-updater.php';
			require_once LINKGUEST_PLUGIN_DIR . 'includes/admin/class-linkguest-settings.php';
			require_once LINKGUEST_PLUGIN_DIR . 'includes/admin/class-linkguest-info.php';
			require_once LINKGUEST_PLUGIN_DIR . 'includes/admin/class-linkguest-orders.php';
		}
	}

	/**
	 * Notice when Woocommerce is not activated
	 * @return mixed
	 */
	function notice_woo() {
		echo '<div class="error"><p>' . sprintf( __( 'The Woocommerce Link Guest Orders plugin depends on the last version of %s to work!', 'linkguest' ), '<a href="http://wordpress.org/extend/plugins/woocommerce/">WooCommerce</a>' ) . '</p></div>';
	}


	/**
	 * Load plugin updater.
	 *
	 * @since 2.0.0
	 */
	public function updater() {

		if ( ! is_admin() ) {
			return;
		}

		$key = LinkGues()->license->get();

		if ( ! $key ) {
			return;
		}

		// Go ahead and initialize the updater.
		new LinkGuest_Updater(
			LINKGUEST_UPDATER_API,
			LINKGUEST_PLUGIN_BASE,
			[
				'version' => LINKGUEST_VERSION,
				'license' => $key,
				'item_id' => LINKGUEST_EDD_ID,
				'author'  => 'Damian Logghe',
				'url'     => home_url(),
			]
		);

		// Fire a hook for Addons to register their updater since we know the key is present.
		do_action( 'linkguest_updater', $key );
	}


	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the ShipArea_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 */
	private function set_locale() {

		$plugin_i18n = new LinkGuest_i18n();
		$plugin_i18n->set_domain( 'linkguest' );

		add_action( 'plugins_loaded', [ $plugin_i18n, 'load_plugin_textdomain' ] );
	}

	/**
	 * Set all global objects
	 */
	private function set_objects() {
		$this->ajax 	= new LinkGuest_Ajax();
		$this->public 	= new LinkGuest_Public();

		if( is_admin() ) {
			$this->order 		= new LinkGuest_Orders();
			$this->settings 	= new LinkGuest_Settings();
			$this->license 		= new LinkGuest_License();
		}
	}
}

?>