<?php
/**
 * Retrieve a value from options
 *
 * @param $key
 * @param bool $default
 * @param string $option
 *
 * @return bool|mixed
 */
if( !function_exists('linkguest_settings') ) {
	function linkguest_settings( $key, $default = false, $option = 'linkguest_options' ) {

		$key     = linkguest_sanitize_key( $key );
		$options = get_option( $option, false );
		$value   = is_array( $options ) && isset( $options[ $key ] ) && ( $options[ $key ] === '0' || ! empty( $options[ $key ] ) ) ? $options[ $key ] : $default;

		return apply_filters( 'linkguest/global/settings', $value, $option);
	}
}

/**
 * Sanitize key, primarily used for looking up options.
 *
 * @param string $key
 *
 * @return string
 */
if( !function_exists('linkguest_sanitize_key') ) {
	function linkguest_sanitize_key( $key = '' ) {

		return preg_replace( '/[^a-zA-Z0-9_\-\.\:\/]/', '', $key );
	}
}



if( !function_exists('linkgues_get_guest_orders') ) {
	function linkguest_get_guest_orders() {

		$params = [
			'type'		=> 'shop_order',
			'customer'	=> '0',
			'paginate'	=> false,
			'limit'		=> -1,
		];

		$guest_orders = wc_get_orders( $params );

		return apply_filters( 'linkguest/global/orders', $guest_orders, $params );
	}
}


if( !function_exists('linkgues_stats_guest_orders') ) {
	function linkgues_stats_guest_orders() {

		$total_links = 0;
		$guest_orders = linkguest_get_guest_orders();

		foreach( $guest_orders as $order ) {
			$data_customer = get_user_by( 'email', $order->get_billing_email() );

			if( $data_customer )
				$total_links++;
		}

		$output = [
			'total_orders'	=> count($guest_orders),
			'total_links'	=> $total_links,
		];

		return apply_filters( 'linkguest/global/stats', $output, $guest_orders );
	}
}


if( ! function_exists('linkgues_link_guest_orders') ) {
	function linkgues_link_guest_orders( $order = null) {
		
		if( is_null( $order ) || ! $order instanceof WC_Order )
			return false;
			
		// instance user from email
		$user = get_user_by( 'email', $order->get_billing_email() );
		
		if( isset( $user->ID )  ) {
			update_post_meta( $order->get_id(), '_customer_user', $user->ID );

			return true;
		}

		return false;
	}
}