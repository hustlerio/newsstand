<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Public Class
 * @since 1.0.0
 */
class LinkGuest_Public {
	
	/**
	 * Public Construct
	 */
	public function __construct() {
		add_action( 'woocommerce_created_customer', [ $this, 'link_new_customer' ], 10, 1 );
		add_action( 'woocommerce_account_dashboard', [ $this, 'maybe_show_linked_orders' ], 1 );
		add_action( 'woocommerce_new_order', [ $this, 'link_new_order' ], 10, 1 );
	}

	/**
	 * Linked new customers
	 * @param  INT $user_id 
	 * @return mixed
	 */
	public function link_new_customer( $user_id = 0 ) {

		if( empty( $user_id ) )
			return;

		$new_customer = linkguest_settings('new_customer', 'no', 'wc_linkguest');

		if( $new_customer == 'no' )
			return;

		$linked = wc_update_new_customer_past_orders( $user_id );
		update_user_meta( $user_id, '_wc_linked_order', $linked );
	}

	/**
	 * show linked orders in the dash
	 * @return [type] [description]
	 */
	public function maybe_show_linked_orders() {
		$user_id = get_current_user_id();

		if ( ! $user_id ) {
			return;
		}

		// check if we've linked orders for this user at registration
		$linked = get_user_meta( $user_id, '_wc_linked_order', true );

		if( ! $linked || $linked == 0 )
			return;

		$user = get_user_by( 'id', $user_id );

		$message  = $user ? sprintf( esc_html__( 'Welcome, %s!', 'linkguest' ), $user->display_name ) : esc_html__( 'Welcome!', 'linkguest' );
		$message .= ' ' . sprintf( _n( 'Your previous order has been linked to this account.', 'Your previous %s orders have been linked to this account.', $linked, 'linkguest' ), $count );
		$message .= ' <a class="button" href="' . esc_url( wc_get_endpoint_url( 'orders' ) ) . '">' . esc_html__( 'View Orders', 'text' ) . '</a>';

		// add a notice with our message and delete our linked order flag
		wc_print_notice( $message, 'notice' );
		delete_user_meta( $user_id, '_wc_linked_order' );
	}


	/**
	 * Linked when there is a new order
	 * @param  INT $order_id
	 * @return mixed
	 */
	public function link_new_order( $order_id = 0 ) {

		if( empty( $order_id ) )
			return;

		$new_order = linkguest_settings( 'new_order', 'no', 'wc_linkguest' );

		if( $new_order == 'no' )
			return;

		$order = new WC_Order( $order_id );
		$user = $order->get_user();
		
		if( ! $user )
			linkgues_link_guest_orders($order);
	}
}