<?php

class LinkGuest_Ajax {
	
	public function __construct() {
		add_action( 'wp_ajax_linkguest_verify_license', [ $this, 'verify_license' ] );
		add_action( 'wp_ajax_linkguest_deactivate_license', [ $this, 'deactivate_license' ] );

		add_action( 'wp_ajax_linkguest_link_orders', [ $this, 'link_orders' ] );
		add_action( 'wp_ajax_nopriv_linkguest_link_orders', [ $this, 'link_orders' ] );
		
	}

	/**
	 * Activate license.
	 *
	 * @since 2.0.0
	 */
	public function verify_license() {
		// Run a security check.
		check_ajax_referer( 'linkguest-wpnonce', 'wpnonce' );

		// Check for license key.
		if ( empty( $_POST['license'] ) ) {
			wp_send_json_error( esc_html__( 'Please enter a license key.', 'linkguest' ) );
		}

		LinkGuest()->license->verify_key( $_POST['license'], true );
	}


	/**
	 * Deactivate license.
	 *
	 * @since 2.0.0
	 */
	public function deactivate_license() {

		// Run a security check.
		check_ajax_referer( 'linkguest-wpnonce', 'wpnonce' );

		LinkGuest()->license->deactivate_key( true );
	}

	/**
	 * Proccess link orders
	 * @return mixed
	 */
	public function link_orders() {
		// Run a security check.
		$result = check_ajax_referer( 'linkguest-wpnonce', 'wpnonce', false );

		if( $result === FALSE )
			wp_send_json_error( [ 'msg' => esc_html( 'Nonce invalid','linkguest' ) ] );

		$counter = 0;
		$guest_orders = linkguest_get_guest_orders();

		if( count($guest_orders) == 0 )
			wp_send_json_error( [ 'msg' => esc_html( 'There is not guest orders','linkguest' ) ] );

		foreach( $guest_orders as $order ) {
			$linked = linkgues_link_guest_orders($order);

			if( $linked )
				$counter++;
		}

		if( $counter == 0 )
			wp_send_json_error( [ 'msg' => esc_html( 'No order was linked','linkguest' ) ] );


		wp_send_json_success( [
			'counter'	=> $counter,
			'msg'		=> sprintf(
				esc_html__( 'Success: %d orders was linked', 'linkguest' ),
				$counter
			),
		] );
	}
}