<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class LinkGuest_Settings {
	public function __construct() {

		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );

		add_filter('woocommerce_get_settings_pages', [$this, 'add_class'], 10, 1);
		add_filter('woocommerce_get_sections_wc_timersys', [$this, 'add_sections'], 10, 1);
		add_filter('woocommerce_get_settings_wc_timersys', [$this, 'add_settings'], 10, 2);

		add_action('woocommerce_admin_field_linkguest_license', [$this, 'add_field']);
		add_action('woocommerce_admin_field_linkguest_to_link', [$this, 'add_button']);
	}

	
	/**
	 * Enqueue assets for the settings page.
	 *
	 * @since 2.0.0
	 */
	public function enqueue_scripts() {
		$screen = get_current_screen();

		if( !isset($screen->base) || strpos( $screen->base, 'page_wc-settings') === FALSE )
			return;

		if( !isset($_GET['tab']) || $_GET['tab'] != 'wc_timersys' )
			return;

		wp_enqueue_style( 'woocommerce_admin_styles' );

		if( !isset($_GET['section']) || $_GET['section'] != 'linkguest' )
			return;

		wp_enqueue_script( 'linkguest-settings-js', LINKGUEST_PLUGIN_URL . 'assets/js/linkguest_settings.js', [ 'jquery' ], false, true );

		wp_localize_script( 'linkguest-settings-js', 'linkguest_vars',
			[
				'url_ajax'			=> admin_url('admin-ajax.php'),
				'url_loading'		=> admin_url('images/spinner.gif'),
				'url_success'		=> admin_url('images/yes.png'),
				'url_failure'		=> admin_url('images/no.png'),
				'nonce'				=> wp_create_nonce( 'linkguest-wpnonce' ),
			]
		);
	}


	public function add_class($settings = []) {

		$include_tab = false;

		foreach( $settings as $obj ) {
			if( $obj instanceof WC_Settings_Timersys )
				$include_tab = true;
		}

		if( ! $include_tab )
			$settings[] = include LINKGUEST_PLUGIN_DIR . 'includes/admin/class-linkguest-wc-tabs.php';

		return $settings;
	}


	public function add_sections($sections = []) {

		if( count($sections) == 0 )
			$sections[''] = esc_html__( 'Welcome', 'linkguest' );

		$sections['linkguest'] = esc_html__( 'Link Guest Orders', 'linkguest' );

		return $sections;
	}

	public function add_settings($settings) {

		global $current_section;

		if( $current_section == 'linkguest' ) {

			$stats = linkgues_stats_guest_orders();

			$settings = [
				[
					'title' => esc_html__( 'Woocommerce Link Guest Orders', 'linkguest' ),
					'type'  => 'title',
					'id'    => 'wc_timersys_page_options',
				],[
					'type' => 'linkguest_license',
				],[
					'type' => 'sectionend',
					'id'   => 'wc_timersys_page_options',
				],
				[
					'title' => esc_html__( 'Advanced', 'linkguest' ),
					'type'  => 'title',
					'id'    => 'wc_timersys_page_advanced',
				],[
					'title'		=> esc_html__( 'Filter', 'linkguest' ),
					'type'		=> 'checkbox',
					'desc'		=> esc_html__( 'Filter by guest', 'linkguest' ),
					'desc_tip'	=> esc_html__( 'If checked, you will be able to type "guest" in the registered customer filter on the Woocommerce orders', 'linkguest' ),
					'id'		=> 'wc_linkguest[filter]',
					'default'	=> 'no',
				],[
					'title'		=> esc_html__( 'Events', 'linkguest' ),
					'type'		=> 'checkbox',
					'desc'		=> esc_html__( 'New customers', 'linkguest' ),
					'desc_tip'	=> esc_html__( 'When the customer registers, this option will search if it has some guest order and it will link.', 'linkguest' ),
					'id'		=> 'wc_linkguest[new_customer]',
					'checkboxgroup'	=> 'start',
					'default'	=> 'yes',
				],[
					'type'		=> 'checkbox',
					'desc'		=> esc_html__( 'New orders', 'linkguest' ),
					'desc_tip'		=> esc_html__( 'Sometimes the customer does orders without logged to the store. This option will link the order with the customer using the email.', 'linkguest' ),
					'id'		=> 'wc_linkguest[new_order]',
					'checkboxgroup' => 'end',
					'default'	=> 'yes',
				],[
					'type' 		=> 'linkguest_to_link',
				],[
					'type' => 'sectionend',
					'id'   => 'wc_timersys_page_advanced',
				],
			];
		}

		return $settings;
	}

	public function add_field() {

		$args_desc = [
			'type' => 'password',
			'desc_tip' => true,
			'desc' => esc_html__( 'Your license key provides access to updates and addons', 'linkguest' ),
		];

		$array_desc = WC_Admin_Settings::get_field_description($args_desc);

		$args_html = [
			'tooltip_html'	=> $array_desc['tooltip_html'],
			'license_key'	=> linkguest_settings( 'key', '', 'linkguest_license' ),
		];

		wc_get_template('layouts/settings_options.php', $args_html, false, LINKGUEST_PLUGIN_DIR );
	}


	public function add_button() {

		$stats = linkgues_stats_guest_orders();

		$disable_button = $stats['total_links'] == 0 ? 'disabled="disabled"' : '';

		$args = [
			'total_orders'		=> $stats['total_orders'],
			'total_links'		=> $stats['total_links'],
			'disable_button'	=> $disable_button,
		];

		wc_get_template('layouts/settings_button.php', $args, false, LINKGUEST_PLUGIN_DIR );
	}
}