<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Info Class
 * Information Link into plugin list
 */
class LinkGuest_Info {

	/**
	 * Constructor
	 */
	public function __construct() {
		add_filter( 'plugin_action_links_' . LINKGUEST_PLUGIN_BASE, [ $this, 'add_links' ] );
	}

	/**
	 * add links to the plugin list
	 * @param ARRAY $links
	 */
	public function add_links( $links ) {
		$settings = [
			'settings' => sprintf(
				'<a href="%s">%s</a>',
				admin_url( 'admin.php?page=wc-settings&tab=wc_timersys&section=linkguest' ),
				esc_html__( 'Settings', 'linkguest' )
			),
		];

		return array_merge( $settings, $links );
	}
}

new LinkGuest_Info();