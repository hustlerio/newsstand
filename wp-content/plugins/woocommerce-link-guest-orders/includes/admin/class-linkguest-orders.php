<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Orders Class
 */
class LinkGuest_Orders {
	public function __construct() {
		add_filter( 'woocommerce_json_search_found_customers', [ $this, 'guest_customer' ]);
		add_filter( 'request', [ $this, 'request_query' ], 15 );
	}

	/**
	 * Answer from JSON Ajax Request
	 * @param  ARRAY  $customers
	 * @return ARRAY  
	 */
	public function guest_customer( $customers = [] ) {

		$filter_option = linkguest_settings('filter', 'no', 'wc_linkguest');

		if( $filter_option == 'no' )
			return $customers;

		$term  = isset( $_GET['term'] ) ? (string) wc_clean( wp_unslash( $_GET['term'] ) ) : '';

		if( empty( $term ) || $term != 'guest' )
			return $customers;
			
		$customers[0] = esc_html__( 'Guest Customers', 'linkguest' );

		return $customers;
	}

	/**
	 * Requesto FROM GET Param
	 * @param  ARRAY $query_vars
	 * @return ARRAY
	 */
	public function request_query($query_vars) {
		global $typenow;

		if ( in_array( $typenow, wc_get_order_types( 'order-meta-boxes' ), true ) &&
			isset( $_GET['_customer_user'] ) && $_GET['_customer_user'] != '' && $_GET['_customer_user'] == 0 ) {

			$meta_query = [
				'key'     => '_customer_user',
				'value'   => 0,
				'compare' => '=',
			];

			if( ! isset( $query_vars['meta_query'] ) || ! is_array( $query_vars['meta_query'] ) )
				$query_vars['meta_query'] = [];

			$query_vars['meta_query'][] = $meta_query;
		}

		return $query_vars;
	}
}