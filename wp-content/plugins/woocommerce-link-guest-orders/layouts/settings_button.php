<tr valign="top">
	<th scope="row" class="titledesc">
		<label for="linkguest_to_link"><?php esc_html_e( 'To Link', 'linkguest' ); ?></label>
	</th>
	<td class="forminp forminp-to_link">
		<table class="wp-list-table widefat linkguest_stats" style="padding: 0px 20px;width: auto;">
			<tr>
				<th><?php esc_html_e('Total guest orders','linkguest'); ?></th>
				<td><span id="total_orders"><?php echo $total_orders; ?></span></td>
				<td>
					<a class="button button-secondary" href="<?php echo admin_url('edit.php?post_type=shop_order&_customer_user=0') ?>">
						<?php esc_html_e('See all the Guest Orders','linkguest'); ?>
					</a>
				</td>
			</tr>
			<tr>
				<th><?php esc_html_e('Orders pending to link','linkguest'); ?></th>
				<td><span id="total_links"><?php echo $total_links; ?></span></td>
				<td>
					<button class="button button-secondary" id="linkguest_button_link_orders" data-total-orders="<?php echo $total_orders; ?>" data-total-links="<?php echo $total_links; ?>" <?php echo $disable_button; ?>>
						<?php esc_html_e('Link Guest Orders','linkguest'); ?>
					</button>
					<span id="linkguest_loading_pending"></span>
				</td>
			</tr>
		</table>
	</td>
</tr>