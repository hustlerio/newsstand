<tr valign="top">
	<th scope="row" class="titledesc">
		<label for="linkguest_license"><?php esc_html_e( 'License Key', 'linkguest' ); ?> <?php echo $tooltip_html; // WPCS: XSS ok. ?></label>
	</th>
	<td class="forminp forminp-license_key">
		<input
			name="linkguest_options[license_key]"
			id="linkguest_key_license"
			type="password"
			value="<?php echo esc_attr( $license_key ); ?>"
			class="regular-text"
			/>
			<button class="button button-primary" id="linkguest_button_verify_license">
				<?php esc_html_e('Verify Key','linkguest'); ?>
			</button>
			
			<?php if(  isset($license_key) && !empty($license_key) ) : ?>
				<button class="button button-secondary" id="linkguest_button_deactivate_license"><?php esc_html_e('Deactivate Key','shiprice'); ?></button>
			<?php endif; ?>

			<span id="linkguest_loading_license"></span>
			<p id="linkguest_message_license"></p>
	</td>
</tr>