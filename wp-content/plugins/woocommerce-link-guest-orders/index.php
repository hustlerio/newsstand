<?php
/*
Plugin Name: Woocommerce Link Guest Orders
Plugin URI: https://timersys.com/
Description: Woocommerce doesn't assign the old orders to the new user registered  (when he bought as a guest). This plugin finds those guest orders and assigns them to their respective users.
Version: 1.0.1
Author: timersys
Author URI: https://timersys.com/
Developer: Damian Logghe
Developer URI: https://timersys.com/
Text Domain: woocommerce-extension
Requires at least: 5.1
Tested up to: 5.4
Stable tag: 5.3
WC requires at least: 3.1.0
WC tested up to: 3.9.3
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'LINKGUEST_EDD_ID', '59722' );
define( 'LINKGUEST_VERSION', '1.0.1' );
define( 'LINKGUEST_UPDATER_API', 'https://timersys.com/' );

define( 'LINKGUEST_PLUGIN_DIR' , plugin_dir_path( __FILE__ ) );
define( 'LINKGUEST_PLUGIN_URL' , plugin_dir_url( __FILE__ ) );
define( 'LINKGUEST_PLUGIN_BASE' , plugin_basename( __FILE__ ) );


/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require_once LINKGUEST_PLUGIN_DIR . 'includes/class-linkguest.php';

/**
 * Store the plugin global
 */
global $linkguest;

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function LinkGuest() {
	return LinkGuest::instance();
}

$GLOBALS['linkguest'] = LinkGuest();
