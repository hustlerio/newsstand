��    1      �  C   ,      8     9     V     ^     f  9   m  1   �     �     �               !     5     C     I     X     d     x     �     �  !   �     �     �     �     �  &   	     0     6  
   U     `     n  +   }     �     �     �     �     �  -   �  *   !  (   L  ?   u     �  	   �     �      �  j   �     Z  Q   z  3   �  �   	  "   �
               $  E   0  9   v  	   �  %   �     �     �               '     -     A     M     c  
   x     �  !   �     �     �     �     
  %        E  (   L  
   u     �     �  :   �  &   �                      *   3  &   ^  &   �  :   �     �     �     �  .     t   6     �  ^   �  9   *        (   &   /                           1   )         +            0   '       $   !   
   #          "                       ,   *      	              .                                                    -         %                      %s orders assigned correctly &laquo; &raquo; 1 Item An Unexpected HTTP Error occurred during the API request. An error occour when try to identify the pluguin. Busted! Click me to assign these orders Country Customer Currency Order Discard this notice Documentation Email Email Customer Error Fatal First Name Customer Guest Orders Items Last Name Customer Letsgodev is down right (from %s) License Code List Guest Orders Method Payment Order More Premiun Plugins Nothing was found with the phrase "%s" Order Please enter your license code Registered Search Orders Search Orders: Server is down right. An email was sent to  Settings Guest Orders Status Submit Support There is not orders to assing There is not registered users with this email There was a problem activating the license There was a problem checking the license There was a problem establishing a connection to the API server Total Try again Username Where do I find my License Code? While the plugin is activated, each time a user registers, it will be assigned to his order if he has one. Woocommerce Assign Guest Orders Woocommerce Assing Guest Orders plugin depends on the last version of %s to work! You do not have pending assignments of guest orders Project-Id-Version: Woocommerce Assign Guest Orders
POT-Creation-Date: 2018-03-07 17:13-0300
PO-Revision-Date: 2018-03-07 17:13-0300
Last-Translator: 
Language-Team: Gopymes SAC <gonzalesc@gopymes.pe>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_nx;_x;_ex;_nx
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 %s pedidos asignados correctamente &laquo; &laquo; 1 artículo Un inesperado error HTTP. Error ocurrido durante la respuesta del API Un error ocurrido cuando se intenta identificar el plugin Atrapado! Clickéame para asignar estos pedidos País del comprador Moneda del pedido Descartar noticia Documentación Email Email del comprador Error Fatal Nombres del comprador Pedidos de invitados Artículos Apellidos del comprador Letsgodev está caído (desde %s) Código de licencia Lista de pedidos de invitados Método de pago del pedido Más Plugins Premiun Nada fue encontrado con la frase "%s" Pedido Por favor ingresa tu código de licencia Registrado Buscar pedidos Buscar Pedidos: Servidor está caído. Un email fue enviado reportándolo. Configuración de pedidos de invitados Estado Enviar Soporte No hay pedidos para asignar No hay usuarios registrados con este email Hubo un problema activando la licencia Hubo un problema validando la licencia Hubo un problema estableciendo una conexión al API server Total Intente otra vez Username Dónde puedo encontrar mi código de licencia? Mientras el plugin está activado, cada vez que un usuario se registre, se asignará a su pedido si es que lo tiene. Woocoomerce Assign Guest Orders El plugin Woocommerce Assign Guest Orders depende de la última versión de %s para funcionar! No tienes asignaciones pendientes de pedidos de invitados 